import { Component } from '@angular/core';
import {
  AuthService,
  Logo,
  NestedNavigationRoute,
  SimpleNavigationRoute,
} from 'shared-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  version = '1.0.0';
  copyRight = '2025 MdOA group';
  title = 'kg-browser';
  homelink: SimpleNavigationRoute = {
    route: '/home',
    label: 'Home',
  };

  routes: NestedNavigationRoute[] = [
    {
      label: 'Tools',
      requireAuth: false,
      children: [{ route: '/template', label: 'Template 1' }],
    },
    {
      label: 'About',
      requireAuth: false,
      children: [
        { route: '/mdoa-team', label: 'MdOA Group' },
        // { route: '/other', label: 'Other Team' },
      ],
    },
  ];

  footerContent: NestedNavigationRoute[] = [
    {
      label: 'Contact Us',
      children: [
        {
          label: 'support@mdoa-tools.bi.denbi.de',
          href: 'mailto:support@mdoa-tools.bi.denbi.de',
        },
        { label: 'About MdOA Toolbox', route: '/about' },
        { label: 'Terms of Service', route: '/termsofservice' },
        { label: 'Privacy Policy', route: '/privacypolicy' },
        { label: 'Impressum', route: '/impressum' },
      ],
    },
    {
      label: 'Funding & Support',
      children: [
        {
          label: 'Universität Bielefeld',
          href: 'https://www.uni-bielefeld.de',
        },
        { label: 'ISAS', href: 'https://www.isas.de' },
        { label: 'DFG', href: 'http://www.dfg.de' },
        { label: 'de.NBI', href: 'http://www.denbi.de' },
        { label: 'de.NBI Cloud', href: 'https://www.denbi.de/cloud' },
        { label: 'BMBF', href: 'https://www.bmbf.de' },
        { label: 'NRW', href: 'https://www.land.nrw' },
        // { label: 'NFDI4microbiota', href: 'https://nfdi4microbiota.de' },
      ],
    },
  ];

  footerLogos: Logo[] = [
    {
      label: 'uni-bielefeld',
      assetPath: 'assets/standard-logos/uni-bielefeld-logo.jpg',
      href: 'https://www.uni-bielefeld.de',
      backgroundColor: 'white',
    },
    {
      label: 'leibniz',
      assetPath: 'assets/standard-logos/leibniz_white.png',
      href: 'https://www.leibniz-gemeinschaft.de',
    },
    {
      label: 'ISAS',
      assetPath: 'assets/standard-logos/isaslogooffizielleformrgbweiss.png',
      href: 'https://www.isas.de',
    },
    {
      label: 'de.NBI',
      assetPath: 'assets/standard-logos/Denbi.svg',
      href: 'http://www.denbi.de',
      backgroundColor: 'white',
    },
    {
      label: 'de.NBI Cloud',
      assetPath: 'assets/standard-logos/denbi-cloud.png',
      href: 'https://www.denbi.de/cloud',
      backgroundColor: 'white',
    },
    {
      label: 'bmbf',
      assetPath: 'assets/standard-logos/bmbf.svg',
      href: 'https://www.bmbf.de',
      backgroundColor: 'white',
    },
    {
      label: 'nrw',
      assetPath: 'assets/standard-logos/nrw_black.svg',
      href: 'https://www.land.nrw',
      backgroundColor: 'white',
    },
    {
      label: 'NFDI4microbiota',
      assetPath: 'assets/standard-logos/Nfdi4microbiota.png',
      href: 'https://nfdi4microbiota.de',
      backgroundColor: 'white',
    },


  ];

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.initializeOAuth();
  }
}
