import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './modules/landing-page-module/landing-page.component';
import { BetterLoginPageComponent } from 'shared-lib';

// TODO: route guard? (the route guard for workflow routes still exists)

const routes: Routes = [
  { path: 'home', component: LandingPageComponent },
  {
    path: 'template',
    loadChildren: () =>
      import(
        './modules/page-modules/tool-modules/template-page-module/template.module'
      ).then((m) => m.TemplateModule),
  },
  {
    path: 'mpa-page',
    loadChildren: () =>
      import(
        './modules/page-modules/tool-modules/mpa-page-module/mpa.module'
      ).then((m) => m.MpaModule),
  },
  {
    path: 'prophane-page',
    loadChildren: () =>
      import(
        './modules/page-modules/tool-modules/prophane-page-module/prophane.module'
      ).then((m) => m.ProphaneModule),
  },
  {
    path: 'metaforge-page',
    loadChildren: () =>
      import(
        './modules/page-modules/tool-modules/metaforge-page-module/metaforge.module'
      ).then((m) => m.MetaForgeModule),
  },
  {
    path: 'multiomics-visualizer-page',
    loadChildren: () =>
      import(
        './modules/page-modules/tool-modules/multiomics-visualizer-module/multiomics-visualizer.module'
      ).then((m) => m.MultiomicsVisualizerModule),
  },
  {
    path: 'page-sbc-shap',
    loadChildren: () =>
      import('./modules/page-modules/tool-modules/sbc/sbc.module').then(
        (m) => m.SbcModule,
      ),
  },
  {
    path: 'omex-page',
    loadChildren: () =>
      import(
        './modules/page-modules/tool-modules/omex-module/omex.module'
      ).then((m) => m.OmexModule),
  },
  {
    path: 'mdoa-team',
    loadChildren: () =>
      import(
        './modules/page-modules/about-modules/mdoa-team-module/mdoa-team.module'
      ).then((m) => m.MdoaTeamModule),
  },
  {
    path: 'template-script-page',
    loadChildren: () =>
      import(
        './modules/page-modules/script-modules/template-script-page.module'
      ).then((m) => m.TemplateScriptModule),
  },
  { path: 'login', component: BetterLoginPageComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
