import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {
  HttpClientModule,
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';

import { StandardPageLayoutModule } from 'shared-lib';
import { MatCardModule } from '@angular/material/card';
import { NgParticlesModule } from 'ng-particles';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { LandingPageModule } from './modules/landing-page-module/landing-page.module';
import { BackgroundModule } from './modules/landing-page-module/submodules/background-module/background.module';
// import { NgxMatomoTrackerModule } from '@ngx-matomo/tracker';
// import { NgxMatomoRouterModule } from '@ngx-matomo/router';
import { provideMatomo, withRouter } from 'ngx-matomo-client';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    StandardPageLayoutModule,
    NgParticlesModule,
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    LandingPageModule,
    BackgroundModule,
    // NgxMatomoTrackerModule.forRoot({ trackerUrl: 'https://piwik.cebitec.uni-bielefeld.de/', siteId: '29' }),
    // NgxMatomoRouterModule
  ],
  providers: [
    provideMatomo(
      {
        trackerUrl: 'https://piwik.cebitec.uni-bielefeld.de/',
        siteId: '29',
      },
      withRouter(), // Enables route tracking
    ),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
