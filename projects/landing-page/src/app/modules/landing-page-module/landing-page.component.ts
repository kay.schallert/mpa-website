import {
  Component,
  ElementRef,
  HostListener,
  OnInit,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  @ViewChild('section1') section1: ElementRef;
  @ViewChild('section2') section2: ElementRef;
  @ViewChild('section3') section3: ElementRef;
  @ViewChild('newsfeed') newsfeed: ElementRef;
  @ViewChild('vision') vision: ElementRef;

  scrollToSection(target: ElementRef) {
    if (target && target.nativeElement) {
      target.nativeElement.scrollIntoView({ behavior: 'smooth' });
    }
  }

  scrollToSection1() {
    this.scrollToSection(this.section1);
  }

  scrollToSection2() {
    this.scrollToSection(this.section2);
  }

  scrollToSection3() {
    this.scrollToSection(this.section3);
  }

  scrollToNewsfeed() {
    this.scrollToSection(this.newsfeed);
  }

  scrollToVision() {
    this.scrollToSection(this.vision);
  }

  tools = [
    {
      title: 'MPA Cloud',
      description:
        'Metaproteomics analysis with heavy focus on latest algorithms and scalability through cloud computing.',
      imageSrc: './assets/img/logos/MPA.webp',
      link: 'https://mdoa-tools.bi.denbi.de/mpacloud/home',
      useToolLink: 'https://mdoa-tools.bi.denbi.de/mpacloud/home',
      learnMoreLink: '/mpa-page',
    },
    {
      title: 'Prophane',
      description:
        'Prophane provides a tailored and fully automated workflow' +
        ' for metaproteomics analysis with special focus on' +
        ' metaprotein taxonomic and functional annotation.',
      imageSrc: './assets/img/logos/P.jpg',
      link: 'https://prophane.de/login',
      useToolLink: 'https://prophane.de/login',
      learnMoreLink: '/prophane-page',
    },
    {
      title: 'Multiomics Visualizer',
      description:
        'Visualization of dimension- ality-reduced clustered multi-omics data in 2D and 3D.',
      imageSrc: './assets/img/logos/Multiomics Visualizer.webp',
      link: 'https://mdoa-tools.bi.denbi.de/multiomics/',
      useToolLink: 'https://mdoa-tools.bi.denbi.de/multiomics/',
      learnMoreLink: '/multiomics-visualizer-page',
    },
    {
      title: 'SBC-SHAP',
      description:
        'Sepsis classification based on age, sex and complete blood count data ' +
        '(red blood cells, white blood cells, mean corpuscular volume, hemo- globin, platelets)',
      imageSrc: './assets/img/logos/sbclogo.jpeg',
      link: 'https://mdoa-tools.bi.denbi.de/sbc-shap',
      useToolLink: 'https://mdoa-tools.bi.denbi.de/sbc-shap',
      learnMoreLink: '/page-sbc-shap',
    },
    {
      title: 'MetaForge Demo',
      description:
        'A streamlined tool for crea- ting and annotating metadata in the field of proteomics and metaproteomics' +
        ', producing the standardized file formats mzID, mzML, and SDRF.',
      imageSrc: './assets/img/demo-logo/metaforge_demo.png',
      link: 'https://mdoa-tools.bi.denbi.de/metaforge_test/home',
      useToolLink: 'https://mdoa-tools.bi.denbi.de/metaforge_test/home',
      learnMoreLink: '/metaforge-page',
    },
    {
      title: 'Omics Molecule Extractor',
      description:
        'Search a list of features from omics experiments (i.e. genes, proteins) to find those features' +
        ' that best explain the difference between groups (i.e. patient and control).',
      imageSrc: './assets/img/demo-logo/off_demo.png',
      link: 'https://mdoa-tools.bi.denbi.de/omex/home',
      useToolLink: 'https://mdoa-tools.bi.denbi.de/omex/home',
      learnMoreLink: '/omex-page',
    },
  ];

  scripts = [
    {
      title: 'Automated Literature Search',
      description:
        'Use this script to obtain an over- view of a research domain ' +
        'and find the most relevant articles.',
      imageSrc: './assets/img/logos/literature-script.JPG',
      link: 'https://github.com/voidsailor/automated_literature_search',
      useToolLink: 'https://github.com/voidsailor/automated_literature_search',
    },
    {
      title: 'Protein Abundance Visualization',
      description:
        'Use this script to create an inter- active plot and data table of your protein abundances.',
      imageSrc: './assets/img/logos/protein_abundance.jpg',
      link: 'https://github.com/voidsailor/protein_abundance_visualization',
      useToolLink:
        'https://github.com/voidsailor/protein_abundance_visualization',
    },
  ];

  comingSoons = [
    // {
    //   title: 'MPA Cloud',
    //   description:
    //     'Meta Proteome Analyzer is a powerful tool for analyzing (meta)- proteomic data.',
    //   imageSrc: './assets/img/logos/MPA.webp',
    // },
    {
      title: 'MPA Pathway Tool',
      description: '',
      imageSrc: './assets/img/logos/default logo.JPG',
    },
    // {
    //   title: 'MetaForge',
    //   showButton: true,
    //   description:
    //     'An advanced tool designed for streamlined creation and annotation of metadata in the field of (meta)proteomics.' +
    //     ' Thanks to its user-friendly interface, the process of metadata creation is accomplished with remarkable efficiency.',
    //   imageSrc: './assets/img/logos/Meta.png',
    //   learnMoreLink: '/metaforge-page',
    // },
    // {
    //   title: 'Omics Feature Finder',
    //   description: '',
    //   imageSrc: './assets/img/logos/off_logo.svg',
    // },
    {
      title: 'Library for Statistical Analysis Script',
      description: '',
      imageSrc: './assets/img/logos/default logo.JPG',
    },
    {
      title: 'Knowledge Graphs for Omics Analysis',
      description: '',
      imageSrc: './assets/img/logos/default logo.JPG',
    },
  ];

  updateList = [
    {
      title: 'Multiomics Visualizer Officially Launched!',
      type: 'Announcement',
      author: 'Daniel Walke',
      link: 'https://mdoa-tools.bi.denbi.de/multiomics/',
    },
    {
      title: "Prophane's Website Receives an Exciting Upgrade!",
      type: 'Update',
      author: 'Kay Schallert',
      link: 'https://prophane.de/login',
    },
    {
      title: 'The Mdoa-Tools Website Officially Launched!',
      type: 'Announcement',
      author: 'Daniel Kautzner',
      link: '#',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
