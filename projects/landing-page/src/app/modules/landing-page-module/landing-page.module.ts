import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from "@angular/material/card";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatIconModule } from "@angular/material/icon";
import { LandingPageComponent } from './landing-page.component';
import { ToolCardModule } from './submodules/tool-card-module/tool-card.module';
import { ScriptCardModule } from './submodules/script-card-module/script-card.module';
import { UpdateListModule } from './submodules/update-list-module/update-list.module';
import { CommonModule } from '@angular/common';
import { BackgroundModule } from './submodules/background-module/background.module';
import { MatButtonModule } from '@angular/material/button';
import { ComingSoonModule } from './submodules/coming-soon-module/coming-soon.module';

@NgModule({
  declarations: [LandingPageComponent],
  imports: [
    BrowserModule,
    CommonModule,
    BackgroundModule,
    ToolCardModule,
    ScriptCardModule,
    ComingSoonModule,
    UpdateListModule,

    MatCardModule,
    MatGridListModule,
    MatIconModule,
    MatButtonModule

  ],

  exports: [LandingPageComponent],
})
export class LandingPageModule {}
