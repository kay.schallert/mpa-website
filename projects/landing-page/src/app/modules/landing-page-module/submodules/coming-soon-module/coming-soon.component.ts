import { Component, Input } from '@angular/core';

@Component({
  selector: 'coming-soon',
  templateUrl: './coming-soon.component.html',
  styleUrls: ['./coming-soon.component.scss'],
})
export class ComingSoonComponent {
  @Input() toolData: any;
  @Input() showButton: boolean = true;

  goToLink(url: string): void {
    window.open(url, "_blank");
  }
}
