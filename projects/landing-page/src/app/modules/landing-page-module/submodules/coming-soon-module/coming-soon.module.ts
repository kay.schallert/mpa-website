import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { ComingSoonComponent } from './coming-soon.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ComingSoonComponent],
  imports: [

    MatButtonModule,
    CommonModule,

  ],
  exports: [ComingSoonComponent],
})
export class ComingSoonModule { }