import { Component, Input } from '@angular/core';

@Component({
  selector: 'script-card',
  templateUrl: './script-card.component.html',
  styleUrls: ['./script-card.component.scss'],
})
export class ScriptCardComponent {
  @Input() scriptData: any;

  goToLink(url: string): void {
    window.open(url, "_blank");
  }
}
