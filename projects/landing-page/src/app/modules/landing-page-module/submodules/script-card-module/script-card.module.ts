import { NgModule } from '@angular/core';
import { ScriptCardComponent } from './script-card.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [ScriptCardComponent],
  imports: [

    MatButtonModule

  ],
  exports: [ScriptCardComponent],
})
export class ScriptCardModule { }