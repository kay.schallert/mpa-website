import { NgModule } from '@angular/core';
import { UpdateListComponent } from './update-list.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [UpdateListComponent],
  imports: [CommonModule],
  exports: [UpdateListComponent], 
})
export class UpdateListModule { }