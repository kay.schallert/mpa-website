import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-expertise-block',
  templateUrl: './expertise-block.component.html',
  styleUrls: ['./expertise-block.component.scss'],
})
export class ExpertiseBlockComponent {
    @Input() title: string;
    isExpanded: boolean = false;
  
    toggleExpansion() {
      this.isExpanded = !this.isExpanded;
    }
  }