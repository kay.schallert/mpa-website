import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MdoaTeamComponent } from './mdoa-team.component';

describe('MdoaTeamComponent', () => {
  let component: MdoaTeamComponent;
  let fixture: ComponentFixture<MdoaTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MdoaTeamComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MdoaTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
