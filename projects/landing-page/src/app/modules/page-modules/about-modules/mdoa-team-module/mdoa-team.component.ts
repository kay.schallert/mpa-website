import { Component, Input } from '@angular/core';

@Component({
  selector: 'mdoa-team-page',
  templateUrl: './mdoa-team.component.html',
  styleUrls: ['./mdoa-team.component.scss'],
})
export class MdoaTeamComponent {
  private isScrolling: boolean = false;

  
  
  members = [
    {
      name: 'Prof. Dr.-Ing. Robert Heyer',
      role: 'Team Leader',
      email: 'robert.heyer@uni-​bielefeld.de',
      email2: 'robert.heyer@isas.de',
      picture: './assets/img/member-images/robert_h.jpeg',
    },
    {
      name: 'Kay Schallert',
      role: 'Assistant Team Leader',
      email: 'kay.schallert@isas.de',
      picture: './assets/img/member-images/Kay_S.jpeg',
    },
    {
      name: 'Emanuel Lange',
      role: 'Scientific Assistant',
      email: 'emanuel.lange@isas.de',
      expertise: 'Software Developer, Modeler',
      picture: './assets/img/member-images/Manny_L.JPG',
    },
    {
      name: 'Daniel Walke',
      role: 'Scientific Assistant',
      email: 'daniel.walke@ovgu.de',
      expertise: 'Machine Learning Engineer',
      picture: './assets/img/member-images/Walke_D.jpeg',
    },
    {
      name: 'Maximilian Wolf',
      role: 'Scientific Assistant',
      email: 'maximilian.wolf@uni-bielefeld.de',
      expertise: 'Data Scientist',
      picture: './assets/img/member-images/Max_W.jpg',
    },
    {
      name: 'Daniel Kautzner',
      role: 'Scientific Assistant',
      email: 'daniel.kautzner@uni-bielefeld.de',
      expertise: 'Software Developer, Data Scientist',
      picture: './assets/img/member-images/Daniel_K.jpg',
    },
    {
      name: 'Luca M. Knipper',
      role: 'Scientific Assistant',
      email: 'luca.knipper@uni-bielefeld.de',
      expertise: 'Data Scientist',
      picture: './assets/img/member-images/Luca_K.jpg',
    },
    {
      name: 'Benjamin J. Saalfeld',
      role: 'Scientific Assistant',
      email: 'benjamin.saalfeld@uni-bielefeld.de',
      expertise: 'Data Scientist',
      picture: './assets/img/member-images/Benni_S.jpeg',
    },
  ];

  scrollPosition: number = 0;
  cardWidth: number = 660; 
  scrollPercentage: number = 0;

  ngOnInit() {
    // Initialize members array with your data
    // e.g., this.members = yourMemberData;
  }

  leftScrollCounter = 0;
  rightScrollCounter = 0;

  scrollLeft() {
    this.isScrolling = true;
    const container = document.getElementById('member-container');
    if (container) {
      const maxScroll = container.scrollWidth - container.clientWidth;
      this.scrollPosition -= this.cardWidth;

      if (this.leftScrollCounter === 1) {
        this.scrollPosition =
          this.scrollPosition < 0 ? maxScroll : this.scrollPosition;
        this.leftScrollCounter = 0;
        container.scrollTo({ left: this.scrollPosition, behavior: 'smooth' });
        this.uploadScrollProgress(); // Call the new function
        this.checkScrollBounds();
      } else if (this.scrollPosition < 0) {
        this.scrollPosition = 0;
        this.leftScrollCounter = 1;
      }
      this.rightScrollCounter = 0;
      container.scrollTo({ left: this.scrollPosition, behavior: 'smooth' });
      this.uploadScrollProgress(); // Call the new function
      this.checkScrollBounds();
    }
  }

  scrollRight(): void {
    this.isScrolling = true;
    const container = document.getElementById('member-container');
    if (container) {
      const maxScroll = container.scrollWidth - container.clientWidth;
      this.scrollPosition += this.cardWidth;

      if (this.rightScrollCounter === 1) {
        this.scrollPosition =
          this.scrollPosition >= maxScroll ? 0 : this.scrollPosition;
        this.rightScrollCounter = 0;
        container.scrollTo({ left: this.scrollPosition, behavior: 'smooth' });
        this.uploadScrollProgress();
        this.checkScrollBounds();
      } else if (this.scrollPosition >= maxScroll) {
        this.scrollPosition = maxScroll;
        this.rightScrollCounter = 1;
      }
      this.leftScrollCounter = 0;
      container.scrollTo({ left: this.scrollPosition, behavior: 'smooth' });
      this.uploadScrollProgress(); // Call the new function
      this.checkScrollBounds();
    }
  }

  uploadScrollProgress() {
    const container = document.getElementById('member-container');
    if (container) {
      const maxScroll = container.scrollWidth - container.clientWidth;
      this.scrollPercentage = (this.scrollPosition / maxScroll) * 100;
      const progressWidth = `${this.scrollPercentage}%`;
      document.documentElement.style.setProperty(
        '--scroll-progress-width',
        progressWidth
      );
    }
  }

  private checkScrollBounds() {
    const container = document.getElementById('member-container');
    if (container) {
      const maxScroll = container.scrollWidth - container.clientWidth;

      if (this.scrollPosition < 0) {
        this.scrollPosition = maxScroll; // Reset to the end when scrolling left from the start
      } else if (this.scrollPosition > maxScroll) {
        this.scrollPosition = 0; // Reset to start when reaching the end
        this.scrollPercentage = 0;
      }

      container.scrollTo({ left: this.scrollPosition, behavior: 'smooth' });
      document.documentElement.style.setProperty(
        '--scroll-progress-width',
        this.scrollPercentage + '%'
      );
    }
  }


}
