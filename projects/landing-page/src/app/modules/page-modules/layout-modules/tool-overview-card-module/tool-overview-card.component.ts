import { Component, Input } from '@angular/core';

@Component({
  selector: 'tool-overview-card',
  templateUrl: './tool-overview-card.component.html',
  styleUrls: ['./tool-overview-card.component.scss'],
})
export class ToolOverviewCardComponent {
  @Input() toolName: string = '';
  @Input() toolLogo: string = '';
  @Input() toolDescription: string = '';
  @Input() Link: string = '#'; 
  @Input() LinkText: string = ''
  @Input() usage: any[] = [];
  @Input() updates: any[];
  @Input() founding: any [];
  @Input() used: any [];
  @Input() colab: any [];

// Booleans for Section show 
  @Input() showFoundingSection: boolean = false;
  @Input() showUsedSection: boolean = false;
  @Input() showColabSection: boolean = false;

  goToLink(url: string): void {
    window.open(url, "_blank");
  }
}
