import { Component } from '@angular/core';

@Component({
  selector: 'metaforge-page',
  templateUrl: './metaforge.component.html',
  styleUrls: ['./metaforge.component.scss'],
})

export class MetaForgeComponent {
  showFoundingSection: boolean = true;
  usage = [
    { rowHeader: 'User Interface', data1: 'Webapplication' },
    { rowHeader: 'Local Installation', data1: 'No'},
    { rowHeader: 'Input', data1: 'Coming Soon' },
    { rowHeader: 'Output', data1: 'SDRF, mzML, mzID'},
    { rowHeader: 'Repository', data1: 'Coming Soon'},

  ];

  // founding = [
  //   { rowHeader: 'de.NBI', text: 'Link', link: 'https://www.denbi.de'},
  //   { rowHeader: 'NFDI', text: 'Link', link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads'}

  // ];

  // used = [
  //   { rowHeader: 'Publication 1', text: 'Pubclication name/Author name', link: 'https://www.denbi.de'},
  //   { rowHeader: 'Publication 2', text: 'Pubclication name/Author name', link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads'}

  // ];

  // colab = [
  //   { rowHeader: 'Partner 1', nameText:'Name', nameLink:'https://www.denbi.de', institutionText:'institution', institutionLink:'https://www.denbi.de'},
  //   { rowHeader: 'Partner 2', name: 'Name', institution: 'institution'}

  // ];

updateList = [
  {
    title: 'In Development',
    type: 'Tool',
    author: 'Daniel Kautzner',
    link: '',
  },

];

  goToLink(url: string): void {
    window.open(url, "_blank");
  }
  constructor() {}

  ngOnInit(): void {}
}
