import { Component } from '@angular/core';

@Component({
  selector: 'mpa-page',
  templateUrl: './mpa.component.html',
  styleUrls: ['./mpa.component.scss'],
})
export class MpaComponent {
  showFoundingSection: boolean = true;
  usage = [
    { rowHeader: 'User Interface', data1: 'Webapplication' },
    // { rowHeader: 'Local Installation', data1: 'Docker, Conda'},
    { rowHeader: 'Input', data1: 'Use standard MS/MS input data (mzml, mgf) or search results from '
      + 'a variety of search engines (Mascot, Proteome Discoverer, MaxQuant).' },
    { rowHeader: 'Output', data1: 'MPA can perform protein grouping, functional and taxonomic annotation, cross-database referencing, X!Tandem search, '
      + 'MS2Rescore validation, and various data filtering methods (i.e. by taxonomy) '
      + 'and multi-sample data integration. Results at various levels can be exported as csv or mzID.'},
    { rowHeader: 'Repository',data1: 'https://gitlab.com/kay.schallert/mpa-cloud-server, https://gitlab.com/kay.schallert/mpa-website and https://hub.docker.com/repositories/mpacloud'}

  ];

  founding = [
    { rowHeader: 'de.NBI', text: 'Link', link: 'https://www.denbi.de'},
    { rowHeader: 'NFDI', text: 'Link', link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads'}

  ];

  used = [
    { rowHeader: 'Coming soon', text: 'Pubclication name/Author name', link: 'https://www.denbi.de'},
    //{ rowHeader: 'Publication 2', text: 'Pubclication name/Author name', link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads'}

  ];

  colab = [
    { rowHeader: 'Partner 1', nameText:'Name', nameLink:'https://www.denbi.de', institutionText:'institution', institutionLink:'https://www.denbi.de'},
    //{ rowHeader: 'Partner 2', name: 'Name', institution: 'institution'}

  ];

updateList = [
  {
    title: 'New Logo',
    type: 'Styling Update',
    author: 'Daniel Kautzner',
    link: '#',
  },
  {
    title: 'Launch of Version 2.0 in 2024',
    type: 'New Version',
    author: 'Daniel Kautzner',
    link: '#',
  },
];

  goToLink(url: string): void {
    window.open(url, "_blank");
  }
  constructor() {}

  ngOnInit(): void {}
}
