import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MpaRoutingModule } from './mpa-routing.component';
import { MpaComponent } from './mpa.component';
import { ToolOverviewCardModule } from '../../layout-modules/tool-overview-card-module/tool-overview-card.module';

@NgModule({
    declarations: [MpaComponent],
    exports: [MpaComponent],
    imports: [
        CommonModule,
        MpaRoutingModule,
        MatButtonModule,
        MatIconModule,
        ToolOverviewCardModule,
    ]
})
export class MpaModule { }
