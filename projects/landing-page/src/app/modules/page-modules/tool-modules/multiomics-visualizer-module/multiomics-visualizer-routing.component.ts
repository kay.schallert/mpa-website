import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MultiomicsVisualizerComponent } from './multiomics-visualizer.component';


const routes: Routes = [
  {path: '', component: MultiomicsVisualizerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MultiomicsVisualizerRoutingModule { }
