import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateComponent } from './multiomics-visualizer.component';

describe('MultiomicsVisualizerComponent', () => {
  let component: MultiomicsVisualizerComponent;
  let fixture: ComponentFixture<MultiomicsVisualizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiomicsVisualizerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MultiomicsVisualizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
