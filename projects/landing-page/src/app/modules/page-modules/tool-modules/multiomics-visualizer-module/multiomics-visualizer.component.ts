import { Component } from '@angular/core';
import { ToolOverviewCardComponent } from '../../layout-modules/tool-overview-card-module/tool-overview-card.component';
@Component({
  selector: 'multiomics-visualizer-page',
  templateUrl: './multiomics-visualizer.component.html',
  styleUrls: ['./multiomics-visualizer.component.scss'],
})
export class MultiomicsVisualizerComponent {
  showFoundingSection: boolean = true;
  usage = [
    {
      rowHeader: 'User Interface',
      data1: 'Web Application',
    },
    {
      rowHeader: 'Local Installation',
      data1: 'Docker-Compose, Docker or Python + npm',
    },
    { rowHeader: 'Input', data1: 'Omics files (CSV) and EC collection (CSV)' },
    {
      rowHeader: 'Output',
      data1: '2D and 3D visualizations on the web application',
    },
    {
      rowHeader: 'Repository',
      text: 'Multiomics Visualizer',
      link: 'https://github.com/danielwalke/multiomics_visualizer/tree/main',
    },
  ];

  founding = [
    {
      rowHeader: 'bmbf',
      text: 'German Federal Ministry of Education and Research (project MetaProtServ. grant No. 031L0103)',
      link: 'https://www.bmbf.de/',
    },
  ];

  used = [
    {
      rowHeader: 'Publication 1',
      text: 'Pubclication name/Author name',
      link: 'https://www.denbi.de',
    },
    {
      rowHeader: 'Publication 2',
      text: 'Pubclication name/Author name',
      link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads',
    },
  ];

  colab = [
    {
      rowHeader: '(Former) OVGU',
      name: 'Prasanna Ramesh',
      institutionText:
        '(former member of) Database and Software Engineering Group | Otto von Guericke University',
      institutionLink: 'https://www.dbse.ovgu.de/',
    },
    {
      rowHeader: 'DZHW',
      nameText: 'David Broneske',
      nameLink: 'https://www.dzhw.eu/gmbh/mitarbeiter?m_id=889',
      institutionText:
        'German Center for Higher Education Research and Science Studies (DZHW)',
      institutionLink: 'https://www.dzhw.eu/',
    },
    {
      rowHeader: 'OVGU',
      nameText: 'Gunter Saake',
      nameLink: 'https://www.dbse.ovgu.de/Mitarbeiter/Gunter+Saake.html',
      institutionText:
        'Database and Software Engineering Group | Otto von Guericke University',
      institutionLink: 'https://www.dbse.ovgu.de/',
    },
  ];

  updateList = [
    {
      title: '2D and 3D Visualizations of clustered multi-omics data',
      type: 'Newest Update',
      author: 'Daniel Walke',
      link: 'https://mdoa-tools.bi.denbi.de/multiomics/',
    },
    {
      title: 'Version 1.0',
      type: 'Newest Version',
      author: 'Daniel Walke',
      link: 'https://github.com/danielwalke/multiomics_visualizer/tree/main',
    },
  ];

  goToLink(url: string): void {
    window.open(url, '_blank');
  }
  constructor() {}

  ngOnInit(): void {}
}
