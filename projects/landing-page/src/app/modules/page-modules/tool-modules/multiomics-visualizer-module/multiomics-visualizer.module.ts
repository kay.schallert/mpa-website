import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {  MultiomicsVisualizerRoutingModule } from './multiomics-visualizer-routing.component';
import { MultiomicsVisualizerComponent } from './multiomics-visualizer.component';
import { ToolOverviewCardModule } from '../../layout-modules/tool-overview-card-module/tool-overview-card.module';

@NgModule({
    declarations: [MultiomicsVisualizerComponent],
    exports: [MultiomicsVisualizerComponent],
    imports: [
        CommonModule,
        MultiomicsVisualizerRoutingModule,
        MatButtonModule,
        MatIconModule,
        ToolOverviewCardModule,
    ]
})
export class MultiomicsVisualizerModule { }
