import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OmexComponent } from './omex.component';

const routes: Routes = [{ path: '', component: OmexComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OmexRoutingModule {}
