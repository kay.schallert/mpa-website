import { Component } from '@angular/core';
import { ToolOverviewCardComponent } from '../../layout-modules/tool-overview-card-module/tool-overview-card.component';
@Component({
  selector: 'template-page',
  templateUrl: './omex.component.html',
  styleUrls: ['./omex.component.scss'],
})
export class OmexComponent {
  showFoundingSection: boolean = true;
  usage = [
    {
      rowHeader: 'User Interface',
      data1: 'Webapplication',
    },
    { rowHeader: 'Local Installation', data1: 'Docker' },
    {
      rowHeader: 'Input',
      data1:
        'csv file with molecules in rows, samples in columns, abundances in cells (go to OMEx for more information)',
    },
    {
      rowHeader: 'Output',
      data1:
        'A molecule panel (usually < 10 molecules), molecule ranking, interactive plots (e.g., volcano, pca), classification metrics based on molecule panel',
    },
    {
      rowHeader: 'Repository (front end)',
      text: 'GitLab Frontend',
      link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development/projects/ofs?ref_type=heads',
    },
    {
      rowHeader: 'Repository (back end)',
      text: 'GitLab Backend',
      link: 'https://gitlab.com/kay.schallert/mpa-cloud-server/-/tree/master/src/main/java/deployment/omex?ref_type=heads',
    },
  ];

  founding = [
    {
      rowHeader:
        'MKW NRW',
      text: 'Ministry of Science of the German State of North-Rhine Westphalia',
      link: 'https://www.mkw.nrw',
    },
    {
      rowHeader: 'BMBF',
      text: 'German Federal Ministry of Education and Research',
      link: 'https://www.bmbf.de/',
    },
  ];

  used = [
    // {
    //   rowHeader: 'Publication 1',
    //   text: 'Pubclication name/Author name',
    //   link: 'https://www.denbi.de',
    // },
    // {
    //   rowHeader: 'Publication 2',
    //   text: 'Pubclication name/Author name',
    //   link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads',
    // },
  ];

  colab = [
    {
      rowHeader: 'HoMe',
      nameText: 'Johannes Schwerdt',
      nameLink:
        'https://www.hs-merseburg.de/hochschule/information/personenverzeichnis/details/person/schwerdt-johannes-1178/',
      institutionText: 'Hochschule Merseburg',
      institutionLink: 'https://www.hs-merseburg.de/',
    },
  ];

  updateList = [
    {
      title: 'Beta Version',
      type: 'Version',
      author: 'Emanuel Lange',
      link: 'https://mdoa-tools.bi.denbi.de/omex/home',
    },
  ];

  goToLink(url: string): void {
    window.open(url, '_blank');
  }
  constructor() {}

  ngOnInit(): void {}
}
