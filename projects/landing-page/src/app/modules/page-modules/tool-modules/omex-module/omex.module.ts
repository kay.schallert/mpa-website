import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { OmexRoutingModule } from './omex-routing.component';
import { OmexComponent } from './omex.component';
import { ToolOverviewCardModule } from '../../layout-modules/tool-overview-card-module/tool-overview-card.module';

@NgModule({
  declarations: [OmexComponent],
  exports: [OmexComponent],
  imports: [
    CommonModule,
    OmexRoutingModule,
    MatButtonModule,
    MatIconModule,
    ToolOverviewCardModule,
  ],
})
export class OmexModule {}
