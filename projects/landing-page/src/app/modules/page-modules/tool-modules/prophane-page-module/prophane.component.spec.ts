import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProphaneComponent } from './prophane.component';

describe('ProphaneComponent', () => {
  let component: ProphaneComponent;
  let fixture: ComponentFixture<ProphaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProphaneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProphaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
