import { Component } from '@angular/core';

@Component({
  selector: 'prophane-page',
  templateUrl: './prophane.component.html',
  styleUrls: ['./prophane.component.scss'],
})
export class ProphaneComponent {
  showFoundingSection: boolean = true;
  usage = [
    { rowHeader: 'User Interface', data1: 'Webapplication' },
    { rowHeader: 'Local Installation', data1: 'No'},
    { rowHeader: 'Input', data1: 'Fasta-File, Report-File (generic input format, mzTab format, mzIdent1.2 format)' },
    { rowHeader: 'Output', data1: 'Tab-Seperated-File (summary.txt) or mzTab (lca-summary.mztab, protein_summary.mztab, LCA_results per protein group) and Krona plots'},
    // { rowHeader: 'Repository', text: 'Link', link: ''}

  ];

  founding = [
    { rowHeader: 'DFG', text: 'Deutsche Forschungsgesellschaft - Research Software Sustainability', link: 'https://www.dfg.de/de'},
    { rowHeader: 'de.NBI', text: 'German Network for Bioinformatics Infrastructure - computational ressources and support', link: 'https://www.denbi.de/'}

  ];
  used = [
    { rowHeader: 'Publication 1', text: 'Pubclication name/Author name', link: ''},

  ];

  colab = [
    { rowHeader: 'Partner 1', nameText:'Name', nameLink:'', institutionText:'institution', institutionLink:''},

  ];

updateList = [
  // {
  //   title: 'New Update',
  //   type: 'Content',
  //   author: 'Kay Schallert',
  //   link: '',
  // },
  {
    title: 'New Website',
    type: 'Version',
    author: 'Kay Schallert',
    link: 'https://prophane.de/about',
  },
];

  goToLink(url: string): void {
    window.open(url, "_blank");
  }
  constructor() {}

  ngOnInit(): void {}
}
