import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ProphaneRoutingModule } from './prophane-routing.component';
import { ProphaneComponent } from './prophane.component';
import { ToolOverviewCardModule } from '../../layout-modules/tool-overview-card-module/tool-overview-card.module';

@NgModule({
    declarations: [ProphaneComponent],
    exports: [ProphaneComponent],
    imports: [
        CommonModule,
        ProphaneRoutingModule,
        MatButtonModule,
        MatIconModule,
        ToolOverviewCardModule,
    ]
})
export class ProphaneModule { }
