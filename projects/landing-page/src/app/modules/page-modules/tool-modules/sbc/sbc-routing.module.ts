import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SbcComponent } from './sbc.component';

const routes: Routes = [
  {path: '', component: SbcComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SbcRoutingModule { }
