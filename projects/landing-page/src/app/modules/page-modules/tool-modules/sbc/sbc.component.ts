import { Component } from '@angular/core';

@Component({
  selector: 'app-sbc',
  templateUrl: './sbc.component.html',
  styleUrls: ['./sbc.component.scss'],
})
export class SbcComponent {
  showFoundingSection: boolean = true;
  usage = [
    { rowHeader: 'User Interface', data1: 'Web application' },
    { rowHeader: 'Local Installation', data1: 'Docker, python' },
    {
      rowHeader: 'Input',
      data1:
        'patient-id, age, sex and complete blood count data (red blood cells, white blood cells, ' +
        'mean corpuscular volume, hemoglobin, platelets, measurement time)',
    },
    {
      rowHeader: 'Output',
      data1:
        'Predicted sepsis risk and SHAP values (i.e., influence of feature values to the prediction)',
    },
    {
      rowHeader: 'Repository',
      text: 'sbc app',
      link: 'https://github.com/danielwalke/sbc_app',
    },
  ];

  founding = [
    {
      rowHeader: 'DFG',
      text: 'Deutsche Forschungsgesellschaft - Research Software Sustainability (Grant Numbers HE 8077/2-1, SA 465/53-1)',
      link: 'https://www.dfg.de/de',
    },
    
  ];
  used = [
    {
      rowHeader: 'Publication 1',
      text: 'Pubclication name/Author name',
      link: '',
    },
  ];

  colab = [
    {
      rowHeader: 'OVGU',
      nameText: 'Daniel Walke',
      nameLink: 'https://www.dbse.ovgu.de/Mitarbeiter/Daniel+Walke.html',
      institutionText: 'Database and Software Engineering Group | Otto von Guericke University',
      institutionLink: 'https://www.dbse.ovgu.de/',
    },
    {
      rowHeader: 'UKL',
      name: 'Daniel Steinbach',
      institutionText: 'Institute of Laboratory Medicine, Clinical Chemistry and Molecular Diagnostics | Leipzig University Hospital',
      institutionLink: 'https://www.uniklinikum-leipzig.de/einrichtungen/labormedizin/EN',
    },
    {
      rowHeader: 'UK OWL',
      name: 'Thorsten Kaiser',
      institutionText: 'University Institute for Laboratory Medicine, Microbiology and Clinical Pathobiochemistry | OWL University Hospital of Bielefeld University',
      institutionLink: 'https://www.klinikum-lippe.de/laboratoriumsmedizin-mikrobiologie-klinische-pathobiochemie/',
    },
    {
      rowHeader: 'OVGU',
      nameText: 'Gunter Saake',
      nameLink: 'https://www.dbse.ovgu.de/Mitarbeiter/Gunter+Saake.html',
      institutionText:
        'Database and Software Engineering Group | Otto von Guericke University',
      institutionLink: 'https://www.dbse.ovgu.de/',
    },
    {
      rowHeader: 'DZHW',
      nameText: 'David Broneske',
      nameLink: 'https://www.dzhw.eu/gmbh/mitarbeiter?m_id=889',
      institutionText:
        'German Center for Higher Education Research and Science Studies (DZHW)',
      institutionLink: 'https://www.dzhw.eu/',
    },
    {
      rowHeader: 'ISAS',
      nameText: 'Robert Heyer',
      nameLink: 'https://www.isas.de/das-institut/personen/1592-robert-heyer',
      institutionText:
        'Multidimensional Omics Data Analysis | Leibniz-Institut für Analytische Wissenschaften - ISAS',
      institutionLink: 'https://www.isas.de/en/the-institute/people/1592-robert-heyer',
    },
  ];

  updateList = [
    // {
    //   title: 'New Update',
    //   type: 'Content',
    //   author: 'Kay Schallert',
    //   link: '',
    // },
    {
      title: 'Web Application Launched',
      type: 'Version 1.0',
      author: 'Daniel Walke',
      link: 'https://mdoa-tools.bi.denbi.de/sbc_frontend',
    },
  ];

  goToLink(url: string): void {
    window.open(url, '_blank');
  }
  constructor() {}

  ngOnInit(): void {}
}


