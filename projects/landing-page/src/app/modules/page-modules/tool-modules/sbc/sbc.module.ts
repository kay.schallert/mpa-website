import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SbcRoutingModule } from './sbc-routing.module';
import { SbcComponent } from '../sbc/sbc.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ToolOverviewCardModule } from '../../layout-modules/tool-overview-card-module/tool-overview-card.module';


@NgModule({
  declarations: [
    SbcComponent
  ],
  imports: [
    CommonModule,
    SbcRoutingModule,
    MatButtonModule,
    MatIconModule,
    ToolOverviewCardModule,
  ]
})
export class SbcModule { }
