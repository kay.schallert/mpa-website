import { Component } from '@angular/core';
import { ToolOverviewCardComponent } from '../../layout-modules/tool-overview-card-module/tool-overview-card.component';
@Component({
  selector: 'template-page',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss'],
})
export class TemplateComponent {
  showFoundingSection: boolean = true;
  usage = [
    { rowHeader: 'User Interface', data1: 'Webapplication / Command-Line-Interface' },
    { rowHeader: 'Local Installation', data1: 'Docker, Conda'},
    { rowHeader: 'Input', data1: 'Value (from MPA)' },
    { rowHeader: 'Output', data1: 'Value (can be used in Prophane)'},
    { rowHeader: 'Repository', text: 'Link', link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads'}

  ];

  founding = [
    { rowHeader: 'de.NBI', text: 'Link', link: 'https://www.denbi.de'},
    { rowHeader: 'NFDI', text: 'Link', link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads'}

  ];

  used = [
    { rowHeader: 'Publication 1', text: 'Pubclication name/Author name', link: 'https://www.denbi.de'},
    { rowHeader: 'Publication 2', text: 'Pubclication name/Author name', link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads'}

  ];

  colab = [
    { rowHeader: 'Partner 1', nameText:'Name', nameLink:'https://www.denbi.de', institutionText:'institution', institutionLink:'https://www.denbi.de'},
    { rowHeader: 'Partner 2', name: 'Name', institution: 'institution'}

  ];

updateList = [
  {
    title: 'New Content',
    type: 'Content',
    author: 'Daniel Kautzner',
    link: 'https://gitlab.com/kay.schallert/mpa-website/-/tree/mpa_development?ref_type=heads',
  },
  {
    title: 'Newest Version',
    type: 'Version',
    author: 'Daniel Kautzner',
    link: '#',
  },
];

  goToLink(url: string): void {
    window.open(url, "_blank");
  }
  constructor() {}

  ngOnInit(): void {}
}
