import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MetadataLandingPageComponent } from './modules/landing-page-module/metadata-landing-page.component';
import { BetterLoginPageComponent } from 'shared-lib';

const routes: Routes = [
  {
    path: "home",
    component: MetadataLandingPageComponent
  },
  {
    path: "workflow",
    loadChildren: () =>
      import("./modules/workflow-module/workflow.module").then(
        (m) => m.WorkflowModule
      )
  },
  // {
  //   path: "sdrf",
  //   loadChildren: () =>
  //     import("./pages/metadata-workflow-page/metadata-workflow.module").then(
  //       (m) => m.WorkflowModule
  //     )
  // },
  {
    path: "jobs",
    loadChildren: () =>
      import("./modules/job-overview-module/job-overview.module").then(
        (m) => m.JobOverviewModule
      )
  },
  {
    path: "about",
    loadChildren: () =>
      import(
        "./modules/metadatatool-about-module/metadatatool-about.module"
      ).then((m) => m.AboutMetadatatoolModule)
  },

  {
    path: "tutorial",
    loadChildren: () =>
      import(
        "./modules/metadatatool-tutorial-module/metadatatool-tutorial.module"
      ).then((m) => m.MetadatatoolTutorialModule)
  },

  { path: 'login', component: BetterLoginPageComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
