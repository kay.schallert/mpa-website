import { Component } from "@angular/core";
import {
  AuthService,
  NestedNavigationRoute,
  SimpleNavigationRoute,
  Logo
} from "shared-lib";
import { environment } from "../environments/environment";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "MetaForge";
  routes: NestedNavigationRoute[] = [
    { route: "/workflow", label: "Workflow", requireAuth: false },
    {
      route: "/jobs",
      label: "Job Overview",
      requireAuth: environment.production ? true : false
    }
  ];

  homelink: SimpleNavigationRoute = {
    route: "/metaforge_test/home",
    label: "Home",
    requireAuth: false
  };

  footerContent: NestedNavigationRoute[] = [
    {
      label: "About",
      children: [
        { label: "About Metadata-Tool", route: "/metaforge_test/about" },
        { label: "Terms of Service", route: "/metaforge_test/termsofservice" },
        { label: "Privacy Policy", route: "/metaforge_test/privacypolicy" },
        { label: "Impressum", route: "/metaforge_test/impressum" }
      ]
    },
    {
      label: "Funding & Support",
      children: [
        { label: "DFG", href: "http://www.dfg.de" },
        { label: "de.NBI", href: "http://www.denbi.de" },
        { label: "de.NBI Cloud", href: "https://www.denbi.de/cloud" },
        { label: "NFDI4Microbiota", href: "http://nfdi4microbiota.de" }
      ]
    }
  ];

  toolLogo: Logo = {
    label: "MetaForge",
    assetPath: "assets/img/IconLogo.png"
  };

  showLogo = true;

  footerLogos: Logo[] = [
    {
      label: "leibniz",
      assetPath: "assets/standard-logos/leibniz_white.png",
      href: "https://www.leibniz-gemeinschaft.de"
    },
    {
      label: "ISAS",
      assetPath: "assets/standard-logos/isaslogooffizielleformrgbweiss.png",
      href: "https://www.isas.de"
    },
    {
      label: "nrw",
      assetPath: "assets/standard-logos/nrw_black.svg",
      href: "https://www.land.nrw",
      backgroundColor: "white"
    },
    {
      label: "bmbf",
      assetPath: "assets/standard-logos/bmbf.svg",
      href: "https://www.bmbf.de",
      backgroundColor: "white"
    }
  ];

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.initializeOAuth();
  }
}
