import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { StandardPageLayoutModule } from 'dist/shared-lib';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NgParticlesModule } from 'ng-particles';
import { LandingModule } from 'projects/signaling-tool/src/app/landing-page/landing-page.module';
import { MetadataLandingPageComponent } from './modules/landing-page-module/metadata-landing-page.component';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { JobOverviewComponent } from './modules/job-overview-module/job-overview.component';
import { provideMatomo, withRouter } from 'ngx-matomo-client';

@NgModule({
  declarations: [AppComponent, MetadataLandingPageComponent],
  bootstrap: [AppComponent, LandingModule],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgParticlesModule,
    StandardPageLayoutModule,
    BrowserAnimationsModule,
    FormlyModule.forRoot(),
    ReactiveFormsModule,
    FormlyMaterialModule,
    // Material design imports
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatSidenavModule,
    MatListModule,
  ],
  providers: [
    provideHttpClient(withInterceptorsFromDi()),
    provideMatomo(
      {
        trackerUrl: 'https://piwik.cebitec.uni-bielefeld.de/',
        siteId: '29',
      },
      withRouter(), // Enables route tracking
    ),
  ],
})
export class AppModule {}
