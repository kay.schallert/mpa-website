export interface DownloadLinksJson {
  jobID: string
	processingFinished: boolean;
	mzidFileDownloads: string[];
	mzmlFileDownloads: string[];
	sdrfFileDownload: string;
  zipDownload: string;

}
