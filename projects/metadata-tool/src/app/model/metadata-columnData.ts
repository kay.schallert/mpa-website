export interface TableRow {
  ontId2Param: Map<string, string>;
  ontId2Enabled: Map<string, boolean>;

  // these are for JSON.stringify, which cant handle maps
  ontIdParamArray: Array<any>;
  ontId2EnabledArray: Array<any>;

  // non ontology entries
  identID: string;
  counter: number;
  psmFile: string;
  peptideFile: string;
  spectrumFile: string;
  mzID: string;
  mzML: string;
}

export class TableRowObject implements TableRow {
  ontId2Param: Map<string, string>;
  ontId2Enabled: Map<string, boolean>;

  // these are for JSON.stringify, which cant handle maps
  ontIdParamArray: Array<any>;
  ontId2EnabledArray: Array<any>;

  // non ontology entries
  identID: string;
  counter: number;
  psmFile: string;
  peptideFile: string;
  spectrumFile: string;
  mzID: string;
  mzML: string;

  constructor() {
    (this.identID = ""),
      (this.counter = -1),
      (this.mzID = ""),
      (this.mzML = ""),
      (this.spectrumFile = ""),
      (this.psmFile = ""),
      (this.peptideFile = ""),
      (this.ontId2Enabled = new Map<string, boolean>([
        ["sourcename", true],
        ["study", true],
        ["group", true],
        ["organization", true],
        ["organism", true],
        ["organismPart", true],
        ["cellType", true],
        ["ancestryCategory", true],
        ["age", true],
        ["sex", true],
        ["disease", true],
        ["individual", true],
        ["sampleStorageTemperature", true],
        ["proteinExtractionMethod", true],
        ["proteinPrecipitationMethod", true],
        ["proteinPurificationMethod", true],
        ["proteinConcentrationMethod", true],
        ["digestionMethod", true],
        ["biologicalReplicate", true],
        ["assayName", true],
        ["technologyType", true],
        ["biologicalReplicate", true],
        ["label", true],
        ["cleavageAgentDetails", true],
        ["instrument", true],
        ["study", true],
        ["program", true],
        ["modificationParameters", true],
        ["modificationParameters1", true],
        ["modificationParameters2", true],
        ["dissociationMethod", true],
        ["precursorMassTolerance", true],
        ["fragmentMassTolerance", true],
        ["comment", true],
        ["factorValue", true]
      ]));
    this.ontId2Param = new Map<string, string>([
      ["sourcename", ""],
      ["study", ""],
      ["group", ""],
      ["organization", ""],
      ["organism", ""],
      ["organismPart", ""],
      ["cellType", ""],
      ["ancestryCategory", ""],
      ["age", ""],
      ["sex", ""],
      ["disease", ""],
      ["individual", ""],
      ["sampleStorageTemperature", ""],
      ["proteinExtractionMethod", ""],
      ["proteinPrecipitationMethod", ""],
      ["proteinPurificationMethod", ""],
      ["proteinConcentrationMethod", ""],
      ["digestionMethod", ""],
      ["biologicalReplicate", ""],
      ["assayName", ""],
      ["technologyType", ""],
      ["biologicalReplicate", ""],
      ["label", ""],
      ["cleavageAgentDetails", ""],
      ["instrument", ""],
      ["study", ""],
      ["program", ""],
      ["modificationParameters", ""],
      ["modificationParameters1", ""],
      ["modificationParameters2", ""],
      ["dissociationMethod", ""],
      ["precursorMassTolerance", ""],
      ["fragmentMassTolerance", ""],
      ["comment", ""],
      ["factorValue", ""]
    ]);
  }
}
