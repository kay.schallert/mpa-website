import { Template } from "./ontology-term.model";
import { TemplateType } from "./templateType";
import { TableRow } from "./metadata-columnData";

type Status =
  | "UPLOAD_FILES_FOR_ANNOTATION"
  | "GET_METADATAJSON_FROM_ID"
  | "SUBMIT_METADATA"
  | "GET_DOWNLOADS_FROM_ID"
  | "GET_USERDATA"
  | "CREATE_NEW_CV"
  | "GET_SELECTED_CVS"
  | "GET_QUESTIONAIRE"
  | "COMPLETED"
  | "FAILED";

export interface JobData {
  jobId: string;
  jobname: string;
  createInitialMetadataJobId: string;
  fileConversionJobId: string;
  processingPipeline: string;
  details: string;
  creationdate: Date;
  statusmessage: string;
  downloadURL: string;
  status: Status;
  metadataForFiles: TableRow[];
  templateID: TemplateType; 
  template: Template;
}

export class JobDataObject implements JobData {
  public jobId: string;
  createInitialMetadataJobId: string;
  fileConversionJobId: string;
  metadataForFiles: TableRow[];
  processingPipeline: string;
  jobname: string;
  details: string;
  creationdate: Date;
  status: Status;
  statusmessage: string;
  downloadURL: string;

  constructor(data?: { jobname?: string; details?: string }) {
    this.jobname = data?.jobname || "";
    this.details = data?.details || "";
    this.creationdate = new Date();
    this.status = "UPLOAD_FILES_FOR_ANNOTATION";
    this.statusmessage = "";
    this.downloadURL = "";
    // Initialize other properties as needed
  }
  templateID: TemplateType; 
	template: Template;
}

// Send to the python microservice
export interface LLMExtractionPayload {
  text: string;
}

// response from the python microservice
export interface LLMExtractionResponse {
  extracted_properties: {
    extractedData: TableRow[];
  };
}
