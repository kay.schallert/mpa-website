import { TermType } from "./termType";

export interface TemplateItem {
  termID: string;
  type: TermType;
  position: number;
  termName: string;
  description: string;
  synonyms: string[];
  choosables: string[];
}

export interface Template {
  templateName: string;  // Name of the template
  templateItem: TemplateItem[]; // List of template items
}