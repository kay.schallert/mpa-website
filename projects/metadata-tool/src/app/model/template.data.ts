import { TemplateType } from "./templateType";

export interface TemplateSelection {
  value: TemplateType;       // unique identifier for the template
  viewValue: string;   // display name for the template
  description: string; // optional description
}

// Example template data; replace this with a backend call in Step 3
export const templateSelectionData: TemplateSelection[] = [
  { value: TemplateType.GENERIC , viewValue: "Generic Template", description: "A generic template for general use" },
  { value: TemplateType.HUMAN , viewValue: "Human Template", description: "Template specific to human data" },
  { value: TemplateType.OCEAN , viewValue: "Ocean Template", description: "Template specific to ocean data" }
];



