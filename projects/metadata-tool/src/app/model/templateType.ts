export enum TemplateType {
  GENERIC = 'GENERIC',
  HUMAN = 'HUMAN',
  OCEAN = 'OCEAN',
}