//import { Endpoints, WebserveraddressService } from '../webserveraddress.service';
import { MetaDataService } from "../../services/metaddata.service";
import {
  MultiFileUploadData,
  UploadDialogComponent,
  UploadFile,
  UploadProgressService
} from "shared-lib";
import { MatDialog } from "@angular/material/dialog";
import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from "@angular/core";
import { JobData } from "../../model/metadatauploadjson";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";

import { TableRow, TableRowObject } from "../../model/metadata-columnData";
import { MatStepper } from "@angular/material/stepper";
import { SDRFTableComponent } from "../workflow-module/SDRF-table-module/sdrf-table.component";
import { pipelineData } from "../../model/pipeline.data";

@Component({
  selector: "app-demo",
  templateUrl: "./demo.component.html",
  styleUrls: ["./demo.component.scss"]
})
export class DemoComponent {
  @ViewChild(SDRFTableComponent)
  metadataWorkflowComponent: SDRFTableComponent;

  selectedFiles: File[] = [];
}
