import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoComponent } from './demo.component';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatStepperModule } from '@angular/material/stepper';
import { SDRFTableModule } from '../workflow-module/SDRF-table-module/sdrf-table.module';
import { CheckboxSelectionModule } from '../workflow-module/checkboxselection/checkboxselection.module';
@NgModule({
  declarations: [DemoComponent],
  imports: [
    CommonModule,
    CheckboxSelectionModule,
    // material
    ReactiveFormsModule,
    MatIconModule,
    MatProgressBarModule,
    MatTooltipModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatGridListModule,
    MatStepperModule,
    SDRFTableModule,
    CheckboxSelectionModule,
  ],
  exports: [DemoComponent],
})
export class DemoModule {}
