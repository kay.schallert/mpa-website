import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobOverviewComponent } from './job-overview.component';

const routes: Routes = [
  {
    path: '',
    component: JobOverviewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobOverviewRoutingModule { }
