import { Component, OnInit } from "@angular/core";
import { MetaDataService } from "../../services/metaddata.service";
import { JobData } from "../../model/metadatauploadjson";
import { AuthService } from "shared-lib";

@Component({
  selector: "app-job-overview",
  templateUrl: "./job-overview.component.html",
  styleUrls: ["./job-overview.component.scss"]
})
export class JobOverviewComponent implements OnInit {
  myDataArray: JobData[] = [];
  columnsToDisplay: string[] = [
    "jobnumber",
    "jobname",
    "details",
    "creationdate",
    "actions"
  ];

  constructor(
    private authService: AuthService,
    private metaDataService: MetaDataService
  ) {}

  ngOnInit(): void {
    const jobs = this.metaDataService.allJobs;
    if (jobs && jobs.length > 0) {
      this.myDataArray = jobs;
    }
  }
}
