import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { JobOverviewRoutingModule } from "./job-overview-routing.module";
import { MatTableModule } from "@angular/material/table";
import { JobOverviewComponent } from "./job-overview.component";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
@NgModule({
  declarations: [JobOverviewComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    JobOverviewRoutingModule,
    MatCardModule,
    MatIconModule
  ]
})
export class JobOverviewModule {}
