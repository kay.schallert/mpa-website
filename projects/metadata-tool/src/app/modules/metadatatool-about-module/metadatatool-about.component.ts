import { Component } from "@angular/core";

@Component({
  selector: "metadatatool-about",
  templateUrl: "./metadatatool-about.component.html",
  styleUrls: ["./metadatatool-about.component.scss"]
})
export class AboutMetadatatool { }