import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SDRFTableComponent } from './sdrf-table.component';
import { applicationConfig, Meta, StoryObj } from '@storybook/angular';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { importProvidersFrom } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';

const meta: Meta = {
  title: 'Project/SDRF Table Component',
  component: SDRFTableComponent,
  decorators: [
    applicationConfig({
      providers: [
        importProvidersFrom(BrowserAnimationsModule),
        importProvidersFrom(HttpClientModule),
        importProvidersFrom(OAuthModule.forRoot()),
        importProvidersFrom(MatDialogModule),
        // ...any other modules that your component requires
      ],
    }),
  ],
};

export default meta;

type Story = StoryObj<typeof SDRFTableComponent>;

export const Default: Story = {
  render: () => ({
    props: {},
  }),
};
