import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SDRFTableComponent } from './sdrf-table.component';

const routes: Routes = [
  {path: '', component: SDRFTableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SDRFTableRoutingModule { }
