import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SDRFTableComponent } from './sdrf-table.component';

describe('SDRFTableComponent', () => {
  let component: SDRFTableComponent;
  let fixture: ComponentFixture<SDRFTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SDRFTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SDRFTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
