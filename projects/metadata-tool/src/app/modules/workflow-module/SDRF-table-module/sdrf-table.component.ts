import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from "@angular/core";
import { MetaDataService } from "../../../services/metaddata.service";
import {
  TableRow,
  TableRowObject as TableRowObject
} from "../../../model/metadata-columnData";
import { ContextMenu } from "handsontable/plugins";
import Handsontable from "handsontable";

import { Subscription } from "rxjs";
import { CheckboxSelectionService } from "../../../services/checkboxselectionservice";
import { TemplateItem } from "../../../model/ontology-term.model";
import { TermType } from "../../../model/termType";

@Component({
  selector: "sdrf-table",
  templateUrl: "./sdrf-table.component.html",
  styleUrls: ["./sdrf-table.component.scss"]
})
export class SDRFTableComponent implements OnInit {
  constructor(
    private metaDataInputService: MetaDataService,
    private checkboxService: CheckboxSelectionService,
    private cd: ChangeDetectorRef
  ) {}

  private mergedSelectionSubscription: Subscription;

  mergedSelection: TemplateItem[] = [];
  titlesArray: string[] = [];
  dataArray: any[] = [];
  hotInstance!: Handsontable;
  @ViewChild("hotContainer") hotContainer!: ElementRef;

  hotSettings: Handsontable.GridSettings = {
    data: [],
    columns: [],
    colHeaders: [],
    rowHeaders: true,
    licenseKey: "non-commercial-and-evaluation",
    manualColumnResize: true,
    height: "auto",
    filters: true,
    search: true,
    multiColumnSorting: true,
  };

  ngOnInit(): void {
    Object.values(TermType).forEach((termType) => {
      if (this.checkboxService.termCounterObservables[termType]) {
        this.checkboxService.termCounterObservables[termType].subscribe((count) => {
          console.log(`Counter updated for ${termType}:`, count);
        });
      }
    });
  
    this.checkboxService.mergedSelection$.subscribe((mergedSelection) => {
      const orderedSelection = this.getOrderedSelection(mergedSelection);
      this.updateTableColumns(orderedSelection);
    });
  }
  
  
  

  ngAfterViewInit(): void {
    this.initializeHandsontable();
  }
  
  private initializeHandsontable(): void {
    this.hotInstance = new Handsontable(
      this.hotContainer.nativeElement,
      this.hotSettings
    );
  }

  private getOrderedSelection(
    mergedSelection: Record<TermType, TemplateItem[]>
  ): TemplateItem[] {
    const orderedSelection: TemplateItem[] = [];
  
    // Ensure all TermTypes are included in the order defined by TermType enum
    Object.values(TermType).forEach((termType) => {
      if (mergedSelection[termType]?.length) {
        orderedSelection.push(...mergedSelection[termType]);
      }
    });
  
    return orderedSelection;
  }
  

  private updateTableColumns(orderedSelection: TemplateItem[]): void {
    this.mergedSelection = orderedSelection;
  
    // Update headers and column configuration
    this.titlesArray = this.mergedSelection.map((item) => item.termName);
    this.hotSettings.columns = this.mergedSelection.map((item) => ({
      data: item.termName,
    }));
    this.hotSettings.colHeaders = this.titlesArray;
  
    // Refresh Handsontable with new settings
    if (this.hotInstance) {
      this.hotInstance.updateSettings({
        columns: this.hotSettings.columns,
        colHeaders: this.hotSettings.colHeaders,
      });
    }
  }  
}