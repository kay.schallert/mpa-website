import { NgModule } from '@angular/core';
import { HotTableModule } from '@handsontable/angular';
import { registerAllModules } from 'handsontable/registry';
import { SDRFTableComponent } from './sdrf-table.component';
import { registerCellType, NumericCellType, } from 'handsontable/cellTypes';
import { registerPlugin,UndoRedo,Filters, DropdownMenu } from 'handsontable/plugins';
import { SDRFTableRoutingModule } from './sdrf-table-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { CheckboxSelectionModule } from '../checkboxselection/checkboxselection.module';

// register the filtering plugins
registerPlugin(Filters);
registerPlugin(DropdownMenu);
registerCellType(NumericCellType);
registerPlugin(UndoRedo);


// register Handsontable's modules
registerAllModules();

@NgModule({
  declarations: [SDRFTableComponent],
  imports: [
    CommonModule,
    HotTableModule,
    SDRFTableRoutingModule,
    MatButtonModule,
    MatIconModule,
    CheckboxSelectionModule,
  
  ],
  exports: [SDRFTableComponent],
})
export class SDRFTableModule { }
