import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckboxSelectionComponent } from './checkboxselection.component';

const routes: Routes = [
  {path: '', component: CheckboxSelectionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CheckboxSelectionRoutingModule { }
