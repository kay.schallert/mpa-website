import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxSelectionComponent } from './checkboxselection.component';

describe('MetaDataCheckboxSelectionComponent', () => {
  let component: CheckboxSelectionComponent;
  let fixture: ComponentFixture<CheckboxSelectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CheckboxSelectionComponent]
    });
    fixture = TestBed.createComponent(CheckboxSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
