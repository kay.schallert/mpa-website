import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { CheckboxSelectionService } from '../../../services/checkboxselectionservice';
import { MetaDataService } from '../../../services/metaddata-input.service';
import { MatDrawer } from '@angular/material/sidenav';
import { TermType } from '../../../model/termType';
import { TemplateItem } from '../../../model/ontology-term.model';
import { distinctUntilChanged } from 'rxjs';
import { TemplateType } from '../../../model/templateType';

@Component({
  selector: "app-checkboxselection",
  templateUrl: "./checkboxselection.component.html",
  styleUrls: ["./checkboxselection.component.scss"]
})
export class CheckboxSelectionComponent implements OnInit {
  form: FormGroup;
  @ViewChild(MatDrawer) drawer!: MatDrawer;

  groupedItems: Record<TermType, TemplateItem[]> = {
    [TermType.PROPERTIES]: [],
    [TermType.PROPERTIES_SELECT]: [],
    [TermType.CHARACTERISTICS]: [],
    [TermType.CHARACTERISTICS_SELECT]: [],
    [TermType.COMMENTS]: [],
    [TermType.COMMENTS_SELECT]: [],
    [TermType.FACTOR_VALUE]: [],
  };

  termCounters: Record<TermType, number> = {
    [TermType.PROPERTIES]: 0,
    [TermType.PROPERTIES_SELECT]: 0,
    [TermType.CHARACTERISTICS]: 0,
    [TermType.CHARACTERISTICS_SELECT]: 0,
    [TermType.COMMENTS]: 0,
    [TermType.COMMENTS_SELECT]: 0,
    [TermType.FACTOR_VALUE]: 0
  };
  
// Split term types into selectable and non-selectable
SELECTABLE_TERM_TYPES: TermType[] = [
  TermType.PROPERTIES_SELECT,
  TermType.CHARACTERISTICS_SELECT,
  TermType.COMMENTS_SELECT,
];
NON_SELECTABLE_TERM_TYPES: TermType[] = [
  TermType.PROPERTIES,
  TermType.CHARACTERISTICS,
  TermType.COMMENTS,
  TermType.FACTOR_VALUE,
];

  selectedTemplate: TemplateType = null

  @Output() continueClicked = new EventEmitter<void>();

  constructor(
    private formBuilder: FormBuilder,
    private checkboxService: CheckboxSelectionService,
    private metaDataService: MetaDataService
  ) {
    this.form = this.formBuilder.group(
      Object.keys(this.groupedItems).reduce((acc, key) => {
        acc[key] = new FormArray([]);
        return acc;
      }, {})
    );
  }

  ngOnInit(): void {
    // Listen for changes in metadataUploadJson and load template items when ready
    this.metaDataService.serverJobData.subscribe((metaDataUploadJson) => {
      const selectedTemplateID = metaDataUploadJson.templateID;
      if (selectedTemplateID && metaDataUploadJson.template?.templateItem) {
        this.loadTemplateItems(selectedTemplateID);
      } else {
        console.warn(`Template items not yet available for templateID: ${selectedTemplateID}`);
      }
    });
  } 
  
  private loadTemplateItems(templateID: string): void {
    console.log('Loading template items for templateID:', templateID);
  
    const templateItems = this.metaDataService.serverJobData.getValue()?.template?.templateItem;
  
    if (!templateItems || templateItems.length === 0) {
      console.warn(`No template items available for templateID: ${templateID}`);
      return;
    }
  
    // Group template items by TermType
    this.groupedItems = this.groupItemsByTermType(templateItems);
  
    // Initialize counters and notify the service
    Object.keys(this.groupedItems).forEach((key) => {
      const termType = key as TermType;
  
      // Initialize counter for all TermTypes
      this.termCounters[termType] = this.groupedItems[termType].length;
      this.checkboxService.updateTermCounter(termType, 0);
  
      // Only set up form arrays and notify service for selectable TermTypes
      if (this.SELECTABLE_TERM_TYPES.includes(termType)) {
        const formArray = this.getFormArray(termType);
        formArray.clear();
        this.addCheckboxes(this.groupedItems[termType], formArray);
        this.subscribeToValueChanges(termType, formArray);
  
        // Initialize the selection based on current checkbox values
        const selectedItems = this.getSelectedItems(termType, formArray);
        this.checkboxService.updateMergedSelection(termType, selectedItems);
      } else {
        // For non-selectable TermTypes, directly update the merged selection
        this.checkboxService.updateMergedSelection(termType, this.groupedItems[termType]);
      }
    });
  
    console.log('Grouped items:', this.groupedItems);
    console.log('Term counters:', this.termCounters);
  }
  
  

  // Expose Object.keys() for use in the template
  getObjectKeys(obj: Record<string, any>): string[] {
    return Object.keys(obj);
  }

  private groupItemsByTermType(items: TemplateItem[]): Record<TermType, TemplateItem[]> {
    const grouped: Record<TermType, TemplateItem[]> = {
      [TermType.PROPERTIES]: [],
      [TermType.PROPERTIES_SELECT]: [],
      [TermType.CHARACTERISTICS]: [],
      [TermType.CHARACTERISTICS_SELECT]: [],
      [TermType.COMMENTS]: [],
      [TermType.COMMENTS_SELECT]: [],
      [TermType.FACTOR_VALUE]: [],
    };

    items.forEach((item) => {
      const normalizedType = item.type?.toUpperCase(); // Normalize the type
      if (grouped[normalizedType as TermType]) {
        grouped[normalizedType as TermType].push(item);
      } else {
        console.warn(`Unexpected or unmapped type: ${item.type}`);
      }
    });

    return grouped;
  }

  private addCheckboxes(items: TemplateItem[], formArray: FormArray): void {
    if (!items || items.length === 0) {
      console.warn('No items available to add checkboxes.');
      return;
    }
  
    items.forEach(() => formArray.push(new FormControl(false)));
  
    // Initialize the counter to 0 for the termType associated with the formArray
    const termType = Object.keys(this.groupedItems).find(
      (key) => this.groupedItems[key as TermType] === items
    ) as TermType;
  
    if (termType && this.SELECTABLE_TERM_TYPES.includes(termType)) {
      this.termCounters[termType] = 0; // Initialize counter to 0
    }
  }
  

  
  private subscribeToValueChanges(termType: TermType, formArray: FormArray): void {
    formArray.valueChanges.subscribe(() => {
      // Update the counter
      this.updateCounter(termType, formArray);
      const selectedCount = formArray.controls.filter((control) => control.value).length;
      this.checkboxService.updateTermCounter(termType, selectedCount);
  
      // Update the selected items for this termType
      const selectedItems = this.getSelectedItems(termType, formArray);
      this.checkboxService.updateMergedSelection(termType, selectedItems);
    });
  }
  
  private updateCounter(termType: TermType, formArray: FormArray): void {
   
    this.termCounters[termType] = formArray.controls.filter((control) => control.value).length;
    this.checkboxService.updateTermCounter(termType, this.termCounters[termType]);
  }

  private getSelectedItems(termType: TermType, formArray: FormArray): TemplateItem[] {
    return formArray.controls
      .map((control, index) => (control.value ? this.groupedItems[termType][index] : null))
      .filter((item) => item !== null);
  }

  getFormArray(termType: TermType): FormArray {
    return this.form.get(termType) as FormArray;
  }
  
  getControls(type: TermType): FormControl[] {
    const formArray = this.form.get(type) as FormArray;
    return formArray ? (formArray.controls as FormControl[]) : [];
  }

  onTemplateChange(templateID: TemplateType): void {
    this.loadTemplateItems(templateID);
  }
  
  toggleDrawer(): void {
    if (this.drawer) {
      this.drawer.toggle();
    }
  }
}
