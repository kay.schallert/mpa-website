import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MetaDataService } from "../../../services/metaddata-input.service";
import {
  JobDataObject,
  LLMExtractionResponse,
} from "../../../model/metadatauploadjson";

/* This component is used to extract metadata from a text input. The text input is a textarea in the UI.
 The metadata is extracted using the MetaDataService.
 The text is send to a microservice and is extracted via a python script.
*/

@Component({
  selector: "app-metadata-extraction",
  templateUrl: "./metadata-extraction.component.html",
  styleUrls: ["./metadata-extraction.component.scss"]
})
export class MetadataExtractionComponent implements OnInit {
  extractionForm: FormGroup;
  isExtracting = false;
  jobDataObject: JobDataObject;

  constructor(
    private fb: FormBuilder,
    private metaDataService: MetaDataService
  ) {
    this.extractionForm = this.fb.group({
      textInput: [""]
    });
  }

  ngOnInit(): void {
    // Subscribe to metadataUploadJson changes
    this.metaDataService.serverJobData.subscribe((data) => {
      this.jobDataObject = data;
    });
  }
  extractTextData(): void {
    if (this.extractionForm.valid) {
      this.isExtracting = true;
      const text = this.extractionForm.get("textInput")?.value;

      this.metaDataService.extractTextData(text).subscribe({
        next: (response: LLMExtractionResponse) => {
          console.log("Extraction successful:", response);
          this.isExtracting = false;
        },
        complete: () => {
          this.isExtracting = false;
        }
      });
    }
  }
}
