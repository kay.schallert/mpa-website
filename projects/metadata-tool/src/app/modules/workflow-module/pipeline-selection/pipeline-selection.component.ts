import {
  Component,
  forwardRef,
  Input,
  Output,
  EventEmitter,
  OnInit
} from "@angular/core";

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { Pipeline } from "../../../model/workflow-types";

@Component({
  selector: "app-pipeline-selection",
  templateUrl: "./pipeline-selection.component.html",
  styleUrls: ["./pipeline-selection.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectionComponent),
      multi: true
    }
  ]
})
export class SelectionComponent implements ControlValueAccessor {
  @Input() entries: Pipeline[];
  @Input() label: string;
  @Output() selectedPipelineChange = new EventEmitter<Pipeline>();

  selectedPipeline: Pipeline;
  onChange: any = () => {};
  onTouched: any = () => {};

  writeValue(obj: any): void {
    this.selectedPipeline = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

 // Called when the user picks a new pipeline from the mat-select
 onValueChange(newValue: Pipeline): void {
  this.selectedPipeline = newValue;
  // Pass the value to the form model
  this.onChange(newValue);
  // Emit a separate event so parent can take action if needed
  this.selectedPipelineChange.emit(newValue);
}
}
