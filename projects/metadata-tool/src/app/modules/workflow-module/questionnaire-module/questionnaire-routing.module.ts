import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { QuestFormComponent } from "./questionnaire.component";
import { ReactiveFormsModule } from "@angular/forms";

const routes: Routes = [{ path: "", component: QuestFormComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes), ReactiveFormsModule],
  exports: [RouterModule]
})
export class QuestRoutingModule {}
