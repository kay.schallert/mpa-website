import {
  Component,
  forwardRef,
  Input,
  Output,
  EventEmitter,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MetaDataService } from '../../../services/metaddata-input.service';
import { TemplateSelection } from '../../../model/template.data';
import { templateSelectionData } from '../../../model/template.data';
import { JobData } from '../../../model/metadatauploadjson';
import { Template } from '../../../model/ontology-term.model';

@Component({
  selector: 'app-template-selection',
  templateUrl: './template-selection.component.html',
  styleUrls: ['./template-selection.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TemplateSelectionComponent),
      multi: true,
    },
  ],
})
export class TemplateSelectionComponent
  implements ControlValueAccessor, OnInit
{
  @Input() label: string = 'Select Template';
  @Input() entries: TemplateSelection[] = []; // Make sure entries is defined with @Input()
  @Output() selectedTemplateChange = new EventEmitter<TemplateSelection>();

  choosenTemplate: Template;
  selectionTemplate: TemplateSelection;

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor(private metaDataService: MetaDataService) {}

  ngOnInit(): void {
    // Load available templates, replace with actual call to backend if needed
    this.entries = templateSelectionData; // Assume templateData is an array of available templates
  }

  writeValue(obj: any): void {
    this.choosenTemplate = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onValueChange(newValue: TemplateSelection): void {
    // Update the selected template and emit change event
    this.selectionTemplate = newValue;
    this.onChange(newValue);
    this.selectedTemplateChange.emit(newValue);

    this.metaDataService.getTemplate(newValue.value).subscribe({
      next: (jobData: JobData) => {
        console.log('Received jobData:', jobData);
        this.choosenTemplate = jobData.template; // Assign the full template object
        console.log('Selected template:', this.choosenTemplate);
      },
      error: (error) => {
        console.error('Failed to load template:', error);
      }
    });
    
  }
}
