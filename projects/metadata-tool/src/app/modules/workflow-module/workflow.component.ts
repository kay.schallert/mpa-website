import { MetaDataService } from "../../services/metaddata.service"; // TODO
import {
  MultiFileUploadData,
  UploadDialogComponent,
  UploadFile,
  UploadProgressService
} from "shared-lib";
import { MatDialog } from "@angular/material/dialog";
import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from "@angular/core";
import { JobData } from "../../model/metadatauploadjson";

import { FormBuilder, FormControl, FormGroup } from "@angular/forms";

import { pipelineData } from "../../model/pipeline.data";
import { TableRow, TableRowObject } from "../../model/metadata-columnData";
import { MatStepper } from "@angular/material/stepper";
import { SDRFTableComponent } from "./SDRF-table-module/sdrf-table.component";
import { FileWithProcessedInfo, Pipeline } from "../../model/workflow-types";
import { TemplateSelection, templateSelectionData } from "../../model/template.data";

@Component({
  selector: "app-workflow",
  templateUrl: "./workflow.component.html",
  styleUrls: ["./workflow.component.scss"]
})
export class WorkflowComponent implements OnInit {
  @ViewChild("stepper") stepper: MatStepper;
  @ViewChild(SDRFTableComponent)
  metadataWorkflowComponent: SDRFTableComponent;

  selectedFiles: File[] = [];

  // controls  material stepper logic in html
  pipelineIsSelected = false;
  secondStepCompleted = false;
  uploadTriggered = false;
  metadataQuestionsCompleted = false;
  metadataTableCompleted = false;
  fileDownloadCompleted = false;
  submitAndGoToDownloadPressed = false;

  @Output() uploadStatusChanged = new EventEmitter<{
    progress: number;
  }>();

  model;
  selectedTemplate: TemplateSelection;
  pipelines: Pipeline[] = pipelineData;
  templates: TemplateSelection[] = templateSelectionData;
  selectedPipeline: Pipeline = this.pipelines[1];
  uploadDialogId: string;
  uploadProgress = 0;
  showContainer = true;
  jobData: JobData;
  onChange: any = () => {};

  pipelineSelectFormGroup: FormGroup;
  templateSelectFormGroup: FormGroup;
  fileUploadFormGroup: FormGroup;
  metadataQuestionFormGroup: FormGroup;
  metadataTableFormGroup: FormGroup;
  fileDownloadFormGroup: FormGroup;
  textExtractionFormGroup = new FormGroup({
    textInput: new FormControl("")
  });

  constructor(
    private dataService: MetaDataService,
    private uploadProgressService: UploadProgressService,
    private dialog: MatDialog,
    private _formBuilder: FormBuilder
  ) {}

  extractTextData(): void {
    const text = this.textExtractionFormGroup.get("textInput").value;
    this.dataService.extractAndProcessText(text).subscribe({
      next: (response) => {
        console.log("Data extracted and processed:", response);
      },
      error: (error) => {
        console.error("Failed to extract data:", error);
      },
      complete: () => {
        console.log("Data extraction process completed.");
      }
    });
  }

  handleFileSelection(files: FileWithProcessedInfo[]) {
    // handle the actual files
    this.selectedFiles = files.map((f) => f.file);
    // we get all files each time, so we reset the table and map again
    this.jobData.metadataForFiles = [];
    const rowMapping = new Map<string, TableRow>();
    files.forEach((f: FileWithProcessedInfo) => {
      const groupKey =
        f.processedInfo.batchDescription + "_" + f.processedInfo.sampleNumber;
      if (rowMapping.has(groupKey)) {
        const existingRow: TableRow = rowMapping.get(groupKey);
        if (f.processedInfo.fileCategory == "PSM") {
          existingRow.psmFile = f.processedInfo.id;
        } else if (f.processedInfo.fileCategory == "peptide") {
          existingRow.peptideFile = f.processedInfo.id;
        } else if (f.processedInfo.fileCategory == "spectra") {
          existingRow.spectrumFile = f.processedInfo.id;
        }
      } else {
        const newRow: TableRow = new TableRowObject();
        if (f.processedInfo.fileCategory == "PSM") {
          newRow.psmFile = f.processedInfo.id;
        } else if (f.processedInfo.fileCategory == "peptide") {
          newRow.peptideFile = f.processedInfo.id;
        } else if (f.processedInfo.fileCategory == "spectra") {
          newRow.spectrumFile = f.processedInfo.id;
        }
        rowMapping.set(groupKey, newRow);
      }
    });
    rowMapping.forEach((row) => {
      this.jobData.metadataForFiles.push(row);
    });
    // push changes to service
    this.dataService.currentJob.next(this.jobData);
  }

  ngOnInit() {
    this.dataService.currentJob.subscribe((json) => {
      this.jobData = json;
    });
    // Emit the progress
    this.uploadProgressService.currentProgress.subscribe((progress) => {
      this.uploadProgress = progress;
      if (this.uploadProgress != 0) {
        this.uploadTriggered = true;
      }
      this.uploadStatusChanged.emit({
        progress: this.uploadProgress
      });
    });

    // TODO: DEACTIVATE
    // activate for deactivating guards for the stepper for easy debugging

    this.pipelineSelectFormGroup = this._formBuilder.group({
      // initializations for the first form group
    });
    this.fileUploadFormGroup = this._formBuilder.group({
      // initializations for the second form group
    });
    this.metadataQuestionFormGroup = this._formBuilder.group({
      // initializations for the third form group
    });
    this.metadataTableFormGroup = this._formBuilder.group({
      // initializations for the third form group
    });
    this.fileDownloadFormGroup = this._formBuilder.group({});
  }

  onUpload(): void {
    // Hide certain UI elements during upload
    this.showContainer = false;

    // Set a unique identifier for this upload session
    this.uploadProgressService.setUUID("UPLOAD");

    // multifile object
    const files: MultiFileUploadData = { files: [] };

    // TODO: add actual file data
    this.jobData.processingPipeline = this.selectedPipeline.value;

    // this is to overcome javascript limitation on maps
    this.jobData.metadataForFiles.forEach((col) => {
      col.ontId2EnabledArray = Array.from(col.ontId2Enabled);
      col.ontIdParamArray = Array.from(col.ontId2Param);
    });

    this.dataService.currentJob.next(this.jobData);

    // this.dataService.updateAllMetaDataUploadJson({
    //   processingPipeline: this.selectedPipeline.value
    // });

    // prepare metadatajson for upload
    const metadataJsonAsFile: File = new File(
      [JSON.stringify(this.jobData)],
      "jobObject"
    );
    files.files.push({ uploadFile: metadataJsonAsFile, fileID: "jobObject" });

    // Prepare the data files for upload
    this.selectedFiles.forEach((file) => {
      files.files.push({ uploadFile: file, fileID: file.name });
    });

    files.files.forEach((file: UploadFile) => {
      this.uploadProgressService.addToTotal(file.uploadFile.size);
    });

    // Upload the files
    this.dataService.uploadFiles(
      files,
      this.uploadProgressService,
      this.dialog
    );

    // no dialog for now
    // Open a dialog indicating the upload has started
    // const dialogRef = this.dialog.open(UploadDialogComponent, {
    //   id: this.uploadDialogId,
    //   disableClose: false,
    // data: { successMessage: "Upload initiated." }
    // });

    // Handle post-upload actions
    // dialogRef.afterClosed().subscribe(/* ... */);
    // Update the metadata with the selected pipeline information

    // Clear the list of selected files and reset the file input
    this.selectedFiles = [];
    this.clearFileInput();

    // Set this to true once upload is successful\
    this.uploadTriggered = true;
    this.stepper.next();
  }

  // Helper method to clear the file input
  clearFileInput(): void {
    const inputElem = document.querySelector(
      ".upload-input"
    ) as HTMLInputElement;
    if (inputElem) {
      inputElem.value = ""; // Clear the file input
    }
  }

  onMetadataQuestCompleted(completed: boolean) {
    this.metadataQuestionsCompleted = completed;
    // Logic to enable Metadata Table step
    if (completed) {
      // Enable the next step
      this.metadataTableCompleted = true;
      this.stepper.next();
    }
  }

  onHorizontalFormSubmit() {
    this.stepper.next();
  }

  // onSubmitAndGoToDownload() {
  //   if (this.metadataWorkflowComponent) {
  //     this.metadataWorkflowComponent.submit();
  //   }
  //   this.submitAndGoToDownloadPressed = true;
  //   this.stepper.next(); // Proceed to the next step
  // }

  onTemplateSelected(template: TemplateSelection) {
    this.selectedTemplate = template;
    console.log("Selected template:", template);
  }

  // For Pipeline-Selection
 /**
   * Called when the pipeline selection changes via ngModelChange.
   * @param pipeline The pipeline selected in the child component
   */
 onPipelineChange(pipeline: Pipeline): void {
  // Store the pipeline locally
  // console.log("Pipeline changed to:", pipeline);
  this.selectedPipeline = pipeline;
  //Guard for Stepper
  this.pipelineIsSelected = !!pipeline;

  // Update the JobDataObject with the pipeline’s unique identifier
  if (pipeline) {
    this.jobData.processingPipeline = pipeline.value;
  }
}

onNextStep(stepper: any): void {
  if (!this.pipelineIsSelected) {
    return; // Or show some validation message
  }
  // Perform any pre-navigation logic here
  stepper.next();
}

}
