import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { WorkflowComponent } from "./workflow.component";
import { MatIconModule } from "@angular/material/icon";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MetadataUploadpageRoutingModule } from "./workflow-routing.module";
import { MatTooltipModule } from "@angular/material/tooltip";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatOptionModule } from "@angular/material/core";
import { MatSelectModule } from "@angular/material/select";
import { MatButtonModule } from "@angular/material/button";
import { ProgressBarComponent } from "./progress-bar/progress-bar.component";
import { SelectionComponent } from "./pipeline-selection/pipeline-selection.component";
import { FileUploadComponent } from "./file-upload/file-upload.component";
import { MatTableModule } from "@angular/material/table";
import { MatGridListModule } from "@angular/material/grid-list";
import { QuestFormModule } from "./questionnaire-module/questionnaire.module";
import { MatStepperModule } from "@angular/material/stepper";

import { SDRFTableModule } from "./SDRF-table-module/sdrf-table.module";
import { CheckboxSelectionModule } from "./checkboxselection/checkboxselection.module";
import { MetadataDownloadpageModule } from "./download-page-module/metadata-downloadpage.module";
import { TemplateSelectionComponent } from "./template-selection/template-selection.component";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MetadataExtractionComponent } from './metadata-extraction/metadata-extraction.component';

@NgModule({
  declarations: [
    WorkflowComponent,
    ProgressBarComponent,
    SelectionComponent,
    TemplateSelectionComponent,
    FileUploadComponent,
    MetadataExtractionComponent
    
  ],
  imports: [
    MatProgressSpinnerModule,
    CommonModule,
    QuestFormModule,

    // material
    ReactiveFormsModule,
    MatIconModule,
    MatProgressBarModule,
    MetadataUploadpageRoutingModule,
    MatTooltipModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatGridListModule,
    MatStepperModule,
    SDRFTableModule,
    CheckboxSelectionModule,
    MetadataDownloadpageModule
  ],
  exports: [
    WorkflowComponent,
    ProgressBarComponent,
    SelectionComponent,
    FileUploadComponent
  ]
})
export class WorkflowModule {}

