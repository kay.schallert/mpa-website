import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CheckboxSelectionComponent } from '../modules/workflow-module/checkboxselection/checkboxselection.component';
import { TermType } from '../model/termType';
import { TemplateItem } from '../model/ontology-term.model';

@Injectable({
  providedIn: 'root',
})
export class CheckboxSelectionService {
  // BehaviorSubjects for each TermType's counters
  private termCounters: Record<TermType, BehaviorSubject<number>> = {
    [TermType.PROPERTIES]: new BehaviorSubject<number>(0),
    [TermType.PROPERTIES_SELECT]: new BehaviorSubject<number>(0),
    [TermType.CHARACTERISTICS]: new BehaviorSubject<number>(0),
    [TermType.CHARACTERISTICS_SELECT]: new BehaviorSubject<number>(0),
    [TermType.COMMENTS]: new BehaviorSubject<number>(0),
    [TermType.COMMENTS_SELECT]: new BehaviorSubject<number>(0),
    [TermType.FACTOR_VALUE]: new BehaviorSubject<number>(0),
  };

  // Observables for each TermType's counters
  termCounterObservables: Record<TermType, Observable<number>> = {
    [TermType.PROPERTIES]: this.termCounters[TermType.PROPERTIES].asObservable(),
    [TermType.PROPERTIES_SELECT]:
      this.termCounters[TermType.PROPERTIES_SELECT].asObservable(),
    [TermType.CHARACTERISTICS]:
      this.termCounters[TermType.CHARACTERISTICS].asObservable(),
    [TermType.CHARACTERISTICS_SELECT]:
      this.termCounters[TermType.CHARACTERISTICS_SELECT].asObservable(),
    [TermType.COMMENTS]: this.termCounters[TermType.COMMENTS].asObservable(),
    [TermType.COMMENTS_SELECT]:
      this.termCounters[TermType.COMMENTS_SELECT].asObservable(),
    [TermType.FACTOR_VALUE]:
      this.termCounters[TermType.FACTOR_VALUE].asObservable(),
  };

  // BehaviorSubject to store the merged selection for all TermTypes
  private mergedSelectionSubject: BehaviorSubject<Record<TermType, TemplateItem[]>> = 
  new BehaviorSubject<Record<TermType, TemplateItem[]>>({
    [TermType.PROPERTIES]: [],
    [TermType.PROPERTIES_SELECT]: [],
    [TermType.CHARACTERISTICS]: [],
    [TermType.CHARACTERISTICS_SELECT]: [],
    [TermType.COMMENTS]: [],
    [TermType.COMMENTS_SELECT]: [],
    [TermType.FACTOR_VALUE]: [],
  });


  // Observable for the merged selection
  mergedSelection$: Observable<Record<TermType, TemplateItem[]>> =
    this.mergedSelectionSubject.asObservable();

    updateTermCounter(termType: TermType, count: number): void {
      if (!this.termCounters[termType]) {
        console.error(`Invalid or uninitialized TermType: ${termType}`);
        return;
      }
      this.termCounters[termType].next(count);
    }

  // Get the current counter value for a TermType
  getTermCounterValue(termType: TermType): number {
    return this.termCounters[termType].value;
  }

  updateMergedSelection(
    termTypeOrSelection: TermType | Record<TermType, TemplateItem[]>,
    items?: TemplateItem[]
  ): void {
    if (typeof termTypeOrSelection === 'string') {
      // Update specific TermType
      const termType = termTypeOrSelection;
      const currentSelection = this.mergedSelectionSubject.value;
      currentSelection[termType] = items || [];
      this.mergedSelectionSubject.next({ ...currentSelection });
    } else {
      // Update entire merged selection
      const allTermTypes = Object.values(TermType);
  
      // Ensure all TermTypes (including non-selectable) are present
      const updatedSelection = { ...this.mergedSelectionSubject.value };
      allTermTypes.forEach((type) => {
        updatedSelection[type] =
          termTypeOrSelection[type] || updatedSelection[type] || [];
      });
  
      this.mergedSelectionSubject.next(updatedSelection);
    }
  }
  

  // Get the current merged selection
  getMergedSelection(): Record<TermType, TemplateItem[]> {
    return this.mergedSelectionSubject.value;
  }
}