import { Injectable } from "@angular/core";
import { TableRow, TableRowObject } from "../model/metadata-columnData";
import {
  HttpClientService,
  MultiFileUploadData,
  UploadProgressService
} from "shared-lib";
import {
  Endpoints,
  WebserveraddressService,
  LLMEndpoints
} from "./webserveraddress.service";
import {
  JobData, JobDataObject
} from "../model/metadatauploadjson";
import { HttpEventType, HttpParams } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { BehaviorSubject, catchError, map, Observable, of, take, tap,throwError,timeout } from "rxjs";
import { DownloadLinksJson } from "../model/download-json";
import {
  LLMExtractionPayload,
  LLMExtractionResponse
} from "../model/metadatauploadjson";
import { TemplateItem } from "../model/ontology-term.model";
import { TemplateSelection } from "../model/template.data";
import { TemplateType } from "../model/templateType";

@Injectable({
  providedIn: 'root',
})
export class MetaDataService {
  downloadUrls: string[] = [];
  zipUrl: string;

  uploadFinish: Boolean;
  jobFinished: Boolean;

  private ontologyTerms = new BehaviorSubject<TemplateItem[]>([]);
  public ontologyTerms$ = this.ontologyTerms.asObservable();

  public serverJobData = new BehaviorSubject<JobData>(new JobDataObject);

  constructor(
    private http: HttpClientService,
    private url: WebserveraddressService
  ) {}

  getDownloads(): string[] {
    return this.downloadUrls;
  }

  getZip(): string {
    return this.zipUrl;
  }

// OLD REVIEW
//   updateJobData(newMetadata: JobData): void {
    
//     // updatePipeline
//   // updateTemplateSelection

//   // updateMetadataForFiles

//   try {
//     const currentMetaData = this.serverJobData.value;
//     this.serverJobData.next({
//       ...currentMetaData,
//       metadataForFiles: newMetadata.metadataForFiles,
//     });
//   } catch (error) {
//     console.error('An error occurred while updating metadata:', error);
//   }
// }

  updateJobData(newMetadata: JobData): void {
      try {
        const currentMetaData = this.serverJobData.value;
        this.serverJobData.next({
          ...currentMetaData,
          ...newMetadata,
  
        });
      } catch (error) {
        console.error("An error occurred while updating job data:", error);
      }
    }
  
    // TODO: Review
    // updatePartialJobData(newMetadata: Partial<JobData>): void {
    //   try {
    //     const currentMetaData = this.serverJobData.value;
  
    //     // Build an object that only overrides the specific fields you care about
    //     const updatedMetaData: JobData = {
    //       ...currentMetaData,
    //       // Only overwrite these three (or however many) fields:
    //       processingPipeline: newMetadata.processingPipeline 
    //         ?? currentMetaData.processingPipeline,
    //       templateID: newMetadata.templateID 
    //         ?? currentMetaData.templateID,
    //       template: newMetadata.template 
    //         ?? currentMetaData.template,
    //       metadataForFiles: newMetadata.metadataForFiles 
    //         ?? currentMetaData.metadataForFiles,
  
    //       // Keep the rest as is.
    //     };
  
    //     // Then push the updated data out to the BehaviorSubject
    //     this.serverJobData.next(updatedMetaData);
    //   } catch (error) {
    //     console.error("An error occurred while updating partial metadata:", error);
    //   }
    // }

   updateMetadataForFiles(newMetadataForFiles: TableRow[]): void {
      try {
        const currentMetaData = this.serverJobData.value;
  
        // Build an object that only overrides the specific fields you care about
        const updatedMetaData: JobData = {
          ...currentMetaData,
          metadataForFiles: newMetadataForFiles
        };
  
        // Then push the updated data out to the BehaviorSubject
        this.serverJobData.next(updatedMetaData);
      } catch (error) {
        console.error("An error occurred while updating partial metadata:", error);
      }
    }
  
  upload(
    files: MultiFileUploadData,
    uploadProgress: UploadProgressService,
    dialog: MatDialog
  ): void {
    this.uploadFinish = false;
    this.jobFinished = false;
    this.http
      .postMultiPartFilesEvents(files, this.url.getURL(Endpoints.UPLOAD_FILES))
      .subscribe({
        next: (event) => {
          if (event.type === HttpEventType.UploadProgress) {
            uploadProgress.changeReportLoaded(event.loaded);
          } else if (event.type === HttpEventType.Response) {
            this.uploadFinish = true;
            const newJobData: JobData =
              event.body as JobData;
            // const copy: MetadataJsonObject = event.body as MetadataJsonObject;
            newJobData.metadataForFiles.forEach((col) => {
              const ontId2Param = new Map<string, string>();
              for (const key in col.ontId2Param) {
                ontId2Param.set(key, col.ontId2Param[key]);
              }
              col.ontId2Param = ontId2Param;
              const ontId2Enabled = new Map<string, boolean>();
              for (const key in col.ontId2Enabled) {
                ontId2Enabled.set(key, col.ontId2Enabled[key]);
              }
              col.ontId2Enabled = ontId2Enabled;
            });
            //this.metadataUploadJson.next(newMetadataJson);
            const params = new HttpParams({
              fromObject: {
                jobid: newJobData.createInitialMetadataJobId,
              },
            });
            this.http
              .repeatedGetObject<JobData>(
                ['jobId'],
                this.url.getURL(Endpoints.GET_INITIAL_METADATA),
                params,
                2000
              )
              .subscribe((response: JobData) => {
                response.metadataForFiles.forEach((col) => {
                  this.jobFinished = true;
                  const ontId2Param = new Map<string, string>();
                  for (const key in col.ontId2Param) {
                    ontId2Param.set(key, col.ontId2Param[key]);
                  }
                  col.ontId2Param = ontId2Param;
                  const ontId2Enabled = new Map<string, boolean>();
                  for (const key in col.ontId2Enabled) {
                    ontId2Enabled.set(key, col.ontId2Enabled[key]);
                  }
                  col.ontId2Enabled = ontId2Enabled;
                });
                //this.metadataUploadJson.next(newMetadataJson);
                this.serverJobData.next(response);
              });
          } else {
            console.log('unknown event');
            console.log(event);
            console.log(event.type);
          }
        },
        error: (error) => {
          console.log(error);
          if (error.status >= 400) {
            // handle failed upload
            if (dialog.getDialogById('UPLOAD')) {
              dialog
                .getDialogById('UPLOAD')
                .componentInstance.setUploadFailed();
              dialog.getDialogById(
                'UPLOAD'
              ).componentInstance.uploadFailedMessage = error.statusText;
            }
          } else {
            throw error;
          }
        },
      });
  }

  extractAndProcessText(text: string) {
    // Example logic for processing the entire text data
    const cleanedText = text.replace(/\n/g, ' ').trim();
    const processedData = { processedText: cleanedText };

    // Assuming 'PROCESS_TEXT' is the endpoint where the server processes the text
    this.http
      .postObject<any, any>(
        processedData,
        this.url.getURL(Endpoints.PROCESS_TEXT)
      )
      .subscribe({
        next: (response) => {
          console.log('Text processed on server:', response);
        },
        error: (error) => {
          console.error('Failed to process text on server:', error);
        },
      });

    return new BehaviorSubject(processedData);
  }

  submiteTable(dataExport: TableRow[]) {
    const uploadJson = this.serverJobData.getValue();
    uploadJson.metadataForFiles = dataExport;

    // this is to overcome javascript limitation on maps
    uploadJson.metadataForFiles.forEach((col) => {
      col.ontId2EnabledArray = Array.from(col.ontId2Enabled);
      col.ontIdParamArray = Array.from(col.ontId2Param);
    });

    this.serverJobData.next(uploadJson);
    this.http
      .postObject<JobData, JobData>(
        this.serverJobData.value,
        this.url.getURL(Endpoints.SUBMIT_METADATA)
      )
      .subscribe((response) => {
        const json = this.serverJobData.value;
        json.fileConversionJobId = response.fileConversionJobId;
        this.serverJobData.next(json);
        this.http
          .repeatedPostObject<JobData, DownloadLinksJson>(
            response,
            ['jobID'],
            this.url.getURL(Endpoints.GET_DOWNLOAD_LINKS),
            new HttpParams()
          )
          .subscribe((response: DownloadLinksJson) => {
            response.mzmlFileDownloads.forEach((url) => {
              this.downloadUrls.push(url);
            });
            response.mzidFileDownloads.forEach((url) => {
              this.downloadUrls.push(url);
            });
            this.downloadUrls.push(response.sdrfFileDownload);
            this.zipUrl = response.zipDownload;
          });
      });
  }

  // // Fetches ontology terms from the backend
  // fetchOntologyTerms(): void {
  //   this.http
  //     .getObject<TemplateItem[]>(this.url.getURL(Endpoints.GET_SELECTED_CVS))
  //     .subscribe({
  //       next: (terms: TemplateItem[]) => {
  //         this.ontologyTerms.next(terms);
  //       },
  //       error: (error) => {
  //         console.error('Error fetching ontology terms:', error);
  //       },
  //     });
  // }


  private templateItemsLoaded$ = new BehaviorSubject<boolean>(false);

  getTemplateItemsLoaded$() {
    return this.templateItemsLoaded$.asObservable();
  }

  setTemplateItemsLoaded(state: boolean) {
    this.templateItemsLoaded$.next(state);
  }

    /**
   * Fetches a specific template from the backend based on the provided templateType.
   * @param templateType The type of template to fetch (e.g., "GENERIC", "HUMAN")
   * @returns Observable with the template data
   */

    getTemplate(templateType: TemplateType): Observable<JobData> {
      const changedJson: JobData = this.serverJobData.getValue();
      changedJson.templateID = templateType; // Set the template ID
      this.serverJobData.next(changedJson); // Update the metadataUploadJson
    
      const apiUrl: string = this.url.getURL(Endpoints.GET_TEMPLATE);
      console.log('Computed getTemplate URL:', apiUrl);
      return this.http.postObject<JobData, JobData>(changedJson, apiUrl).pipe(
        tap((response) => {
          console.log('Raw backend response:', response);
          if (response.template?.templateItem?.length > 0) {
            this.serverJobData.next(response);
            this.setTemplateItemsLoaded(true); // Signal items are ready
          } else {
            console.warn('Template fetched but contains no items:', response);
            this.setTemplateItemsLoaded(false); // Signal items are not ready
          }
        }),
        catchError((error) => {
          console.error('Failed to fetch template:', error);
          this.setTemplateItemsLoaded(false);
          return of(null);
        })
      );
    }
    


    getTemplateItems(templateID: TemplateType): Observable<TemplateItem[]> {
      const currentMetadata = this.serverJobData.getValue();
      
      // Avoid unnecessary updates
      if (currentMetadata.templateID === templateID) {
        return of(currentMetadata.template?.templateItem || []); // Return existing items if the templateID hasn't changed
      }
      currentMetadata.templateID = templateID;
      this.serverJobData.next(currentMetadata); // Update the current metadata state
    
      const apiUrl: string = this.url.getURL(Endpoints.GET_TEMPLATE);
      return this.http.postObject<JobData, JobData>(currentMetadata, apiUrl).pipe(
        map((response) => {
          if (response.template && response.template.templateItem) {
            return response.template.templateItem; // Return template items
          } else {
            console.warn(`No template items found for templateID: ${templateID}`);
            return []; // Fallback for null templates
          }
        }),
        catchError((error) => {
          console.error('Failed to fetch template items:', error);
          return of([]); // Return empty array on error
        })
      );
    }
    
    extractTextData(text: string): Observable<LLMExtractionResponse> {
    const cleanedText = text.replace(/\n/g, " ").trim();
    const processedData: LLMExtractionPayload = { text: cleanedText };

    console.log("Sending data:", processedData);

    const extractionEndpoint = this.url.getLLMURL(LLMEndpoints.EXTRACT);

    return this.http
      .postObject<LLMExtractionPayload, LLMExtractionResponse>(
        processedData,
        extractionEndpoint
      )
      .pipe(
        timeout(60000), // 60 second timeout
        tap((response: LLMExtractionResponse) => {
          console.log("Response received:", response);
          this.updateMetadataForFiles(response.extracted_properties.extractedData);
        }),
        catchError(this.handleExtractionError)
      );
  }

  private handleExtractionError(error: any) {
    console.error("An error occurred during extraction:", error);
    // You can add more specific error handling here if needed
    return throwError(() => new Error("Extraction failed. Please try again."));
  }    
}
