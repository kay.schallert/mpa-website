import { Injectable } from "@angular/core";
import { TableRowObject, TableRow } from "../model/metadata-columnData";
import {
  AuthService,
  HttpClientService,
  MultiFileUploadData,
  UploadProgressService,
  UserToken
} from "shared-lib";
import {
  Endpoints,
  WebserveraddressService
} from "./webserveraddress.service";
import { JobData, JobDataObject } from "../model/metadatauploadjson";
import { HttpEventType, HttpParams } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { BehaviorSubject } from "rxjs";
import { DownloadLinksJson } from "../model/download-json";
import { dummyData as data } from "../model/dummy-data";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class MetaDataService {
  allJobs: JobData[] = [];
  dummyData: JobData = data; // TODO: for testing purposes, remove later
  downloadUrls: string[] = [];
  zipUrl: string;

  uploadFinish: boolean;
  jobFinished: boolean;

  public currentJob = new BehaviorSubject<JobData>(new JobDataObject());

  constructor(
    private http: HttpClientService,
    private url: WebserveraddressService,
    private authService: AuthService
    
  ) {}
  
// TODO: Make Job and User work
  private fetchJobsFromServer(userID: UserToken): JobData[] {
    if (!userID) {
      console.info("User is not logged in");
      return [];
    }

    else {
      const params = new HttpParams().set("userID", userID.toString());
      
      let jobs: JobData[] = []; // Initialize an array to store the jobs

      this.http
        .getObject<JobData[]>(this.url.getURL(Endpoints.GET_USER_JOBS), params)
        .subscribe({
          next: (data: JobData[]) => {
            jobs = data;
            this.currentJob.next(data[0]);
          },
          error: (error) => {
            console.error("Failed to fetch jobs for user:", error);
          }
        });

      return jobs ? jobs : []; // Return the jobs array
    }
  }

  getDownloads(): string[] {
    return this.downloadUrls;
  }

  getZip(): string {
    return this.zipUrl;
  }

  updateJobs(newMetadata: { [key: string]: any }): void {
    try {
      const currentMetaData = this.currentJob.value;
      // Update metadataJson with the processingPipeline value from the model
      const updatedMetadataJson = currentMetaData.metadataForFiles.map(
        (data) => {
          return {
            ...data,
            processingPipeline: newMetadata.processingPipeline
          };
        }
      );
      // Update the metadataUploadJson with the new metadataJson array
      this.currentJob.next({
        ...currentMetaData,
        metadataForFiles: updatedMetadataJson
      });
    } catch (error) {}
  }

  uploadFiles(
    files: MultiFileUploadData,
    uploadProgress: UploadProgressService,
    dialog: MatDialog
  ): void {
    this.uploadFinish = false;
    this.jobFinished = false;
    this.http
      .postMultiPartFilesEvents(files, this.url.getURL(Endpoints.UPLOAD_FILES))
      .subscribe({
        next: (event) => {
          if (event.type === HttpEventType.UploadProgress) {
            uploadProgress.changeReportLoaded(event.loaded);
          } else if (event.type === HttpEventType.Response) {
            this.handleUploadResponse(event.body as JobData);
          } else {
            console.log("unknown event", event);
          }
        },
        error: (error) => this.handleUploadError(error, dialog)
      });
  }

  private handleUploadResponse(newJobData: JobData): void {
    this.uploadFinish = true;
    this.convertMetadataJsonMaps(newJobData.metadataForFiles);

    const params = new HttpParams({
      fromObject: { jobid: newJobData.createInitialMetadataJobId }
    });

    this.http
      .repeatedGetObject<JobData>(
        ["jobId"],
        this.url.getURL(Endpoints.GET_INITIAL_METADATA),
        params,
        2000
      )
      .subscribe((response: JobData) => {
        this.jobFinished = true;
        this.convertMetadataJsonMaps(response.metadataForFiles);
        this.currentJob.next(response);
      });
  }

  private convertMetadataJsonMaps(metadataJson: TableRow[]): void {
    metadataJson.forEach((col) => {
      col.ontId2Param = this.convertObjectToMap(col.ontId2Param);
      col.ontId2Enabled = this.convertObjectToMap(col.ontId2Enabled);
    });
  }

  private convertObjectToMap(obj: { [key: string]: any }): Map<string, any> {
    const map = new Map<string, any>();
    for (const key in obj) {
      map.set(key, obj[key]);
    }
    return map;
  }

  private handleUploadError(error: any, dialog: MatDialog): void {
    console.log(error);
    if (error.status >= 400) {
      const uploadDialog = dialog.getDialogById("UPLOAD");
      if (uploadDialog) {
        uploadDialog.componentInstance.setUploadFailed();
        uploadDialog.componentInstance.uploadFailedMessage = error.statusText;
      }
    } else {
      throw error;
    }
  }

  extractAndProcessText(text: string) {
    // Example logic for processing the entire text data
    const cleanedText = text.replace(/\n/g, " ").trim();
    const processedData = { processedText: cleanedText };

    // Assuming 'PROCESS_TEXT' is the endpoint where the server processes the text
    this.http
      .postObject<any, any>(
        processedData,
        this.url.getURL(Endpoints.PROCESS_TEXT)
      )
      .subscribe({
        next: (response) => {
          console.log("Text processed on server:", response);
        },
        error: (error) => {
          console.error("Failed to process text on server:", error);
        }
      });

    return new BehaviorSubject(processedData);
  }

  uploadJob(dataExport: JobData) {
    const uploadJson = this.prepareUploadJson(dataExport);

    this.currentJob.next(uploadJson);

    this.http
      .postObject<JobData, JobData>(
        this.currentJob.value,
        this.url.getURL(Endpoints.GET_USER_JOBS)
      )
      .subscribe((response) => {
        this.handlePostResponse(response);
      });
  }

  private prepareUploadJson(dataExport: JobData): JobData {
    const uploadJson = this.currentJob.getValue();
    uploadJson.metadataForFiles = dataExport.metadataForFiles;

    // this is to overcome javascript limitation on maps
    uploadJson.metadataForFiles.forEach((col) => {
      col.ontId2EnabledArray = Array.from(col.ontId2Enabled);
      col.ontIdParamArray = Array.from(col.ontId2Param);
    });

    return uploadJson;
  }

  private handlePostResponse(response: JobData): void {
    const json = this.currentJob.value;
    json.fileConversionJobId = response.fileConversionJobId;
    this.currentJob.next(json);

    this.http
      .repeatedPostObject<JobData, DownloadLinksJson>(
        response,
        ["jobID"],
        this.url.getURL(Endpoints.GET_DOWNLOAD_LINKS),
        new HttpParams()
      )
      .subscribe((response: DownloadLinksJson) => {
        this.processDownloadLinks(response);
      });
  }

  private processDownloadLinks(response: DownloadLinksJson): void {
    response.mzmlFileDownloads.forEach((url) => {
      this.downloadUrls.push(url);
    });
    response.mzidFileDownloads.forEach((url) => {
      this.downloadUrls.push(url);
    });
    this.downloadUrls.push(response.sdrfFileDownload);
    this.zipUrl = response.zipDownload;
  }
}
