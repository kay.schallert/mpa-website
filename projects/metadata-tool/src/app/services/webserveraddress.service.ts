import { Injectable } from '@angular/core';

export enum WebserverUrls {
  // TODO: rename variable. TEST is not a good name.
  TEST = 'http://localhost:9102/metaforge_server_test/service/',
  LLM = 'http://localhost:5000/',
  // DENBIPUBLIC = 'https://mdoa-tools.bi.denbi.de/metaforge_server_test/service/'
  }

export enum Endpoints {
  UPLOAD_FILES = 'uploadfiles',
  GET_INITIAL_METADATA = 'getmetadatajson',
  SUBMIT_METADATA = 'submitmetadata',
  GET_DOWNLOAD_LINKS = 'getdownload',
  GET_USER_JOBS = "GET_USER_JOBS",
  PROCESS_TEXT = "PROCESS_TEXT",
  GET_TEMPLATE = "gettemplate",

  // TODO: UPDATE BACKEND WITH THESE ENDPOINTS
  UPDATE_USER_DATA = 'mpauser/updateuserdata',
  GET_USER_DATA = 'mpauser/getuserdata',
  GENERAL_FILE_UPLOAD='mpauser/uploadfiles',
 
}

export enum LLMEndpoints {
  EXTRACT = 'extract',
}

@Injectable({
  providedIn: 'root',
})
export class WebserveraddressService {

  constructor() {
  }

  public getURL(endpoint: Endpoints): string {
    return WebserverUrls.TEST + endpoint;
    // return WebserverUrls.DENBIPUBLIC + endpoint;
  }

  public getLLMURL(endpoint: LLMEndpoints): string {
    return WebserverUrls.LLM + endpoint;
  }
}