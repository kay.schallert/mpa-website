import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BetterLoginPageComponent } from 'shared-lib';
import { LandingPageComponent } from './components/landing-page/landing-page.component';

const routes: Routes = [
  { path: 'login', component: BetterLoginPageComponent },
  { path: 'home', component: LandingPageComponent },
  {
    path: 'calculator',
    loadChildren: () =>
      import('./modules/calculator/calculator.module').then(
        (m) => m.CalculatorModule
      ),
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
