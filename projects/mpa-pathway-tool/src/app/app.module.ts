// Angular imports
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';

// shared lib imports
import { StandardPageLayoutModule } from 'shared-lib';

// Module imports
import { AppComponent } from './app.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';

@NgModule({ declarations: [AppComponent, LandingPageComponent],
    bootstrap: [AppComponent], imports: [BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        StandardPageLayoutModule,
        MatButtonModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class AppModule {}
