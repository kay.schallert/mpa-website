export interface Job {
  jobId: string;
  state: string;
  jobName?: string;
  details?: string;
  creationDate?: Date;
}
