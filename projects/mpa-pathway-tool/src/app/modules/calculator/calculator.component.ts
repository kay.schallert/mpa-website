import { Component, OnDestroy, OnInit } from '@angular/core';
import { CalculatorWorkflowService } from './services/calculator-workflow.service';
import { CustomStepperService, Step } from 'shared-lib';
import { Subscription } from 'rxjs';
import { CalculatorData } from './models/calculator-data.model';

export const workflowSteps: Step[] = [
  {
    index: 0,
    label: 'Data upload',
  },
  {
    index: 1,
    label: 'Pathway mapping',
  },
];

@Component({
  selector: 'mpapt-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent implements OnInit, OnDestroy {
  private Subscriptions: Subscription[];

  public workflowSteps = workflowSteps;

  public completedSteps$ = this.stepper.completedSteps$;
  public currentStepIndex$ = this.stepper?.currentIndex$;

  constructor(
    private workflow: CalculatorWorkflowService,
    private stepper: CustomStepperService
  ) {
    this.Subscriptions = [];
  }

  ngOnInit(): void {
    this.doSubscriptions();
    this.stepper.initialize(this.workflowSteps);
    this.workflow.createCalculatorJob();
    // this.workflow.setDummyConfig();
  }

  ngOnDestroy() {
    this.Subscriptions.forEach((sub) => sub.unsubscribe());
  }

  doSubscriptions() {
    this.Subscriptions = [
      this.workflow.calculatorData$.subscribe((data) => {
        console.log(data);
        this.updateStepper(data);
      }),
    ];
  }

  updateStepper(data: CalculatorData) {
    const completed = [true, true];

    this.stepper.completedSteps$.next(completed);

    for (let index = 0; index < completed.length; index++) {
      if (!completed[index]) {
        this.setStep(index);
        break;
      }
    }
  }

  setStep(selectedIndex: number) {
    this.stepper.setStep(selectedIndex);
  }
}
