import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { DropzoneCdkModule } from '@ngx-dropzone/cdk';
import { DropzoneMaterialModule } from '@ngx-dropzone/material';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { CalculatorComponent } from './calculator.component';
import { CalculatorRoutingModule } from './calculator-routing.module';
import { WorkflowPanelModule } from 'shared-lib';
import { StepperModule } from 'shared-lib';
import { MappingResultsComponent } from './components/mapping-results/mapping-results.component';
import { CalculatorUploadComponent } from './components/upload/calculator-upload.component';
import { CalculatorTableComponent } from './components/calculator-table/calculator-table.component';
import { DetailedResultsDialogContentComponent } from './components/detailed-results-dialog-content/detailed-results-dialog-content.component';

@NgModule({
  declarations: [
    CalculatorComponent,
    MappingResultsComponent,
    CalculatorUploadComponent,
    CalculatorTableComponent,
    DetailedResultsDialogContentComponent,
  ],
  imports: [
    CommonModule,
    CalculatorRoutingModule,
    WorkflowPanelModule,
    StepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    NgxMatFileInputModule,
    DropzoneCdkModule,
    DropzoneMaterialModule,
    MatIconModule,
    MatChipsModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
  ],
})
export class CalculatorModule {}
