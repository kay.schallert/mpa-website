import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorTableComponent } from './calculator-table.component';

describe('CalculatorTableComponent', () => {
  let component: CalculatorTableComponent;
  let fixture: ComponentFixture<CalculatorTableComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CalculatorTableComponent]
    });
    fixture = TestBed.createComponent(CalculatorTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
