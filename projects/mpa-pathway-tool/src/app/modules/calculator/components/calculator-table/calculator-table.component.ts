import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {
  Module,
  taxonomies,
  TaxonomyCounts,
} from '../../models/calculator-response-data.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CalculatorWorkflowService } from '../../services/calculator-workflow.service';
import { BehaviorSubject, filter, Subscription } from 'rxjs';
import { CalculatorData } from '../../models/calculator-data.model';

/**
 * Component for displaying the calculator results.
 *
 * Input data are nested, i.e., one module contains multiple entries for different taxonomies.
 * For the table, input data are transformed to a "flat" array of objects that can represent
 * modules or taxonomies part of a module.
 *
 */
@Component({
  selector: 'mpapt-calculator-table',
  templateUrl: './calculator-table.component.html',
  styleUrls: ['./calculator-table.component.scss'],
})
export class CalculatorTableComponent
  implements OnInit, AfterViewInit, OnDestroy
{
  // column names that are not dynamic and always displayed
  originalDisplayedColumns: string[] = [
    'module',
    'identifiedReactions',
    'totalReactions',
  ];

  // array required for dynamic displaying of taxonomies
  // taxon included --> taxon not displayed
  nonDisplayedTaxa: string[] = taxonomies.slice(2, 8);

  sampleNames: string[] = [];

  // displayed columns depend on input data
  displayedColumns: string[] = [];

  // column names for which filtering is performed for
  filterColumns: string[] = [];

  // used for dynamic dysplaying of rows
  expandedModules: string[] = [];

  // data source is of type any, because sample names are dynamic
  dataSource = new MatTableDataSource<any>();

  Subscriptions: Subscription[] = [];

  @Input() calculatorData: BehaviorSubject<CalculatorData>;
  @Input() showDetailed: boolean = false;
  @Output() showDetailedResultsEmitter = new EventEmitter<string>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(): void {
    // flat table data requires custom sorting which puts rows representing modules at first place
    this.dataSource.sortData = this.customSort;

    this.Subscriptions.push(
      this.calculatorData
        .pipe(filter((data) => data?.responseData?.modules !== undefined))
        .subscribe((data) => {
          this.filterColumns = this.originalDisplayedColumns.slice();

          if (this.showDetailed) {
            // minimizing chance for duplicate column names by adding prefix for samples
            this.sampleNames = data.responseData.sampleNames.map(
              (name) => 'sample ' + name
            );

            this.filterColumns = [
              ...this.originalDisplayedColumns.slice(0, 1),
              ...taxonomies.slice().reverse(),
              ...this.originalDisplayedColumns.slice(1),
            ];

            this.displayedColumns.push('expand');
          }

          // flatten input data
          this.dataSource.data = this.flattenCalculatorTableData(
            data.responseData.modules
          );

          this.displayedColumns.push(
            ...this.filterColumns,
            ...this.sampleNames
          );
        })
    );

    // defines columns that are used for filtering
    this.dataSource.filterPredicate = this.setFilterPredicate();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.Subscriptions.forEach((sub) => sub.unsubscribe());
  }

  flattenCalculatorTableData(data: Module[]): any[] {
    const tableData = [];

    for (let module of data) {
      tableData.push(this.createTableObject(module));

      if (!this.showDetailed) {
        continue;
      }

      for (let taxonomy of module.taxonomyCounts) {
        tableData.push(this.createTableObject(module, taxonomy));
      }
    }

    console.log(tableData);

    return tableData;
  }

  createTableObject(
    moduleObject: Module,
    taxonomyObject?: TaxonomyCounts
  ): any {
    const tableDataObject = {};
    const object = taxonomyObject ? taxonomyObject : moduleObject;

    tableDataObject['module'] = moduleObject.name;
    tableDataObject['identifiedReactions'] = object.identifiedReactions;
    tableDataObject['totalReactions'] = moduleObject.totalReactions;

    for (let i = 0; i < this.sampleNames.length; i++) {
      tableDataObject[this.sampleNames[i]] = object.spectralCounts[i];
    }

    if (taxonomyObject) {
      tableDataObject['isModule'] = false;

      for (let [key, value] of Object.entries(taxonomyObject.taxonomy)) {
        tableDataObject[key] = value;
      }

      return tableDataObject;
    }

    tableDataObject['isModule'] = true;

    return tableDataObject;
  }

  customSort(data: any[], matSort: MatSort): any[] {
    if (!matSort.active || matSort.direction === '') {
      return data;
    }

    let sorted = data.sort((a, b) => {
      // sort entries into modules
      if (a.module != b.module) {
        if (matSort.active === 'module' && matSort.direction === 'desc') {
          return b.module.localeCompare(a.module);
        }
        return a.module.localeCompare(b.module);
      }

      // sort module rows to the top
      if (a.isModule && !b.isModule) {
        return -1;
      } else if (!a.isModule && b.isModule) {
        return 1;
      }

      // ignore if property is not present
      if (a[matSort.active] === undefined || b[matSort.active] === undefined) {
        return 0;
      }

      // sort string
      if (typeof a[matSort.active] === 'string') {
        if (matSort.direction === 'asc') {
          return a[matSort.active].localeCompare(b[matSort.active]);
        }
        return b[matSort.active].localeCompare(a[matSort.active]);
      }

      // sort number
      if (matSort.direction === 'asc') {
        return a[matSort.active] - b[matSort.active];
      }
      return b[matSort.active] - a[matSort.active];
    });

    return sorted;
  }

  setFilterPredicate(): (data: any, filter: string) => boolean {
    return (data, filter) => {
      const filterColumns = this.filterColumns.filter(
        (col) => !this.nonDisplayedTaxa.includes(col)
      );

      let found = false;

      for (let col of filterColumns) {
        if (data[col]?.toString().toLowerCase().includes(filter)) {
          found = true;
          break;
        }
      }

      return found;
    };
  }

  isHidden(column: string): boolean {
    return this.nonDisplayedTaxa.includes(column);
  }

  showRanks() {
    if (this.nonDisplayedTaxa.length === 0) {
      this.nonDisplayedTaxa = taxonomies.slice(2, 8);
      return;
    }
    this.nonDisplayedTaxa = [];
  }

  expandModule(row): void {
    if (row.isModule) {
      if (this.expandedModules.includes(row.module)) {
        this.expandedModules = this.expandedModules.filter((module) => {
          return module !== row.module;
        });
      } else {
        this.expandedModules.push(row.module);
      }
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  showDetailedResults() {
    this.showDetailedResultsEmitter.emit('show details');
  }
}
