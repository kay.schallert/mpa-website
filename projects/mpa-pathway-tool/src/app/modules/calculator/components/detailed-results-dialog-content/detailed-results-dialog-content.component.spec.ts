import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedResultsDialogContentComponent } from './detailed-results-dialog-content.component';

describe('DetailedResultsDialogContentComponent', () => {
  let component: DetailedResultsDialogContentComponent;
  let fixture: ComponentFixture<DetailedResultsDialogContentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailedResultsDialogContentComponent]
    });
    fixture = TestBed.createComponent(DetailedResultsDialogContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
