import { Component, Inject, OnInit } from '@angular/core';
import { CalculatorWorkflowService } from '../../services/calculator-workflow.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { CalculatorData } from '../../models/calculator-data.model';

@Component({
  selector: 'mpapt-detailed-results-dialog-content',
  templateUrl: './detailed-results-dialog-content.component.html',
  styleUrls: ['./detailed-results-dialog-content.component.scss'],
})
export class DetailedResultsDialogContentComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: BehaviorSubject<CalculatorData>
  ) {}

  ngOnInit(): void {}
}
