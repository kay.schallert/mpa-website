import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingResultsComponent } from './mapping-results.component';

describe('MappingResultsComponent', () => {
  let component: MappingResultsComponent;
  let fixture: ComponentFixture<MappingResultsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MappingResultsComponent]
    });
    fixture = TestBed.createComponent(MappingResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
