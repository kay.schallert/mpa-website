import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DetailedResultsDialogContentComponent } from '../detailed-results-dialog-content/detailed-results-dialog-content.component';
import { CalculatorWorkflowService } from '../../services/calculator-workflow.service';
import { BehaviorSubject, filter, Subscription } from 'rxjs';
import { CalculatorData } from '../../models/calculator-data.model';

@Component({
  selector: 'mpapt-mapping-results',
  templateUrl: './mapping-results.component.html',
  styleUrls: ['./mapping-results.component.scss'],
})
export class MappingResultsComponent {
  public calculatorData$ = this.calculatorWorkflow.calculatorData$.pipe(
    filter((data) => data?.responseData?.modules !== undefined)
  ) as BehaviorSubject<CalculatorData>;

  constructor(
    private calculatorWorkflow: CalculatorWorkflowService,
    public dialog: MatDialog
  ) {}

  showDetailedResults($event) {
    const dialogRef = this.dialog.open(DetailedResultsDialogContentComponent, {
      data: this.calculatorData$,
    });
  }
}
