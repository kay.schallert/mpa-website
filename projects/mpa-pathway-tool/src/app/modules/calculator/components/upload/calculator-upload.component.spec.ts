import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorUploadComponent } from './calculator-upload.component';

describe('CalculatorUploadComponent', () => {
  let component: CalculatorUploadComponent;
  let fixture: ComponentFixture<CalculatorUploadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CalculatorUploadComponent],
    });
    fixture = TestBed.createComponent(CalculatorUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
