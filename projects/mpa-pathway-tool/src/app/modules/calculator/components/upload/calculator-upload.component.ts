import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { InputFormComponent } from 'shared-lib';
import { CalculatorWorkflowService } from '../../services/calculator-workflow.service';
import { CalculatorInputFilesForm } from '../../models/calculator-config-data.model';
import { FileInputValue } from '@ngx-dropzone/cdk/public-api';

@Component({
  selector: 'mpapt-calculator-upload',
  templateUrl: './calculator-upload.component.html',
  styleUrls: ['./calculator-upload.component.scss'],
})
export class CalculatorUploadComponent
  extends InputFormComponent
  implements OnInit, OnDestroy
{
  formModel: CalculatorInputFilesForm;

  constructor(
    public builder: FormBuilder,
    private workflow: CalculatorWorkflowService
  ) {
    super(builder);
  }

  ngOnInit(): void {
    this.formModel = this.buildForm();
    this.doSubscriptions();
  }

  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

  //  Subscriptions
  doSubscriptions() {
    // updates Validators if form value changed, necessary to check for duplicate names (see arrayDuplicateValidator)
    // this.subscriptions.push(
    //   this.groups.valueChanges.subscribe(() => {
    //     CustomValidators.updateValidators(this.groups);
    //   })
    // );
    // form depends on values in ofsdata - react to changes
    // this.subscriptions.push(
    //   this.workflow.ofsData$.subscribe((ofsData) => {
    //     this.setExistingFormInput(ofsData);
    //   })
    // );
  }

  //  Methods
  buildForm(): CalculatorInputFilesForm {
    let dataFile: File | null;

    const dataFileControl = new FormControl(dataFile, {
      updateOn: 'change',
      validators: Validators.required,
    });

    const pathwayFilesControl = new FormControl<FileInputValue>(null, {
      updateOn: 'change',
      validators: Validators.required,
    });

    const formModel = this.builder.group(
      {
        experimentalData: dataFileControl,
        pathwayFiles: pathwayFilesControl,
      },
      {
        updateOn: 'blur',
      }
    ) as CalculatorInputFilesForm;

    return formModel;
  }

  get files() {
    const _files = this.formModel.controls.pathwayFiles.value;

    if (!_files) {
      return [];
    }

    return Array.isArray(_files) ? _files : [_files];
  }

  remove(file: File) {
    if (Array.isArray(this.formModel.controls.pathwayFiles.value)) {
      this.formModel.controls.pathwayFiles.setValue(
        this.formModel.controls.pathwayFiles.value.filter((i) => i !== file)
      );
      return;
    }

    this.formModel.controls.pathwayFiles.setValue(null);
  }

  submitCalculatorConfig(): void {
    this.workflow.submitCalculatorInput(this.formModel.getRawValue());
  }
}
