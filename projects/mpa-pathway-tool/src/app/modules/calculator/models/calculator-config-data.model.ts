import { FormControl, FormGroup } from '@angular/forms';

export interface CalculatorConfigData {
  mapperJobId: string;
  experimentalDataId: string;
  pathwayFileIds: Map<string, string>;
}

export class CalculatorConfigData implements CalculatorConfigData {
  constructor() {}

  public static fromJson(json: CalculatorConfigData) {
    const configData = new CalculatorConfigData();

    configData.mapperJobId = json.mapperJobId;
    configData.experimentalDataId = json.experimentalDataId;
    configData.pathwayFileIds = new Map<string, string>();

    for (const [pathwayName, pathwayId] of Object.entries(
      json.pathwayFileIds
    )) {
      configData.pathwayFileIds.set(pathwayName, pathwayId);
    }

    return configData;
  }
}

export interface CalculatorInputFiles {
  experimentalData: File;
  pathwayFiles: File[];
}

export class CalculatorInputFiles implements CalculatorInputFiles {
  constructor() {}
}

export interface CalculatorInputFilesForm extends FormGroup {
  value: CalculatorInputFiles;

  controls: {
    experimentalData: FormControl;
    pathwayFiles: FormControl;
  };
}
