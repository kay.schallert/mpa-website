import { CalculatorJob, CalculatorJobState } from './calculator-job.model';
import { CalculatorConfigData } from './calculator-config-data.model';
import { CalculatorResponseData } from './calculator-response-data.model';
import { from } from 'rxjs';

export interface CalculatorData {
  job: CalculatorJob;
  configData: CalculatorConfigData;
  responseData: CalculatorResponseData;
}

export class CalculatorData implements CalculatorData {
  constructor() {
    this.job = new CalculatorJob();
    this.configData = new CalculatorConfigData();
    this.responseData = new CalculatorResponseData();
  }

  public static fromJson(json: CalculatorData) {
    const calculatorData = new CalculatorData();

    calculatorData.job = json.job;
    calculatorData.configData = CalculatorConfigData.fromJson(json.configData);
    calculatorData.responseData = json.responseData;

    return calculatorData;
  }
}
