import { Job } from '../../../models/job.model';

export enum CalculatorJobState {
  NOJOB = 'nojob',
  CREATED = 'created',
  CREATIONFAIL = 'creationfail',
  UPLOAD = 'data uploaded',
  UPLOADFAIL = 'upload failed',
  MAPPING = 'mapping',
  MAPPINGFAIL = 'mapping failed',
}

export interface CalculatorJob extends Job {
  state: CalculatorJobState;
}

export class CalculatorJob implements CalculatorJob {
  constructor(job = '', state = CalculatorJobState.NOJOB) {
    this.jobId = job;
    this.state = state;
  }
}
