export interface CalculatorResponseData {
  modules: Module[];
  sampleNames: string[];
}

export class CalculatorResponseData implements CalculatorResponseData {
  constructor() {
    this.modules = [];
    this.sampleNames = [];
  }
}

export interface Module {
  name: string;
  identifiedReactions: number;
  totalReactions: number;
  spectralCounts: number[];

  taxonomyCounts: TaxonomyCounts[];
}

export const taxonomies = [
  'species',
  'genus',
  'family',
  'order',
  'classT',
  'phylum',
  'kingdom',
  'superkingdom',
];

export interface Taxonomy {
  species: string;
  genus: string;
  family: string;
  order: string;
  classT: string;
  phylum: string;
  kingdom: string;
  superkingdom: string;
}

export interface TaxonomyCounts {
  taxonomy: Taxonomy;
  identifiedReactions: number;
  spectralCounts: number[];
}
