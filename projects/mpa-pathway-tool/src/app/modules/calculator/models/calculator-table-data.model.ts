import { Taxonomy } from './calculator-response-data.model';

export interface CalculatorTableData {
  isModule: boolean;
  name: string;
  identifiedReactions: number;
  totalReactions: number;
  spectralCounts: number[];
  taxonomy?: Taxonomy;
}
