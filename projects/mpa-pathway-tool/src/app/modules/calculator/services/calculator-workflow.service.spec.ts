import { TestBed } from '@angular/core/testing';

import { CalculatorWorkflowService } from './calculator-workflow.service';

describe('CalculatorWorkflowService', () => {
  let service: CalculatorWorkflowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculatorWorkflowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
