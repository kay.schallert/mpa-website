import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  catchError,
  concatMap,
  delay,
  finalize,
  mergeMap,
  of,
  tap,
} from 'rxjs';
import { MultiFileUploadData } from 'shared-lib';
import { HttpClientService } from 'shared-lib';
import {
  WebserveraddressService,
  Endpoints,
  MpaptModules,
} from '../../../services/mpaptwebserveraddress.service';
import { CalculatorData } from '../models/calculator-data.model';
import {
  CalculatorJob,
  CalculatorJobState,
} from '../models/calculator-job.model';
import {
  CalculatorConfigData,
  CalculatorInputFiles,
} from '../models/calculator-config-data.model';
import { dummy } from '../../../../assets/dummy-data/dummy-data';
// import { JobOverviewService } from '../../job-overview/services/job-overview.service';

export interface SimpleMessage {
  message: string;
}

@Injectable({
  providedIn: 'any',
})
export class CalculatorWorkflowService {
  private module = MpaptModules.CALCULATOR;

  public calculatorData$ = new BehaviorSubject<CalculatorData>(undefined);

  constructor(
    private http: HttpClientService,
    private address: WebserveraddressService
  ) {}

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      // console.error(error);
      return of(result as T);
    };
  }

  setDummyConfig() {
    const existingConfig = dummy as unknown as CalculatorData;
    this.calculatorData$.next(existingConfig);
  }

  createCalculatorJob(): void {
    /**
     * Requests new job from server
     */

    // TODO: get job id from job service, retrieve job from server for that id
    // this.loading = true;
    this.http
      .getObject<CalculatorData>(
        this.address.getEndpoint(this.module, Endpoints.CREATE_JOB),
        new HttpParams()
      )
      .pipe(
        // logs error and returns exchange object
        catchError(
          this.handleError('createCalculatorJob', this.calculatorData$.value)
        ),
        // executes when observable completes
        finalize(() => {})
      )
      .subscribe((response: CalculatorData) => {
        this.calculatorData$.next(CalculatorData.fromJson(response));
      });
  }

  submitCalculatorInput(inputFiles: CalculatorInputFiles): void {
    const filesForRequest = [];

    const calculatorData = this.calculatorData$.value;

    for (let pathwayFile of inputFiles.pathwayFiles) {
      filesForRequest.push({
        uploadFile: pathwayFile as File,
        fileID: pathwayFile.name,
      });

      calculatorData.configData.pathwayFileIds.set(pathwayFile.name, '');
    }

    // update calculator data with new pathway filenames
    this.calculatorData$.next(calculatorData);

    const configFile = new File(
      [JSON.stringify(this.calculatorData$.value)],
      'calculatorJSON'
    );

    filesForRequest.push(
      {
        uploadFile: configFile,
        fileID: 'inputJSON',
      },
      {
        uploadFile: inputFiles.experimentalData as File,
        fileID: 'experimentalData',
      }
    );

    const filesToUpload: MultiFileUploadData = {
      files: filesForRequest,
      httpParameters: new HttpParams(),
    };

    this.http
      .postMultiPartFiles(
        filesToUpload,
        this.address.getEndpoint(this.module, Endpoints.MAPPING)
      )
      .pipe(
        // side effect: server responds with new CalculatorData (updated job status) --> update service state
        tap((response: CalculatorData) => {
          this.calculatorData$.next(CalculatorData.fromJson(response));
        }),
        // new observable: map previous response to new request; concatMap waits for the previous observable to complete
        concatMap((response: CalculatorData, index: number) => {
          return this.http.repeatedPostObject<CalculatorData, CalculatorData>(
            response,
            ['responseData', 'modules'],
            this.address.getEndpoint(this.module, Endpoints.MAPPER_RESULTS),
            new HttpParams()
          );
        }),
        catchError(
          this.handleError('submitCalculatorInput', this.calculatorData$.value)
        )
      )
      .subscribe((response: CalculatorData) => {
        this.calculatorData$.next(CalculatorData.fromJson(response));
      });
  }
}
