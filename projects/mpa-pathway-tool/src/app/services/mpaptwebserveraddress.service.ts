import { Injectable } from '@angular/core';

export enum WebserverUrls {
  LOCALHOST = 'http://localhost:10002/',
  MDOASERVER = 'http://mdoa.isas.de:80/test/ofs/',
  DENBIPUBLIC = 'https://mdoa-tools.bi.denbi.de/off_server_test/ofs/',
}

export enum MpaptModules {
  CALCULATOR = 'calculator/',
  CREATOR = 'creator/',
  SIMULATOR = 'simulator/',
  VISUALIZER = 'visualizer/',
}

export enum Endpoints {
  CREATE_JOB = 'createjob',
  GET_JOBS = 'getjobs',
  MAPPING = 'mapping',
  MAPPER_RESULTS = 'mapperresults',
}

@Injectable({
  providedIn: 'root',
})
export class WebserveraddressService {
  system: WebserverUrls = WebserverUrls.LOCALHOST;

  constructor() {}

  public getEndpoint(module: MpaptModules, endpoint: Endpoints): string {
    return this.system + module + endpoint;
  }

  public getEndpointWithQueryParams(
    module: MpaptModules,
    endpoint: Endpoints,
    queryParams: Map<string, string>
  ): string {
    let address: string = this.getEndpoint(module, endpoint) + '?';
    queryParams.forEach((value, key) => {
      address += key + '=' + value + '&';
    });
    return address;
  }
}
