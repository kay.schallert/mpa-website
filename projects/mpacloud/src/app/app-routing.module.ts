import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, BetterLoginPageComponent } from 'shared-lib';

const routes: Routes = [
  { path: 'login', component: BetterLoginPageComponent },
  {
    path: 'mpa',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/mpa/mpa.module').then((m) => m.MpaModule),
  },
  {
    path: 'termsofservice',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/terms-of-service/terms-of-service.module').then((m) => m.TermsOfServiceModule),
  },
  {
    path: 'privacypolicy',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/privacy-policy/privacy-policy.module').then((m) => m.PrivacyPolicyModule),
  },
  {
    path: 'impressum',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/impressum/impressum.module').then((m) => m.ImpressumModule),
  },
  {
    path: 'overview',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'tutorial',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/tutorial/tutorial.module').then((m) => m.TutorialModule),
  },
  {
    path: 'team',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/team/team.module').then((m) => m.TeamModule),
  },
  {
    path: 'version-history',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/version-history/version-history.module').then((m) => m.VersionHistoryModule),
  },
  {
    path: 'downloads',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/downloads/downloads.module').then((m) => m.DownloadsModule),
  },
  // {
  //   path: 'documentation',
  //   // canActivate: [AuthGuard],
  //   loadChildren: () => import('./modules/documentation/documentation.module').then((m) => m.DocumentationModule),
  // },
  {
    path: 'publications',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/publications/publication.module').then((m) => m.PublicationModule),
  },
  {
    path: 'home',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/landing-page/mpa-landing-page.module').then((m) => m.MPALandingPageModule),
  },
  {
    path: 'error',
    // canActivate: [AuthGuard],
    loadChildren: () => import('./modules/error-page-component/error-page.module').then((m) => m.ErrorPageModule),
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
