import { Component, OnInit } from '@angular/core';
import { NestedNavigationRoute, SimpleNavigationRoute, Logo, AuthService } from 'shared-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'MPA';
  version = '1.0.1';
  copyRight = '2025 de.NBI';
  routes: NestedNavigationRoute[] = [

    { label: 'Getting Started', requireAuth: false,
      children:[
        // { route: '/home', label: 'Home', requireAuth: false },
        { route: '/overview', label: 'Overview', requireAuth: false },
        { route: '/tutorial', label: 'Tutorial', requireAuth: false },
        { route: '/downloads', label: 'Example Data', requireAuth: false },

        // { route: '/documentation', label: 'Documentation', requireAuth: false },
      ]},
      { route: '/mpa', label: 'Workflow', requireAuth: false },
      { label: 'About', requireAuth: false,
        children:[
          { route: '/team', label: 'Team', requireAuth: false },
          { route: '/publications', label: 'Publications', requireAuth: false },
          { route: '/version-history', label: 'Version History', requireAuth: false },
        ]}
  ];

  homelink: SimpleNavigationRoute = {
    route: '/home',
    label: 'MPA',
    requireAuth: false,
  };

  footerContent: NestedNavigationRoute[] = [
    {
      label: 'Contact Us',
      children: [
        { label: 'support@mdoa-tools.bi.denbi.de', href: 'mailto:support@mdoa-tools.bi.denbi.de'},
      ],
    },
    {
      label: 'About',
      children: [
        { label: 'support@mdoa-tools.bi.denbi.de'},
        { label: 'About MPA', route: 'about' },
        { label: 'Terms of Service', route: 'termsofservice' },
        { label: 'Privacy Policy', route: 'privacypolicy' },
        { label: 'Impressum', route: 'impressum' },
      ],
    },
    {
      label: 'Funding & Support',
      children: [

        { label: 'DFG', href: 'http://www.dfg.de' },
        { label: 'de.NBI', href: 'http://www.denbi.de' },
        { label: 'de.NBI Cloud', href: 'https://www.denbi.de/cloud' },
      ],
    },
  ];

  footerLogos: Logo[] = [
    {
      label: 'leibniz',
      assetPath: 'assets/standard-logos/leibniz_white.png',
      href: 'https://www.leibniz-gemeinschaft.de',
    },
    {
      label: 'ISAS',
      assetPath: 'assets/standard-logos/isaslogooffizielleformrgbweiss.png',
      href: 'https://www.isas.de',
    },
    {
      label: 'nrw',
      assetPath: 'assets/standard-logos/nrw_black.svg',
      href: 'https://www.land.nrw',
      backgroundColor: 'white',
    },
    {
      label: 'bmbf',
      assetPath: 'assets/standard-logos/bmbf.svg',
      href: 'https://www.bmbf.de',
      backgroundColor: 'white',
    },
    {
      label: 'denbicloud',
      assetPath: 'assets/denbi-cloud.png',
      href: 'https://www.cloud.denbi.de',
      backgroundColor: 'black',
    },
    {
      label: 'denbi',
      assetPath: 'assets/Denbi.svg',
      href: 'https://www.denbi.de',
      backgroundColor: 'white',
    },
    {
      label: 'ELIXIR Germany',
      assetPath: 'assets/ELIXIR.png',
      href: 'https://elixir-europe.org/about-us/who-we-are/nodes/germany',
      backgroundColor: 'black',
    },
  ];

  showLogo: boolean = true;

  toolLogo: Logo = {
    label: 'MPA Cloud',
    assetPath: 'assets/img/MPA.webp',
  };

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.initializeOAuth();
  }

}
