import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OAuthModule } from 'angular-oauth2-oidc';
import { StandardPageLayoutModule } from 'shared-lib';
import { GlobalHttpInterceptorService } from './global-http-interceptor.service';
import { NgParticlesModule } from 'ng-particles';
import { MPALandingPageModule } from './modules/landing-page/mpa-landing-page.module';
import { provideMatomo, withRouter } from 'ngx-matomo-client';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    OAuthModule.forRoot(),
    AppRoutingModule,
    StandardPageLayoutModule,
    NgParticlesModule,
    MPALandingPageModule
  ],
  providers: [
    provideMatomo(
      {
        trackerUrl: 'https://piwik.cebitec.uni-bielefeld.de/',
        siteId: '29'
      },
      withRouter() // Enables route tracking
    ),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalHttpInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
