import { TestBed } from '@angular/core/testing';

import { EnumToTextService } from './enum-to-text.service';

describe('EnumToTextService', () => {
  let service: EnumToTextService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnumToTextService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
