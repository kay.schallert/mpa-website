import { Injectable } from '@angular/core';
import { ExperimentStatus, SearchStatus } from './modules/mpa/model/user-data-json';

@Injectable({
  providedIn: 'root'
})
export class EnumToTextService {

  constructor() { }

  search_ttext(Status: SearchStatus): string {
    switch (Status) {
      case 'SEARCH_REQUESTED':
        return 'Search has been requested. Awaiting processing.';
      case 'SEARCH_QUEUED':
        return 'Search is queued and will start soon.';
      case 'SEARCH':
        return 'Search is running...';
      case 'SEARCH_FINISHED':
        return 'Search completed successfully!';
      case 'SEARCH_FAILED':
        return 'Search has failed. Please contact support.';
      default:
        return 'Unknown search status.';
    }
  }

  // For experiments
  experiment_ttext(Status: ExperimentStatus): string {
    switch (Status) {
      case 'REQUEST_CREATION':
        return 'Experiment creation requested...';
      case 'REQUEST_CREATION_BATCH':
        return 'Batch experiment creation requested...';
      case 'CREATED':
        return 'Experiment has been created.';
      case 'WAITING_FOR_UPLOAD':
        return 'Waiting for data/files to be uploaded.';
      case 'FILES_UPLOADED':
        return 'All required files have been uploaded.';
      case 'PEAKLIST_PROCESSING':
        return 'Peak list is being processed...';
      case 'SEARCH_QUEUED':
        return 'Experiment search is queued and will start soon.';
      case 'SEARCH':
        return 'Experiment search is in progress...';
      case 'RESULT_PROCESSING':
        return 'Processing the results of the search...';
      case 'RESULTS_AVAILABLE':
        return 'Results are available.';
      case 'RESULTS_AVAILABLE_AND_LOCKED':
        return 'Results are available and are locked for changes.';
      case 'DELETE':
        return 'Experiment is scheduled for deletion.';
      case 'DELETED':
        return 'Experiment has been deleted.';
      case 'ERROR_STATE':
        return 'An error occurred with this experiment.';
      default:
        return 'Unknown experiment status.';
    }
  }
}