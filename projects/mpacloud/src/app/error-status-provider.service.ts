import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorStatusProviderService {
  private errorCode: number | undefined;
  private errorDetails: { errorCode: number; errorMessage: string; additionalInfo?: string | null } | null = null;

  constructor() {}

  // Methods for Error Code
  setErrorCode(code: number): void {
    this.errorCode = code;
  }

  resetErrorCode(): void {
    this.errorCode = undefined;
  }

  getErrorCode(): number | undefined {
    return this.errorCode;
  }

  // Methods for Error Details
  setErrorDetails(details: { errorCode: number; errorMessage: string; additionalInfo?: string | null }): void {
    this.errorDetails = details;
  }

  getErrorDetails(): { errorCode: number; errorMessage: string; additionalInfo?: string | null } | null {
    return this.errorDetails;
  }

  resetErrorDetails(): void {
    this.errorDetails = null;
  }
}
