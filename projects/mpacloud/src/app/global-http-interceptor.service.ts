import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'shared-lib';
import { ErrorStatusProviderService } from './error-status-provider.service';

@Injectable({
  providedIn: 'root',
})
export class GlobalHttpInterceptorService implements HttpInterceptor {
  constructor(
    public router: Router,
    private authGuard: AuthService,
    private errorStatusProvider: ErrorStatusProviderService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const started = Date.now();
    // Log the request
    // console.log('Outgoing request', req);
    // Handle the request and catch errors
    return next.handle(req).pipe(
      tap((event) => {
        // Log the response if it's an HttpResponse
        if (event instanceof HttpResponse) {
          const elapsed = Date.now() - started;
          // console.log(`Response status: ${event.status}`);
          // console.log(`Request duration: ${elapsed} ms`);
        }
      }),
      catchError((err: HttpErrorResponse) => {
        const elapsed = Date.now() - started;
        
        console.log(`Request duration: ${elapsed} ms`);
        console.error('HTTP Error occurred:', err);
        console.log(`Error Status: ${err.status}`);
        console.log(`Error Status Text: ${err.statusText}`);

        switch (err.status) {
          case 0: // Network or CORS error
            this.authGuard.logout();
            this.router.navigateByUrl('/login');
            break;
        
          case 401: // Unauthorized - logout and redirect to login
            this.authGuard.logout();
            this.router.navigateByUrl('/login');
            break;
        
          case 403: // Forbidden - logout and redirect to login
            console.log('403 - Forbidden');
            this.authGuard.logout();
            this.router.navigateByUrl('/login');
            break;
        
          case 400: // Bad request
            console.error('400 - Bad request');
            this.errorStatusProvider.setErrorDetails({
              errorCode: err.status,
              errorMessage: err.statusText || 'Bad request',
              // additionalInfo: `URL: ${err.url}`,
            });
            this.router.navigate(['/error']);
            break;
        
          case 404: // Resource not found
            console.error('404 - Resource not found');
            this.errorStatusProvider.setErrorDetails({
              errorCode: err.status,
              errorMessage: err.statusText || 'Resource not found',
              // additionalInfo: `URL: ${err.url}`,
            });
            // this.router.navigate(['/error']);
            break;
        
          case 500: // Internal server error
            console.error('500 - Internal server error');
            // this.errorStatusProvider.setErrorDetails({
            //   errorCode: err.status,
            //   errorMessage: err.statusText || 'Internal server error',
            //   // additionalInfo: `URL: ${err.url}`,
            // });
            // this.router.navigate(['/error']);
            break;
        
          case 501: // Not implemented
            console.error('501 - Not implemented');
            this.errorStatusProvider.setErrorDetails({
              errorCode: err.status,
              errorMessage: err.statusText || 'Not implemented',
              // additionalInfo: `URL: ${err.url}`,
            });
            this.router.navigate(['/error']);
            break;
        
          case 502: // Bad gateway
            console.error('502 - Bad gateway');
            this.errorStatusProvider.setErrorDetails({
              errorCode: err.status,
              errorMessage: err.statusText || 'Bad gateway',
              // additionalInfo: `URL: ${err.url}`,
            });
            this.router.navigate(['/error']);
            break;
        
          case 503: // Service unavailable
            console.error('503 - Service unavailable');
            this.errorStatusProvider.setErrorDetails({
              errorCode: err.status,
              errorMessage: err.statusText || 'Service unavailable',
              // additionalInfo: `URL: ${err.url}`,
            });
            this.router.navigate(['/error']);
            break;
        
          case 504: // Gateway timeout
            console.error('504 - Gateway timeout');
            this.errorStatusProvider.setErrorDetails({
              errorCode: err.status,
              errorMessage: err.statusText || 'Gateway timeout',
              // additionalInfo: `URL: ${err.url}`,
            });
            this.router.navigate(['/error']);
            break;
        
          default: // Other errors - redirect to error page
            console.error(`Unhandled error: ${err.status}`);
            this.errorStatusProvider.setErrorDetails({
              errorCode: err.status,
              errorMessage: err.statusText || 'An unknown error occurred',
              // additionalInfo: `URL: ${err.url}`,
            });
            this.router.navigate(['/error']);
            break;
        }
        

        // Pass the error to the caller
        return throwError(() => err);
      })
    );
  }


  // intercept(
  //   req: HttpRequest<any>,
  //   next: HttpHandler
  // ): Observable<HttpEvent<any>> {
  //   return next.handle(req).pipe(
  //     catchError((error) => {
  //       console.log("catch error");
  //       console.log(error);
  //       if (error instanceof HttpErrorResponse) {
  //         console.log("catch HttpErrorResponse");
  //         this.errorStatusProvider.resetErrorCode();
  //         // if (error.error instanceof ErrorEvent) {
  //           console.error('Error Event');
  //         // } else {
  //           this.errorStatusProvider.setErrorCode(error.status);
  //           console.log(`error status : ${error.status} ${error.statusText}`);
  //           switch (error.status) {
  //             case 401: // login
  //               this.authGuard.logout();
  //               this.router.navigateByUrl('/login');
  //               break;
  //             case 403: // forbidden
  //               console.log("403");
  //               this.authGuard.logout();
  //               this.router.navigateByUrl('/login');
  //               break;
  //             // case 400:
  //             //   console.error('Bad request');
  //             //   break;
  //             // case 404:
  //             //   console.error('Resource not found');
  //             //   break;
  //             // case 500:
  //             //   console.error('internal server error');
  //             //   break;
  //             // case 501:
  //             //   console.error('not implemented');
  //             //   break;
  //             // case 502:
  //             //   console.error('Bad gateway');
  //             //   break;
  //             // case 503:
  //             //   console.error('Service unavailable');
  //             //   break;
  //             // case 504:
  //             //   console.error('Gateway timeout');
  //             //   break;
  //             default:
  //               // this.router.navigateByUrl('/error');
  //               // handle other errors
  //               break;
  //           }
  //         // }
  //         return throwError(error);
  //       } else {
  //         console.error('something else happened');
  //         return next.handle(req);
  //       }
  //     })
  //   );
  // }

}
