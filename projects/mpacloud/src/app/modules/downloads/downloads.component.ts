import { Component } from '@angular/core';
import { MatAnchor } from '@angular/material/button';
import { MatCardImage } from '@angular/material/card';
import { NgForOf } from '@angular/common';
import { MpaModule } from '../mpa/mpa.module';
import {
  ExperimentFiles,
  ExperimentObject,
  ExperimentStatus, MpaFileType,
  NodeType,
  Search, SearchStatus,
  UserDataTreeObject, XtandemRescoreSearchParameters,
} from '../mpa/model/user-data-json';
import { Datstats } from '../mpa/navigation-components/experiment/experiment.component';
import { NONE_TYPE } from '@angular/compiler';

@Component({
  selector: 'app-downloads',
  standalone: true,
  imports: [MatAnchor, MatCardImage, NgForOf, MpaModule],
  templateUrl: './downloads.component.html',
  styleUrl: './downloads.component.scss',
})
export class DownloadsComponent {
  downloadabels = [
    {
      title: 'Spectra_1.mgf',
      image: '',
      download: './assets/downloads/Spectra_1.mgf',
    },
    {
      title: 'Spectra_2.mgf',
      image: '',
      download: './assets/downloads/Spectra_2.mgf',
    },
    {
      title: 'Spectra_1.dat',
      image: '',
      download: './assets/downloads/Spectra_1.dat',
    },
    {
      title: 'Micro_Test_Metagenome.fasta',
      image: '',
      download: './assets/downloads/Micro_Test_Metagenome.fasta',
    },
    {
      title: 'Micro_Test_MG_Annt.fasta',
      image: '',
      download: './assets/downloads/Micro_Test_Metagenome_MPA_ANNOTATION.fasta',
    },
  ];
  // datStats: Datstats = {
  //   totalNoProteinGroups: 100,
  //   totalNoProteins: 100,
  //   totalNoPeptides: 100,
  //   totalNoPsms: 100,
  //   totalNoSpectra: 100,
  // };
  // dataItemOfThisComponent: UserDataTreeObject = {
  //   type: undefined,
  //   children: [],
  //   comments: [],
  //   creationDate: '',
  //   icon: '',
  //   userId: '',
  //   id: 111,
  //   parent: 111,
  //   depth: 122,
  //   displayName: 'Test',
  //   expanded: false,
  //   description: 'testi',
  //   experiment: {
  //     experimentid: 'exp123',
  //     userid: 'user123', // Ensure this matches the userId if needed
  //     status: ExperimentStatus.RESULTS_AVAILABLE_AND_LOCKED,
  //     creationdate: '2023-10-01',
  //     uploadType: 'typeA',
  //     searches: [
  //       {
  //         searchid: 'testi',
  //         experimentid: 'testi',
  //         searchQueuedTime: 123,
  //         status: SearchStatus.SEARCH_FINISHED,
  //         proteinDBId: 'testi',
  //         searchParameters: {
  //           fragemntIonTolerance: 'testi',
  //           fragemntIonToleranceUnit: 'testi',
  //           precursorIonTolerance: 'testi',
  //           precursorIonToleranceUnit: 'testi',
  //           modificationCarbamidomethyl: true,
  //           modificationOxidation: true,
  //         },
  //         peakListDownloadUrl: 'testi',
  //         proteinDBDownloadUrl: 'testi',
  //         rescoreFileId: 'testi',
  // //         userid: '',
  //         spectralLibraryId: '',
  //         spectralLibraryDownloadUrl: '',
  //         searchAlgorithm: undefined,

  //     experimentFiles: {
  //       mergedPeakListsMgf: 'testi',
  //       mergedPeakListsMzMl: 'testi',
  //       mascotFastaFilesForDat: [
  //         {
  //           originalFileName: 'bambum.bam',
  //           uploadId: '',
  //           fileId: '',
  //           fileType: MpaFileType.MGF,
  //           uploadStatus: 'zustand',
  //         },{
  //           originalFileName: 'bumbam.bam',
  //           uploadId: '',
  //           fileId: '',
  //           fileType: MpaFileType.MGF,
  //           uploadStatus: 'zustand',
  //         },{
  //           originalFileName: 'bambam.bam',
  //           uploadId: '',
  //           fileId: '',
  //           fileType: MpaFileType.MGF,
  //           uploadStatus: 'zustand',
  //         },
  //       ],
  //       mergedMascotFasta: 'testi',
  //       uploadedSearchResultFiles: [
  //         // {
  //         //   originalFileName: 'blabli.blub',
  //         //   uploadId: '',
  //         //   fileId: '',
  //         //   fileType: MpaFileType.MGF,
  //         //   uploadStatus: 'zustand',
  //         // }
  //       ],
  //       mergedSearchResultsMzId: 'testi',
  //       uploadedPeakLists: [
  //         {
  //           originalFileName: 'pawpeng.pum',
  //           uploadId: '',
  //           fileId: '',
  //           fileType: MpaFileType.MGF,
  //           uploadStatus: 'zustand',
  //         }
  //       ],
  //     },
  //     targetFDR: 0.01,
  //     proteinGroupJsonFileId: 'jsonFile123',
  //   },
  // };
}
