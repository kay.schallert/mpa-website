import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorStatusProviderService } from '../../error-status-provider.service';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {
  errorCode: number | null = null;
  errorMessage: string = 'An unknown error occurred.';
  // additionalInfo: string | null = null;

  constructor(
    private errorStatusProvider: ErrorStatusProviderService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const details = this.errorStatusProvider.getErrorDetails();
    if (details) {
      this.errorCode = details.errorCode;
      this.errorMessage = details.errorMessage;
      // this.additionalInfo = details.additionalInfo;
    } else {
      this.errorMessage = 'No error details available.';
    }
  }

  goBack(): void {
    this.router.navigate(['/login']);
  }
}
