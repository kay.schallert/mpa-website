import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImpressumRoutingModule } from './impressum-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ImpressumRoutingModule
  ]
})
export class ImpressumModule { }
