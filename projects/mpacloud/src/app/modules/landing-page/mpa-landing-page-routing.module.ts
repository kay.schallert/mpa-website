import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MPALandingPageComponent } from './mpa-landing-page.component';

const routes: Routes = [
  { path: '', component: MPALandingPageComponent }, // Adjust path as needed
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MPALandingPageRoutingModule {}
