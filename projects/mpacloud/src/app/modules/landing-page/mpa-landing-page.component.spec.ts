import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MPALandingPageComponent } from './mpa-landing-page.component';


describe('MPALandingPageComponent', () => {
  let component: MPALandingPageComponent;
  let fixture: ComponentFixture<MPALandingPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MPALandingPageComponent]
    });
    fixture = TestBed.createComponent(MPALandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
