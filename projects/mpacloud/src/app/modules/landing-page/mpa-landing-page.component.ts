import { Component, Input } from "@angular/core";
import {
  MoveDirection,
  ClickMode,
  HoverMode,
  OutMode
} from "tsparticles-engine";
import { loadSlim } from "tsparticles-slim"; // if you are going to use `loadSlim`, install the "tsparticles-slim" package too.
import { AuthService } from "shared-lib";


@Component({
  selector: "mpa-landing-page",
  templateUrl: "./mpa-landing-page.component.html",
  styleUrls: ["./mpa-landing-page.component.scss"]
})
export class MPALandingPageComponent {
  // @Input() showGuestLogin = true; //this needs to be a variable
  // @Input() showGoogleLogin = true; //this needs to be a variable
  // @Input() showLiveScienceLogin = true;

  // constructor( public auth: AuthService) {}

  goToLink(url: string): void {
    window.open(url, "_blank");
  }

  id = "tsparticles";

/* Starting from 1.19.0 you can use a remote url (AJAX request) to a JSON with the configuration */
particlesUrl = "http://foo.bar/particles.json";

/* Updated Particle Options */
particlesOptions = {
  background: {
    color: {
      value: "#ffffff" // Updated background color to white
    }
  },
  fpsLimit: 120,
  interactivity: {
    events: {
      onClick: {
        enable: true,
        mode: ClickMode.push
      },
      onHover: {
        enable: true,
        mode: HoverMode.attract
      },
      resize: true
    },
    modes: {
      push: {
        quantity: 1
      },
      repulse: {
        distance: 20,
        duration: 0.4
      },
      attract: {
        distance: 100,
        duration: 0.4
      }
    }
  },
  particles: {
    color: {
      value: "#eb008b" // Node color updated to pink
    },
    links: {
      color: ["#005aa9", "#00adef"],// Default connection line color (can also alternate with #00adef)
      distance: 200,
      enable: true,
      opacity: 0.5,
      width: 1
    },
    move: {
      direction: MoveDirection.none,
      enable: true,
      outModes: {
        default: OutMode.bounce
      },
      random: false,
      speed: 1,
      straight: false
    },
    number: {
      density: {
        enable: true,
        area: 800
      },
      value: 50
    },
    opacity: {
      value: 0.5
    },
    shape: {
      type: "circle"
    },
    size: {
      value: { min: 1, max: 5 }
    }
  },
  detectRetina: true
};

  

  particlesLoaded(container): void {
    console.log(container);
  }

  async particlesInit(engine): Promise<void> {
    console.log(engine);

    // Starting from 1.19.0 you can add custom presets or shape here, using the current tsParticles instance (main)
    // this loads the tsparticles package bundle, it's the easiest method for getting everything ready
    // starting from v2 you can add only the features you need reducing the bundle size
    //await loadFull(engine);
    await loadSlim(engine);
  }
}
