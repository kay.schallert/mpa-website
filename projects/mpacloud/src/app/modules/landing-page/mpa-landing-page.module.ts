import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {  MatExpansionModule } from '@angular/material/expansion';
import { MPALandingPageComponent } from './mpa-landing-page.component';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { NgParticlesModule } from 'ng-particles';
import { MPALandingPageRoutingModule } from './mpa-landing-page-routing.module';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [
    MPALandingPageComponent
  ],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    NgParticlesModule,
    MPALandingPageRoutingModule,
    MatButtonModule
  ],
  exports: [MPALandingPageComponent] // Export the component
})
export class MPALandingPageModule {}
