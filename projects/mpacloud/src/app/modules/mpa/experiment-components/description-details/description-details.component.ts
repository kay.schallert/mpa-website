import { Component, OnInit } from '@angular/core';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { EC, Keyword, KO, ProteinAccession, ProteinDescription } from '../../model/experiment-data-json';

@Component({
  selector: 'app-description-details',
  templateUrl: './description-details.component.html',
  styleUrls: ['./description-details.component.css']
})
export class DescriptionDetailsComponent implements OnInit {

  selectedProteinAccessions: string = '';
  selectedProteinDescription: string = '';
  selectedProteinKOs: string = '';
  selectedProteinECs: string = '';
  selectedProteinKWs: string = '';
  selectedProteinTax: string = '';

  constructor(private mpaTableDataService: MpaTableDataService) {

  }

  ngOnInit() {
    this.mpaTableDataService.detailViewDataChange.subscribe(() => {
      const dataObject: any = this.mpaTableDataService.getProteinAnnotationDetails();
      if (dataObject !== null) {
        console.log(dataObject);
        this.selectedProteinAccessions = '';
        dataObject.accessions.forEach((acc: ProteinAccession) => {
          this.selectedProteinAccessions += " " + acc.accession;
        });
        this.selectedProteinDescription = '';
        dataObject.descriptions.forEach((desc: ProteinDescription) => {
          this.selectedProteinDescription += " " + desc.description;
        });
        this.selectedProteinKOs = '';
        dataObject.kos.forEach((ko: KO) => {
          this.selectedProteinKOs += " " + ko.koNames;
        });
        this.selectedProteinECs = '';
        dataObject.ecs.forEach((ec: EC) => {
          this.selectedProteinECs += " " + ec.ecNames;
        });
        this.selectedProteinKWs = '';
        dataObject.kws.forEach((kw: Keyword) => {
          this.selectedProteinKWs += " " + kw.kwAccession;
        });
        this.selectedProteinTax = dataObject.tax.scientificname;
      }
    });
  }

}
