import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {ChangeDetectionStrategy, Component, ElementRef, Input, ViewChild, inject, signal, computed, OnInit} from '@angular/core';
import { ComparisonStatus, ExperimentObject, ExperimentStatus, NodeType, UserDataTreeObject } from '../../model/user-data-json';
import {LiveAnnouncer} from '@angular/cdk/a11y';
import {FormControl} from '@angular/forms';
import {MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable, startWith, map, filter } from 'rxjs';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';
import { User } from 'projects/metadata-tool/src/stories/user';


@Component({
  selector: 'app-experiment-selection',
  templateUrl: './experiment-selection.component.html',
  styleUrls: ['./experiment-selection.component.scss']
})
export class ExperimentSelectionComponent implements OnInit {

  @Input() parentDataItem: UserDataTreeObject;

  separatorKeysCodes: number[] = [ENTER, COMMA];
  experimentCtrl = new FormControl('');

  allExperiments: string[] = [];
  filteredExperiments: Observable<string[]>;
  selectedExperiments: string[] = [];
  comparedExperiments: UserDataTreeObject[] = [];

  // UI-RELATED
  selectionConfirmed: boolean = false;
  canSelect: boolean = false;
  canSubmit: boolean = false;
  ComparisonStatus: ComparisonStatus; // make enum available for html-template

  @ViewChild('experimentInput') experimentInput: ElementRef<HTMLInputElement>;

  announcer = inject(LiveAnnouncer);

  constructor (
    private data: MPAUserDataNavigationService
  ) {}

  ngOnInit() {
    if (this.parentDataItem.comparison.status === ComparisonStatus.CREATED) {
      this.initializeExperiments();
    } else {
      // if comparison has already been submitted, get corresponding experiments to display
      const allExps: UserDataTreeObject[] = this.data.getAllNodesAsListByType([NodeType.EXPERIMENT]);
      allExps.forEach((exp: UserDataTreeObject) => {
        this.parentDataItem.comparison
        if (this.parentDataItem.comparison.experimentIds.includes(exp.experiment.experimentid)) {
          this.comparedExperiments.push(exp);
        }
      });
    }
  }

  initializeExperiments(): void {
    // populate list with all available experiments
    let experiments: UserDataTreeObject[] = this.data.getAllNodesAsListByType([NodeType.EXPERIMENT]);
    this.allExperiments = experiments
    // .filter((exp: UserDataTreeObject) => {
    //  ExperimentStatus.RESULTS_AVAILABLE === exp.experiment.status || ExperimentStatus.RESULTS_AVAILABLE_AND_LOCKED === exp.experiment.status
    // })
    .map((exp: UserDataTreeObject) => exp.displayName);
    console.log(this.allExperiments)
    // pre-filter list with all available experiments
    this.updateExperimentLists();
  }

  checkValidity(): void {
    // checks if submission should be enabled
    if (this.selectionConfirmed && this.selectedExperiments.length < 2) {
      this.selectionConfirmed = false;
    }

    if (this.selectionConfirmed && this.selectedExperiments.length > 1) {
      this.canSubmit = true;
    } else {
      this.canSubmit = false;
    }
    // checks if selection should be enabled, positioned after submission so it gets disabled if submission is enabled
    if (this.parentDataItem.comparison.status === ComparisonStatus.CREATED && !this.canSubmit && this.selectedExperiments.length > 1) {
      this.canSelect = true;
    } else {
      this.canSelect = false;
    }
  }

  confirmSelection() {
    this.selectionConfirmed = true;
    this.checkValidity();
  }

  onSubmit() {
    this.parentDataItem.comparison.status = ComparisonStatus.CHOOSE_EXPERIMENTS;
    // transform experiments (which are just the names) to UserDataTreeObjects, in order to get their uuids
    const allExps: UserDataTreeObject[] = this.data.getAllNodesAsListByType([NodeType.EXPERIMENT]);
    allExps.forEach((exp: UserDataTreeObject) => {
      if (this.selectedExperiments.includes(exp.displayName)) {
        this.parentDataItem.comparison.experimentIds.push(exp.experiment.experimentid);
      }
    });
  }

  remove(exp: string): void {
    const index = this.selectedExperiments.indexOf(exp);
    if (index >= 0) {
      this.selectedExperiments.splice(index, 1);
      this.announcer.announce(`Removed ${exp}`);
    }
    this.updateExperimentLists();
    this.checkValidity();
  }


  selected(event: MatAutocompleteSelectedEvent): void {
    this.selectedExperiments.push(event.option.viewValue);
    this.experimentInput.nativeElement.value = '';
    this.experimentCtrl.setValue(null);
    this.updateExperimentLists();
    this.checkValidity();
  }

  clearAllSelected(): void {
    this.comparedExperiments = [];
    this.selectedExperiments = [];
    this.experimentCtrl.reset();
    this.experimentCtrl.markAsPristine();
    this.experimentCtrl.markAsUntouched();
    this.experimentCtrl.setValue('');
    this.selectionConfirmed = false;
    this.checkValidity();
    this.updateExperimentLists();
  }

  updateExperimentLists(): void {
    this.filteredExperiments = this.experimentCtrl.valueChanges.pipe(
      startWith(null),
      map((selectedExperiment: string | null) => {
        let filteredArray = [];
        if (selectedExperiment) {
          // filter based on input search-term
          filteredArray = this._filter(selectedExperiment);
          // return filteredExperiments;
        } else {
          filteredArray = this.allExperiments.slice();
          // return this.allExperiments.slice();
        }
        //lastly, filter based on already selected experiments
        filteredArray = filteredArray.filter(exp => {
          let pass = true;
          this.selectedExperiments.map(selected => {
            exp.toString() == selected.toString() ? pass = false : '';
          });
          return pass;
        })
        return filteredArray;
      }),
    )
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allExperiments.filter(experiment => experiment.toLowerCase().includes(filterValue));
  }
}
