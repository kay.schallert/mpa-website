import { Component, Input, OnInit } from '@angular/core';
import { HttpClientService, MultiFileUploadData, UploadDialogComponent, UploadFile, UploadProgressService } from 'shared-lib';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { HttpEventType } from '@angular/common/http';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Endpoints, MpaWebserverAddressService } from 'projects/mpacloud/src/app/mpa-webserver-address.service';
import {
  ExperimentFileMetadata,
  ExperimentFiles,
  ExperimentObject,
  ExperimentStatus,
  MpaFileType,
  NodeType,
  ProteinDbStatus,
  Search,
  SearchAlgorithm,
  SearchStatus,
  SpectralLibrary,
  UserDataTreeObject,
} from 'projects/mpacloud/src/app/modules/mpa/model/user-data-json';
import { MPAUserDataNavigationService } from 'projects/mpacloud/src/app/modules/mpa/services/mpa-user-data-navigation.service';
import { NodeNameValidator } from 'projects/mpacloud/src/app/modules/mpa/shared-components/textfield-dialog/textfield-dialog.component';
// TODO: remove these:
import { SearchParameters } from '../../model/search-parameters';
import { FilesUploadMetadata, FilesUploadMetadataJSON } from 'projects/mpacloud/src/old_to_check/model/filesuploadmetadatajson';
import * as uuid from 'uuid';
import { GetDateService } from '../../services/get-date.service';
import { DiaSearchParameters } from '../../model/dia-search-parameters';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ visibility: 'hidden', height: 0, opacity: 0 })),
      state('expanded', style({ height: '*', opacity: 1 })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ),
    ]),
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)'),
      ),
    ]),
  ],
})
export class FileUploadComponent implements OnInit {

  @Input() parentDataItem: UserDataTreeObject;

  FileType = MpaFileType;

  existingNodeNames: string[];
  experimentForm: UntypedFormGroup;
  uploadDialogId: string;
  // UI VARIABLES
  // available upload options
  dataUploadSelection: string = 'Search Result';
  // dataUploadOptions: string[] = ['Search Result', 'Peaklist + Search Result'];
  dataUploadOptions: string[] = ['Search Result'];

  //available interaction modes
  dataUploadModeSelection: string = 'Search DDA';
  // TODO: reintroduce Result Upload
  // dataUploadModeOptions: string[] = ['Search'];
  dataUploadModeOptions: string[] = ['Result Upload', 'Search DDA', 'Search DIA']; //TODO disable again!!!!

  //available upload types (amount of files)
  fileAmountSelection: string = 'Single';
  fileAmountOptions: string[] = ['Single', 'Multi'];

  //advanced search parameters
  advancedSearchExpanded: boolean = false;
  proteinDatabases: UserDataTreeObject[] = [];
  proteinDBselection: UserDataTreeObject = null;
  spectralLibraries: SpectralLibrary[] = [];
  spectralLibrarySelection: SpectralLibrary = null;

  // button disabling, etc.
  // TODO: can be removed
  hasPeaklistFile: boolean;
  hasSearchFile: boolean;

  // to handle displayed options upon File selection
  fastaFileSelected = true;

  peaklistSelection = this.FileType.MGF;
  searchFileSelection = this.FileType.DAT;

  // available options
  // TODO: reintroduce MZML
  uploadFileTypePeaklist: string[] = [this.FileType.MGF];
  // uploadFileTypePeaklist: string[] = [FileType.MZML, FileType.MGF];
  uploadFileTypeSearch: string[] = [this.FileType.DAT];
  // uploadFileTypeSearch: string[] = [this.FileType.MZIDENTML, this.FileType.DAT];
  buttonDisabled: boolean = true;

  // DATA SUBMISSION VARIABLES
  // Files input via dropzones, always File[]
  selectedPeaklistFiles: File[];
  selectedSearchFiles: File[];
  selectedFastaFiles: File[];
  selectedDatFiles: File[]; // TODO utilize in disableButton() and submit

  //search options
  searchParameters: SearchParameters;
  ruminationSearchParameters: DiaSearchParameters = new DiaSearchParameters();

  // files to upload to server
  // metadata for file upload (expid etc.)
  filesUploadMetadata: FilesUploadMetadata;
  filesToUpload: MultiFileUploadData;

  constructor(
    private dataService: MPAUserDataNavigationService,
    private addressService: MpaWebserverAddressService,
    private httpClientService: HttpClientService,
    private uploadProgressService: UploadProgressService,
    private fb: UntypedFormBuilder,
    private dialog: MatDialog,
    private currentDateService: GetDateService,
  ) {
  }

  ngOnInit(): void {
    this.initializeProteinDBs();
    this.clearSelectedFiles(); // initializes all file arrays
    this.searchParameters = new SearchParameters();
    this.filesUploadMetadata = new FilesUploadMetadataJSON();
    this.parentDataItem.type == NodeType.FOLDER ? this.dataUploadModeSelection = 'Search DDA' : '';

    // TODO: is this still neccessary / used?
    this.existingNodeNames = []; //this.dataService.getExistingNodeNames();
    this.experimentForm = this.fb.group({
      expName: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(24),
          Validators.pattern('[äÄöÖüÜa-zA-Z0-9_-]*'),
          NodeNameValidator(this.existingNodeNames),
        ],
      ],
    });
    this.experimentForm.get('expName').valueChanges.subscribe({
      next: value => {
        this.disableButton();
      },
    });
  }

  // Get all proteinDBs, filter for available and locked status, set currently selected db
  initializeProteinDBs(): void {
    this.proteinDatabases = [];
    let allDBs = this.dataService.getAllNodesAsListByType([NodeType.PROTEINDB]);
    this.proteinDatabases = allDBs.filter((db: UserDataTreeObject) => ProteinDbStatus.AVAILABLE_AND_LOCKED == db.proteinDB.status);
    if (this.proteinDatabases.length > 0) {
      this.proteinDBselection = this.proteinDatabases[0];
    }
  }

  onProteinDBChange(item: UserDataTreeObject): void {
    console.log(item);
    this.filesUploadMetadata.protdbID = item.proteinDB.protdbId;
    this.spectralLibraries = item.proteinDB.spectralLibraries.filter((sl: SpectralLibrary) => sl.status == 'AVAILABLE_AND_LOCKED');
  }

  fileAmountSelectionChange() {
    console.log(this.fileAmountSelection);
    this.disableButton();
  }

  /**
   * handles change of the upload type selection (peaklist, search file, peaklist + search file)
   */
  onUploadSelectionChange(option: string): void {
    this.clearSelectedFiles();
    this.dataUploadSelection = option;
    this.disableButton();
  }

  dataUploadModeSelectionChange(option: string): void {
    this.clearSelectedFiles();
    this.dataUploadModeSelection = option;
    this.disableButton();
  }

  /**
   * handles change of upload data type
   */
  onFileTypeSelectionChange(): void {
    this.clearSelectedFiles();
    this.disableButton();
  }

  clearSelectedFiles(): void {
    this.selectedPeaklistFiles = [];
    this.selectedSearchFiles = [];
    this.selectedFastaFiles = [];
    this.selectedDatFiles = [];
    // this.fastaFileSelected = this.searchFileSelection !== this.FileType.DAT;
    this.disableButton();
  }

  expandAdvancedSearchParameters(): void {
    this.advancedSearchExpanded = !this.advancedSearchExpanded;
  }

  // FILE INPUT HANDLERS
  // these methods should only receive files of the correct type (handled by dropzone)
  // note: they also receive a File[], even if allowMultiple is set to false in the dropzone
  peaklistFileInput(fileList: File[]): void {
    this.selectedPeaklistFiles = [...fileList];
    this.disableButton();
  }

  // input handler for mzidentml
  searchFileInput(fileList: File[]): void {
    this.selectedSearchFiles = [...fileList];
    this.disableButton();
  }

  fastaFileInput(fileList: File[]): void {
    this.selectedFastaFiles = [...fileList];
    this.disableButton();
  }

  datFileInput(fileList: File[]): void {
    this.selectedDatFiles = [...fileList];
    this.disableButton();
  }

  // UI-RELATED
  // checks if files are selected or uploaded already and disables the submit button
  disableButton(): void {
    if (this.dataUploadModeSelection === 'Search DDA') {
      this.buttonDisabled =
        this.hasPeaklistFile ||
        !this.proteinDBselection ||
        this.selectedPeaklistFiles.length < 1;
    } else if (this.dataUploadModeSelection === 'Result Upload') {
      if (this.dataUploadSelection === 'Peaklist + Search Result') {
        this.buttonDisabled =
          this.hasSearchFile ||
          this.hasPeaklistFile ||
          // !this.selectedPeaklistFile ||
          // !this.selectedSearchFile ||
          this.selectedPeaklistFiles.length < 1 ||
          this.selectedSearchFiles.length < 1 ||
          !this.fastaFileSelected;
      } else if (this.dataUploadSelection === 'Search Result') {
        switch (this.searchFileSelection) {
          case this.FileType.DAT:
            this.buttonDisabled = this.selectedDatFiles.length < 1 || this.selectedFastaFiles.length < 1;
            break;
          case this.FileType.MZID:
            this.buttonDisabled = this.selectedFastaFiles.length < 1;
            break;
        }
      }
    } else if (this.dataUploadSelection === 'Search DIA') {
      this.buttonDisabled =
        this.hasPeaklistFile ||
        !this.proteinDBselection ||
        !this.spectralLibrarySelection ||
        this.selectedPeaklistFiles.length < 1;
    }
  }

  onSubmit(): void {
    this.uploadProgressService.reset();
    const multiFile: MultiFileUploadData = {
      files: [],
    };
    // BATCH OR EXPERIMENT?
    switch (this.parentDataItem.type) {
      case NodeType.EXPERIMENT:
        this.parentDataItem.experiment.experimentFiles.uploadedSearchResultFiles = [];
        // EXPERIMENT
        // UPLOADTYPE?
        switch (this.dataUploadModeSelection) {
          case 'Search DIA':
          case 'Search DDA':
            // This adds peaklists to the multFile-object and returns peaklist objects for the UserDataTreeObject
            const peakLists: ExperimentFileMetadata[] = this.processPeakLists(multiFile);
            // TODO: isnt there some kind of "addAll()"

            peakLists.forEach((pl: ExperimentFileMetadata) => {
              this.parentDataItem.experiment.experimentFiles.uploadedPeakLists.push(pl);
            });
            // prepare search
            let search: Search = this.createNewSearch(this.parentDataItem.experiment.userid, this.parentDataItem.experiment.experimentid);
            if (this.dataUploadModeSelection === 'Search DDA') {
              search.searchParameters.xtandemRescoreSearchParameters = {
                fragemntIonTolerance: this.searchParameters.fragmentIonTolerance.toString(),
                fragemntIonToleranceUnit: this.searchParameters.fragmentIonToleranceUnitSelection.toString(),
                precursorIonTolerance: this.searchParameters.precursorIonTolerance.toString(),
                precursorIonToleranceUnit: this.searchParameters.precursorIonToleranceUnitSelection.toString(),
                modificationCarbamidomethyl: true,
                modificationOxidation: true,
              };
              this.parentDataItem.experiment.uploadType = 'PEAKLIST_AND_SEARCH_DDA';
              search.searchAlgorithm = SearchAlgorithm.XTANDEM_RESCORE;
            } else if (this.dataUploadModeSelection === 'Search DIA') {
              search.searchParameters.ruminationSearchParameters = {
                msAccuracy: this.ruminationSearchParameters.ionToleracnce,
                intensityThreshold: this.ruminationSearchParameters.intensityThreshold,
              };
              this.parentDataItem.experiment.uploadType = 'PEAKLIST_AND_SEARCH_DIA';
              search.searchAlgorithm = SearchAlgorithm.RUMINATION;
            }
            this.parentDataItem.experiment.searches.push(search);
            // trigger upload and update
            this.parentDataItem.experiment.status = ExperimentStatus.WAITING_FOR_UPLOAD;
            this.dataService.triggerHttpUpdateEvent$.next(true);
            this.dataService.executeUpload(multiFile);
            break;
          case 'Result Upload':
            switch (this.dataUploadSelection) {
              case 'Peaklist + Search Result':
                // This adds peaklists to the multFile-object and returns peaklist objects for the UserDataTreeObject
                const peakLists: ExperimentFileMetadata[] = this.processPeakLists(multiFile);
                this.parentDataItem.experiment.uploadType = 'PEAKLIST_AND_SEARCH_RESULTS';
                // TODO: isnt there some kind of "addAll()"
                peakLists.forEach((pl: ExperimentFileMetadata) => {
                  this.parentDataItem.experiment.experimentFiles.uploadedPeakLists.push(pl);
                });
              // No BREAK, we continue
              case 'Search Result':
                this.parentDataItem.experiment.uploadType = 'SEARCH_RESULTS';
                switch (this.searchFileSelection) {
                  case MpaFileType.DAT:
                    const datAndFastaFiles = this.processMascotFiles(multiFile);
                    // TODO: isnt there some kind of "addAll()"
                    const search: Search = this.createNewSearch(this.parentDataItem.experiment.userid, this.parentDataItem.experiment.experimentid);
                    search.status = SearchStatus.SEARCH_FINISHED;
                    this.parentDataItem.experiment.searches.push(search);
                    datAndFastaFiles.get('fasta').forEach((faa: ExperimentFileMetadata) => {
                      this.parentDataItem.experiment.experimentFiles.mascotFastaFilesForDat.push(faa);
                    });
                    datAndFastaFiles.get('dat').forEach((sf: ExperimentFileMetadata) => {
                      this.parentDataItem.experiment.experimentFiles.uploadedSearchResultFiles.push(sf);
                    });
                    break;
                  case MpaFileType.MZID:
                    // This adds peaklists to the multFile-object and returns peaklist objects for the UserDataTreeObject
                    const searchFiles: ExperimentFileMetadata[] = this.processNonMascotSearchResultFiles(multiFile);
                    // TODO: isnt there some kind of "addAll()"
                    searchFiles.forEach((sf: ExperimentFileMetadata) => {
                      this.parentDataItem.experiment.experimentFiles.uploadedSearchResultFiles.push(sf);
                    });
                    break;
                }
                // trigger upload and update

                this.parentDataItem.experiment.status = ExperimentStatus.WAITING_FOR_UPLOAD;
                this.dataService.triggerHttpUpdateEvent$.next(true);
                this.dataService.executeUpload(multiFile);
                break;
            }
            break;
        }
        break;
      case NodeType.FOLDER:
        // BATCH
        switch (this.dataUploadModeSelection) {
          case 'Search DDA':
            // BATCH SEARCH
            const peakLists: ExperimentFileMetadata[] = this.processPeakLists(multiFile);
            // create one experiment for each selected file
            peakLists.forEach((file: ExperimentFileMetadata) => {
              const search: Search = this.createNewSearch(this.parentDataItem.userId, '');
              const experimentFiles: ExperimentFiles = {
                uploadedPeakLists: [file],
                mergedPeakListsMgf: '',
                mergedPeakListsMzMl: '',
                mascotFastaFilesForDat: [],
                mergedMascotFasta: '',
                uploadedSearchResultFiles: [],
                mergedSearchResultsMzId: '',
              };
              const experimentObject: ExperimentObject = {
                experimentid: '',
                userid: this.parentDataItem.userId,
                status: ExperimentStatus.REQUEST_CREATION_BATCH,
                creationdate: this.currentDateService.getDate(),
                uploadType: 'PEAKLIST_AND_SEARCH',
                searches: [search],
                experimentFiles: experimentFiles,
                targetFDR: 0.01,
                proteinGroupJsonFileId: '',
              };
              this.dataService.createNode(this.parentDataItem, file.originalFileName, NodeType.EXPERIMENT, experimentObject);
            });
            // TODO: more stuff to do?
            // trigger upload and update
            this.dataService.triggerHttpUpdateEvent$.next(true);
            this.dataService.executeUpload(multiFile);
            break;
          case 'Result Upload':
            if (this.dataUploadSelection === 'Search Result') {
              // BATCH RESULTS
              // TODO
            }
            break;
        }
        break;
    }
    // reset after submit
    this.clearSelectedFiles();
  }

  private createNewSearch(userid: string, experimentid: string): Search {
    return {
      searchid: uuid.v4(),
      experimentid: experimentid,
      userid: userid,
      searchQueuedTime: -1,
      status: SearchStatus.SEARCH_REQUESTED,
      proteinDBId: this.proteinDBselection.proteinDB.protdbId,
      searchParameters: {
        ruminationSearchParameters: null,
        xtandemRescoreSearchParameters: null,
      },
      peakListDownloadUrl: '',
      proteinDBDownloadUrl: '',
      spectralLibraryId: this.spectralLibrarySelection?.id,
      spectralLibraryDownloadUrl: '',
      rescoreFileId: '',
      searchAlgorithm: SearchAlgorithm.XTANDEM_RESCORE
    };
  }

  // HELPER METHOD
  // This adds peaklists to the multFile-object and returns peaklist objects for the UserDataTreeObject
  private processPeakLists(multiFile: MultiFileUploadData): ExperimentFileMetadata[] {
    const peakLists: ExperimentFileMetadata[] = [];
    this.selectedPeaklistFiles.forEach((file: File) => {
      // provide an uploadid
      const uploadIdPeakList = uuid.v4();
      // prepare file for upload
      const uploadFilePeakList: UploadFile = {
        uploadFile: file,
        fileID: uploadIdPeakList,
      };
      multiFile.files.push(uploadFilePeakList);
      // create peaklist object for experiment
      const peakList: ExperimentFileMetadata = {
        originalFileName: uploadFilePeakList.uploadFile.name,
        uploadId: uploadIdPeakList,
        fileId: '',
        fileType: this.peaklistSelection,
        uploadStatus: 'WAITING_FOR_UPLOAD',
      };
      peakLists.push(peakList);
    });
    return peakLists;
  }

  // HELPER METHOD
  // This adds peaklists to the multFile-object and returns peaklist objects for the UserDataTreeObject
  private processNonMascotSearchResultFiles(multiFile: MultiFileUploadData): ExperimentFileMetadata[] {
    const searchFiles: ExperimentFileMetadata[] = [];
    this.selectedSearchFiles.forEach((file: File) => {
      // provide an uploadid
      const uploadIdSearchFile = uuid.v4();
      // prepare file for upload
      const uploadFileSearchFile: UploadFile = {
        uploadFile: file,
        fileID: uploadIdSearchFile,
      };
      multiFile.files.push(uploadFileSearchFile);
      // create peaklist object for experiment
      const searchResult: ExperimentFileMetadata = {
        originalFileName: uploadFileSearchFile.uploadFile.name,
        uploadId: uploadIdSearchFile,
        fileId: '',
        fileType: this.searchFileSelection,
        uploadStatus: 'WAITING_FOR_UPLOAD',
      };
      searchFiles.push(searchResult);
    });
    return searchFiles;
  }

  // HELPER METHOD
  // This adds peaklists to the multFile-object and returns peaklist objects for the UserDataTreeObject
  private processMascotFiles(multiFile: MultiFileUploadData): Map<string, ExperimentFileMetadata[]> {
    const datAndFastaFiles: Map<string, ExperimentFileMetadata[]> = new Map();
    datAndFastaFiles.set('dat', []);
    datAndFastaFiles.set('fasta', []);
    this.selectedDatFiles.forEach((file: File) => {
      // provide an uploadid
      const uploadIdDat = uuid.v4();
      // prepare file for upload
      const uploadDatFile: UploadFile = {
        uploadFile: file,
        fileID: uploadIdDat,
      };
      multiFile.files.push(uploadDatFile);
      // create peaklist object for experiment
      const datFile: ExperimentFileMetadata = {
        originalFileName: uploadDatFile.uploadFile.name,
        uploadId: uploadIdDat,
        fileId: '',
        fileType: this.searchFileSelection,
        uploadStatus: 'WAITING_FOR_UPLOAD',
      };
      datAndFastaFiles.get('dat').push(datFile);
    });
    this.selectedFastaFiles.forEach((file: File) => {
      // provide an uploadid
      const uploadIdFasta = uuid.v4();
      // prepare file for upload
      const uploadFastaFile: UploadFile = {
        uploadFile: file,
        fileID: uploadIdFasta,
      };
      multiFile.files.push(uploadFastaFile);
      // create peaklist object for experiment
      const fastaFile: ExperimentFileMetadata = {
        originalFileName: uploadFastaFile.uploadFile.name,
        uploadId: uploadIdFasta,
        fileId: '',
        fileType: MpaFileType.MASCOT_FASTA,
        uploadStatus: 'WAITING_FOR_UPLOAD',
      };
      datAndFastaFiles.get('fasta').push(fastaFile);
    });
    return datAndFastaFiles;
  }

  invokeUploadDialog(): Observable<any> {
    this.uploadProgressService.setUUID(this.parentDataItem.id);
    const dialogRef = this.dialog.open(UploadDialogComponent, {
      id: this.uploadDialogId,
      disableClose: true,
      data: { successMessage: 'Upload successful.' },
    });
    return dialogRef.afterClosed();
  }

  executeUpload(files: MultiFileUploadData) {
    this.httpClientService
      .postMultiPartFilesEvents(files, this.addressService.getEndpoint(Endpoints.FILES_UPLOAD))
      .subscribe({
        next: (event) => {
          console.log('unknown event');
          console.log(event);
          console.log(event.type);
          if (event.type === HttpEventType.UploadProgress) {
            this.uploadProgressService.changeReportLoaded(event.loaded);
            console.log('UploadProgress event');
            console.log(event);
          } else if (event.type === HttpEventType.Response) {
            console.log('Response event');
            console.log(event);
          }
        },
        error: (error) => {
          console.log(error);
          if (error.status >= 400) {
            // handle failed upload
            if (this.dialog.getDialogById(this.uploadDialogId)) {
              this.dialog
                .getDialogById(this.uploadDialogId)
                .componentInstance.setUploadFailed();
              this.dialog.getDialogById(
                this.uploadDialogId,
              ).componentInstance.uploadFailedMessage = error.statusText;
            }
          } else {
            throw error;
          }
        },
      });
  }

  //errorMessage for the name form
  getNameErrorMessage(): string {
    if (this.experimentForm.get('expName').hasError('required')) {
      return 'Please enter a name';
    } else if (this.experimentForm.get('expName').hasError('pattern')) {
      return 'No white spaces or special chars';
    } else {
      return '';
    }
  }

}
