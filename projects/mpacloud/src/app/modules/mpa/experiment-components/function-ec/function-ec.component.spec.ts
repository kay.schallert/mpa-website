import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionTabComponent } from './function-ec.component';

describe('FunctionTabComponent', () => {
  let component: FunctionTabComponent;
  let fixture: ComponentFixture<FunctionTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FunctionTabComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FunctionTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
