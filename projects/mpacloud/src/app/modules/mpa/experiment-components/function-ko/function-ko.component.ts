import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort, Sort } from '@angular/material/sort';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { KO } from '../../model/experiment-data-json';


@Component({
  selector: 'app-function-ko',
  templateUrl: './function-ko.component.html',
  animations: [trigger('indicatorRotate', [state('asc', style({ transform: 'rotate(0deg)' })), state('desc', style({ transform: 'rotate(180deg)' })), transition('desc <=> asc', animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),])],
  styleUrls: ['./function-ko.component.scss']
})
export class FunctionKOComponent implements OnInit {

   @ViewChild(MatPaginator) paginator: MatPaginator;
   @ViewChild(MatSort) sort: MatSort;

   displayedColumns = [
     'functionID',
     'names',
     'symbols',
   ];
   dataSourceKO: MatTableDataSource<KO>;
   quantificationColumns = [];
   index2experimentIdMap: Map<number, string> = new Map();

   filterString: string;
   selectedAll: boolean = false;

   highlightedKO: KO = null;
   ecData: KO[];
   selectedKO: KO;

   constructor(
     public mpaTableDataService: MpaTableDataService
   ) {
     this.dataSourceKO = new MatTableDataSource([]);
     this.index2experimentIdMap = new Map();
     this.mpaTableDataService.getExperiment2IndexMap().forEach((value: number, key: string) => {
       this.index2experimentIdMap.set(value, key);
     });
     for (let i = 0; i < this.index2experimentIdMap.size; i++) {
       // quantificationColumns is used in template to generate the right amount of columns and extract the relevant data
       this.quantificationColumns.push({"expID":this.index2experimentIdMap.get(i),"index":i});
       this.displayedColumns.push(this.index2experimentIdMap.get(i));
     }
     this.displayedColumns.push('checkbox');
   }

   ngOnInit() {
     this.filterString = '';
     this.dataSourceKO.data = this.mpaTableDataService.getKOs();
   }

   ngAfterViewInit(): void {
     this.dataSourceKO.paginator = this.paginator;
   }


   applyFilter() {
     this.dataSourceKO.filter = this.filterString.toLowerCase();
     // TODO: add actual filtering ...
     if (this.sort != undefined) {
       this.sortTableData({active:this.sort.active,direction:this.sort.direction});
     } else {
       this.sortTableData({active:'functionID',direction:'asc'});
     };
   }

   sortTableData(sort: Sort) {
     // check if sorting should be reset to functionID
     if (sort.direction == '') {
       sort.active = 'functionID';
       sort.direction = 'asc';
       // update ui
       this.sort.active = sort.active;
       this.sort.direction = sort.direction;
       this.sort.sortChange.emit(this.sort);
     }
     let data = this.dataSourceKO.data.slice();
     data.sort((a, b) => {
       const isAsc = sort.direction === 'asc';
       switch (sort.active) {
         case 'functionID':
           return a.koId.localeCompare(b.koId) * (isAsc ? 1 : -1);
         case 'names':
           return a.koNames[0].localeCompare(b.koNames[0]) * (isAsc ? 1 : -1);
         case 'quantification':
           return (a.quantifications[0] - b.quantifications[0]) * (isAsc ? 1 : -1);
       }
     })
     //after sorting, split into displayed and hidden arrays, append to each other
     let displayedEntries = [];
     let hiddenEntries = [];
     data.map(entry => entry.isDisplayed ? displayedEntries.push(entry) : hiddenEntries.push(entry));
     this.dataSourceKO.data = [...displayedEntries, ...hiddenEntries];
   }

     onSelectAllGroups(): void {
       this.dataSourceKO.data.map((ko: KO) => {
         ko.isSelected = this.selectedAll;
       });
     }

     disableSelectedGroups(): void {
       this.dataSourceKO.data.forEach((ko: KO) => {
         if (ko.isSelected) {
           if (ko.isHighlighted) {
             this.highlightedKO = null;
             ko.isHighlighted = false;
           }
           ko.isSelected = false;
           this.mpaTableDataService.changeAndPropagateKOVisibility(ko.koId, false);
         }
       });
       if (this.sort != undefined) {
         this.sortTableData({active:this.sort.active.slice(),direction:this.sort.direction});
       } else {
         this.sortTableData({active:'functionID',direction:'asc'});
       };
     }

     enableSelectedGroups(): void {
       this.dataSourceKO.data.forEach((ko: KO) => {
         if (ko.isSelected) {
           if (ko.isHighlighted) {
             this.highlightedKO = null;
             ko.isHighlighted = false;
           }
           // protein.isDisplayed = true;
           ko.isSelected = false;
           this.mpaTableDataService.changeAndPropagateKOVisibility(ko.koId, true);
         }
       });
       if (this.sort != undefined) {
         this.sortTableData({active:this.sort.active.slice(),direction:this.sort.direction});
       } else {
         this.sortTableData({active:'functionID',direction:'asc'});
       };
     }

   onClick(row: KO): void {
     if (row.isDisplayed) {
       this.selectedKO = row;
     }
     console.log(row);
   }

}
