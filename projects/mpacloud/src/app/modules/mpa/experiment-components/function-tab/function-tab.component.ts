import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort, Sort } from '@angular/material/sort';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { EC, KO } from '../../model/experiment-data-json';


export enum FunctionDisplayMode {
  EC = 'EC',
  KO = 'KO'
}

@Component({
  selector: 'app-function-tab',
  templateUrl: './function-tab.component.html',
  animations: [trigger('indicatorRotate', [state('asc', style({ transform: 'rotate(0deg)' })), state('desc', style({ transform: 'rotate(180deg)' })), transition('desc <=> asc', animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),])],
  styleUrls: ['./function-tab.component.scss']
})
export class FunctionTabComponent {

  FunctionDisplayMode = FunctionDisplayMode;
  public functionDisplayMode: FunctionDisplayMode = FunctionDisplayMode.EC;

}
