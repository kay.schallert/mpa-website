import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterViewInit,
  Component,
  Input, OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import {
  Protein,
  ProteinMainGroup,
  ProteinSubgroup,
} from '../../model/experiment-data-json';

export enum GroupSelection {
  MAINGROUPS = 'maingroups',
  SUBGROUPS = 'subgroups',
}

@Component({
  selector: 'app-group-table',
  templateUrl: './group-table.component.html',
  styleUrls: ['./group-table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ visibility: 'hidden' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ),
    ]),
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)'),
      ),
    ]),
  ],
})
export class GroupTableComponent implements OnInit, AfterViewInit, OnDestroy {
  //TODO: ? is this still something we want to do (by @Benni)
  // move groupSelection here
  // pull tableData once and modify locally
  // still need "selected group" in tabledataservice
  // try to get rid of ProteinGroup class or move it here as local helper class
  // no subscriptions -> tabledataservice: replace with get
  //                  -> selectedGroup, should be handled here
  // need variable displayedRows --> maybe not
  // separate group table + subgroup table
  // for hierachrchical: double table --> roll into group table
  // to separate components for subgroup and group table

  displayedColumns = [];
  quantificationColumns = [];

  groupDataSource: MatTableDataSource<ProteinMainGroup>;
  //subgroupDataSource: MatTableDataSource<ProteinSubgroup>;

  //showDetails: boolean = false;
  highlightedGroup: ProteinMainGroup = null;
  selectedAll: boolean = false;
  //highlightedProtein: Protein = null;


  expandedElement: string = '';
  expandedSubgroups: ProteinMainGroup[] = [];
  mainGroupSorting: Sort;
  //GroupSelection = GroupSelection; // make enum available to use in html
  filterString: string;
  // NEW properties for expansion


  index2experimentIdMap: Map<number, string> = new Map();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() experimentUUID: string;

  constructor(public mpaTableDataService: MpaTableDataService) {
    this.groupDataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.filterString = '';
    const proteinGroupData: ProteinMainGroup[] =
      this.mpaTableDataService.getProteinGroupData();
    // Dynamically adjust number of quantification columns (relevance: comparison)
    if (this.quantificationColumns.length == 0) {
      this.displayedColumns = ['expandButton', 'proteinGroupId'];
      // const index2experimentIdMap: Map<number, string> = new Map();
      this.index2experimentIdMap = new Map();
      this.mpaTableDataService
        .getExperiment2IndexMap()
        .forEach((value: number, key: string) => {
          this.index2experimentIdMap.set(value, key);
        });
      for (let i = 0; i < this.index2experimentIdMap.size; i++) {
        // quantificationColumns is used in template to generate the right amount of columns and extract the relevant data
        this.quantificationColumns.push({
          expID: this.index2experimentIdMap.get(i),
          index: i,
        });
        this.displayedColumns.push(this.index2experimentIdMap.get(i));
      }
      this.displayedColumns.push(
        'representativeAccession',
        'representativeDescription',
        'checkbox',
      );
    }
    this.groupDataSource.data = proteinGroupData;
    this.applyFilter(); // re-use filterString if groupSelection changes
    console.log(this.groupDataSource.data);
  }

  ngOnDestroy() {
    this.highlightedGroup.isHighlighted = false;
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */

  ngAfterViewInit() {
    this.groupDataSource.paginator = this.paginator;
  }

  sortTableData(sort: Sort) {
    // check if sorting should be reset to proteinGroupID
    if (sort.direction == '') {
      sort.active = 'proteinGroupId';
      sort.direction = 'asc';
      // update ui
      this.sort.active = sort.active;
      this.sort.direction = sort.direction;
      this.sort.sortChange.emit(this.sort);
    }
    // check if only an expanded element in hierarchical view should be sorted
    if (this.expandedElement.length > 0) {
      let sortedSubgroups = this.expandedSubgroups;
      const isAsc = sort.direction === 'asc';
      sortedSubgroups.sort((a, b) => {
        console.log('sort 1');
        console.log(sort.active);
        console.log(
          this.index2experimentIdMap.get(this.quantificationColumns[0].index),
        );
        switch (sort.active) {
          case 'proteinGroupId':
            return isAsc ? 1 : -1;
          case 'representativeAccession':
            return (
              a.representativeAccession.accession.localeCompare(
                b.representativeAccession.accession,
              ) * (isAsc ? 1 : -1)
            );
          case 'representativeDescription':
            return (
              a.representativeDescription.description.localeCompare(
                b.representativeDescription.description,
              ) * (isAsc ? 1 : -1)
            );
          // TODO handle multiple quantification columns
          case this.index2experimentIdMap.get(
            this.quantificationColumns[0].index,
          ):
            return (
              (a.quantifications[0] - b.quantifications[0]) * (isAsc ? 1 : -1)
            );
        }
      });
    }
    // if not, sort whole table
    else {
      let data = this.groupDataSource.data.slice();
      const isAsc = sort.direction === 'asc';
      data.sort((a, b) => {
        console.log('sort 2');
        console.log(sort.active);
        console.log(
          this.index2experimentIdMap.get(this.quantificationColumns[0].index),
        );
        switch (sort.active) {
          case 'representativeAccession':
            return (
              a.representativeAccession.accession.localeCompare(
                b.representativeAccession.accession,
              ) * (isAsc ? 1 : -1)
            );
          case 'representativeDescription':
            return (
              a.representativeDescription.description.localeCompare(
                b.representativeDescription.description,
              ) * (isAsc ? 1 : -1)
            );
          // TODO handle multiple quantification columns
          case this.index2experimentIdMap.get(
            this.quantificationColumns[0].index,
          ):
            return (
              (a.quantifications[0] - b.quantifications[0]) * (isAsc ? 1 : -1)
            );
          case 'proteinGroupId':
            return (
              (parseInt(a.proteinGroupId) - parseInt(b.proteinGroupId)) *
              (isAsc ? 1 : -1)
            );
        }
      });
      //after sorting, split into displayed and hidden arrays, append to each other
      let displayedEntries = [];
      let hiddenEntries = [];
      data.map((entry) =>
        entry.isDisplayed
          ? hiddenEntries.push(entry)
          : displayedEntries.push(entry),
      );
      this.groupDataSource.data = [...displayedEntries, ...hiddenEntries];
    }
  }

  applyFilter(): void {
    if (this.filterString.length > 0) {
      let regExp = new RegExp(this.filterString.toLowerCase(), 'i');
      let newData = this.mpaTableDataService.getProteinGroupData();
      newData = newData.filter((obj) => {
        return (
          regExp.test(obj.proteinGroupId.toLowerCase()) ||
          regExp.test(obj.representativeAccession.accession.toLowerCase()) ||
          regExp.test(obj.representativeDescription.description.toLowerCase())
        );
      });
      this.groupDataSource.data = newData;
    } else {
      this.groupDataSource.data =
        this.mpaTableDataService.getProteinGroupData();
    }
    //TODO does expandedElement break this?
    if (this.sort != undefined) {
      this.sortTableData({
        active: this.sort.active.slice(),
        direction: this.sort.direction,
      });
    } else {
      this.sortTableData({ active: 'proteinGroupId', direction: 'asc' });
    }
  }

  onExpand(row: ProteinMainGroup): void {
    console.log('onExpand');
    if (this.expandedElement === row.proteinGroupId) {
      this.expandedElement = '';
      this.expandedSubgroups = [];
      this.sort.active = this.mainGroupSorting.active;
      this.sort.direction = this.mainGroupSorting.direction;
      this.sort.sortChange.emit(this.sort); // workaround as just setting .active and .direction does not update the UI -> known, but unfixed bug of MatSort for years.
    } else {
      if (this.expandedElement === '') {
        this.mainGroupSorting = {
          active: this.sort.active,
          direction: this.sort.direction,
        }; // reset to this after closing subgroups
      }
      this.expandedElement = row.proteinGroupId;
      this.expandedSubgroups = [];
      row.subgroups.forEach((subgroup: ProteinSubgroup) => {
        this.expandedSubgroups.push({
          proteinGroupId: subgroup.proteinSubgroupId,
          representativeAccession: subgroup.representativeAccession,
          representativeDescription: subgroup.representativeDescription,
          quantifications: subgroup.quantifications,
          subgroups: [],
          isDisplayed: true,
          isHighlighted: false,
          isSelected: false,
        });
      });
      console.log('subgroups');
      console.log(this.expandedSubgroups);
    }
  }

  onClick(row: ProteinMainGroup): void {
    console.log(row);
    if (this.highlightedGroup !== null && this.highlightedGroup !== row) {
      this.highlightedGroup.isHighlighted = false;
      this.highlightedGroup = null;
    }
    if (row.isDisplayed) {
      row.isHighlighted = !row.isHighlighted;
      this.highlightedGroup = row;
      this.mpaTableDataService.selectedGroupId = row.proteinGroupId;
      this.mpaTableDataService.detailViewDataChange.next(true);
    }
    console.log(row);
  }

  downloadGroups(): void {
    // Filter out only visible groups.
    const groups = this.groupDataSource.data.filter(
      (group) => group.isDisplayed,
    );
    console.log(`Export Groups Only: Found ${groups.length} visible groups.`);

    if (groups.length === 0) {
      console.warn('Export Groups Only: No visible groups to export.');
      return;
    }

    // CSV header for groups-only export.
    const headers = [
      'Protein Group ID',
      'Quantification',
      'Accession',
      'Description',
    ];
    let csvContent = headers.join(',') + '\n';

    groups.forEach((group) => {
      let rowData: string[] = [];
      // Group ID.
      rowData.push(`"${group.proteinGroupId}"`);
      // Combine quantification values from the group row.
      const quantValues =
        this.quantificationColumns && this.quantificationColumns.length > 0
          ? this.quantificationColumns.map(
              (col) => group.quantifications[col.index],
            )
          : [];
      rowData.push(`"${quantValues.join(';')}"`);
      // Accession and Description.
      rowData.push(`"${group.representativeAccession}"`);
      rowData.push(`"${group.representativeDescription}"`);

      csvContent += rowData.join(',') + '\n';
    });

    console.log('Export Groups Only CSV:\n', csvContent);
    // Create a Blob and trigger download.
    const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'groups_only.csv';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  }

  downloadGroupsAndSubgroups(): void {
    // Filter out visible groups.
    const groups = this.groupDataSource.data.filter(
      (group) => group.isDisplayed,
    );
    console.log(
      `Export Groups and Subgroups: Found ${groups.length} visible groups.`,
    );

    if (groups.length === 0) {
      console.warn('Export Groups and Subgroups: No visible groups to export.');
      return;
    }

    // CSV header with an extra column for Subgroup ID.
    const headers = [
      'Protein Group ID',
      'Subgroup ID',
      'Quantification',
      'Accession',
      'Description',
    ];
    let csvContent = headers.join(',') + '\n';

    groups.forEach((group) => {
      // If the group has subgroups, export one row per subgroup.
      if (group.subgroups && group.subgroups.length > 0) {
        group.subgroups.forEach((subgroup) => {
          let rowData: string[] = [];
          // Group ID remains the same.
          rowData.push(`"${group.proteinGroupId}"`);
          // Extra column: Subgroup ID.
          rowData.push(`"${subgroup.proteinSubgroupId}"`);
          // Quantification: use subgroup values.
          const quantValues =
            this.quantificationColumns && this.quantificationColumns.length > 0
              ? this.quantificationColumns.map(
                  (col) => subgroup.quantifications[col.index],
                )
              : [];
          rowData.push(`"${quantValues.join(';')}"`);
          rowData.push(`"${subgroup.representativeAccession}"`);
          rowData.push(`"${subgroup.representativeDescription}"`);

          csvContent += rowData.join(',') + '\n';
        });
      } else {
        // If there are no subgroups, export the group row with an empty Subgroup ID column.
        let rowData: string[] = [];
        rowData.push(`"${group.proteinGroupId}"`);
        rowData.push(`""`); // Empty subgroup cell.
        const quantValues =
          this.quantificationColumns && this.quantificationColumns.length > 0
            ? this.quantificationColumns.map(
                (col) => group.quantifications[col.index],
              )
            : [];
        rowData.push(`"${quantValues.join(';')}"`);
        rowData.push(`"${group.representativeAccession}"`);
        rowData.push(`"${group.representativeDescription}"`);

        csvContent += rowData.join(',') + '\n';
      }
    });

    console.log('Export Groups and Subgroups CSV:\n', csvContent);
    // Create a Blob and trigger download.
    const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'groups_and_subgroups.csv';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  }

  onSelectAllGroups(): void {
    this.groupDataSource.data.map((group) => {
      group.isSelected = this.selectedAll;
    });
  }

  //makes sure to select all subgroups, if a maingroup is selected in hierarchical view
  onSelectGroup(row: ProteinMainGroup): void {
    row.subgroups.map((subgroup) => {
      subgroup.isSelected = row.isSelected;
    });
  }

  disableSelectedGroups(): void {
    const elements = document.getElementsByClassName('checkbox-selected');
    // animation doesn't work as intended right now
    // for (let i = 0; i<elements.length; i++) {
    //   let element = elements[i] as HTMLElement;
    //   element.style.opacity = "0";
    // }
    elements.length > 0
      ? setTimeout(() => {
          this.mpaTableDataService.onToggleDisableGroup(true);
        }, 1000)
      : {};


  }

  enableSelectedGroups(): void {
    const elements = document.getElementsByClassName('checkbox-selected');
    elements.length > 0
      ? this.mpaTableDataService.onToggleDisableGroup(false)
      : {};
  }

  //onGroupSelection(): void {
  //  this.selectedAll = false;
  //  this.mpaTableDataService.onGroupSelection();
  //  this.onSelectAllGroups();
  //}

  //getRowtype(row: ProteinMainGroup): string {
  //  return 'parentProteinGroupId' in row ? 'subgroup' : 'maingroup';
  //}

  downloadProteinGroupDetails(): void {
    //TODO wait for decision on batching and simplification of getProteinGroups()
  }

  downloadSelectedGroups(): void {
    //TODO wait for decision on batching and simplification of getProteinGroups()
  }

}
