export const kwDummy = {
    "accession": "root",
    "id": "root",
    "category": [],
    "children": [
        {
            "accession": "KW-9992",
            "id": "Molecular function",
            "category": [],
            "children": [
                {
                    "accession": "KW-0678",
                    "id": "Repressor",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "aa221aae-ffc9-3950-ae60-2943921ef46a",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "38a74cb1-c9ac-34e2-aec5-a461bd6814cf",
                        "64bea006-ef1b-376c-8639-c5dd6c38a563",
                        "64a1a1fc-28a6-3223-a237-a509090132ae",
                        "0ff36b3a-ba1f-3928-a34c-e9e69083cf4f",
                        "8772cb04-0f2b-3d72-93f5-83d2f328de82",
                        "958ba293-34f7-39f4-b0d0-7397b634d739",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "9a58754a-5c56-3c07-84b7-5360a962f090",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                        "a3aebedd-25dd-3dfe-9ade-83d1e8475627",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54"
                    ]
                },
                {
                    "accession": "KW-0021",
                    "id": "Allosteric enzyme",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                        "bd628120-a237-3e4e-9d75-52d98e2ea191",
                        "5209eac8-8071-3d0d-b7e3-e72045ac99b9",
                        "a4a016f7-af3c-37b2-bbb0-869da4729061",
                        "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                        "ba9ea4f2-a2be-3e4d-b65c-5a32e4fd7009",
                        "f1dee98d-c68e-3f06-869a-54029dbd5aa2",
                        "80831e58-97dc-3bfa-995d-6398bb4b3eea",
                        "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "7db61943-6cc0-36bd-a5e9-7e46e4f5de58",
                        "5dfb8cec-10b6-33ad-b722-072f815ea2e4",
                        "884a1f2f-fa42-38b2-a97a-4732ced1b997",
                        "9675ae45-136a-397a-984c-15dedd7ec1c6",
                        "03976b83-6c48-3a5e-a899-01e09d2fb723",
                        "73f9cc84-4db1-3636-90c2-5f1a7990353e",
                        "9f6606c2-e93c-3ac7-9fdc-60dff2f54955",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65"
                    ]
                },
                {
                    "accession": "KW-0010",
                    "id": "Activator",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "9a58754a-5c56-3c07-84b7-5360a962f090",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "2438b215-94d2-3e1e-8722-63acfe9ba76f",
                        "893a3274-dc45-3f91-b256-983a83c8b153",
                        "a3aebedd-25dd-3dfe-9ade-83d1e8475627",
                        "64a1a1fc-28a6-3223-a237-a509090132ae",
                        "a681313b-a131-3404-a255-6ff70a8ea5db",
                        "958ba293-34f7-39f4-b0d0-7397b634d739"
                    ]
                },
                {
                    "accession": "KW-0343",
                    "id": "GTPase activation",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d6d01f40-3ba2-3c38-9616-3106ecc766cc"
                    ]
                },
                {
                    "accession": "KW-0143",
                    "id": "Chaperone",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "c36e50bc-6de3-3773-bd97-b65e8aeeab07",
                        "abc501f5-c77c-3a30-b015-13b7fa11f8ce",
                        "a32c23cc-0d6c-3bb5-a17f-484494775219",
                        "6279ec37-2219-3af3-933a-9fd2b8d54074",
                        "fa0d3c94-9697-3f67-b435-b8aef0a96a5a",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "d2a66d40-466d-3cd7-824e-b1c172bed1e1",
                        "bfaf87cb-327c-3863-922f-5b21037726d2",
                        "3ebfa9ba-a4ef-3004-8b2d-24c105fbd7db",
                        "603148ec-4d7d-3bec-9a06-3da63c3730d0",
                        "be175fae-90c6-3d4e-a517-42b3a98a8abe",
                        "78e337f5-5c30-38db-a09d-8c2c35083444",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "ccdb8a84-5e15-3840-87f4-9a2366f7bc56",
                        "4305fb19-88a4-3d68-9990-01842b2345ea",
                        "14aed608-b903-3874-a9e8-61e7f4f44d91",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "7494b152-dbc6-395d-abdd-1b549511a731",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                        "fbe5b850-e94f-3077-af8a-dd514946b672",
                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                        "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                        "a8caa264-b568-3bad-9e7a-13fb2c1eb459",
                        "cdf158db-6f74-3ade-84ab-6e18b09bab2a",
                        "33ddad67-eb91-398c-965b-de74be7d1547",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                    ]
                },
                {
                    "accession": "KW-0049",
                    "id": "Antioxidant",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "d0a863af-2c08-32bb-a635-085fb5147e18",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "07e6492c-255a-3b65-8400-bb99932324d2"
                    ]
                },
                {
                    "accession": "KW-0511",
                    "id": "Multifunctional enzyme",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1bec76be-687a-3641-8a3a-114017a4ad6f",
                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "b314da77-03a0-3453-babc-0e706147208f",
                        "93ad988d-60fa-32a7-b4eb-6f08673a912f",
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "982e8fe6-4b3c-3e5a-9ad4-4dbbd2c9d022",
                        "8c74d913-e2fc-37c6-a6d9-2d97771b0e31",
                        "3a1dc7b1-7eef-302a-8d65-c1b776272a30",
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                        "df67d6cd-1f4d-3caa-94e7-bea09d5c729e",
                        "92cbdcce-3849-3418-9dd9-edf958868746",
                        "5364d153-bc0b-39b7-aabd-d129e885ca0a",
                        "90996f0f-25a2-3513-b36d-a389a5d2166d"
                    ]
                },
                {
                    "accession": "KW-0731",
                    "id": "Sigma factor",
                    "category": [
                        "Molecular function",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "76c71bc8-c46d-3acc-ad17-d1ca3625783a"
                    ]
                },
                {
                    "accession": "KW-0560",
                    "id": "Oxidoreductase",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0223",
                            "id": "Dioxygenase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                                "3f062636-bed0-3f06-ba22-e81645b5893b"
                            ]
                        },
                        {
                            "accession": "KW-0575",
                            "id": "Peroxidase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0376",
                                    "id": "Hydrogen peroxide",
                                    "category": [
                                        "Molecular function",
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "589687f2-9044-364c-aaac-707b0f97823c",
                                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c"
                                    ]
                                }
                            ],
                            "proteinIds": [
                                "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                                "589687f2-9044-364c-aaac-707b0f97823c",
                                "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                                "d0a863af-2c08-32bb-a635-085fb5147e18",
                                "41f8e0c9-6167-3fdf-b413-51b3b1031efe",
                                "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                                "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                                "676d3538-9c27-340d-af8c-dec20faf357b",
                                "07e6492c-255a-3b65-8400-bb99932324d2"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0378",
                    "id": "Hydrolase",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0719",
                            "id": "Serine esterase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "18f7f6b8-26d0-397f-af46-71b3e2fa2ea8"
                            ]
                        },
                        {
                            "accession": "KW-0326",
                            "id": "Glycosidase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "d4bc5910-f71b-3ea9-b946-28450da93565",
                                "6d505dc1-74f4-3a3d-a659-b5f22e58656d",
                                "4086e8f6-19f3-3e85-876e-0cf86edea088",
                                "bca15919-3309-3168-839d-caf318718843"
                            ]
                        },
                        {
                            "accession": "KW-0645",
                            "id": "Protease",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0720",
                                    "id": "Serine protease",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "ce48f6ed-2f5e-3052-95b4-c46400f3e1d6",
                                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4",
                                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                                        "c80cda9e-fd4d-38c1-9240-3bd0ab9e6143"
                                    ]
                                },
                                {
                                    "accession": "KW-0224",
                                    "id": "Dipeptidase",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "20366e64-78e7-38e8-9158-29d148f86506",
                                        "65d5288c-2db8-322e-8e4e-8f0eb59d4d79"
                                    ]
                                },
                                {
                                    "accession": "KW-0888",
                                    "id": "Threonine protease",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "bd628120-a237-3e4e-9d75-52d98e2ea191"
                                    ]
                                },
                                {
                                    "accession": "KW-0121",
                                    "id": "Carboxypeptidase",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "2cf69a4d-d912-302f-930d-4313ca35a9d3"
                                    ]
                                },
                                {
                                    "accession": "KW-0031",
                                    "id": "Aminopeptidase",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "275512af-b219-34ae-bcc9-a9a502193acd",
                                        "cc539b5e-aec6-36aa-b132-f5766adb2d13",
                                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                                        "6ee152a5-a913-3982-9baf-e71b6237e54c",
                                        "265d8170-54fc-33ea-846c-9c4d888ed0e2",
                                        "c168653e-3c8d-3bb3-9f52-032fea2ce506"
                                    ]
                                },
                                {
                                    "accession": "KW-0482",
                                    "id": "Metalloprotease",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                        "275512af-b219-34ae-bcc9-a9a502193acd",
                                        "cc539b5e-aec6-36aa-b132-f5766adb2d13",
                                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                                        "20366e64-78e7-38e8-9158-29d148f86506",
                                        "56ef52fc-84e7-3855-a588-dd43eb465591",
                                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                                        "5a887edb-44ab-3e1a-b8c5-115a9fa91f79",
                                        "65d5288c-2db8-322e-8e4e-8f0eb59d4d79",
                                        "d29caf3d-891e-37fc-806b-e50965ca02db",
                                        "c638711c-bdde-33be-a890-efdd9a8123be"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0347",
                            "id": "Helicase",
                            "category": [
                                "Ligand",
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "1f2107a6-54c7-349a-9ecb-0304f17fb7f7",
                                "e130092e-4136-3244-a836-2f098cdd0a85"
                            ]
                        },
                        {
                            "accession": "KW-0540",
                            "id": "Nuclease",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0269",
                                    "id": "Exonuclease",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "cf7b4c0c-5e3e-35ed-80de-4e6a56a14bee",
                                        "2d260263-5040-3f64-8349-e31eaa98ca1d",
                                        "cc0fe12b-f306-32df-bd3d-635784fe57c9",
                                        "b32cef3a-9da0-3b0d-b90e-b1895645e93a",
                                        "afb5765c-5d4f-354c-92be-d11f02153167",
                                        "0f0feee8-b183-3f8c-8fdd-4ac0df27ae16"
                                    ]
                                },
                                {
                                    "accession": "KW-0255",
                                    "id": "Endonuclease",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "a6ae3b93-949f-3b83-a1f4-8a1c29a8077d",
                                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                                        "83b737ff-b679-380a-a9c0-d56ed58ff728",
                                        "10ad4b48-fcee-3460-a4d5-710b2cf281c5"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-1278",
                    "id": "Translocase",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "24e689b0-03b1-3408-a49a-151692cf629c",
                        "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                        "df67d6cd-1f4d-3caa-94e7-bea09d5c729e",
                        "5eedc69d-cc8a-394a-ac34-31061697c90c",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                        "87a0407f-808f-3b21-b2ac-263219666221"
                    ]
                },
                {
                    "accession": "KW-0396",
                    "id": "Initiation factor",
                    "category": [
                        "Molecular function",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "c9697069-9494-3aca-92c7-21498e70a573",
                        "bf16af9c-8140-3c4c-8f16-7b4d9487a2d0"
                    ]
                },
                {
                    "accession": "KW-0413",
                    "id": "Isomerase",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0799",
                            "id": "Topoisomerase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "162027c6-2ad1-36be-ba31-257d5026d55d",
                                "16725ace-e627-3252-ae2a-45c4c783d2b6",
                                "76463a0c-c647-39ea-b9e9-12ca32648522"
                            ]
                        },
                        {
                            "accession": "KW-0697",
                            "id": "Rotamase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "70d716e7-2e21-3fc7-9d5a-eaf73bac2f60",
                                "806279ae-507c-35c6-bffa-477f138c7919",
                                "be175fae-90c6-3d4e-a517-42b3a98a8abe",
                                "3279d575-a3d1-3c30-8518-47f89440205b",
                                "f49aebeb-90a4-3d3a-b26b-adae55e4200e",
                                "9b645bb4-da5c-34a3-a6dc-2063b5c3faf4",
                                "d846b344-6c74-3696-b0da-84764ee91886",
                                "6ed557e6-3f94-30a4-87a0-ba7bc8dff3e8"
                            ]
                        }
                    ],
                    "proteinIds": [
                        "6279ec37-2219-3af3-933a-9fd2b8d54074",
                        "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                        "d42624d4-9e07-3165-8a8c-e2c4357ecf2d",
                        "327c877d-6f5c-3e3c-9044-d7e2ffeb1d4e",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "12c49188-e280-38b3-9c95-08073edea871",
                        "224c9b5c-ddbe-3cce-94d4-b0d9efb673f8",
                        "76463a0c-c647-39ea-b9e9-12ca32648522",
                        "b27dd7db-316f-3eb6-a914-64fc033cd01c",
                        "16b312fe-c87b-3fba-b087-d72530affc71",
                        "be175fae-90c6-3d4e-a517-42b3a98a8abe",
                        "fa03fcb6-1a8e-3c1b-9b91-fad4d162279e",
                        "f49aebeb-90a4-3d3a-b26b-adae55e4200e",
                        "5de89c9e-69d5-346a-8e30-928078064499",
                        "fc22b497-2a0a-39ca-915b-8037852bb5a9",
                        "6ed557e6-3f94-30a4-87a0-ba7bc8dff3e8",
                        "db79e4db-ecd2-3e0c-ba77-36850a23e810",
                        "a48afbb2-e862-3406-9018-8d89fb0f7d0e",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "98822156-c91c-3e15-9fc3-94de8e11cfce",
                        "b44f9392-66db-3350-9732-6798f5f7f138",
                        "37cc44e3-1053-3f01-ba4b-b7cee5d65c85",
                        "9c564811-95c0-3848-87bc-132b90327655",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "18d90788-23e9-3bd4-a17e-dbb417129a72",
                        "f02c85ff-e778-3203-a229-1bb5cd740ffe",
                        "70d716e7-2e21-3fc7-9d5a-eaf73bac2f60",
                        "806279ae-507c-35c6-bffa-477f138c7919",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "9b645bb4-da5c-34a3-a6dc-2063b5c3faf4",
                        "c93a72a2-8bd0-3165-9bc2-221b16efd538",
                        "362ca19e-c97b-3ac5-a5bd-34a13e2a82da",
                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                        "3651de6c-1487-3148-bbd8-963d04a47b07"
                    ]
                },
                {
                    "accession": "KW-0251",
                    "id": "Elongation factor",
                    "category": [
                        "Molecular function",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "8a819b13-19a3-30a3-a369-306016a392b8",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "3ba5ec97-1eda-3dc5-b5c1-c093a5df4e11",
                        "5ed92216-217c-3fca-9f10-3a2d101359d4",
                        "5c95adf3-bad4-3a4f-b76b-b94204974137"
                    ]
                },
                {
                    "accession": "KW-0646",
                    "id": "Protease inhibitor",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0722",
                            "id": "Serine protease inhibitor",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "52f8ff6d-7ba8-3d7b-a836-b845ad79c30c"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0267",
                    "id": "Excision nuclease",
                    "category": [
                        "Molecular function",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1c7ba006-0968-38fb-81e4-325187699247"
                    ]
                },
                {
                    "accession": "KW-0687",
                    "id": "Ribonucleoprotein",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0733",
                            "id": "Signal recognition particle",
                            "category": [
                                "Cellular component",
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "dc53afad-12a0-3784-858f-00d211e93e90"
                            ]
                        },
                        {
                            "accession": "KW-0689",
                            "id": "Ribosomal protein",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "e0f63aa8-74b2-38a4-89d3-59851efe3e90",
                                "fca3d6d1-8d39-3b02-98ec-fea1adc85eee",
                                "98f61553-7815-342b-a2a8-6925403a8f47",
                                "a9eb05a1-16a7-34d3-9881-e542cf92b1be",
                                "4363326f-335e-3c29-87d8-e609b33aa633",
                                "85d82584-bc67-314d-bc63-ee6ceabd5751",
                                "bb3e0d7c-f8c7-3e99-9b25-5695bc36b470",
                                "9d612962-6f9f-307a-8ce6-0eb3fa40b527",
                                "1946a92e-91ee-3dd8-9b68-dd04d4cc8747",
                                "21ff7290-df02-3585-8c7a-7e36a8c2ec66",
                                "dbe6bebe-e855-3c80-bfd1-f9fd03723cdc",
                                "75dc3891-3038-3ce3-a8c6-078ff4f868d1",
                                "eb3b97ed-1353-3ca6-9682-58f44c46e3b4",
                                "7e83a2f8-447b-3a95-9eb0-26f7a47d4917",
                                "fde9fa85-e2bb-35ca-a2b2-d6f368888a0a",
                                "f4f08edd-306f-348c-81f9-dd6169c602a6",
                                "79ee4cfd-2559-3648-b645-a5993f85b1d1",
                                "8db149aa-24ca-333f-b58a-d37447a8e174",
                                "e6c4cddd-b12f-3fb6-b2e0-d7a24acabdd7",
                                "1a6377b3-ef5b-3fc6-b96a-ffa39279224a",
                                "4d776f2b-ecc4-35db-8210-658806b3f9df",
                                "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                                "aa8c8bde-afb5-301d-ac2f-4e8ac5114d55",
                                "269efc68-9247-30e5-8d96-bb26e1d8210a",
                                "aa221aae-ffc9-3950-ae60-2943921ef46a",
                                "d6e069e7-f59d-3a6e-9d5c-cea42b8ae93a",
                                "f2a48cf6-2f93-3b72-a86a-7a3f4e4c92ad",
                                "7db5f0a9-ed08-3687-bd02-3064e70bb876",
                                "1b64f96e-17b3-332b-98d4-f8cfe5373512",
                                "3a22854b-60f2-3397-a949-92484f1a8b31",
                                "17619cac-d879-3734-b1e7-bcc936a920d2",
                                "f3f9af9f-0f3b-3a9e-8331-9b1573a90dfc",
                                "32db77da-fcb2-34bb-b2dc-be9329a202fe",
                                "56aa23d4-e137-382c-b428-a6ee5372d2cc",
                                "f5a64710-e99e-3f58-ad30-566f0129af5e",
                                "159e6ede-332e-3763-b3b5-6f0b925339f5",
                                "527e4333-49d6-3445-a6c1-224986f9ef24",
                                "015d00c9-b7f7-3289-ba9e-bd700c9a9d69",
                                "d44845a0-2f8e-359d-9e9b-327b0534f8f6",
                                "b3f2d60e-9479-362b-a013-fc0afb0be596",
                                "951984a2-4105-3eca-b517-09a149fd5e19",
                                "33e7dc7d-11d7-3539-b198-95ea6c933e23",
                                "211eeaac-246f-353b-a7ca-2ccd0477fe3b",
                                "5e26b926-1841-3edf-83f8-86e196ad600b",
                                "a7d6f728-c27f-3151-a16c-a9b508e75108",
                                "4c31ca78-7dff-3e89-b9a1-2d5731b91705",
                                "b8dda3d5-2230-38c5-a7ea-5f3ca22e9fbc",
                                "af999efc-bd0a-3375-832b-f21906bd585c",
                                "88dcf3c6-3abf-3783-b33a-f222cd11271c",
                                "f0974d30-d783-3f4b-92b2-7c2f40e89ca4"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0808",
                    "id": "Transferase",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0328",
                            "id": "Glycosyltransferase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "3f6a3d6f-f223-3a91-bb28-445f5e4c327c",
                                "1ecf1123-362c-35c4-a234-eb46e078ae1c",
                                "2803dfeb-0ae7-3d64-9845-703d52f5fbb8",
                                "ab622b9a-421a-3a59-a5fe-ff745ba0d578",
                                "e4686fc4-66af-3047-9254-e77709e9fcd6",
                                "72687302-febc-300a-8b6a-aa54e09d2bef",
                                "7db61943-6cc0-36bd-a5e9-7e46e4f5de58",
                                "fc16d218-6350-3e2e-9c9c-05fefd49e5a3",
                                "03976b83-6c48-3a5e-a899-01e09d2fb723",
                                "73f9cc84-4db1-3636-90c2-5f1a7990353e",
                                "bf533f2a-ef1f-3d05-b6d0-754109696a77",
                                "ade07610-2156-3b77-af52-71aaa33a8a32",
                                "11a22032-f695-3d84-9446-133a246dcd2f"
                            ]
                        },
                        {
                            "accession": "KW-0418",
                            "id": "Kinase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "aff29f09-97aa-3745-82d5-95a81522ee3f",
                                "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                                "91bd43f5-ae68-3b80-aa75-7a9c967b9398",
                                "19579ce4-0a7e-389d-beaf-ceb415f215e8",
                                "54fef755-2ef7-3990-9656-2610b89c5009",
                                "b364e47c-2699-3e4e-943e-4d21553d6140",
                                "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                                "8c74d913-e2fc-37c6-a6d9-2d97771b0e31",
                                "124c6ffa-0fb6-38b2-b175-247458f097f9",
                                "b60ea458-1a31-3a7e-af6d-1bb8a1af8d12",
                                "5dfb8cec-10b6-33ad-b722-072f815ea2e4",
                                "76d373ba-0068-3d85-894e-2559dd07a94a",
                                "4231711d-4aa5-3b4f-b679-3a29453a8390",
                                "c3a5104c-6764-35d8-9332-10aa5391f2f8",
                                "9f6606c2-e93c-3ac7-9fdc-60dff2f54955",
                                "29dbb1a1-17a0-3251-a3ad-258fc2b25495",
                                "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                                "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                                "5209eac8-8071-3d0d-b7e3-e72045ac99b9",
                                "8bfa1252-1f9b-3043-93d8-f5639d9cf074",
                                "33b4c0cf-74a0-325e-a1d3-086af7ba8f90",
                                "504bbf4f-f5da-3e94-966a-0a3a6e793cfe",
                                "8d98038c-6dec-3415-91e1-26fe011fc089",
                                "5c9e9fca-f450-396d-87a0-2972c30b517c",
                                "d3380e9e-3ca9-30d9-9fbe-d22064b59906",
                                "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                                "1c961799-c8e6-3571-b63c-81642cf2ab91",
                                "dad5c340-4a7c-3b50-9851-087af9277516",
                                "cafca265-a3bf-3afb-a1ec-32aea9845b77",
                                "e6f72b1a-051d-3cd0-8205-145dd9f16f65"
                            ]
                        },
                        {
                            "accession": "KW-0012",
                            "id": "Acyltransferase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "e7d1fcbc-7893-3f9f-b547-6c58816c821b",
                                "d1085cd5-55e3-3f30-9c78-69b9c267bdd5",
                                "278f5526-d165-387c-a9bb-f3981199b3ac",
                                "67d5473b-2d87-339e-8eb3-8dacbf198014",
                                "d6474761-a2d8-3278-bd17-c08775c8d77c",
                                "45663484-0ecc-3c24-be1f-fa2773ca7012",
                                "dbf5255d-b46f-3ff6-af72-b63754671791",
                                "cdf8527e-4138-3172-9447-a37f11f48ad1",
                                "4f77b69d-5001-3a9f-8759-0c12d8fa535f",
                                "24875adb-4d25-30b6-9a24-948f620f7f72",
                                "982e8fe6-4b3c-3e5a-9ad4-4dbbd2c9d022",
                                "741fa666-fa9f-3650-990b-c1458057cfdf",
                                "25774368-eecd-3e1b-85be-65bb8277cfaa",
                                "05afc641-0745-31bb-bc3c-ff9689e350ff",
                                "77b25841-c09c-36dd-bfbc-cfeff60a4a30",
                                "71a490c7-33e5-39df-bd3b-5b5c0a876a35"
                            ]
                        },
                        {
                            "accession": "KW-0032",
                            "id": "Aminotransferase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "dea33edd-752f-3ad5-8e6b-7172e0c08b5f",
                                "1d3a670d-1bfa-3117-9025-fba33bbc4a4c",
                                "378fb80b-3f49-35db-a3a3-09d2cca355f1",
                                "81c0dd1a-7823-3d99-a23c-d0ae419b6e0f",
                                "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                                "84044b10-cc38-3dc6-b5a1-3d1ae107b0b9",
                                "3d0070cc-e1db-31f6-8f49-580883bc373d",
                                "4027df06-664f-319e-a3d3-bfb1459894cf"
                            ]
                        },
                        {
                            "accession": "KW-0548",
                            "id": "Nucleotidyltransferase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0239",
                                    "id": "DNA-directed DNA polymerase",
                                    "category": [
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4",
                                        "b32cef3a-9da0-3b0d-b90e-b1895645e93a"
                                    ]
                                }
                            ],
                            "proteinIds": [
                                "a30532fe-425b-31bf-a47c-fa9b90939faa",
                                "5a6db542-70d3-3f1e-8bfa-723559a235e2",
                                "0b0bf4f1-305f-3216-953b-fac86a8062a8",
                                "456bfd92-36c5-3fd9-8803-aed14a8ec2d0",
                                "82b18d47-2bab-379a-8c6f-7c73093e156d",
                                "f1dee98d-c68e-3f06-869a-54029dbd5aa2",
                                "aee8e5d8-a562-367c-aef5-9667b804c2ed",
                                "4181cf83-cf26-3a2f-acc1-8adef347d76c",
                                "4958103b-c713-31f7-956b-ffe93334285c",
                                "8c74d913-e2fc-37c6-a6d9-2d97771b0e31",
                                "82023d76-0233-39ac-8726-4b5f659b2e18",
                                "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4",
                                "b32cef3a-9da0-3b0d-b90e-b1895645e93a"
                            ]
                        },
                        {
                            "accession": "KW-0489",
                            "id": "Methyltransferase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "09f9c8a9-8471-372e-ae8a-4f00a1503757",
                                "0daba42f-9cb1-320c-95ca-742c40e802af",
                                "5be94738-4dd2-3fa2-92a2-508e53a21ef7",
                                "b81ec75a-547b-3fc9-b14c-0fc07e7e9f7f",
                                "188a9c3f-c403-3be2-a94c-df304e4a6cb2",
                                "1a5b4cfd-7c25-3375-86ea-0f23a4f8d07d",
                                "eaaada09-fde0-346f-8595-8c91e4029379",
                                "b77f1138-e788-395b-a99d-d42a9de71400"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0626",
                    "id": "Porin",
                    "category": [
                        "Domain",
                        "Molecular function",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "b6ec19d9-dd21-3506-899a-f675868a1b58"
                    ]
                },
                {
                    "accession": "KW-0436",
                    "id": "Ligase",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0030",
                            "id": "Aminoacyl-tRNA synthetase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "7e88afdd-0d0a-3c72-876b-8ed02b3c7385",
                                "da7e0901-e128-3c06-bb26-9c7d371b992b",
                                "283f56ac-866e-3f77-aea1-e5027be10b57",
                                "96efecbb-175f-371e-97b4-badca3f602a9",
                                "3823599b-bfe6-3544-824c-6b0b0820909c",
                                "94d890c8-500b-33dc-89b6-7543b299a8ec",
                                "94de208d-a804-3a38-9f33-570c2fb3ac2d",
                                "2455e392-b06f-30d6-ba60-f5dde5dbac53",
                                "b0e3a6ee-8141-3d32-8a4a-e3b08c47a7ac",
                                "2edc2c6f-ef1d-35dd-bc17-edb06208498b",
                                "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                                "fde8f8b6-cc11-31c3-ae29-baf3aac96d3e",
                                "8ce280e3-65c9-3634-8594-3ffe355b2d58",
                                "9bfb277b-5f3a-3a91-b152-4f312c6fb860",
                                "49d56ab9-28be-353c-921c-5ab492835eaf",
                                "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                                "5428c38a-6a43-31cd-885e-19ad69956708",
                                "4f3be65d-3506-3cd4-a538-ea72f62baa95",
                                "38ca34b1-ed35-38ea-98cb-6d66102b10c6",
                                "6ad37a60-5968-37cf-a753-b39ddd19e379",
                                "4f44ee31-5bb3-3865-8752-46f599e9eeed",
                                "2b152b67-d38b-3ef1-a9de-4703d23adf22",
                                "9f714626-b769-35ac-aafd-600c07ba1299",
                                "2d207fb8-1116-3415-92ee-b7ee964cf312",
                                "681af775-1575-3a11-9b37-bcd4ed5ebe76",
                                "51eed60c-eceb-3511-8506-03c78bfaebed"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0456",
                    "id": "Lyase",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0210",
                            "id": "Decarboxylase",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "b640034e-80a9-37d0-aa8b-98b1180e4740",
                                "2b4f5dc3-75d1-35b3-abcd-059ce2986a5b",
                                "5532cc3e-59d5-358b-8546-2ce4b04f665f",
                                "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                                "c11af8cb-9a5d-3df5-a3e0-2e64908817c1",
                                "0c4b65c4-4498-3227-8121-0287e17540e2",
                                "36a9a461-0ce7-3e5e-bc50-08059008fecd",
                                "67322d9e-fd74-3328-ae1f-0f9c34caf12f",
                                "202dc8fc-4a86-3ba6-a131-35dd9fb94db3"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0238",
                    "id": "DNA-binding",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                        "ff9f96d7-f456-39da-8a23-a25a01e0c64d",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "38a74cb1-c9ac-34e2-aec5-a461bd6814cf",
                        "64bea006-ef1b-376c-8639-c5dd6c38a563",
                        "64a1a1fc-28a6-3223-a237-a509090132ae",
                        "76463a0c-c647-39ea-b9e9-12ca32648522",
                        "a681313b-a131-3404-a255-6ff70a8ea5db",
                        "1c7ba006-0968-38fb-81e4-325187699247",
                        "958ba293-34f7-39f4-b0d0-7397b634d739",
                        "603148ec-4d7d-3bec-9a06-3da63c3730d0",
                        "59ec5d46-b9f3-3d5f-8929-7c10221bdeba",
                        "76c71bc8-c46d-3acc-ad17-d1ca3625783a",
                        "9a58754a-5c56-3c07-84b7-5360a962f090",
                        "51aaf3dc-b27b-37b8-b84c-06c1895d0b3c",
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                        "2438b215-94d2-3e1e-8722-63acfe9ba76f",
                        "893a3274-dc45-3f91-b256-983a83c8b153",
                        "a3aebedd-25dd-3dfe-9ade-83d1e8475627",
                        "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4",
                        "b748cc77-caf6-35a8-a970-94dae8798068",
                        "b32cef3a-9da0-3b0d-b90e-b1895645e93a",
                        "f95204fe-2fc7-3512-9ae4-69e673ef6f03",
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "4c68f24b-5a00-39df-abd4-c44f5bf36720",
                        "6c1b608e-bee8-33fc-b621-d63906ff068a",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "f008373a-488a-36c1-85e9-5fdfb99fa95a",
                        "0ff36b3a-ba1f-3928-a34c-e9e69083cf4f",
                        "8772cb04-0f2b-3d72-93f5-83d2f328de82",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                        "2e51831b-d5af-3419-8685-ea29ced2c512",
                        "a7c2dce1-0c2f-334d-a2b8-aa674aa75c8c",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "a8caa264-b568-3bad-9e7a-13fb2c1eb459",
                        "4939a1cd-d4b2-3c7d-9053-b02c16db7e41",
                        "fbfddc81-7c4c-3720-8aa0-df1f266bcd50"
                    ]
                },
                {
                    "accession": "KW-0675",
                    "id": "Receptor",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "233bc275-5a82-338e-8bb6-ecca7feb7762"
                    ]
                },
                {
                    "accession": "KW-0694",
                    "id": "RNA-binding",
                    "category": [
                        "Molecular function"
                    ],
                    "children": [
                        {
                            "accession": "KW-0820",
                            "id": "tRNA-binding",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "7e88afdd-0d0a-3c72-876b-8ed02b3c7385",
                                "da7e0901-e128-3c06-bb26-9c7d371b992b",
                                "8ce280e3-65c9-3634-8594-3ffe355b2d58",
                                "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                                "aa221aae-ffc9-3950-ae60-2943921ef46a",
                                "159e6ede-332e-3763-b3b5-6f0b925339f5",
                                "d6e069e7-f59d-3a6e-9d5c-cea42b8ae93a",
                                "94d890c8-500b-33dc-89b6-7543b299a8ec",
                                "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                                "e6c4cddd-b12f-3fb6-b2e0-d7a24acabdd7",
                                "2da1b3db-c2fd-3a86-bf9b-48e45087b1d6",
                                "1a6377b3-ef5b-3fc6-b96a-ffa39279224a",
                                "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3"
                            ]
                        },
                        {
                            "accession": "KW-0699",
                            "id": "rRNA-binding",
                            "category": [
                                "Molecular function"
                            ],
                            "children": [],
                            "proteinIds": [
                                "fca3d6d1-8d39-3b02-98ec-fea1adc85eee",
                                "56a38ea7-2f0b-3578-bfef-7f17b93bd275",
                                "aa221aae-ffc9-3950-ae60-2943921ef46a",
                                "d6e069e7-f59d-3a6e-9d5c-cea42b8ae93a",
                                "98f61553-7815-342b-a2a8-6925403a8f47",
                                "7db5f0a9-ed08-3687-bd02-3064e70bb876",
                                "3a22854b-60f2-3397-a949-92484f1a8b31",
                                "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                                "17619cac-d879-3734-b1e7-bcc936a920d2",
                                "a9eb05a1-16a7-34d3-9881-e542cf92b1be",
                                "85d82584-bc67-314d-bc63-ee6ceabd5751",
                                "bb3e0d7c-f8c7-3e99-9b25-5695bc36b470",
                                "32db77da-fcb2-34bb-b2dc-be9329a202fe",
                                "56aa23d4-e137-382c-b428-a6ee5372d2cc",
                                "1946a92e-91ee-3dd8-9b68-dd04d4cc8747",
                                "21ff7290-df02-3585-8c7a-7e36a8c2ec66",
                                "dbe6bebe-e855-3c80-bfd1-f9fd03723cdc",
                                "75dc3891-3038-3ce3-a8c6-078ff4f868d1",
                                "eb3b97ed-1353-3ca6-9682-58f44c46e3b4",
                                "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                                "fde9fa85-e2bb-35ca-a2b2-d6f368888a0a",
                                "159e6ede-332e-3763-b3b5-6f0b925339f5",
                                "015d00c9-b7f7-3289-ba9e-bd700c9a9d69",
                                "d44845a0-2f8e-359d-9e9b-327b0534f8f6",
                                "b3f2d60e-9479-362b-a013-fc0afb0be596",
                                "f4f08edd-306f-348c-81f9-dd6169c602a6",
                                "8db149aa-24ca-333f-b58a-d37447a8e174",
                                "e6c4cddd-b12f-3fb6-b2e0-d7a24acabdd7",
                                "1a6377b3-ef5b-3fc6-b96a-ffa39279224a",
                                "5e26b926-1841-3edf-83f8-86e196ad600b",
                                "4d776f2b-ecc4-35db-8210-658806b3f9df",
                                "4c31ca78-7dff-3e89-b9a1-2d5731b91705",
                                "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                                "b8dda3d5-2230-38c5-a7ea-5f3ca22e9fbc",
                                "af999efc-bd0a-3375-832b-f21906bd585c",
                                "88dcf3c6-3abf-3783-b33a-f222cd11271c",
                                "aa8c8bde-afb5-301d-ac2f-4e8ac5114d55",
                                "269efc68-9247-30e5-8d96-bb26e1d8210a"
                            ]
                        }
                    ],
                    "proteinIds": []
                }
            ],
            "proteinIds": []
        },
        {
            "accession": "KW-9997",
            "id": "Coding sequence diversity",
            "category": [],
            "children": [
                {
                    "accession": "KW-0024",
                    "id": "Alternative initiation",
                    "category": [
                        "Coding sequence diversity"
                    ],
                    "children": [],
                    "proteinIds": [
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22"
                    ]
                },
                {
                    "accession": "KW-0877",
                    "id": "Alternative promoter usage",
                    "category": [
                        "Coding sequence diversity"
                    ],
                    "children": [],
                    "proteinIds": [
                        "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4"
                    ]
                }
            ],
            "proteinIds": []
        },
        {
            "accession": "KW-9991",
            "id": "PTM",
            "category": [],
            "children": [
                {
                    "accession": "KW-0572",
                    "id": "Peptidoglycan-anchor",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "45b221a7-f65f-318a-afee-f1bec5704c51"
                    ]
                },
                {
                    "accession": "KW-1015",
                    "id": "Disulfide bond",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9e2aad1c-75e8-3a3d-b508-aa845635d7b7",
                        "d0a863af-2c08-32bb-a635-085fb5147e18",
                        "e7011909-03cc-3eb0-aef7-b8a96ce4f2ce",
                        "850143d4-9315-3889-9aad-60bd107000ef",
                        "89ded291-4348-3f8d-be43-56e42b9311d0",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                        "a87ba622-c92d-35b1-935e-52f538af07f4",
                        "17d3560c-19d3-3141-b1e2-c20ba87599ce",
                        "d1cdacd5-d00d-333b-bdba-0df200229b25",
                        "3ebfa9ba-a4ef-3004-8b2d-24c105fbd7db",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "78e337f5-5c30-38db-a09d-8c2c35083444",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                        "448e34c1-4fd2-3d95-b078-9642f7347452",
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "c4cdd1b7-0c46-3cb6-90c9-f2b2a1983a58",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "52f8ff6d-7ba8-3d7b-a836-b845ad79c30c",
                        "65a926bf-d216-3943-9809-9013a707a4ac",
                        "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                        "884a1f2f-fa42-38b2-a97a-4732ced1b997",
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "ff86e71c-2da2-3e32-83da-8fa5fc4342ad",
                        "e8c576ae-9b77-3b43-991e-d7dca04bcfd9",
                        "238b3de0-b3c2-3f07-911a-decfc93154ab"
                    ]
                },
                {
                    "accession": "KW-0013",
                    "id": "ADP-ribosylation",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "c16a16e2-43d4-3291-a840-fefd5939be2e",
                        "456bfd92-36c5-3fd9-8803-aed14a8ec2d0"
                    ]
                },
                {
                    "accession": "KW-0558",
                    "id": "Oxidation",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1bec76be-687a-3641-8a3a-114017a4ad6f",
                        "fa0d3c94-9697-3f67-b435-b8aef0a96a5a",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd"
                    ]
                },
                {
                    "accession": "KW-0874",
                    "id": "Quinone",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                        "df67d6cd-1f4d-3caa-94e7-bea09d5c729e",
                        "262dfcac-448e-3bfa-818a-ad6589081358"
                    ]
                },
                {
                    "accession": "KW-0488",
                    "id": "Methylation",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1be8f6b8-ebe6-3e6e-9851-cf096fc1f6ab",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "4c31ca78-7dff-3e89-b9a1-2d5731b91705",
                        "8db149aa-24ca-333f-b58a-d37447a8e174"
                    ]
                },
                {
                    "accession": "KW-0597",
                    "id": "Phosphoprotein",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "5cf06ce6-2b9e-3618-a775-454fd37dbb54",
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "5c3e9984-9197-37f7-8c69-deb288ba224a",
                        "54fef755-2ef7-3990-9656-2610b89c5009",
                        "124c6ffa-0fb6-38b2-b175-247458f097f9",
                        "51aaf3dc-b27b-37b8-b84c-06c1895d0b3c",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "5de89c9e-69d5-346a-8e30-928078064499",
                        "893a3274-dc45-3f91-b256-983a83c8b153",
                        "76d373ba-0068-3d85-894e-2559dd07a94a",
                        "b748cc77-caf6-35a8-a970-94dae8798068",
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "a48afbb2-e862-3406-9018-8d89fb0f7d0e",
                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                        "c43a0411-8126-3d5f-8344-bb6d01bcee7b",
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "10c00c05-61de-3dcf-8030-f0d748031d11",
                        "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                        "db66e621-4ba9-3952-8ecb-c161094c8675",
                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                    ]
                },
                {
                    "accession": "KW-0556",
                    "id": "Organic radical",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "af241637-a7d0-3f31-8284-ba8a7d712c26",
                        "4f77b69d-5001-3a9f-8759-0c12d8fa535f",
                        "24875adb-4d25-30b6-9a24-948f620f7f72"
                    ]
                },
                {
                    "accession": "KW-0596",
                    "id": "Phosphopantetheine",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "10c00c05-61de-3dcf-8030-f0d748031d11"
                    ]
                },
                {
                    "accession": "KW-0291",
                    "id": "Formylation",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "24e689b0-03b1-3408-a49a-151692cf629c",
                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388"
                    ]
                },
                {
                    "accession": "KW-0379",
                    "id": "Hydroxylation",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "8a819b13-19a3-30a3-a369-306016a392b8",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "19ba0f71-8f9b-31d9-90d8-73666790014f"
                    ]
                },
                {
                    "accession": "KW-0007",
                    "id": "Acetylation",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "7e88afdd-0d0a-3c72-876b-8ed02b3c7385",
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "d0894567-8e24-31ed-9ad2-5fc20c27125f",
                        "c1c007e5-cd0f-3bdd-9089-7ed8ba9a91b9",
                        "aff29f09-97aa-3745-82d5-95a81522ee3f",
                        "fca3d6d1-8d39-3b02-98ec-fea1adc85eee",
                        "764ab033-a4f5-3e54-b067-b601a3c64ab2",
                        "94d890c8-500b-33dc-89b6-7543b299a8ec",
                        "e9f58804-0b00-30a7-a5a0-9edb29d4dc3c",
                        "d42624d4-9e07-3165-8a8c-e2c4357ecf2d",
                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                        "456bfd92-36c5-3fd9-8803-aed14a8ec2d0",
                        "12c49188-e280-38b3-9c95-08073edea871",
                        "36a9a461-0ce7-3e5e-bc50-08059008fecd",
                        "4f77b69d-5001-3a9f-8759-0c12d8fa535f",
                        "bb3e0d7c-f8c7-3e99-9b25-5695bc36b470",
                        "19579ce4-0a7e-389d-beaf-ceb415f215e8",
                        "8c74d913-e2fc-37c6-a6d9-2d97771b0e31",
                        "5ed92216-217c-3fca-9f10-3a2d101359d4",
                        "3ae4d435-05c9-3016-9419-f906e48968a9",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "1946a92e-91ee-3dd8-9b68-dd04d4cc8747",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "73f9cc84-4db1-3636-90c2-5f1a7990353e",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "af241637-a7d0-3f31-8284-ba8a7d712c26",
                        "07e6492c-255a-3b65-8400-bb99932324d2",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752",
                        "eb3b97ed-1353-3ca6-9682-58f44c46e3b4",
                        "71a490c7-33e5-39df-bd3b-5b5c0a876a35",
                        "11a22032-f695-3d84-9446-133a246dcd2f",
                        "ce422a1e-705d-3108-8141-d9bb318b50ce",
                        "db79e4db-ecd2-3e0c-ba77-36850a23e810",
                        "fb9a8b64-0c46-3a4a-a7ca-8951cb3a41f7",
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "20366e64-78e7-38e8-9158-29d148f86506",
                        "80831e58-97dc-3bfa-995d-6398bb4b3eea",
                        "cf7b4c0c-5e3e-35ed-80de-4e6a56a14bee",
                        "6ad37a60-5968-37cf-a753-b39ddd19e379",
                        "1a6377b3-ef5b-3fc6-b96a-ffa39279224a",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                        "adec9c8c-296a-3769-9203-3969d932bc85",
                        "55adc234-ce10-3909-a3a2-04887fe6fa3b",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                        "67322d9e-fd74-3328-ae1f-0f9c34caf12f",
                        "51eed60c-eceb-3511-8506-03c78bfaebed",
                        "e7d1fcbc-7893-3f9f-b547-6c58816c821b",
                        "8b4efabd-0c5b-3f60-af7e-fd20fd31a750",
                        "5a6db542-70d3-3f1e-8bfa-723559a235e2",
                        "63370929-3862-33c6-a04f-8b8faf23d245",
                        "56a38ea7-2f0b-3578-bfef-7f17b93bd275",
                        "850143d4-9315-3889-9aad-60bd107000ef",
                        "59f12aa5-5fd7-35dc-a73e-ffc903cb995b",
                        "d2a66d40-466d-3cd7-824e-b1c172bed1e1",
                        "4fb159bd-103d-3af0-a750-ffb1d7efc7c4",
                        "93ad988d-60fa-32a7-b4eb-6f08673a912f",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "32db77da-fcb2-34bb-b2dc-be9329a202fe",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "b27dd7db-316f-3eb6-a914-64fc033cd01c",
                        "9a58754a-5c56-3c07-84b7-5360a962f090",
                        "2850d15f-f930-31e8-8764-356a1cb98b51",
                        "00926371-a557-341d-8941-45ad2fafee4b",
                        "3e6c6495-8fff-3169-846d-0e965cdd2dfe",
                        "bf16af9c-8140-3c4c-8f16-7b4d9487a2d0",
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "5adfc32c-6203-33e3-8308-e89f4358b1d3",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "a30532fe-425b-31bf-a47c-fa9b90939faa",
                        "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                        "dcfc4b07-5be4-3d8d-8d4d-72d556b4bdb7",
                        "71b366fe-e434-327c-90f6-5222f4cb5ea6",
                        "4f44ee31-5bb3-3865-8752-46f599e9eeed",
                        "5e26b926-1841-3edf-83f8-86e196ad600b",
                        "b8dda3d5-2230-38c5-a7ea-5f3ca22e9fbc",
                        "dccf3c9e-dddf-39e4-9061-2e563185c9b9",
                        "90996f0f-25a2-3513-b36d-a389a5d2166d",
                        "681af775-1575-3a11-9b37-bcd4ed5ebe76"
                    ]
                },
                {
                    "accession": "KW-0865",
                    "id": "Zymogen",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                        "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                        "0c4b65c4-4498-3227-8121-0287e17540e2",
                        "c80cda9e-fd4d-38c1-9240-3bd0ab9e6143"
                    ]
                },
                {
                    "accession": "KW-0449",
                    "id": "Lipoprotein",
                    "category": [
                        "PTM"
                    ],
                    "children": [
                        {
                            "accession": "KW-0564",
                            "id": "Palmitate",
                            "category": [
                                "PTM"
                            ],
                            "children": [],
                            "proteinIds": [
                                "45b221a7-f65f-318a-afee-f1bec5704c51",
                                "8e45ccea-21f6-38f7-b11e-aa8914e50a61",
                                "b2a03d2e-7fe3-3375-a542-95440bd78a9a",
                                "3cce71b9-bb9f-3f5f-a191-89455ca5884d",
                                "80ee5820-8165-3fc1-858f-330ea6212f00",
                                "a195423a-3a12-3931-95da-fa2d60d0a3ab"
                            ]
                        }
                    ],
                    "proteinIds": [
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "8e45ccea-21f6-38f7-b11e-aa8914e50a61",
                        "b2a03d2e-7fe3-3375-a542-95440bd78a9a",
                        "3cce71b9-bb9f-3f5f-a191-89455ca5884d",
                        "80ee5820-8165-3fc1-858f-330ea6212f00",
                        "a195423a-3a12-3931-95da-fa2d60d0a3ab"
                    ]
                },
                {
                    "accession": "KW-0068",
                    "id": "Autocatalytic cleavage",
                    "category": [
                        "PTM"
                    ],
                    "children": [],
                    "proteinIds": [
                        "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                        "0c4b65c4-4498-3227-8121-0287e17540e2"
                    ]
                }
            ],
            "proteinIds": []
        },
        {
            "accession": "KW-9993",
            "id": "Ligand",
            "category": [],
            "children": [
                {
                    "accession": "KW-0521",
                    "id": "NADP",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "ee7e3205-3af8-3742-84ba-5d1197f85c7e",
                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                        "9ce746d7-6118-32d3-89bb-4dd16adfb71a",
                        "52a834ce-1be0-32e0-b9df-5ff1b3364874",
                        "62169f87-c02b-34c1-a2ea-1adb1fec4587",
                        "1776f92b-eb74-3f0c-8d9a-c47a65663644",
                        "e6c1af58-a061-30fc-94e7-02b4ca64be72",
                        "d42624d4-9e07-3165-8a8c-e2c4357ecf2d",
                        "303b9156-9818-3779-ab63-ca148d2df81d",
                        "89ded291-4348-3f8d-be43-56e42b9311d0",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "5f414d39-9df3-3411-b4a1-7c6e35b3487a",
                        "42d49741-9a55-38f8-9fbb-f0897fb40734",
                        "f1778da0-779c-30a6-a761-8d0f864a75d5",
                        "f639e2ef-b23c-3eac-82d4-2f822e5ad56f",
                        "c9a3ff3d-57da-39c5-8e00-846dfb3dcc5c",
                        "ad28a8bf-fcd4-3132-968d-795293745983",
                        "f322a8ae-ec9a-3952-9737-150d950df3d6",
                        "88e41fc4-5017-3b85-b56d-7a3a9feda349",
                        "b5d80ba7-a45d-3460-98ef-af0d15f43c1c",
                        "b41e72a4-c433-3aa2-b623-d4acc83b24c6",
                        "260d44be-d7fa-3dca-b1d8-96019c54f35a",
                        "5bcfadc2-f07e-3d47-ab27-8c734ba3eb09",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "d24c523a-8ef4-3b91-a35a-85a55ac2da72",
                        "ff9ecfec-7cdf-3b16-9f58-df1393f56211",
                        "e465935f-f61a-353e-8542-14ab25c51637",
                        "71b3ae7c-a1eb-3390-ae68-92cf4f9f8c00",
                        "0deac7ee-d1b5-3ac9-b421-d54513714945",
                        "03bc60d2-2bbe-34fb-87c4-0413718dd28a",
                        "6214d45c-e9b8-30c3-9887-4acaee29688e",
                        "01c0a4d3-91cd-3222-b59c-2cb2c5b1bea8",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "2adc85c0-4e20-3bb3-a2f6-838caacfc474",
                        "c22a1936-08c9-38f2-b5ee-499e05b45f4b",
                        "92cbdcce-3849-3418-9dd9-edf958868746",
                        "5364d153-bc0b-39b7-aabd-d129e885ca0a",
                        "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                        "90996f0f-25a2-3513-b36d-a389a5d2166d",
                        "a5e34328-e12e-3425-add0-7428af49849e",
                        "89743da0-e8d3-3386-967f-46ec472079d8",
                        "9ec8e26f-cf5c-360f-933b-f9a69dec0212"
                    ]
                },
                {
                    "accession": "KW-0862",
                    "id": "Zinc",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "7e88afdd-0d0a-3c72-876b-8ed02b3c7385",
                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                        "3823599b-bfe6-3544-824c-6b0b0820909c",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "38a74cb1-c9ac-34e2-aec5-a461bd6814cf",
                        "94d890c8-500b-33dc-89b6-7543b299a8ec",
                        "56ef52fc-84e7-3855-a588-dd43eb465591",
                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                        "327c877d-6f5c-3e3c-9044-d7e2ffeb1d4e",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "7a19fc76-3018-3ea7-a941-ca7feb7122bf",
                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                        "97eec5eb-fd52-3f21-b970-f2ab55a49582",
                        "a6ae3b93-949f-3b83-a1f4-8a1c29a8077d",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "3a1dc7b1-7eef-302a-8d65-c1b776272a30",
                        "5dfb8cec-10b6-33ad-b722-072f815ea2e4",
                        "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                        "cde21cad-6c21-3513-bc4f-c18792eb9152",
                        "b5d80ba7-a45d-3460-98ef-af0d15f43c1c",
                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                        "7494b152-dbc6-395d-abdd-1b549511a731",
                        "db79e4db-ecd2-3e0c-ba77-36850a23e810",
                        "20366e64-78e7-38e8-9158-29d148f86506",
                        "83b737ff-b679-380a-a9c0-d56ed58ff728",
                        "7747d65c-098a-364b-8683-352e8734b7c0",
                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                        "5492ee99-1adc-3f4a-8494-70a45e6b2141",
                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                        "61d1c28a-4135-3674-af97-75e61945e833",
                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                        "41885b05-5303-35e8-b45c-b8ae67bb2a19",
                        "5364d153-bc0b-39b7-aabd-d129e885ca0a",
                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                        "a5e34328-e12e-3425-add0-7428af49849e",
                        "da7e0901-e128-3c06-bb26-9c7d371b992b",
                        "6f42f3aa-5abc-3149-9c90-8514279c3116",
                        "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                        "cc0fe12b-f306-32df-bd3d-635784fe57c9",
                        "2c242910-cb3c-3f41-b750-2dac1ebc7215",
                        "1c7ba006-0968-38fb-81e4-325187699247",
                        "fb76d466-0b71-3eca-8541-66496335aa3d",
                        "5a887edb-44ab-3e1a-b8c5-115a9fa91f79",
                        "2850d15f-f930-31e8-8764-356a1cb98b51",
                        "8a61fe9f-cea2-3471-8e8f-d8ed4543e47e",
                        "a30532fe-425b-31bf-a47c-fa9b90939faa",
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "cc539b5e-aec6-36aa-b132-f5766adb2d13",
                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                        "d24c523a-8ef4-3b91-a35a-85a55ac2da72",
                        "d44a2fd7-d351-36cf-9108-95eb20e4d3bd",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "5209eac8-8071-3d0d-b7e3-e72045ac99b9",
                        "3b479813-9750-307d-944a-7cf29d8b335c",
                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                        "acca6325-d1d2-3dea-b1f6-106e3f4038c3",
                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                        "4f44ee31-5bb3-3865-8752-46f599e9eeed",
                        "eaaada09-fde0-346f-8595-8c91e4029379",
                        "ef90ba8d-7bb1-386d-ac40-6e3764fb4b2d"
                    ]
                },
                {
                    "accession": "KW-0479",
                    "id": "Metal-binding",
                    "category": [
                        "Ligand"
                    ],
                    "children": [
                        {
                            "accession": "KW-0863",
                            "id": "Zinc-finger",
                            "category": [
                                "Ligand",
                                "Domain"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0862",
                                    "id": "Zinc",
                                    "category": [
                                        "Ligand"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "7e88afdd-0d0a-3c72-876b-8ed02b3c7385",
                                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                                        "3823599b-bfe6-3544-824c-6b0b0820909c",
                                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                                        "38a74cb1-c9ac-34e2-aec5-a461bd6814cf",
                                        "94d890c8-500b-33dc-89b6-7543b299a8ec",
                                        "56ef52fc-84e7-3855-a588-dd43eb465591",
                                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                                        "327c877d-6f5c-3e3c-9044-d7e2ffeb1d4e",
                                        "d846b344-6c74-3696-b0da-84764ee91886",
                                        "7a19fc76-3018-3ea7-a941-ca7feb7122bf",
                                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                                        "97eec5eb-fd52-3f21-b970-f2ab55a49582",
                                        "a6ae3b93-949f-3b83-a1f4-8a1c29a8077d",
                                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                                        "3a1dc7b1-7eef-302a-8d65-c1b776272a30",
                                        "5dfb8cec-10b6-33ad-b722-072f815ea2e4",
                                        "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                                        "cde21cad-6c21-3513-bc4f-c18792eb9152",
                                        "b5d80ba7-a45d-3460-98ef-af0d15f43c1c",
                                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                                        "7494b152-dbc6-395d-abdd-1b549511a731",
                                        "db79e4db-ecd2-3e0c-ba77-36850a23e810",
                                        "20366e64-78e7-38e8-9158-29d148f86506",
                                        "83b737ff-b679-380a-a9c0-d56ed58ff728",
                                        "7747d65c-098a-364b-8683-352e8734b7c0",
                                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                                        "5492ee99-1adc-3f4a-8494-70a45e6b2141",
                                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                        "61d1c28a-4135-3674-af97-75e61945e833",
                                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                                        "41885b05-5303-35e8-b45c-b8ae67bb2a19",
                                        "5364d153-bc0b-39b7-aabd-d129e885ca0a",
                                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                                        "a5e34328-e12e-3425-add0-7428af49849e",
                                        "da7e0901-e128-3c06-bb26-9c7d371b992b",
                                        "6f42f3aa-5abc-3149-9c90-8514279c3116",
                                        "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                                        "cc0fe12b-f306-32df-bd3d-635784fe57c9",
                                        "2c242910-cb3c-3f41-b750-2dac1ebc7215",
                                        "1c7ba006-0968-38fb-81e4-325187699247",
                                        "fb76d466-0b71-3eca-8541-66496335aa3d",
                                        "5a887edb-44ab-3e1a-b8c5-115a9fa91f79",
                                        "2850d15f-f930-31e8-8764-356a1cb98b51",
                                        "8a61fe9f-cea2-3471-8e8f-d8ed4543e47e",
                                        "a30532fe-425b-31bf-a47c-fa9b90939faa",
                                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                                        "cc539b5e-aec6-36aa-b132-f5766adb2d13",
                                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                                        "d24c523a-8ef4-3b91-a35a-85a55ac2da72",
                                        "d44a2fd7-d351-36cf-9108-95eb20e4d3bd",
                                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                                        "5209eac8-8071-3d0d-b7e3-e72045ac99b9",
                                        "3b479813-9750-307d-944a-7cf29d8b335c",
                                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                                        "acca6325-d1d2-3dea-b1f6-106e3f4038c3",
                                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                                        "4f44ee31-5bb3-3865-8752-46f599e9eeed",
                                        "eaaada09-fde0-346f-8595-8c91e4029379",
                                        "ef90ba8d-7bb1-386d-ac40-6e3764fb4b2d"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0001",
                            "id": "2Fe-2S",
                            "category": [
                                "Ligand"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0411",
                                    "id": "Iron-sulfur",
                                    "category": [
                                        "Ligand"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-0408",
                                            "id": "Iron",
                                            "category": [
                                                "Ligand"
                                            ],
                                            "children": [
                                                {
                                                    "accession": "KW-0409",
                                                    "id": "Iron storage",
                                                    "category": [
                                                        "Ligand",
                                                        "Biological process"
                                                    ],
                                                    "children": [],
                                                    "proteinIds": [
                                                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                        "41794e2f-1527-3aa8-a594-ce47f97a857d"
                                                    ]
                                                }
                                            ],
                                            "proteinIds": [
                                                "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                                                "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                                                "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                                                "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                                "9e5e4071-a9d1-39e3-b657-84083543f591",
                                                "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                                                "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                                                "9e01ce48-38b0-3bee-8178-f00a8b68ada0",
                                                "d3e155df-63b4-326d-95a6-a761b36e6453",
                                                "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                                                "6edaccfd-0cf9-37a7-8924-ac8da9f6b01f",
                                                "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                                                "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                                                "8fdfc5b7-72bb-357d-9e57-c9710e060f7e",
                                                "24e689b0-03b1-3408-a49a-151692cf629c",
                                                "589687f2-9044-364c-aaac-707b0f97823c",
                                                "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                                                "41794e2f-1527-3aa8-a594-ce47f97a857d",
                                                "3ae4d435-05c9-3016-9419-f906e48968a9",
                                                "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                                                "3f062636-bed0-3f06-ba22-e81645b5893b",
                                                "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                                                "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                                                "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                                "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                                                "e4f20fbf-39c5-391b-8f30-96141689d0d9",
                                                "ce422a1e-705d-3108-8141-d9bb318b50ce",
                                                "62942481-2b09-3054-8699-744b7968ce01",
                                                "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                                                "08db55e4-1db7-3bce-9bf1-770750acbb97",
                                                "af13e9f3-b720-3a88-b110-d3da50d6671d",
                                                "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                                "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                                                "65886732-974b-3f7a-a011-90682a3b3ef5",
                                                "8924b747-df2e-32fc-99fc-59d93ddb35c8",
                                                "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                                                "b3db6762-1dfb-3268-818b-228994a05da0",
                                                "f820d441-4786-3f2b-8d9e-5cf6b9f34043",
                                                "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                                                "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                                                "affccc91-69b0-350a-b326-170650a89211"
                                            ]
                                        }
                                    ],
                                    "proteinIds": []
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0349",
                            "id": "Heme",
                            "category": [
                                "Ligand"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0408",
                                    "id": "Iron",
                                    "category": [
                                        "Ligand"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-0409",
                                            "id": "Iron storage",
                                            "category": [
                                                "Ligand",
                                                "Biological process"
                                            ],
                                            "children": [],
                                            "proteinIds": [
                                                "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                "41794e2f-1527-3aa8-a594-ce47f97a857d"
                                            ]
                                        }
                                    ],
                                    "proteinIds": [
                                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                        "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                        "9e5e4071-a9d1-39e3-b657-84083543f591",
                                        "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                                        "9e01ce48-38b0-3bee-8178-f00a8b68ada0",
                                        "d3e155df-63b4-326d-95a6-a761b36e6453",
                                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                                        "6edaccfd-0cf9-37a7-8924-ac8da9f6b01f",
                                        "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                                        "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                                        "8fdfc5b7-72bb-357d-9e57-c9710e060f7e",
                                        "24e689b0-03b1-3408-a49a-151692cf629c",
                                        "589687f2-9044-364c-aaac-707b0f97823c",
                                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                                        "41794e2f-1527-3aa8-a594-ce47f97a857d",
                                        "3ae4d435-05c9-3016-9419-f906e48968a9",
                                        "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                                        "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                        "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                                        "e4f20fbf-39c5-391b-8f30-96141689d0d9",
                                        "ce422a1e-705d-3108-8141-d9bb318b50ce",
                                        "62942481-2b09-3054-8699-744b7968ce01",
                                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                        "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                                        "08db55e4-1db7-3bce-9bf1-770750acbb97",
                                        "af13e9f3-b720-3a88-b110-d3da50d6671d",
                                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                                        "65886732-974b-3f7a-a011-90682a3b3ef5",
                                        "8924b747-df2e-32fc-99fc-59d93ddb35c8",
                                        "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                                        "b3db6762-1dfb-3268-818b-228994a05da0",
                                        "f820d441-4786-3f2b-8d9e-5cf6b9f34043",
                                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                                        "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                                        "affccc91-69b0-350a-b326-170650a89211"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0411",
                            "id": "Iron-sulfur",
                            "category": [
                                "Ligand"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0003",
                                    "id": "3Fe-4S",
                                    "category": [
                                        "Ligand"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-0408",
                                            "id": "Iron",
                                            "category": [
                                                "Ligand"
                                            ],
                                            "children": [
                                                {
                                                    "accession": "KW-0409",
                                                    "id": "Iron storage",
                                                    "category": [
                                                        "Ligand",
                                                        "Biological process"
                                                    ],
                                                    "children": [],
                                                    "proteinIds": [
                                                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                        "41794e2f-1527-3aa8-a594-ce47f97a857d"
                                                    ]
                                                }
                                            ],
                                            "proteinIds": [
                                                "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                                                "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                                                "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                                                "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                                "9e5e4071-a9d1-39e3-b657-84083543f591",
                                                "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                                                "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                                                "9e01ce48-38b0-3bee-8178-f00a8b68ada0",
                                                "d3e155df-63b4-326d-95a6-a761b36e6453",
                                                "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                                                "6edaccfd-0cf9-37a7-8924-ac8da9f6b01f",
                                                "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                                                "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                                                "8fdfc5b7-72bb-357d-9e57-c9710e060f7e",
                                                "24e689b0-03b1-3408-a49a-151692cf629c",
                                                "589687f2-9044-364c-aaac-707b0f97823c",
                                                "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                                                "41794e2f-1527-3aa8-a594-ce47f97a857d",
                                                "3ae4d435-05c9-3016-9419-f906e48968a9",
                                                "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                                                "3f062636-bed0-3f06-ba22-e81645b5893b",
                                                "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                                                "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                                                "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                                "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                                                "e4f20fbf-39c5-391b-8f30-96141689d0d9",
                                                "ce422a1e-705d-3108-8141-d9bb318b50ce",
                                                "62942481-2b09-3054-8699-744b7968ce01",
                                                "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                                                "08db55e4-1db7-3bce-9bf1-770750acbb97",
                                                "af13e9f3-b720-3a88-b110-d3da50d6671d",
                                                "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                                "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                                                "65886732-974b-3f7a-a011-90682a3b3ef5",
                                                "8924b747-df2e-32fc-99fc-59d93ddb35c8",
                                                "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                                                "b3db6762-1dfb-3268-818b-228994a05da0",
                                                "f820d441-4786-3f2b-8d9e-5cf6b9f34043",
                                                "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                                                "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                                                "affccc91-69b0-350a-b326-170650a89211"
                                            ]
                                        }
                                    ],
                                    "proteinIds": []
                                },
                                {
                                    "accession": "KW-0408",
                                    "id": "Iron",
                                    "category": [
                                        "Ligand"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-0409",
                                            "id": "Iron storage",
                                            "category": [
                                                "Ligand",
                                                "Biological process"
                                            ],
                                            "children": [],
                                            "proteinIds": [
                                                "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                "41794e2f-1527-3aa8-a594-ce47f97a857d"
                                            ]
                                        }
                                    ],
                                    "proteinIds": [
                                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                        "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                        "9e5e4071-a9d1-39e3-b657-84083543f591",
                                        "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                                        "9e01ce48-38b0-3bee-8178-f00a8b68ada0",
                                        "d3e155df-63b4-326d-95a6-a761b36e6453",
                                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                                        "6edaccfd-0cf9-37a7-8924-ac8da9f6b01f",
                                        "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                                        "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                                        "8fdfc5b7-72bb-357d-9e57-c9710e060f7e",
                                        "24e689b0-03b1-3408-a49a-151692cf629c",
                                        "589687f2-9044-364c-aaac-707b0f97823c",
                                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                                        "41794e2f-1527-3aa8-a594-ce47f97a857d",
                                        "3ae4d435-05c9-3016-9419-f906e48968a9",
                                        "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                                        "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                        "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                                        "e4f20fbf-39c5-391b-8f30-96141689d0d9",
                                        "ce422a1e-705d-3108-8141-d9bb318b50ce",
                                        "62942481-2b09-3054-8699-744b7968ce01",
                                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                        "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                                        "08db55e4-1db7-3bce-9bf1-770750acbb97",
                                        "af13e9f3-b720-3a88-b110-d3da50d6671d",
                                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                                        "65886732-974b-3f7a-a011-90682a3b3ef5",
                                        "8924b747-df2e-32fc-99fc-59d93ddb35c8",
                                        "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                                        "b3db6762-1dfb-3268-818b-228994a05da0",
                                        "f820d441-4786-3f2b-8d9e-5cf6b9f34043",
                                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                                        "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                                        "affccc91-69b0-350a-b326-170650a89211"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0004",
                            "id": "4Fe-4S",
                            "category": [
                                "Ligand"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0411",
                                    "id": "Iron-sulfur",
                                    "category": [
                                        "Ligand"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-0408",
                                            "id": "Iron",
                                            "category": [
                                                "Ligand"
                                            ],
                                            "children": [
                                                {
                                                    "accession": "KW-0409",
                                                    "id": "Iron storage",
                                                    "category": [
                                                        "Ligand",
                                                        "Biological process"
                                                    ],
                                                    "children": [],
                                                    "proteinIds": [
                                                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                        "41794e2f-1527-3aa8-a594-ce47f97a857d"
                                                    ]
                                                }
                                            ],
                                            "proteinIds": [
                                                "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                                                "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                                                "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                                "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                                                "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                                "9e5e4071-a9d1-39e3-b657-84083543f591",
                                                "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                                                "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                                                "9e01ce48-38b0-3bee-8178-f00a8b68ada0",
                                                "d3e155df-63b4-326d-95a6-a761b36e6453",
                                                "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                                                "6edaccfd-0cf9-37a7-8924-ac8da9f6b01f",
                                                "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                                                "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                                                "8fdfc5b7-72bb-357d-9e57-c9710e060f7e",
                                                "24e689b0-03b1-3408-a49a-151692cf629c",
                                                "589687f2-9044-364c-aaac-707b0f97823c",
                                                "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                                                "41794e2f-1527-3aa8-a594-ce47f97a857d",
                                                "3ae4d435-05c9-3016-9419-f906e48968a9",
                                                "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                                                "3f062636-bed0-3f06-ba22-e81645b5893b",
                                                "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                                                "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                                                "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                                "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                                                "e4f20fbf-39c5-391b-8f30-96141689d0d9",
                                                "ce422a1e-705d-3108-8141-d9bb318b50ce",
                                                "62942481-2b09-3054-8699-744b7968ce01",
                                                "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                                "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                                                "08db55e4-1db7-3bce-9bf1-770750acbb97",
                                                "af13e9f3-b720-3a88-b110-d3da50d6671d",
                                                "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                                "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                                                "65886732-974b-3f7a-a011-90682a3b3ef5",
                                                "8924b747-df2e-32fc-99fc-59d93ddb35c8",
                                                "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                                                "b3db6762-1dfb-3268-818b-228994a05da0",
                                                "f820d441-4786-3f2b-8d9e-5cf6b9f34043",
                                                "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                                                "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                                                "affccc91-69b0-350a-b326-170650a89211"
                                            ]
                                        }
                                    ],
                                    "proteinIds": []
                                }
                            ],
                            "proteinIds": []
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0670",
                    "id": "Pyruvate",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752",
                        "0c4b65c4-4498-3227-8121-0287e17540e2"
                    ]
                },
                {
                    "accession": "KW-0949",
                    "id": "S-adenosyl-L-methionine",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                        "09f9c8a9-8471-372e-ae8a-4f00a1503757",
                        "5be94738-4dd2-3fa2-92a2-508e53a21ef7",
                        "b81ec75a-547b-3fc9-b14c-0fc07e7e9f7f",
                        "1a5b4cfd-7c25-3375-86ea-0f23a4f8d07d",
                        "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                        "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                        "b77f1138-e788-395b-a99d-d42a9de71400",
                        "f35ceae8-be8a-3b1d-a98b-a3a79f389400"
                    ]
                },
                {
                    "accession": "KW-0500",
                    "id": "Molybdenum",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "984c81a6-e816-32b6-b07b-69dadffc4109",
                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5",
                        "e4f20fbf-39c5-391b-8f30-96141689d0d9"
                    ]
                },
                {
                    "accession": "KW-0786",
                    "id": "Thiamine pyrophosphate",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "5cf06ce6-2b9e-3618-a775-454fd37dbb54",
                        "764ab033-a4f5-3e54-b067-b601a3c64ab2",
                        "dccf3c9e-dddf-39e4-9061-2e563185c9b9",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752"
                    ]
                },
                {
                    "accession": "KW-0464",
                    "id": "Manganese",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "8738a805-3daf-35e9-9fe4-295483914470",
                        "275512af-b219-34ae-bcc9-a9a502193acd",
                        "6d505dc1-74f4-3a3d-a659-b5f22e58656d",
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "08db55e4-1db7-3bce-9bf1-770750acbb97",
                        "da971fe7-5f46-30e6-bd0f-7a765e069a77",
                        "18d90788-23e9-3bd4-a17e-dbb417129a72",
                        "12c49188-e280-38b3-9c95-08073edea871",
                        "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                        "265d8170-54fc-33ea-846c-9c4d888ed0e2",
                        "b378c052-6677-38dc-a7b5-3174eed7d879",
                        "3ba845c9-be72-3c9c-93e7-2f5f445d2750",
                        "6ee152a5-a913-3982-9baf-e71b6237e54c",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "65d5288c-2db8-322e-8e4e-8f0eb59d4d79",
                        "9fb7f91c-80c2-3572-b7a2-26f2fc597147",
                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                        "90996f0f-25a2-3513-b36d-a389a5d2166d",
                        "67322d9e-fd74-3328-ae1f-0f9c34caf12f",
                        "ed1aec27-3381-3466-912b-0a050407a275",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65"
                    ]
                },
                {
                    "accession": "KW-0830",
                    "id": "Ubiquinone",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "df67d6cd-1f4d-3caa-94e7-bea09d5c729e",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7"
                    ]
                },
                {
                    "accession": "KW-0663",
                    "id": "Pyridoxal phosphate",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "c1c007e5-cd0f-3bdd-9089-7ed8ba9a91b9",
                        "1d3a670d-1bfa-3117-9025-fba33bbc4a4c",
                        "5532cc3e-59d5-358b-8546-2ce4b04f665f",
                        "2b68a56e-970a-3df3-bf67-70665e758a03",
                        "36a9a461-0ce7-3e5e-bc50-08059008fecd",
                        "dea33edd-752f-3ad5-8e6b-7172e0c08b5f",
                        "378fb80b-3f49-35db-a3a3-09d2cca355f1",
                        "5915fd29-4adc-30be-ad12-c662ce0d2e54",
                        "73f9cc84-4db1-3636-90c2-5f1a7990353e",
                        "05afc641-0745-31bb-bc3c-ff9689e350ff",
                        "806d2373-2eec-33a0-bd4a-5478b9c46f34",
                        "4027df06-664f-319e-a3d3-bfb1459894cf",
                        "b640034e-80a9-37d0-aa8b-98b1180e4740",
                        "81c0dd1a-7823-3d99-a23c-d0ae419b6e0f",
                        "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                        "4e97c206-8758-3007-acc2-d47bb6638ecd",
                        "dcfc4b07-5be4-3d8d-8d4d-72d556b4bdb7",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3",
                        "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                        "2b4f5dc3-75d1-35b3-abcd-059ce2986a5b",
                        "3fe5ee9b-cebd-3413-ad18-e2b50b8b92c3",
                        "7db61943-6cc0-36bd-a5e9-7e46e4f5de58",
                        "47dbf2b2-9d17-378f-b54b-ad48adfcdd1f",
                        "9675ae45-136a-397a-984c-15dedd7ec1c6",
                        "c93a72a2-8bd0-3165-9bc2-221b16efd538"
                    ]
                },
                {
                    "accession": "KW-0704",
                    "id": "Schiff base",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "55ea46e6-b9c6-39e5-a343-70717064842e",
                        "4fb159bd-103d-3af0-a750-ffb1d7efc7c4",
                        "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                        "fe506aea-138e-36a4-ad7f-b74ea420b651",
                        "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                        "466c700e-81ac-3678-953b-c88c7e4df6dd",
                        "8bc41f8b-7a09-3d58-94e5-f48e87ab5611",
                        "0c4b65c4-4498-3227-8121-0287e17540e2",
                        "b314da77-03a0-3453-babc-0e706147208f"
                    ]
                },
                {
                    "accession": "KW-0915",
                    "id": "Sodium",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "bd628120-a237-3e4e-9d75-52d98e2ea191"
                    ]
                },
                {
                    "accession": "KW-0630",
                    "id": "Potassium",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "6e54240b-2467-35d5-a285-824d43665226",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "4f563011-ab0d-30a7-9851-097347585757",
                        "8bfa1252-1f9b-3043-93d8-f5639d9cf074"
                    ]
                },
                {
                    "accession": "KW-0520",
                    "id": "NAD",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "6f42f3aa-5abc-3149-9c90-8514279c3116",
                        "6d505dc1-74f4-3a3d-a659-b5f22e58656d",
                        "9ce746d7-6118-32d3-89bb-4dd16adfb71a",
                        "3717bd7a-b503-325b-be3e-bf6dbc73ec6e",
                        "e6c1af58-a061-30fc-94e7-02b4ca64be72",
                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                        "303b9156-9818-3779-ab63-ca148d2df81d",
                        "e900d423-6746-3bfd-9643-6dab2615ca4b",
                        "83d458c1-eb11-3b0d-956b-a66b398fdfab",
                        "59f12aa5-5fd7-35dc-a73e-ffc903cb995b",
                        "f1778da0-779c-30a6-a761-8d0f864a75d5",
                        "f639e2ef-b23c-3eac-82d4-2f822e5ad56f",
                        "fb76d466-0b71-3eca-8541-66496335aa3d",
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                        "1405208a-ed5c-36f9-b344-3b549f31dbc1",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "7499aa9a-5092-368d-8815-fce129e040d6",
                        "ea37cb76-b651-390d-b9b8-d4dbf6861145",
                        "62942481-2b09-3054-8699-744b7968ce01",
                        "b41e72a4-c433-3aa2-b623-d4acc83b24c6",
                        "8a61fe9f-cea2-3471-8e8f-d8ed4543e47e",
                        "1a8711b9-d137-3136-af7c-a5a23ee78b64",
                        "17517a24-1b56-396f-b53c-1748f4deac12",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                        "78b9d6d6-8e32-38f0-862f-745586f1af9b",
                        "ff9ecfec-7cdf-3b16-9f58-df1393f56211",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "035d3181-8ee3-3152-bed8-9644e47d97dc",
                        "71b3ae7c-a1eb-3390-ae68-92cf4f9f8c00",
                        "0deac7ee-d1b5-3ac9-b421-d54513714945",
                        "41885b05-5303-35e8-b45c-b8ae67bb2a19",
                        "231cde29-fd90-3fde-b0d3-6f6deea14d2e",
                        "30ff119c-62bd-325c-986f-1e7889578200",
                        "df67d6cd-1f4d-3caa-94e7-bea09d5c729e",
                        "abe2547b-09e9-3ff8-bf83-8cbd1ce4e266",
                        "adec9c8c-296a-3769-9203-3969d932bc85",
                        "55adc234-ce10-3909-a3a2-04887fe6fa3b",
                        "ef90ba8d-7bb1-386d-ac40-6e3764fb4b2d",
                        "328fc96a-37df-345d-8150-16e902fc921c"
                    ]
                },
                {
                    "accession": "KW-0547",
                    "id": "Nucleotide-binding",
                    "category": [
                        "Ligand"
                    ],
                    "children": [
                        {
                            "accession": "KW-0067",
                            "id": "ATP-binding",
                            "category": [
                                "Ligand"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0347",
                                    "id": "Helicase",
                                    "category": [
                                        "Ligand",
                                        "Molecular function"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "1f2107a6-54c7-349a-9ecb-0304f17fb7f7",
                                        "e130092e-4136-3244-a836-2f098cdd0a85"
                                    ]
                                }
                            ],
                            "proteinIds": [
                                "6279ec37-2219-3af3-933a-9fd2b8d54074",
                                "4bb7e457-7040-302b-a82c-076c07ca7304",
                                "ff9f96d7-f456-39da-8a23-a25a01e0c64d",
                                "94d890c8-500b-33dc-89b6-7543b299a8ec",
                                "6ca00c54-ed8f-3b5d-8329-051e1839dbf9",
                                "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                                "2edc2c6f-ef1d-35dd-bc17-edb06208498b",
                                "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                                "4305fb19-88a4-3d68-9990-01842b2345ea",
                                "e130092e-4136-3244-a836-2f098cdd0a85",
                                "c3a5104c-6764-35d8-9332-10aa5391f2f8",
                                "29dbb1a1-17a0-3251-a3ad-258fc2b25495",
                                "ed1aec27-3381-3466-912b-0a050407a275",
                                "8ce280e3-65c9-3634-8594-3ffe355b2d58",
                                "9bfb277b-5f3a-3a91-b152-4f312c6fb860",
                                "89cccdf2-60e0-341f-85e5-960f73e90682",
                                "57e0a113-56ce-3ca0-9c08-6400937f1a7e",
                                "d3e35944-1fbb-3d15-ad5d-4ade4c910750",
                                "38ca34b1-ed35-38ea-98cb-6d66102b10c6",
                                "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                "504bbf4f-f5da-3e94-966a-0a3a6e793cfe",
                                "8d98038c-6dec-3415-91e1-26fe011fc089",
                                "2b152b67-d38b-3ef1-a9de-4703d23adf22",
                                "9f714626-b769-35ac-aafd-600c07ba1299",
                                "67322d9e-fd74-3328-ae1f-0f9c34caf12f",
                                "e6f72b1a-051d-3cd0-8205-145dd9f16f65",
                                "ce48f6ed-2f5e-3052-95b4-c46400f3e1d6",
                                "da7e0901-e128-3c06-bb26-9c7d371b992b",
                                "283f56ac-866e-3f77-aea1-e5027be10b57",
                                "96efecbb-175f-371e-97b4-badca3f602a9",
                                "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                                "94de208d-a804-3a38-9f33-570c2fb3ac2d",
                                "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                                "9cda00b9-5bfa-3c8d-b2f0-b70e58afbcb2",
                                "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                                "76463a0c-c647-39ea-b9e9-12ca32648522",
                                "1c7ba006-0968-38fb-81e4-325187699247",
                                "aee8e5d8-a562-367c-aef5-9667b804c2ed",
                                "b3550340-b861-318d-bbd2-e853824eb6ea",
                                "3e6c6495-8fff-3169-846d-0e965cdd2dfe",
                                "fde8f8b6-cc11-31c3-ae29-baf3aac96d3e",
                                "ea37cb76-b651-390d-b9b8-d4dbf6861145",
                                "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                                "5eedc69d-cc8a-394a-ac34-31061697c90c",
                                "5209eac8-8071-3d0d-b7e3-e72045ac99b9",
                                "8bfa1252-1f9b-3043-93d8-f5639d9cf074",
                                "6e54240b-2467-35d5-a285-824d43665226",
                                "1c961799-c8e6-3571-b63c-81642cf2ab91",
                                "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                                "9fb7f91c-80c2-3572-b7a2-26f2fc597147",
                                "681af775-1575-3a11-9b37-bcd4ed5ebe76",
                                "7e88afdd-0d0a-3c72-876b-8ed02b3c7385",
                                "1bec76be-687a-3641-8a3a-114017a4ad6f",
                                "a32c23cc-0d6c-3bb5-a17f-484494775219",
                                "aff29f09-97aa-3745-82d5-95a81522ee3f",
                                "1f2107a6-54c7-349a-9ecb-0304f17fb7f7",
                                "3823599b-bfe6-3544-824c-6b0b0820909c",
                                "38a74cb1-c9ac-34e2-aec5-a461bd6814cf",
                                "f1dee98d-c68e-3f06-869a-54029dbd5aa2",
                                "19579ce4-0a7e-389d-beaf-ceb415f215e8",
                                "8c74d913-e2fc-37c6-a6d9-2d97771b0e31",
                                "5dfb8cec-10b6-33ad-b722-072f815ea2e4",
                                "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                                "8854d795-3ec4-373d-a0c1-10b3373df75d",
                                "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                                "9f6606c2-e93c-3ac7-9fdc-60dff2f54955",
                                "73d2ba4e-5d84-3d4b-86ef-9989dcc9beba",
                                "694c6f10-0972-33dd-bc1f-d8859599cbca",
                                "49d56ab9-28be-353c-921c-5ab492835eaf",
                                "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                                "2b8b93cd-9934-3ca7-bf02-031505a26bbe",
                                "5428c38a-6a43-31cd-885e-19ad69956708",
                                "4f3be65d-3506-3cd4-a538-ea72f62baa95",
                                "9d502900-4b70-3bc5-8723-daa4db8308a8",
                                "ecad122e-a2e9-35a8-b798-d20bffccbf20",
                                "6ad37a60-5968-37cf-a753-b39ddd19e379",
                                "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                                "162027c6-2ad1-36be-ba31-257d5026d55d",
                                "ade07610-2156-3b77-af52-71aaa33a8a32",
                                "2d207fb8-1116-3415-92ee-b7ee964cf312",
                                "51eed60c-eceb-3511-8506-03c78bfaebed",
                                "ff9d2182-ad09-3f34-9be0-f72eb299b944",
                                "2455e392-b06f-30d6-ba60-f5dde5dbac53",
                                "b0e3a6ee-8141-3d32-8a4a-e3b08c47a7ac",
                                "54fef755-2ef7-3990-9656-2610b89c5009",
                                "b364e47c-2699-3e4e-943e-4d21553d6140",
                                "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                                "b60ea458-1a31-3a7e-af6d-1bb8a1af8d12",
                                "87a0407f-808f-3b21-b2ac-263219666221",
                                "3d21682c-3229-3273-aa6f-4ff872d59326",
                                "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                                "da971fe7-5f46-30e6-bd0f-7a765e069a77",
                                "41dd90ee-e557-32a8-928c-ce90ddaca9a8",
                                "e83511de-fef5-3734-8e5a-9d02ae774102",
                                "4b6b69c8-934c-3886-8346-1a12c57bfc30",
                                "33b4c0cf-74a0-325e-a1d3-086af7ba8f90",
                                "65fa8534-717f-3498-8121-97a9bedf951b",
                                "b378c052-6677-38dc-a7b5-3174eed7d879",
                                "3cf4551f-131d-3653-b022-7023b6c1c1fb",
                                "5c9e9fca-f450-396d-87a0-2972c30b517c",
                                "d3380e9e-3ca9-30d9-9fbe-d22064b59906",
                                "4f44ee31-5bb3-3865-8752-46f599e9eeed",
                                "dad5c340-4a7c-3b50-9851-087af9277516",
                                "db66e621-4ba9-3952-8ecb-c161094c8675",
                                "cafca265-a3bf-3afb-a1ec-32aea9845b77",
                                "46be049f-6f54-305e-8e47-50ffc9649e9d"
                            ]
                        },
                        {
                            "accession": "KW-0116",
                            "id": "cAMP-binding",
                            "category": [
                                "Ligand"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0114",
                                    "id": "cAMP",
                                    "category": [
                                        "Ligand"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "9a58754a-5c56-3c07-84b7-5360a962f090"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0342",
                            "id": "GTP-binding",
                            "category": [
                                "Ligand"
                            ],
                            "children": [],
                            "proteinIds": [
                                "233bc275-5a82-338e-8bb6-ecca7feb7762",
                                "b837a743-46c7-3c8a-9580-8938a4589cfa",
                                "3ba5ec97-1eda-3dc5-b5c1-c093a5df4e11",
                                "c16a16e2-43d4-3291-a840-fefd5939be2e",
                                "1d192ad9-a2cf-3749-8986-2f362feb18bc",
                                "7201463f-2ac9-3fc8-9f98-af3c39a9de8f",
                                "75317e6a-dd30-3fb7-8e1b-6394237d64db",
                                "2c242910-cb3c-3f41-b750-2dac1ebc7215",
                                "c75382ff-f9ec-30ed-a3bf-989eaa3734cf",
                                "5ed92216-217c-3fca-9f10-3a2d101359d4",
                                "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                                "03976b83-6c48-3a5e-a899-01e09d2fb723",
                                "dc53afad-12a0-3784-858f-00d211e93e90",
                                "bf16af9c-8140-3c4c-8f16-7b4d9487a2d0",
                                "eed02db9-5ce2-3bb7-8374-65a5fac24899"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0092",
                    "id": "Biotin",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "41dd90ee-e557-32a8-928c-ce90ddaca9a8",
                        "36871dcf-e157-3efa-83dd-6a24b6f8caf0"
                    ]
                },
                {
                    "accession": "KW-0533",
                    "id": "Nickel",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d90a1ebb-127f-354d-b3d4-a7c56ce5df07",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "d45584d2-50ec-3dca-9d34-f5bd15067a15"
                    ]
                },
                {
                    "accession": "KW-0446",
                    "id": "Lipid-binding",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7"
                    ]
                },
                {
                    "accession": "KW-0711",
                    "id": "Selenium",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "cafca265-a3bf-3afb-a1ec-32aea9845b77"
                    ]
                },
                {
                    "accession": "KW-0114",
                    "id": "cAMP",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9a58754a-5c56-3c07-84b7-5360a962f090"
                    ]
                },
                {
                    "accession": "KW-0170",
                    "id": "Cobalt",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "20366e64-78e7-38e8-9158-29d148f86506",
                        "1bc1abec-4cdd-3bfd-a2cc-56666581251f",
                        "56ef52fc-84e7-3855-a588-dd43eb465591",
                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                        "ef90ba8d-7bb1-386d-ac40-6e3764fb4b2d"
                    ]
                },
                {
                    "accession": "KW-0186",
                    "id": "Copper",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                        "d846b344-6c74-3696-b0da-84764ee91886"
                    ]
                },
                {
                    "accession": "KW-0408",
                    "id": "Iron",
                    "category": [
                        "Ligand"
                    ],
                    "children": [
                        {
                            "accession": "KW-0409",
                            "id": "Iron storage",
                            "category": [
                                "Ligand",
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                                "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                                "41794e2f-1527-3aa8-a594-ce47f97a857d"
                            ]
                        }
                    ],
                    "proteinIds": [
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                        "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                        "9e5e4071-a9d1-39e3-b657-84083543f591",
                        "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "9e01ce48-38b0-3bee-8178-f00a8b68ada0",
                        "d3e155df-63b4-326d-95a6-a761b36e6453",
                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                        "6edaccfd-0cf9-37a7-8924-ac8da9f6b01f",
                        "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                        "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                        "8fdfc5b7-72bb-357d-9e57-c9710e060f7e",
                        "24e689b0-03b1-3408-a49a-151692cf629c",
                        "589687f2-9044-364c-aaac-707b0f97823c",
                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                        "41794e2f-1527-3aa8-a594-ce47f97a857d",
                        "3ae4d435-05c9-3016-9419-f906e48968a9",
                        "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                        "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                        "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                        "e4f20fbf-39c5-391b-8f30-96141689d0d9",
                        "ce422a1e-705d-3108-8141-d9bb318b50ce",
                        "62942481-2b09-3054-8699-744b7968ce01",
                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                        "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                        "08db55e4-1db7-3bce-9bf1-770750acbb97",
                        "af13e9f3-b720-3a88-b110-d3da50d6671d",
                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                        "65886732-974b-3f7a-a011-90682a3b3ef5",
                        "8924b747-df2e-32fc-99fc-59d93ddb35c8",
                        "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                        "b3db6762-1dfb-3268-818b-228994a05da0",
                        "f820d441-4786-3f2b-8d9e-5cf6b9f34043",
                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                        "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                        "affccc91-69b0-350a-b326-170650a89211"
                    ]
                },
                {
                    "accession": "KW-0826",
                    "id": "Tungsten",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5"
                    ]
                },
                {
                    "accession": "KW-0285",
                    "id": "Flavoprotein",
                    "category": [
                        "Ligand"
                    ],
                    "children": [
                        {
                            "accession": "KW-0288",
                            "id": "FMN",
                            "category": [
                                "Ligand"
                            ],
                            "children": [],
                            "proteinIds": [
                                "71b3ae7c-a1eb-3390-ae68-92cf4f9f8c00",
                                "0deac7ee-d1b5-3ac9-b421-d54513714945",
                                "03bc60d2-2bbe-34fb-87c4-0413718dd28a",
                                "0791e3a6-5ee6-329f-a7d1-b8450305c4f7",
                                "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                "e6c1af58-a061-30fc-94e7-02b4ca64be72",
                                "703f9b97-7871-3f81-80d9-df88d0cc741a",
                                "ff9ecfec-7cdf-3b16-9f58-df1393f56211",
                                "7ad93b7c-3ef5-38d5-be11-abfadefb412f"
                            ]
                        },
                        {
                            "accession": "KW-0274",
                            "id": "FAD",
                            "category": [
                                "Ligand"
                            ],
                            "children": [],
                            "proteinIds": [
                                "62942481-2b09-3054-8699-744b7968ce01",
                                "b41e72a4-c433-3aa2-b623-d4acc83b24c6",
                                "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                                "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                                "89ded291-4348-3f8d-be43-56e42b9311d0",
                                "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                                "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                                "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                                "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                                "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                                "631f444e-b319-3aaf-a57c-b7836d934fa5",
                                "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                                "fc22b497-2a0a-39ca-915b-8037852bb5a9",
                                "89743da0-e8d3-3386-967f-46ec472079d8"
                            ]
                        }
                    ],
                    "proteinIds": [
                        "62942481-2b09-3054-8699-744b7968ce01",
                        "b41e72a4-c433-3aa2-b623-d4acc83b24c6",
                        "0791e3a6-5ee6-329f-a7d1-b8450305c4f7",
                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                        "e6c1af58-a061-30fc-94e7-02b4ca64be72",
                        "703f9b97-7871-3f81-80d9-df88d0cc741a",
                        "89ded291-4348-3f8d-be43-56e42b9311d0",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "ff9ecfec-7cdf-3b16-9f58-df1393f56211",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                        "7ad93b7c-3ef5-38d5-be11-abfadefb412f",
                        "71b3ae7c-a1eb-3390-ae68-92cf4f9f8c00",
                        "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                        "0deac7ee-d1b5-3ac9-b421-d54513714945",
                        "03bc60d2-2bbe-34fb-87c4-0413718dd28a",
                        "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                        "fc22b497-2a0a-39ca-915b-8037852bb5a9",
                        "89743da0-e8d3-3386-967f-46ec472079d8"
                    ]
                },
                {
                    "accession": "KW-0290",
                    "id": "Folate-binding",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9c4f1688-3ddb-3192-ad15-f77e1284e619"
                    ]
                },
                {
                    "accession": "KW-0106",
                    "id": "Calcium",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "25039a14-8246-3b5f-ae47-56711abe51d9",
                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                        "764ab033-a4f5-3e54-b067-b601a3c64ab2",
                        "dccf3c9e-dddf-39e4-9061-2e563185c9b9"
                    ]
                },
                {
                    "accession": "KW-0460",
                    "id": "Magnesium",
                    "category": [
                        "Ligand"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1bec76be-687a-3641-8a3a-114017a4ad6f",
                        "984c81a6-e816-32b6-b07b-69dadffc4109",
                        "10a0f74c-8ecc-30a4-8f42-d060baed0b77",
                        "a75fa3e2-2c2a-364e-8d56-32867b4e2dd8",
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "764ab033-a4f5-3e54-b067-b601a3c64ab2",
                        "347fa5f1-329d-3667-823d-4c5873cd8b63",
                        "0b0bf4f1-305f-3216-953b-fac86a8062a8",
                        "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                        "afb5765c-5d4f-354c-92be-d11f02153167",
                        "91bd43f5-ae68-3b80-aa75-7a9c967b9398",
                        "c9a3ff3d-57da-39c5-8e00-846dfb3dcc5c",
                        "3a1dc7b1-7eef-302a-8d65-c1b776272a30",
                        "ccdb8a84-5e15-3840-87f4-9a2366f7bc56",
                        "03976b83-6c48-3a5e-a899-01e09d2fb723",
                        "c3a5104c-6764-35d8-9332-10aa5391f2f8",
                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752",
                        "9f6606c2-e93c-3ac7-9fdc-60dff2f54955",
                        "ed1aec27-3381-3466-912b-0a050407a275",
                        "8ce280e3-65c9-3634-8594-3ffe355b2d58",
                        "57e0a113-56ce-3ca0-9c08-6400937f1a7e",
                        "d303538a-3c87-3104-ab84-288d4297de26",
                        "d3e35944-1fbb-3d15-ad5d-4ade4c910750",
                        "4e89fea4-1ab6-3157-a335-2adf9b1b33c2",
                        "2b8b93cd-9934-3ca7-bf02-031505a26bbe",
                        "1bc1abec-4cdd-3bfd-a2cc-56666581251f",
                        "108cc793-6765-346b-85b7-9ea261151b2d",
                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3",
                        "8d98038c-6dec-3415-91e1-26fe011fc089",
                        "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "2b152b67-d38b-3ef1-a9de-4703d23adf22",
                        "ade07610-2156-3b77-af52-71aaa33a8a32",
                        "eed02db9-5ce2-3bb7-8374-65a5fac24899",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd",
                        "94de208d-a804-3a38-9f33-570c2fb3ac2d",
                        "f1778da0-779c-30a6-a761-8d0f864a75d5",
                        "1687c17e-e4de-31b3-a6c0-b710eeee0895",
                        "54fef755-2ef7-3990-9656-2610b89c5009",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "f322a8ae-ec9a-3952-9737-150d950df3d6",
                        "82023d76-0233-39ac-8726-4b5f659b2e18",
                        "5de89c9e-69d5-346a-8e30-928078064499",
                        "3e6c6495-8fff-3169-846d-0e965cdd2dfe",
                        "ea37cb76-b651-390d-b9b8-d4dbf6861145",
                        "a30532fe-425b-31bf-a47c-fa9b90939faa",
                        "6dce545e-fd3b-3d72-b609-138fd3a8b0fe",
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "a48afbb2-e862-3406-9018-8d89fb0f7d0e",
                        "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                        "a5f2faa4-5802-3b3a-bc58-e907abc60363",
                        "da971fe7-5f46-30e6-bd0f-7a765e069a77",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "8bfa1252-1f9b-3043-93d8-f5639d9cf074",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                        "e83511de-fef5-3734-8e5a-9d02ae774102",
                        "c75382ff-f9ec-30ed-a3bf-989eaa3734cf",
                        "6e54240b-2467-35d5-a285-824d43665226",
                        "65fa8534-717f-3498-8121-97a9bedf951b",
                        "b378c052-6677-38dc-a7b5-3174eed7d879",
                        "3cf4551f-131d-3653-b022-7023b6c1c1fb",
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "5ead7332-b2e5-3089-a871-a22450e06376",
                        "d3380e9e-3ca9-30d9-9fbe-d22064b59906",
                        "1c961799-c8e6-3571-b63c-81642cf2ab91",
                        "dad5c340-4a7c-3b50-9851-087af9277516",
                        "d196eab6-2d33-3619-a7f6-3166a310bad3",
                        "dccf3c9e-dddf-39e4-9061-2e563185c9b9",
                        "9fb7f91c-80c2-3572-b7a2-26f2fc597147",
                        "db66e621-4ba9-3952-8ecb-c161094c8675",
                        "90996f0f-25a2-3513-b36d-a389a5d2166d",
                        "cafca265-a3bf-3afb-a1ec-32aea9845b77"
                    ]
                }
            ],
            "proteinIds": []
        },
        {
            "accession": "KW-9998",
            "id": "Cellular component",
            "category": [],
            "children": [
                {
                    "accession": "KW-0139",
                    "id": "CF(1)",
                    "category": [
                        "Cellular component",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d21bdcc2-f9fc-3b3e-87ce-5eaed473d7bb",
                        "9493d28b-2334-335f-bf3a-5971f94c0d6b",
                        "5eedc69d-cc8a-394a-ac34-31061697c90c",
                        "87a0407f-808f-3b21-b2ac-263219666221"
                    ]
                },
                {
                    "accession": "KW-0733",
                    "id": "Signal recognition particle",
                    "category": [
                        "Cellular component",
                        "Molecular function"
                    ],
                    "children": [],
                    "proteinIds": [
                        "dc53afad-12a0-3784-858f-00d211e93e90"
                    ]
                },
                {
                    "accession": "KW-0964",
                    "id": "Secreted",
                    "category": [
                        "Cellular component"
                    ],
                    "children": [
                        {
                            "accession": "KW-0134",
                            "id": "Cell wall",
                            "category": [
                                "Cellular component"
                            ],
                            "children": [],
                            "proteinIds": [
                                "45b221a7-f65f-318a-afee-f1bec5704c51"
                            ]
                        }
                    ],
                    "proteinIds": [
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "d196eab6-2d33-3619-a7f6-3166a310bad3",
                        "e0cfd0ed-8abf-376e-b766-987fded19ccb"
                    ]
                },
                {
                    "accession": "KW-0574",
                    "id": "Periplasm",
                    "category": [
                        "Cellular component"
                    ],
                    "children": [],
                    "proteinIds": [
                        "91e1fc09-c4c0-371f-b84e-7272bf3d625a",
                        "abc501f5-c77c-3a30-b015-13b7fa11f8ce",
                        "9e2aad1c-75e8-3a3d-b508-aa845635d7b7",
                        "18dca3e9-3ec2-3bfd-ae59-bd227ada733e",
                        "41f8e0c9-6167-3fdf-b413-51b3b1031efe",
                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                        "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                        "d1cdacd5-d00d-333b-bdba-0df200229b25",
                        "62418546-9356-318c-93fe-d6830db6d92a",
                        "3ebfa9ba-a4ef-3004-8b2d-24c105fbd7db",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "72687302-febc-300a-8b6a-aa54e09d2bef",
                        "5c3518d4-8a4b-3d79-b52a-29fbe0fa98b6",
                        "fc16d218-6350-3e2e-9c9c-05fefd49e5a3",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "c3f30764-2155-3be3-ab4c-91383e8cadac",
                        "59c6e653-4609-3099-9739-383b53092172",
                        "8d312403-4af6-3fe9-ac93-7d07034f0424",
                        "448e34c1-4fd2-3d95-b078-9642f7347452",
                        "c4cdd1b7-0c46-3cb6-90c9-f2b2a1983a58",
                        "d303538a-3c87-3104-ab84-288d4297de26",
                        "96a76a6b-7f10-333e-b525-f2784214ee48",
                        "f9202d3d-2dde-33d7-bae6-e3b73b4adb52",
                        "65a926bf-d216-3943-9809-9013a707a4ac",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3",
                        "c907cf8f-adfa-3c36-8478-87a02c27fd25",
                        "70d716e7-2e21-3fc7-9d5a-eaf73bac2f60",
                        "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                        "17a73fa6-5041-35e9-99ec-2bc37e59a4af",
                        "08a9b134-8813-352b-90bb-807c80c4b69b",
                        "238b3de0-b3c2-3f07-911a-decfc93154ab",
                        "5506f1e2-c28b-3da3-90e5-9cb3d8ba2457",
                        "e7011909-03cc-3eb0-aef7-b8a96ce4f2ce",
                        "17d3560c-19d3-3141-b1e2-c20ba87599ce",
                        "d2a66d40-466d-3cd7-824e-b1c172bed1e1",
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5",
                        "8dada997-5a23-3c7b-9833-68bc56ba560f",
                        "bfaf87cb-327c-3863-922f-5b21037726d2",
                        "6ed557e6-3f94-30a4-87a0-ba7bc8dff3e8",
                        "25039a14-8246-3b5f-ae47-56711abe51d9",
                        "3fb2549c-0452-3210-8be4-9f3a0d2cffea",
                        "d4bc5910-f71b-3ea9-b946-28450da93565",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "52f8ff6d-7ba8-3d7b-a836-b845ad79c30c",
                        "c893794f-f410-32ba-9458-4f795169055c",
                        "59bd70e6-31fe-3e3e-9f6b-a0e4fbdaef5d",
                        "d572aa6a-b6a6-3fad-9282-a20959414f10",
                        "cacb2a3c-4558-30e7-93cd-c700f6ed12ea",
                        "806279ae-507c-35c6-bffa-477f138c7919",
                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4"
                    ]
                },
                {
                    "accession": "KW-0472",
                    "id": "Membrane",
                    "category": [
                        "Cellular component"
                    ],
                    "children": [
                        {
                            "accession": "KW-1003",
                            "id": "Cell membrane",
                            "category": [
                                "Cellular component"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0997",
                                    "id": "Cell inner membrane",
                                    "category": [
                                        "Cellular component"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                                        "233bc275-5a82-338e-8bb6-ecca7feb7762",
                                        "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                                        "e8d0005d-a3a3-349a-9db4-d3d95377b718",
                                        "347fa5f1-329d-3667-823d-4c5873cd8b63",
                                        "9493d28b-2334-335f-bf3a-5971f94c0d6b",
                                        "88dafdd2-f1e6-3992-aee5-9fd6763d3056",
                                        "24e689b0-03b1-3408-a49a-151692cf629c",
                                        "d21bdcc2-f9fc-3b3e-87ce-5eaed473d7bb",
                                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                                        "00926371-a557-341d-8941-45ad2fafee4b",
                                        "80ee5820-8165-3fc1-858f-330ea6212f00",
                                        "0d552860-d03d-3427-9dfb-58c6c723f3b7",
                                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                        "87a0407f-808f-3b21-b2ac-263219666221",
                                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                                        "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                                        "7201463f-2ac9-3fc8-9f98-af3c39a9de8f",
                                        "5eedc69d-cc8a-394a-ac34-31061697c90c",
                                        "9d502900-4b70-3bc5-8723-daa4db8308a8",
                                        "262dfcac-448e-3bfa-818a-ad6589081358",
                                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                                        "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                                        "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                                        "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                                        "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                                        "df67d6cd-1f4d-3caa-94e7-bea09d5c729e",
                                        "fa1a4201-301f-3503-9d5d-51aa06fb8a10",
                                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                                    ]
                                }
                            ],
                            "proteinIds": [
                                "8e45ccea-21f6-38f7-b11e-aa8914e50a61",
                                "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                                "233bc275-5a82-338e-8bb6-ecca7feb7762",
                                "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                                "e8d0005d-a3a3-349a-9db4-d3d95377b718",
                                "347fa5f1-329d-3667-823d-4c5873cd8b63",
                                "9493d28b-2334-335f-bf3a-5971f94c0d6b",
                                "d45584d2-50ec-3dca-9d34-f5bd15067a15",
                                "88dafdd2-f1e6-3992-aee5-9fd6763d3056",
                                "d90a1ebb-127f-354d-b3d4-a7c56ce5df07",
                                "24e689b0-03b1-3408-a49a-151692cf629c",
                                "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                                "d21bdcc2-f9fc-3b3e-87ce-5eaed473d7bb",
                                "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                                "631f444e-b319-3aaf-a57c-b7836d934fa5",
                                "fb8c2814-f376-3032-9735-34386ddd20a2",
                                "00926371-a557-341d-8941-45ad2fafee4b",
                                "80ee5820-8165-3fc1-858f-330ea6212f00",
                                "0d552860-d03d-3427-9dfb-58c6c723f3b7",
                                "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                "87a0407f-808f-3b21-b2ac-263219666221",
                                "b837a743-46c7-3c8a-9580-8938a4589cfa",
                                "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                                "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                                "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                                "af13e9f3-b720-3a88-b110-d3da50d6671d",
                                "7201463f-2ac9-3fc8-9f98-af3c39a9de8f",
                                "5eedc69d-cc8a-394a-ac34-31061697c90c",
                                "9d502900-4b70-3bc5-8723-daa4db8308a8",
                                "262dfcac-448e-3bfa-818a-ad6589081358",
                                "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                                "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                                "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                                "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                                "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                                "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                                "3cce71b9-bb9f-3f5f-a191-89455ca5884d",
                                "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                                "df67d6cd-1f4d-3caa-94e7-bea09d5c729e",
                                "fa1a4201-301f-3503-9d5d-51aa06fb8a10",
                                "46be049f-6f54-305e-8e47-50ffc9649e9d"
                            ]
                        },
                        {
                            "accession": "KW-1134",
                            "id": "Transmembrane beta strand",
                            "category": [
                                "Cellular component",
                                "Domain"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0812",
                                    "id": "Transmembrane",
                                    "category": [
                                        "Cellular component",
                                        "Domain"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "e8d0005d-a3a3-349a-9db4-d3d95377b718",
                                        "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                                        "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                                        "88dafdd2-f1e6-3992-aee5-9fd6763d3056",
                                        "1f420626-52e3-37a6-aa7f-ccdab455504e",
                                        "24e689b0-03b1-3408-a49a-151692cf629c",
                                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                        "00926371-a557-341d-8941-45ad2fafee4b",
                                        "0d552860-d03d-3427-9dfb-58c6c723f3b7",
                                        "fa1a4201-301f-3503-9d5d-51aa06fb8a10",
                                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0998",
                            "id": "Cell outer membrane",
                            "category": [
                                "Cellular component"
                            ],
                            "children": [],
                            "proteinIds": [
                                "45b221a7-f65f-318a-afee-f1bec5704c51",
                                "b2a03d2e-7fe3-3375-a542-95440bd78a9a",
                                "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                "a195423a-3a12-3931-95da-fa2d60d0a3ab",
                                "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                                "b6ec19d9-dd21-3506-899a-f675868a1b58",
                                "1f420626-52e3-37a6-aa7f-ccdab455504e"
                            ]
                        },
                        {
                            "accession": "KW-1133",
                            "id": "Transmembrane helix",
                            "category": [
                                "Cellular component",
                                "Domain"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0812",
                                    "id": "Transmembrane",
                                    "category": [
                                        "Cellular component",
                                        "Domain"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "e8d0005d-a3a3-349a-9db4-d3d95377b718",
                                        "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                                        "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                                        "88dafdd2-f1e6-3992-aee5-9fd6763d3056",
                                        "1f420626-52e3-37a6-aa7f-ccdab455504e",
                                        "24e689b0-03b1-3408-a49a-151692cf629c",
                                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                        "00926371-a557-341d-8941-45ad2fafee4b",
                                        "0d552860-d03d-3427-9dfb-58c6c723f3b7",
                                        "fa1a4201-301f-3503-9d5d-51aa06fb8a10",
                                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0812",
                            "id": "Transmembrane",
                            "category": [
                                "Cellular component",
                                "Domain"
                            ],
                            "children": [],
                            "proteinIds": [
                                "e8d0005d-a3a3-349a-9db4-d3d95377b718",
                                "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                                "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                                "88dafdd2-f1e6-3992-aee5-9fd6763d3056",
                                "1f420626-52e3-37a6-aa7f-ccdab455504e",
                                "24e689b0-03b1-3408-a49a-151692cf629c",
                                "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                "00926371-a557-341d-8941-45ad2fafee4b",
                                "0d552860-d03d-3427-9dfb-58c6c723f3b7",
                                "fa1a4201-301f-3503-9d5d-51aa06fb8a10",
                                "b6ec19d9-dd21-3506-899a-f675868a1b58",
                                "b9f03a52-e78f-3a0c-9f3d-4d4301c34388"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0281",
                    "id": "Fimbrium",
                    "category": [
                        "Cellular component"
                    ],
                    "children": [],
                    "proteinIds": [
                        "a87ba622-c92d-35b1-935e-52f538af07f4"
                    ]
                },
                {
                    "accession": "KW-0240",
                    "id": "DNA-directed RNA polymerase",
                    "category": [
                        "Cellular component",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "4181cf83-cf26-3a2f-acc1-8adef347d76c",
                        "a30532fe-425b-31bf-a47c-fa9b90939faa",
                        "5a6db542-70d3-3f1e-8bfa-723559a235e2",
                        "456bfd92-36c5-3fd9-8803-aed14a8ec2d0"
                    ]
                },
                {
                    "accession": "KW-0963",
                    "id": "Cytoplasm",
                    "category": [
                        "Cellular component"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d0894567-8e24-31ed-9ad2-5fc20c27125f",
                        "6279ec37-2219-3af3-933a-9fd2b8d54074",
                        "188a9c3f-c403-3be2-a94c-df304e4a6cb2",
                        "9ce746d7-6118-32d3-89bb-4dd16adfb71a",
                        "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                        "4f77b69d-5001-3a9f-8759-0c12d8fa535f",
                        "91bd43f5-ae68-3b80-aa75-7a9c967b9398",
                        "723afa78-dd8c-30f4-b92b-bdc68d14adc4",
                        "c3a5104c-6764-35d8-9332-10aa5391f2f8",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497",
                        "07e6492c-255a-3b65-8400-bb99932324d2",
                        "0e5b9e7c-05a9-3724-a675-93844409d8e5",
                        "cac64301-0e27-3e86-918a-d3bb0a83df4c",
                        "0f0feee8-b183-3f8c-8fdd-4ac0df27ae16",
                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                        "7494b152-dbc6-395d-abdd-1b549511a731",
                        "1a8711b9-d137-3136-af7c-a5a23ee78b64",
                        "9be2c96a-399f-3a27-b6b4-61b69d22d4db",
                        "8ce280e3-65c9-3634-8594-3ffe355b2d58",
                        "0ff36b3a-ba1f-3928-a34c-e9e69083cf4f",
                        "e14bec50-261c-3f00-a21d-a4e69f9eee3b",
                        "5492ee99-1adc-3f4a-8494-70a45e6b2141",
                        "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                        "504bbf4f-f5da-3e94-966a-0a3a6e793cfe",
                        "8aa62ad5-4534-391c-9963-a1c34edc7e3d",
                        "2b152b67-d38b-3ef1-a9de-4703d23adf22",
                        "9f714626-b769-35ac-aafd-600c07ba1299",
                        "0c4b65c4-4498-3227-8121-0287e17540e2",
                        "abeed2e5-253b-308d-b872-44c57c304b75",
                        "89743da0-e8d3-3386-967f-46ec472079d8",
                        "d29caf3d-891e-37fc-806b-e50965ca02db",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65",
                        "ce48f6ed-2f5e-3052-95b4-c46400f3e1d6",
                        "da7e0901-e128-3c06-bb26-9c7d371b992b",
                        "283f56ac-866e-3f77-aea1-e5027be10b57",
                        "94de208d-a804-3a38-9f33-570c2fb3ac2d",
                        "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                        "9cda00b9-5bfa-3c8d-b2f0-b70e58afbcb2",
                        "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                        "76463a0c-c647-39ea-b9e9-12ca32648522",
                        "1c7ba006-0968-38fb-81e4-325187699247",
                        "f639e2ef-b23c-3eac-82d4-2f822e5ad56f",
                        "09f9c8a9-8471-372e-ae8a-4f00a1503757",
                        "1687c17e-e4de-31b3-a6c0-b710eeee0895",
                        "ad28a8bf-fcd4-3132-968d-795293745983",
                        "be175fae-90c6-3d4e-a517-42b3a98a8abe",
                        "8b9be9c8-0db3-3271-bd73-dd7b1e08fe27",
                        "3d0070cc-e1db-31f6-8f49-580883bc373d",
                        "4027df06-664f-319e-a3d3-bfb1459894cf",
                        "fde8f8b6-cc11-31c3-ae29-baf3aac96d3e",
                        "275512af-b219-34ae-bcc9-a9a502193acd",
                        "2d260263-5040-3f64-8349-e31eaa98ca1d",
                        "a5f2faa4-5802-3b3a-bc58-e907abc60363",
                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                        "dcfc4b07-5be4-3d8d-8d4d-72d556b4bdb7",
                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                        "6e54240b-2467-35d5-a285-824d43665226",
                        "cdf158db-6f74-3ade-84ab-6e18b09bab2a",
                        "7e88afdd-0d0a-3c72-876b-8ed02b3c7385",
                        "09824aca-3ccb-3f94-9b21-7d95f424c697",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "a681313b-a131-3404-a255-6ff70a8ea5db",
                        "24875adb-4d25-30b6-9a24-948f620f7f72",
                        "9aa82d48-90c7-39ac-9158-573fb5dcfc10",
                        "982e8fe6-4b3c-3e5a-9ad4-4dbbd2c9d022",
                        "124c6ffa-0fb6-38b2-b175-247458f097f9",
                        "ce57e563-d2a4-34e5-80c0-9854d14c2e25",
                        "5ed92216-217c-3fca-9f10-3a2d101359d4",
                        "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                        "8854d795-3ec4-373d-a0c1-10b3373df75d",
                        "2438b215-94d2-3e1e-8722-63acfe9ba76f",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "9f6606c2-e93c-3ac7-9fdc-60dff2f54955",
                        "2b8b93cd-9934-3ca7-bf02-031505a26bbe",
                        "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                        "108cc793-6765-346b-85b7-9ea261151b2d",
                        "5428c38a-6a43-31cd-885e-19ad69956708",
                        "45663484-0ecc-3c24-be1f-fa2773ca7012",
                        "f02c85ff-e778-3203-a229-1bb5cd740ffe",
                        "10c00c05-61de-3dcf-8030-f0d748031d11",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "8bc41f8b-7a09-3d58-94e5-f48e87ab5611",
                        "4939a1cd-d4b2-3c7d-9053-b02c16db7e41",
                        "adec9c8c-296a-3769-9203-3969d932bc85",
                        "fbfddc81-7c4c-3720-8aa0-df1f266bcd50",
                        "3651de6c-1487-3148-bbd8-963d04a47b07",
                        "eed02db9-5ce2-3bb7-8374-65a5fac24899",
                        "1be8f6b8-ebe6-3e6e-9851-cf096fc1f6ab",
                        "ff9d2182-ad09-3f34-9be0-f72eb299b944",
                        "5532cc3e-59d5-358b-8546-2ce4b04f665f",
                        "1c4eb8da-7d0e-3933-9ecc-32a9284914d4",
                        "2455e392-b06f-30d6-ba60-f5dde5dbac53",
                        "89ded291-4348-3f8d-be43-56e42b9311d0",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "dbf5255d-b46f-3ff6-af72-b63754671791",
                        "ba9ea4f2-a2be-3e4d-b65c-5a32e4fd7009",
                        "4fb159bd-103d-3af0-a750-ffb1d7efc7c4",
                        "54fef755-2ef7-3990-9656-2610b89c5009",
                        "b27dd7db-316f-3eb6-a914-64fc033cd01c",
                        "76c71bc8-c46d-3acc-ad17-d1ca3625783a",
                        "82023d76-0233-39ac-8726-4b5f659b2e18",
                        "6ee152a5-a913-3982-9baf-e71b6237e54c",
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "8a61fe9f-cea2-3471-8e8f-d8ed4543e47e",
                        "b640034e-80a9-37d0-aa8b-98b1180e4740",
                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                        "cc539b5e-aec6-36aa-b132-f5766adb2d13",
                        "5c95adf3-bad4-3a4f-b76b-b94204974137",
                        "bd628120-a237-3e4e-9d75-52d98e2ea191",
                        "da971fe7-5f46-30e6-bd0f-7a765e069a77",
                        "977ce880-6a2a-3645-a22b-342adb4254db",
                        "ab622b9a-421a-3a59-a5fe-ff745ba0d578",
                        "e83511de-fef5-3734-8e5a-9d02ae774102",
                        "5efa2c07-4e05-30c7-8cfb-1f83c789005a",
                        "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "9c4f1688-3ddb-3192-ad15-f77e1284e619",
                        "a7c2dce1-0c2f-334d-a2b8-aa674aa75c8c",
                        "d3380e9e-3ca9-30d9-9fbe-d22064b59906",
                        "4f44ee31-5bb3-3865-8752-46f599e9eeed",
                        "d196eab6-2d33-3619-a7f6-3166a310bad3",
                        "db66e621-4ba9-3952-8ecb-c161094c8675",
                        "c36e50bc-6de3-3773-bd97-b65e8aeeab07",
                        "278f5526-d165-387c-a9bb-f3981199b3ac",
                        "ff9f96d7-f456-39da-8a23-a25a01e0c64d",
                        "94d890c8-500b-33dc-89b6-7543b299a8ec",
                        "327c877d-6f5c-3e3c-9044-d7e2ffeb1d4e",
                        "303b9156-9818-3779-ab63-ca148d2df81d",
                        "12c49188-e280-38b3-9c95-08073edea871",
                        "c11af8cb-9a5d-3df5-a3e0-2e64908817c1",
                        "224c9b5c-ddbe-3cce-94d4-b0d9efb673f8",
                        "603148ec-4d7d-3bec-9a06-3da63c3730d0",
                        "2edc2c6f-ef1d-35dd-bc17-edb06208498b",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "ccdb8a84-5e15-3840-87f4-9a2366f7bc56",
                        "76d373ba-0068-3d85-894e-2559dd07a94a",
                        "4305fb19-88a4-3d68-9990-01842b2345ea",
                        "14aed608-b903-3874-a9e8-61e7f4f44d91",
                        "b41e72a4-c433-3aa2-b623-d4acc83b24c6",
                        "9bfb277b-5f3a-3a91-b152-4f312c6fb860",
                        "83b737ff-b679-380a-a9c0-d56ed58ff728",
                        "438a6158-29ba-3eaa-b7a6-b39401811d6e",
                        "cf7b4c0c-5e3e-35ed-80de-4e6a56a14bee",
                        "38ca34b1-ed35-38ea-98cb-6d66102b10c6",
                        "70d716e7-2e21-3fc7-9d5a-eaf73bac2f60",
                        "4958103b-c713-31f7-956b-ffe93334285c",
                        "858f1fe3-5d5f-31a7-9188-c72882269fe8",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                        "67322d9e-fd74-3328-ae1f-0f9c34caf12f",
                        "d14ba956-67ec-339b-bc82-f5d87a3b9a28",
                        "96efecbb-175f-371e-97b4-badca3f602a9",
                        "3ba5ec97-1eda-3dc5-b5c1-c093a5df4e11",
                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                        "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                        "c16a16e2-43d4-3291-a840-fefd5939be2e",
                        "62169f87-c02b-34c1-a2ea-1adb1fec4587",
                        "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd",
                        "716e68a9-7fbc-316b-b5ec-7ff2a5d22076",
                        "b314da77-03a0-3453-babc-0e706147208f",
                        "2da1b3db-c2fd-3a86-bf9b-48e45087b1d6",
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "878e895a-9d6c-367c-ab7a-ea963aedcd08",
                        "d63d6106-8f94-3528-9d7c-8b5c6079f26a",
                        "6dce545e-fd3b-3d72-b609-138fd3a8b0fe",
                        "d76a3d39-6566-3b7f-a805-9d8501c00579",
                        "fbe5b850-e94f-3077-af8a-dd514946b672",
                        "b81ec75a-547b-3fc9-b14c-0fc07e7e9f7f",
                        "5a422db3-84aa-3ff6-a9de-57f0759b81fb",
                        "09de6da6-b585-35e0-a2e5-9620ac8a4c4e",
                        "b77f1138-e788-395b-a99d-d42a9de71400",
                        "681af775-1575-3a11-9b37-bcd4ed5ebe76",
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "aff29f09-97aa-3745-82d5-95a81522ee3f",
                        "1f2107a6-54c7-349a-9ecb-0304f17fb7f7",
                        "3823599b-bfe6-3544-824c-6b0b0820909c",
                        "67d5473b-2d87-339e-8eb3-8dacbf198014",
                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "5c3e9984-9197-37f7-8c69-deb288ba224a",
                        "36a9a461-0ce7-3e5e-bc50-08059008fecd",
                        "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                        "19579ce4-0a7e-389d-beaf-ceb415f215e8",
                        "589687f2-9044-364c-aaac-707b0f97823c",
                        "378fb80b-3f49-35db-a3a3-09d2cca355f1",
                        "3a1dc7b1-7eef-302a-8d65-c1b776272a30",
                        "5dfb8cec-10b6-33ad-b722-072f815ea2e4",
                        "466c700e-81ac-3678-953b-c88c7e4df6dd",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "88e41fc4-5017-3b85-b56d-7a3a9feda349",
                        "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4",
                        "dc53afad-12a0-3784-858f-00d211e93e90",
                        "55ea46e6-b9c6-39e5-a343-70717064842e",
                        "73d2ba4e-5d84-3d4b-86ef-9989dcc9beba",
                        "0749bf7d-d4e7-35fa-aea8-3da9bb5b3937",
                        "db79e4db-ecd2-3e0c-ba77-36850a23e810",
                        "49d56ab9-28be-353c-921c-5ab492835eaf",
                        "4e89fea4-1ab6-3157-a335-2adf9b1b33c2",
                        "37cc44e3-1053-3f01-ba4b-b7cee5d65c85",
                        "75317e6a-dd30-3fb7-8e1b-6394237d64db",
                        "4f3be65d-3506-3cd4-a538-ea72f62baa95",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "556eb168-c8d8-36f0-98d0-edf9b331c30b",
                        "265d8170-54fc-33ea-846c-9c4d888ed0e2",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "ecad122e-a2e9-35a8-b798-d20bffccbf20",
                        "6ad37a60-5968-37cf-a753-b39ddd19e379",
                        "30ff119c-62bd-325c-986f-1e7889578200",
                        "ade07610-2156-3b77-af52-71aaa33a8a32",
                        "2d207fb8-1116-3415-92ee-b7ee964cf312",
                        "51eed60c-eceb-3511-8506-03c78bfaebed",
                        "8a819b13-19a3-30a3-a369-306016a392b8",
                        "233bc275-5a82-338e-8bb6-ecca7feb7762",
                        "9e5e4071-a9d1-39e3-b657-84083543f591",
                        "1a5b4cfd-7c25-3375-86ea-0f23a4f8d07d",
                        "cc0fe12b-f306-32df-bd3d-635784fe57c9",
                        "4f563011-ab0d-30a7-9851-097347585757",
                        "42d49741-9a55-38f8-9fbb-f0897fb40734",
                        "b0e3a6ee-8141-3d32-8a4a-e3b08c47a7ac",
                        "f49aebeb-90a4-3d3a-b26b-adae55e4200e",
                        "5de89c9e-69d5-346a-8e30-928078064499",
                        "893a3274-dc45-3f91-b256-983a83c8b153",
                        "bf16af9c-8140-3c4c-8f16-7b4d9487a2d0",
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                        "fe506aea-138e-36a4-ad7f-b74ea420b651",
                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                        "e465935f-f61a-353e-8542-14ab25c51637",
                        "c75382ff-f9ec-30ed-a3bf-989eaa3734cf",
                        "4b6b69c8-934c-3886-8346-1a12c57bfc30",
                        "33b4c0cf-74a0-325e-a1d3-086af7ba8f90",
                        "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                        "3cf4551f-131d-3653-b022-7023b6c1c1fb",
                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                        "b6424b2f-d8a2-3715-8cf2-fe3b6d9e67f5",
                        "c93a72a2-8bd0-3165-9bc2-221b16efd538",
                        "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                        "a8caa264-b568-3bad-9e7a-13fb2c1eb459",
                        "c9697069-9494-3aca-92c7-21498e70a573",
                        "ef90ba8d-7bb1-386d-ac40-6e3764fb4b2d",
                        "c80cda9e-fd4d-38c1-9240-3bd0ab9e6143",
                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                    ]
                }
            ],
            "proteinIds": []
        },
        {
            "accession": "KW-9994",
            "id": "Domain",
            "category": [],
            "children": [
                {
                    "accession": "KW-0676",
                    "id": "Redox-active center",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                        "9e5e4071-a9d1-39e3-b657-84083543f591",
                        "d0a863af-2c08-32bb-a635-085fb5147e18",
                        "850143d4-9315-3889-9aad-60bd107000ef",
                        "89ded291-4348-3f8d-be43-56e42b9311d0",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "ff86e71c-2da2-3e32-83da-8fa5fc4342ad",
                        "e8c576ae-9b77-3b43-991e-d7dca04bcfd9",
                        "448e34c1-4fd2-3d95-b078-9642f7347452"
                    ]
                },
                {
                    "accession": "KW-0129",
                    "id": "CBS domain",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "1bc1abec-4cdd-3bfd-a2cc-56666581251f"
                    ]
                },
                {
                    "accession": "KW-0802",
                    "id": "TPR repeat",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "62418546-9356-318c-93fe-d6830db6d92a"
                    ]
                },
                {
                    "accession": "KW-0175",
                    "id": "Coiled coil",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "d0894567-8e24-31ed-9ad2-5fc20c27125f",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                        "49d56ab9-28be-353c-921c-5ab492835eaf",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "5492ee99-1adc-3f4a-8494-70a45e6b2141",
                        "62418546-9356-318c-93fe-d6830db6d92a"
                    ]
                },
                {
                    "accession": "KW-0677",
                    "id": "Repeat",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "a32c23cc-0d6c-3bb5-a17f-484494775219",
                        "67d5473b-2d87-339e-8eb3-8dacbf198014",
                        "dbf5255d-b46f-3ff6-af72-b63754671791",
                        "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                        "1c7ba006-0968-38fb-81e4-325187699247",
                        "62418546-9356-318c-93fe-d6830db6d92a",
                        "c9a3ff3d-57da-39c5-8e00-846dfb3dcc5c",
                        "41ac41e5-9f5c-3581-89e0-4af87818445c",
                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497",
                        "3d0070cc-e1db-31f6-8f49-580883bc373d",
                        "71a490c7-33e5-39df-bd3b-5b5c0a876a35",
                        "ed1aec27-3381-3466-912b-0a050407a275",
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "0749bf7d-d4e7-35fa-aea8-3da9bb5b3937",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "ba36ec06-b78c-367a-8292-4910795c560c",
                        "1bc1abec-4cdd-3bfd-a2cc-56666581251f",
                        "af13e9f3-b720-3a88-b110-d3da50d6671d",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "c893794f-f410-32ba-9458-4f795169055c",
                        "59bd70e6-31fe-3e3e-9f6b-a0e4fbdaef5d",
                        "45663484-0ecc-3c24-be1f-fa2773ca7012",
                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4",
                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                        "741fa666-fa9f-3650-990b-c1458057cfdf",
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "eaaada09-fde0-346f-8595-8c91e4029379",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54"
                    ]
                },
                {
                    "accession": "KW-0450",
                    "id": "Lipoyl",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "e7d1fcbc-7893-3f9f-b547-6c58816c821b",
                        "b8d23914-181f-355f-8228-0029e8189235",
                        "71a490c7-33e5-39df-bd3b-5b5c0a876a35"
                    ]
                },
                {
                    "accession": "KW-0315",
                    "id": "Glutamine amidotransferase",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "65fa8534-717f-3498-8121-97a9bedf951b",
                        "694c6f10-0972-33dd-bc1f-d8859599cbca",
                        "4bb7e457-7040-302b-a82c-076c07ca7304",
                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                        "2b8b93cd-9934-3ca7-bf02-031505a26bbe",
                        "3d0070cc-e1db-31f6-8f49-580883bc373d"
                    ]
                },
                {
                    "accession": "KW-0812",
                    "id": "Transmembrane",
                    "category": [
                        "Cellular component",
                        "Domain"
                    ],
                    "children": [
                        {
                            "accession": "KW-1133",
                            "id": "Transmembrane helix",
                            "category": [
                                "Cellular component",
                                "Domain"
                            ],
                            "children": [],
                            "proteinIds": [
                                "24e689b0-03b1-3408-a49a-151692cf629c",
                                "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                                "e8d0005d-a3a3-349a-9db4-d3d95377b718",
                                "00926371-a557-341d-8941-45ad2fafee4b",
                                "0d552860-d03d-3427-9dfb-58c6c723f3b7",
                                "fa1a4201-301f-3503-9d5d-51aa06fb8a10",
                                "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                                "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                                "88dafdd2-f1e6-3992-aee5-9fd6763d3056"
                            ]
                        },
                        {
                            "accession": "KW-0626",
                            "id": "Porin",
                            "category": [
                                "Domain",
                                "Molecular function",
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                "b6ec19d9-dd21-3506-899a-f675868a1b58"
                            ]
                        },
                        {
                            "accession": "KW-1134",
                            "id": "Transmembrane beta strand",
                            "category": [
                                "Cellular component",
                                "Domain"
                            ],
                            "children": [],
                            "proteinIds": [
                                "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                                "b6ec19d9-dd21-3506-899a-f675868a1b58",
                                "1f420626-52e3-37a6-aa7f-ccdab455504e"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0732",
                    "id": "Signal",
                    "category": [
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "8e45ccea-21f6-38f7-b11e-aa8914e50a61",
                        "91e1fc09-c4c0-371f-b84e-7272bf3d625a",
                        "abc501f5-c77c-3a30-b015-13b7fa11f8ce",
                        "9e2aad1c-75e8-3a3d-b508-aa845635d7b7",
                        "18dca3e9-3ec2-3bfd-ae59-bd227ada733e",
                        "a649cce1-bf69-3fe5-beec-c47f52985976",
                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                        "a87ba622-c92d-35b1-935e-52f538af07f4",
                        "6ca00c54-ed8f-3b5d-8329-051e1839dbf9",
                        "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                        "d1cdacd5-d00d-333b-bdba-0df200229b25",
                        "62418546-9356-318c-93fe-d6830db6d92a",
                        "3ebfa9ba-a4ef-3004-8b2d-24c105fbd7db",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "72687302-febc-300a-8b6a-aa54e09d2bef",
                        "5c3518d4-8a4b-3d79-b52a-29fbe0fa98b6",
                        "fc16d218-6350-3e2e-9c9c-05fefd49e5a3",
                        "c3f30764-2155-3be3-ab4c-91383e8cadac",
                        "59c6e653-4609-3099-9739-383b53092172",
                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                        "8d312403-4af6-3fe9-ac93-7d07034f0424",
                        "448e34c1-4fd2-3d95-b078-9642f7347452",
                        "c4cdd1b7-0c46-3cb6-90c9-f2b2a1983a58",
                        "d303538a-3c87-3104-ab84-288d4297de26",
                        "96a76a6b-7f10-333e-b525-f2784214ee48",
                        "f9202d3d-2dde-33d7-bae6-e3b73b4adb52",
                        "65a926bf-d216-3943-9809-9013a707a4ac",
                        "73cf2129-d481-3182-8234-5c58426cbd8b",
                        "c907cf8f-adfa-3c36-8478-87a02c27fd25",
                        "fd3ba7bc-eb46-3a01-85a3-d00a8dd4a8e9",
                        "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                        "3cce71b9-bb9f-3f5f-a191-89455ca5884d",
                        "17a73fa6-5041-35e9-99ec-2bc37e59a4af",
                        "08a9b134-8813-352b-90bb-807c80c4b69b",
                        "238b3de0-b3c2-3f07-911a-decfc93154ab",
                        "b2a03d2e-7fe3-3375-a542-95440bd78a9a",
                        "5506f1e2-c28b-3da3-90e5-9cb3d8ba2457",
                        "e7011909-03cc-3eb0-aef7-b8a96ce4f2ce",
                        "17d3560c-19d3-3141-b1e2-c20ba87599ce",
                        "d2a66d40-466d-3cd7-824e-b1c172bed1e1",
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5",
                        "8dada997-5a23-3c7b-9833-68bc56ba560f",
                        "bfaf87cb-327c-3863-922f-5b21037726d2",
                        "1f420626-52e3-37a6-aa7f-ccdab455504e",
                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                        "80ee5820-8165-3fc1-858f-330ea6212f00",
                        "a195423a-3a12-3931-95da-fa2d60d0a3ab",
                        "9fad2f14-2923-3e89-ae56-7411fd6817d6",
                        "6ed557e6-3f94-30a4-87a0-ba7bc8dff3e8",
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "25039a14-8246-3b5f-ae47-56711abe51d9",
                        "3fb2549c-0452-3210-8be4-9f3a0d2cffea",
                        "d4bc5910-f71b-3ea9-b946-28450da93565",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "52f8ff6d-7ba8-3d7b-a836-b845ad79c30c",
                        "c893794f-f410-32ba-9458-4f795169055c",
                        "59bd70e6-31fe-3e3e-9f6b-a0e4fbdaef5d",
                        "d572aa6a-b6a6-3fad-9282-a20959414f10",
                        "cacb2a3c-4558-30e7-93cd-c700f6ed12ea",
                        "806279ae-507c-35c6-bffa-477f138c7919",
                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4",
                        "e39c02ac-664f-3b5e-8d8a-b20af3741469",
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "0cea6dce-3ee8-34fd-bbaa-3dfde75abf19"
                    ]
                },
                {
                    "accession": "KW-0863",
                    "id": "Zinc-finger",
                    "category": [
                        "Ligand",
                        "Domain"
                    ],
                    "children": [],
                    "proteinIds": [
                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                        "38a74cb1-c9ac-34e2-aec5-a461bd6814cf",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                        "1c7ba006-0968-38fb-81e4-325187699247",
                        "5492ee99-1adc-3f4a-8494-70a45e6b2141"
                    ]
                }
            ],
            "proteinIds": []
        },
        {
            "accession": "KW-9990",
            "id": "Technical term",
            "category": [],
            "children": [
                {
                    "accession": "KW-0903",
                    "id": "Direct protein sequencing",
                    "category": [
                        "Technical term"
                    ],
                    "children": [],
                    "proteinIds": [
                        "c540cf02-6cfa-3db3-96bb-ab069be25221",
                        "9e9ed0b1-71c7-32bc-b2b8-7b9fa84028bb",
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "0b0d8759-2956-37cb-8c34-5679bc86a9ec",
                        "a4a016f7-af3c-37b2-bbb0-869da4729061",
                        "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                        "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                        "4f77b69d-5001-3a9f-8759-0c12d8fa535f",
                        "91bd43f5-ae68-3b80-aa75-7a9c967b9398",
                        "a9eb05a1-16a7-34d3-9881-e542cf92b1be",
                        "3ebfa9ba-a4ef-3004-8b2d-24c105fbd7db",
                        "24e689b0-03b1-3408-a49a-151692cf629c",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497",
                        "07e6492c-255a-3b65-8400-bb99932324d2",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752",
                        "ed1aec27-3381-3466-912b-0a050407a275",
                        "c4cdd1b7-0c46-3cb6-90c9-f2b2a1983a58",
                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                        "7494b152-dbc6-395d-abdd-1b549511a731",
                        "d303538a-3c87-3104-ab84-288d4297de26",
                        "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                        "f008373a-488a-36c1-85e9-5fdfb99fa95a",
                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                        "5492ee99-1adc-3f4a-8494-70a45e6b2141",
                        "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                        "8d98038c-6dec-3415-91e1-26fe011fc089",
                        "2b4f5dc3-75d1-35b3-abcd-059ce2986a5b",
                        "8aa62ad5-4534-391c-9963-a1c34edc7e3d",
                        "238b3de0-b3c2-3f07-911a-decfc93154ab",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65",
                        "ce48f6ed-2f5e-3052-95b4-c46400f3e1d6",
                        "e7d1fcbc-7893-3f9f-b547-6c58816c821b",
                        "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                        "56a38ea7-2f0b-3578-bfef-7f17b93bd275",
                        "d6e069e7-f59d-3a6e-9d5c-cea42b8ae93a",
                        "1d192ad9-a2cf-3749-8986-2f362feb18bc",
                        "94de208d-a804-3a38-9f33-570c2fb3ac2d",
                        "36871dcf-e157-3efa-83dd-6a24b6f8caf0",
                        "83d458c1-eb11-3b0d-956b-a66b398fdfab",
                        "3a22854b-60f2-3397-a949-92484f1a8b31",
                        "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                        "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                        "17d3560c-19d3-3141-b1e2-c20ba87599ce",
                        "cdf8527e-4138-3172-9447-a37f11f48ad1",
                        "76463a0c-c647-39ea-b9e9-12ca32648522",
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5",
                        "f69c6421-9e2f-3f78-94c8-2b7c6d5a6bc7",
                        "e2082585-5d94-3f70-bae9-f801d1264938",
                        "fb76d466-0b71-3eca-8541-66496335aa3d",
                        "ad28a8bf-fcd4-3132-968d-795293745983",
                        "9a58754a-5c56-3c07-84b7-5360a962f090",
                        "05afc641-0745-31bb-bc3c-ff9689e350ff",
                        "3d0070cc-e1db-31f6-8f49-580883bc373d",
                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                        "25039a14-8246-3b5f-ae47-56711abe51d9",
                        "275512af-b219-34ae-bcc9-a9a502193acd",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "a5f2faa4-5802-3b3a-bc58-e907abc60363",
                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                        "65886732-974b-3f7a-a011-90682a3b3ef5",
                        "806279ae-507c-35c6-bffa-477f138c7919",
                        "b3db6762-1dfb-3268-818b-228994a05da0",
                        "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                        "01c0a4d3-91cd-3222-b59c-2cb2c5b1bea8",
                        "91e1fc09-c4c0-371f-b84e-7272bf3d625a",
                        "a32c23cc-0d6c-3bb5-a17f-484494775219",
                        "9e2aad1c-75e8-3a3d-b508-aa845635d7b7",
                        "0791e3a6-5ee6-329f-a7d1-b8450305c4f7",
                        "09824aca-3ccb-3f94-9b21-7d95f424c697",
                        "18dca3e9-3ec2-3bfd-ae59-bd227ada733e",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                        "97eec5eb-fd52-3f21-b970-f2ab55a49582",
                        "a681313b-a131-3404-a255-6ff70a8ea5db",
                        "9aa82d48-90c7-39ac-9158-573fb5dcfc10",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "ce57e563-d2a4-34e5-80c0-9854d14c2e25",
                        "fa03fcb6-1a8e-3c1b-9b91-fad4d162279e",
                        "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                        "8854d795-3ec4-373d-a0c1-10b3373df75d",
                        "2438b215-94d2-3e1e-8722-63acfe9ba76f",
                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                        "71a490c7-33e5-39df-bd3b-5b5c0a876a35",
                        "448e34c1-4fd2-3d95-b078-9642f7347452",
                        "8738a805-3daf-35e9-9fe4-295483914470",
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "20366e64-78e7-38e8-9158-29d148f86506",
                        "2b8b93cd-9934-3ca7-bf02-031505a26bbe",
                        "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                        "108cc793-6765-346b-85b7-9ea261151b2d",
                        "741fa666-fa9f-3650-990b-c1458057cfdf",
                        "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                        "41885b05-5303-35e8-b45c-b8ae67bb2a19",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "8bc41f8b-7a09-3d58-94e5-f48e87ab5611",
                        "4939a1cd-d4b2-3c7d-9053-b02c16db7e41",
                        "adec9c8c-296a-3769-9203-3969d932bc85",
                        "3651de6c-1487-3148-bbd8-963d04a47b07",
                        "8b4efabd-0c5b-3f60-af7e-fd20fd31a750",
                        "5532cc3e-59d5-358b-8546-2ce4b04f665f",
                        "e7011909-03cc-3eb0-aef7-b8a96ce4f2ce",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "952a1258-8809-3789-8903-5bbbb8605308",
                        "ba9ea4f2-a2be-3e4d-b65c-5a32e4fd7009",
                        "8dada997-5a23-3c7b-9833-68bc56ba560f",
                        "1f420626-52e3-37a6-aa7f-ccdab455504e",
                        "4fb159bd-103d-3af0-a750-ffb1d7efc7c4",
                        "76c71bc8-c46d-3acc-ad17-d1ca3625783a",
                        "51aaf3dc-b27b-37b8-b84c-06c1895d0b3c",
                        "2850d15f-f930-31e8-8764-356a1cb98b51",
                        "b32cef3a-9da0-3b0d-b90e-b1895645e93a",
                        "6ed557e6-3f94-30a4-87a0-ba7bc8dff3e8",
                        "831a5687-da56-3321-a6ae-8999dc605323",
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "8a61fe9f-cea2-3471-8e8f-d8ed4543e47e",
                        "b640034e-80a9-37d0-aa8b-98b1180e4740",
                        "3fb2549c-0452-3210-8be4-9f3a0d2cffea",
                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                        "81c0dd1a-7823-3d99-a23c-d0ae419b6e0f",
                        "c43a0411-8126-3d5f-8344-bb6d01bcee7b",
                        "59bd70e6-31fe-3e3e-9f6b-a0e4fbdaef5d",
                        "ab622b9a-421a-3a59-a5fe-ff745ba0d578",
                        "e83511de-fef5-3734-8e5a-9d02ae774102",
                        "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                        "b378c052-6677-38dc-a7b5-3174eed7d879",
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "3fe5ee9b-cebd-3413-ad18-e2b50b8b92c3",
                        "d3380e9e-3ca9-30d9-9fbe-d22064b59906",
                        "5e26b926-1841-3edf-83f8-86e196ad600b",
                        "9675ae45-136a-397a-984c-15dedd7ec1c6",
                        "231cde29-fd90-3fde-b0d3-6f6deea14d2e",
                        "a1fec884-fbd2-33ee-94fe-6eaf5b7f10c7",
                        "db66e621-4ba9-3952-8ecb-c161094c8675",
                        "9ec8e26f-cf5c-360f-933b-f9a69dec0212",
                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                        "278f5526-d165-387c-a9bb-f3981199b3ac",
                        "4bb7e457-7040-302b-a82c-076c07ca7304",
                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                        "d1cdacd5-d00d-333b-bdba-0df200229b25",
                        "d7906d1a-5ded-3887-95dd-1dd0f82ce01a",
                        "4363326f-335e-3c29-87d8-e609b33aa633",
                        "d90a1ebb-127f-354d-b3d4-a7c56ce5df07",
                        "16b312fe-c87b-3fba-b087-d72530affc71",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "e130092e-4136-3244-a836-2f098cdd0a85",
                        "73f9cc84-4db1-3636-90c2-5f1a7990353e",
                        "59c6e653-4609-3099-9739-383b53092172",
                        "96a76a6b-7f10-333e-b525-f2784214ee48",
                        "80831e58-97dc-3bfa-995d-6398bb4b3eea",
                        "cf7b4c0c-5e3e-35ed-80de-4e6a56a14bee",
                        "c907cf8f-adfa-3c36-8478-87a02c27fd25",
                        "70d716e7-2e21-3fc7-9d5a-eaf73bac2f60",
                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "e8c576ae-9b77-3b43-991e-d7dca04bcfd9",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                        "d14ba956-67ec-339b-bc82-f5d87a3b9a28",
                        "c16a16e2-43d4-3291-a840-fefd5939be2e",
                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                        "d0a863af-2c08-32bb-a635-085fb5147e18",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd",
                        "850143d4-9315-3889-9aad-60bd107000ef",
                        "d45584d2-50ec-3dca-9d34-f5bd15067a15",
                        "716e68a9-7fbc-316b-b5ec-7ff2a5d22076",
                        "b314da77-03a0-3453-babc-0e706147208f",
                        "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                        "f3f9af9f-0f3b-3a9e-8331-9b1573a90dfc",
                        "93fa29eb-4f4d-35c1-8ab2-ab5e3612a356",
                        "f322a8ae-ec9a-3952-9737-150d950df3d6",
                        "bf533f2a-ef1f-3d05-b6d0-754109696a77",
                        "c168653e-3c8d-3bb3-9f52-032fea2ce506",
                        "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "d76a3d39-6566-3b7f-a805-9d8501c00579",
                        "b5d2ac66-2405-3d81-a2bc-520d5bb79ca1",
                        "4b892b3a-20ad-3325-ba60-7d03b9f71716",
                        "71b3ae7c-a1eb-3390-ae68-92cf4f9f8c00",
                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4",
                        "ff86e71c-2da2-3e32-83da-8fa5fc4342ad",
                        "9fb7f91c-80c2-3572-b7a2-26f2fc597147",
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "1bec76be-687a-3641-8a3a-114017a4ad6f",
                        "aff29f09-97aa-3745-82d5-95a81522ee3f",
                        "0b0bf4f1-305f-3216-953b-fac86a8062a8",
                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                        "e9f58804-0b00-30a7-a5a0-9edb29d4dc3c",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "84044b10-cc38-3dc6-b5a1-3d1ae107b0b9",
                        "5c3e9984-9197-37f7-8c69-deb288ba224a",
                        "456bfd92-36c5-3fd9-8803-aed14a8ec2d0",
                        "36a9a461-0ce7-3e5e-bc50-08059008fecd",
                        "72687302-febc-300a-8b6a-aa54e09d2bef",
                        "589687f2-9044-364c-aaac-707b0f97823c",
                        "378fb80b-3f49-35db-a3a3-09d2cca355f1",
                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                        "41794e2f-1527-3aa8-a594-ce47f97a857d",
                        "1946a92e-91ee-3dd8-9b68-dd04d4cc8747",
                        "45589e0d-6140-3039-b9ea-5e4c18478b35",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "c3f30764-2155-3be3-ab4c-91383e8cadac",
                        "ce422a1e-705d-3108-8141-d9bb318b50ce",
                        "73d2ba4e-5d84-3d4b-86ef-9989dcc9beba",
                        "0749bf7d-d4e7-35fa-aea8-3da9bb5b3937",
                        "49d56ab9-28be-353c-921c-5ab492835eaf",
                        "fb9a8b64-0c46-3a4a-a7ca-8951cb3a41f7",
                        "9c564811-95c0-3848-87bc-132b90327655",
                        "af13e9f3-b720-3a88-b110-d3da50d6671d",
                        "9d502900-4b70-3bc5-8723-daa4db8308a8",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "65a926bf-d216-3943-9809-9013a707a4ac",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "6214d45c-e9b8-30c3-9887-4acaee29688e",
                        "47dbf2b2-9d17-378f-b54b-ad48adfcdd1f",
                        "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                        "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                        "55adc234-ce10-3909-a3a2-04887fe6fa3b",
                        "2d207fb8-1116-3415-92ee-b7ee964cf312",
                        "08a9b134-8813-352b-90bb-807c80c4b69b",
                        "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                        "233bc275-5a82-338e-8bb6-ecca7feb7762",
                        "6d505dc1-74f4-3a3d-a659-b5f22e58656d",
                        "cc0fe12b-f306-32df-bd3d-635784fe57c9",
                        "3717bd7a-b503-325b-be3e-bf6dbc73ec6e",
                        "4f563011-ab0d-30a7-9851-097347585757",
                        "d2a66d40-466d-3cd7-824e-b1c172bed1e1",
                        "bfaf87cb-327c-3863-922f-5b21037726d2",
                        "01f51658-daa5-3b6f-8e36-cc556c007d30",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "00ed2ace-ce39-3bc2-94fb-a1e81e860271",
                        "f49aebeb-90a4-3d3a-b26b-adae55e4200e",
                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                        "fc22b497-2a0a-39ca-915b-8037852bb5a9",
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                        "78b9d6d6-8e32-38f0-862f-745586f1af9b",
                        "cacb2a3c-4558-30e7-93cd-c700f6ed12ea",
                        "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                        "b07f5eb7-a050-352d-80c3-6de294a6ba3f",
                        "c80cda9e-fd4d-38c1-9240-3bd0ab9e6143",
                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                    ]
                },
                {
                    "accession": "KW-0582",
                    "id": "Pharmaceutical",
                    "category": [
                        "Technical term"
                    ],
                    "children": [],
                    "proteinIds": [
                        "17d3560c-19d3-3141-b1e2-c20ba87599ce"
                    ]
                },
                {
                    "accession": "KW-0002",
                    "id": "3D-structure",
                    "category": [
                        "Technical term"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9e9ed0b1-71c7-32bc-b2b8-7b9fa84028bb",
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "0b0d8759-2956-37cb-8c34-5679bc86a9ec",
                        "7a19fc76-3018-3ea7-a941-ca7feb7122bf",
                        "a87ba622-c92d-35b1-935e-52f538af07f4",
                        "a4a016f7-af3c-37b2-bbb0-869da4729061",
                        "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                        "4f77b69d-5001-3a9f-8759-0c12d8fa535f",
                        "91bd43f5-ae68-3b80-aa75-7a9c967b9398",
                        "a9eb05a1-16a7-34d3-9881-e542cf92b1be",
                        "3ebfa9ba-a4ef-3004-8b2d-24c105fbd7db",
                        "24e689b0-03b1-3408-a49a-151692cf629c",
                        "78e337f5-5c30-38db-a09d-8c2c35083444",
                        "a3aebedd-25dd-3dfe-9ade-83d1e8475627",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497",
                        "07e6492c-255a-3b65-8400-bb99932324d2",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752",
                        "ed1aec27-3381-3466-912b-0a050407a275",
                        "b5d80ba7-a45d-3460-98ef-af0d15f43c1c",
                        "c4cdd1b7-0c46-3cb6-90c9-f2b2a1983a58",
                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                        "7494b152-dbc6-395d-abdd-1b549511a731",
                        "8ce280e3-65c9-3634-8594-3ffe355b2d58",
                        "d303538a-3c87-3104-ab84-288d4297de26",
                        "8ef52e4a-e7b1-3daf-ba97-b79e67a3d302",
                        "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                        "08db55e4-1db7-3bce-9bf1-770750acbb97",
                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                        "f008373a-488a-36c1-85e9-5fdfb99fa95a",
                        "ff9ecfec-7cdf-3b16-9f58-df1393f56211",
                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                        "0ff36b3a-ba1f-3928-a34c-e9e69083cf4f",
                        "035d3181-8ee3-3152-bed8-9644e47d97dc",
                        "450d855a-66b5-3220-a455-c3824dd9226b",
                        "5492ee99-1adc-3f4a-8494-70a45e6b2141",
                        "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                        "504bbf4f-f5da-3e94-966a-0a3a6e793cfe",
                        "2b4f5dc3-75d1-35b3-abcd-059ce2986a5b",
                        "8aa62ad5-4534-391c-9963-a1c34edc7e3d",
                        "5364d153-bc0b-39b7-aabd-d129e885ca0a",
                        "9f714626-b769-35ac-aafd-600c07ba1299",
                        "238b3de0-b3c2-3f07-911a-decfc93154ab",
                        "d29caf3d-891e-37fc-806b-e50965ca02db",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65",
                        "5cf06ce6-2b9e-3618-a775-454fd37dbb54",
                        "ce48f6ed-2f5e-3052-95b4-c46400f3e1d6",
                        "e7d1fcbc-7893-3f9f-b547-6c58816c821b",
                        "56a38ea7-2f0b-3578-bfef-7f17b93bd275",
                        "d6e069e7-f59d-3a6e-9d5c-cea42b8ae93a",
                        "1d192ad9-a2cf-3749-8986-2f362feb18bc",
                        "94de208d-a804-3a38-9f33-570c2fb3ac2d",
                        "e4776e4a-90b7-3421-af46-968294e1b9ec",
                        "36871dcf-e157-3efa-83dd-6a24b6f8caf0",
                        "83d458c1-eb11-3b0d-956b-a66b398fdfab",
                        "3a22854b-60f2-3397-a949-92484f1a8b31",
                        "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                        "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                        "17d3560c-19d3-3141-b1e2-c20ba87599ce",
                        "cdf8527e-4138-3172-9447-a37f11f48ad1",
                        "76463a0c-c647-39ea-b9e9-12ca32648522",
                        "1c7ba006-0968-38fb-81e4-325187699247",
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5",
                        "f69c6421-9e2f-3f78-94c8-2b7c6d5a6bc7",
                        "aee8e5d8-a562-367c-aef5-9667b804c2ed",
                        "1f8e5f6c-cb93-3658-a7d0-407519b276a7",
                        "09f9c8a9-8471-372e-ae8a-4f00a1503757",
                        "fb76d466-0b71-3eca-8541-66496335aa3d",
                        "9a58754a-5c56-3c07-84b7-5360a962f090",
                        "8b9be9c8-0db3-3271-bd73-dd7b1e08fe27",
                        "05afc641-0745-31bb-bc3c-ff9689e350ff",
                        "3d0070cc-e1db-31f6-8f49-580883bc373d",
                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                        "f95204fe-2fc7-3512-9ae4-69e673ef6f03",
                        "4027df06-664f-319e-a3d3-bfb1459894cf",
                        "25039a14-8246-3b5f-ae47-56711abe51d9",
                        "275512af-b219-34ae-bcc9-a9a502193acd",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "a5f2faa4-5802-3b3a-bc58-e907abc60363",
                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                        "8bfa1252-1f9b-3043-93d8-f5639d9cf074",
                        "d572aa6a-b6a6-3fad-9282-a20959414f10",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                        "e4686fc4-66af-3047-9254-e77709e9fcd6",
                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                        "65886732-974b-3f7a-a011-90682a3b3ef5",
                        "806279ae-507c-35c6-bffa-477f138c7919",
                        "5ead7332-b2e5-3089-a871-a22450e06376",
                        "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                        "1c961799-c8e6-3571-b63c-81642cf2ab91",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "dccf3c9e-dddf-39e4-9061-2e563185c9b9",
                        "0cea6dce-3ee8-34fd-bbaa-3dfde75abf19",
                        "91e1fc09-c4c0-371f-b84e-7272bf3d625a",
                        "a32c23cc-0d6c-3bb5-a17f-484494775219",
                        "9e2aad1c-75e8-3a3d-b508-aa845635d7b7",
                        "0791e3a6-5ee6-329f-a7d1-b8450305c4f7",
                        "09824aca-3ccb-3f94-9b21-7d95f424c697",
                        "18dca3e9-3ec2-3bfd-ae59-bd227ada733e",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "fee40d4c-a185-33ea-b50a-dd88f5e77e13",
                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                        "afb5765c-5d4f-354c-92be-d11f02153167",
                        "9aa82d48-90c7-39ac-9158-573fb5dcfc10",
                        "62418546-9356-318c-93fe-d6830db6d92a",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "fa03fcb6-1a8e-3c1b-9b91-fad4d162279e",
                        "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                        "8854d795-3ec4-373d-a0c1-10b3373df75d",
                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                        "71a490c7-33e5-39df-bd3b-5b5c0a876a35",
                        "448e34c1-4fd2-3d95-b078-9642f7347452",
                        "8738a805-3daf-35e9-9fe4-295483914470",
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                        "108cc793-6765-346b-85b7-9ea261151b2d",
                        "3d521670-8170-3b6b-a8e3-4ff38f616c53",
                        "fd3ba7bc-eb46-3a01-85a3-d00a8dd4a8e9",
                        "03bc60d2-2bbe-34fb-87c4-0413718dd28a",
                        "741fa666-fa9f-3650-990b-c1458057cfdf",
                        "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                        "41885b05-5303-35e8-b45c-b8ae67bb2a19",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "8bc41f8b-7a09-3d58-94e5-f48e87ab5611",
                        "4939a1cd-d4b2-3c7d-9053-b02c16db7e41",
                        "adec9c8c-296a-3769-9203-3969d932bc85",
                        "fbfddc81-7c4c-3720-8aa0-df1f266bcd50",
                        "6f42f3aa-5abc-3149-9c90-8514279c3116",
                        "8b4efabd-0c5b-3f60-af7e-fd20fd31a750",
                        "5532cc3e-59d5-358b-8546-2ce4b04f665f",
                        "e7011909-03cc-3eb0-aef7-b8a96ce4f2ce",
                        "89ded291-4348-3f8d-be43-56e42b9311d0",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "dbf5255d-b46f-3ff6-af72-b63754671791",
                        "952a1258-8809-3789-8903-5bbbb8605308",
                        "ba9ea4f2-a2be-3e4d-b65c-5a32e4fd7009",
                        "1f420626-52e3-37a6-aa7f-ccdab455504e",
                        "76c71bc8-c46d-3acc-ad17-d1ca3625783a",
                        "51aaf3dc-b27b-37b8-b84c-06c1895d0b3c",
                        "2850d15f-f930-31e8-8764-356a1cb98b51",
                        "41ac41e5-9f5c-3581-89e0-4af87818445c",
                        "806d2373-2eec-33a0-bd4a-5478b9c46f34",
                        "b32cef3a-9da0-3b0d-b90e-b1895645e93a",
                        "6ed557e6-3f94-30a4-87a0-ba7bc8dff3e8",
                        "831a5687-da56-3321-a6ae-8999dc605323",
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "b640034e-80a9-37d0-aa8b-98b1180e4740",
                        "3fb2549c-0452-3210-8be4-9f3a0d2cffea",
                        "3d21682c-3229-3273-aa6f-4ff872d59326",
                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                        "81c0dd1a-7823-3d99-a23c-d0ae419b6e0f",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "c893794f-f410-32ba-9458-4f795169055c",
                        "ab622b9a-421a-3a59-a5fe-ff745ba0d578",
                        "e83511de-fef5-3734-8e5a-9d02ae774102",
                        "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                        "b378c052-6677-38dc-a7b5-3174eed7d879",
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "3fe5ee9b-cebd-3413-ad18-e2b50b8b92c3",
                        "5e26b926-1841-3edf-83f8-86e196ad600b",
                        "9675ae45-136a-397a-984c-15dedd7ec1c6",
                        "231cde29-fd90-3fde-b0d3-6f6deea14d2e",
                        "db66e621-4ba9-3952-8ecb-c161094c8675",
                        "cafca265-a3bf-3afb-a1ec-32aea9845b77",
                        "9ec8e26f-cf5c-360f-933b-f9a69dec0212",
                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                        "4a37b120-5d96-307b-9f0e-47e33b8e4e86",
                        "984c81a6-e816-32b6-b07b-69dadffc4109",
                        "278f5526-d165-387c-a9bb-f3981199b3ac",
                        "4bb7e457-7040-302b-a82c-076c07ca7304",
                        "fa0d3c94-9697-3f67-b435-b8aef0a96a5a",
                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                        "5f414d39-9df3-3411-b4a1-7c6e35b3487a",
                        "d1cdacd5-d00d-333b-bdba-0df200229b25",
                        "4363326f-335e-3c29-87d8-e609b33aa633",
                        "d90a1ebb-127f-354d-b3d4-a7c56ce5df07",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "ccdb8a84-5e15-3840-87f4-9a2366f7bc56",
                        "e130092e-4136-3244-a836-2f098cdd0a85",
                        "73f9cc84-4db1-3636-90c2-5f1a7990353e",
                        "4231711d-4aa5-3b4f-b679-3a29453a8390",
                        "59c6e653-4609-3099-9739-383b53092172",
                        "57e0a113-56ce-3ca0-9c08-6400937f1a7e",
                        "ba36ec06-b78c-367a-8292-4910795c560c",
                        "4086e8f6-19f3-3e85-876e-0cf86edea088",
                        "f9202d3d-2dde-33d7-bae6-e3b73b4adb52",
                        "6eaa3a76-c2e7-3366-8a74-34cd0d3f6a23",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3",
                        "80831e58-97dc-3bfa-995d-6398bb4b3eea",
                        "c638711c-bdde-33be-a890-efdd9a8123be",
                        "c907cf8f-adfa-3c36-8478-87a02c27fd25",
                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                        "61d1c28a-4135-3674-af97-75e61945e833",
                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "e8c576ae-9b77-3b43-991e-d7dca04bcfd9",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                        "d14ba956-67ec-339b-bc82-f5d87a3b9a28",
                        "96efecbb-175f-371e-97b4-badca3f602a9",
                        "3ba5ec97-1eda-3dc5-b5c1-c093a5df4e11",
                        "c16a16e2-43d4-3291-a840-fefd5939be2e",
                        "33859abd-93ad-305a-839c-1c145e0e2e4a",
                        "62169f87-c02b-34c1-a2ea-1adb1fec4587",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd",
                        "e6c1af58-a061-30fc-94e7-02b4ca64be72",
                        "850143d4-9315-3889-9aad-60bd107000ef",
                        "d45584d2-50ec-3dca-9d34-f5bd15067a15",
                        "716e68a9-7fbc-316b-b5ec-7ff2a5d22076",
                        "b314da77-03a0-3453-babc-0e706147208f",
                        "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                        "f3f9af9f-0f3b-3a9e-8331-9b1573a90dfc",
                        "93fa29eb-4f4d-35c1-8ab2-ab5e3612a356",
                        "bf533f2a-ef1f-3d05-b6d0-754109696a77",
                        "c168653e-3c8d-3bb3-9f52-032fea2ce506",
                        "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                        "f6c700aa-e087-3675-889d-290443b8a219",
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "878e895a-9d6c-367c-ab7a-ea963aedcd08",
                        "d76a3d39-6566-3b7f-a805-9d8501c00579",
                        "b5d2ac66-2405-3d81-a2bc-520d5bb79ca1",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "71b3ae7c-a1eb-3390-ae68-92cf4f9f8c00",
                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4",
                        "ecad0a2b-6166-3c60-a6ff-039f2211b326",
                        "ff86e71c-2da2-3e32-83da-8fa5fc4342ad",
                        "9fb7f91c-80c2-3572-b7a2-26f2fc597147",
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "1bec76be-687a-3641-8a3a-114017a4ad6f",
                        "aff29f09-97aa-3745-82d5-95a81522ee3f",
                        "1d3a670d-1bfa-3117-9025-fba33bbc4a4c",
                        "ee7e3205-3af8-3742-84ba-5d1197f85c7e",
                        "1f2107a6-54c7-349a-9ecb-0304f17fb7f7",
                        "0b0bf4f1-305f-3216-953b-fac86a8062a8",
                        "1776f92b-eb74-3f0c-8d9a-c47a65663644",
                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                        "d96aa128-8e0e-3e85-9a47-31e1dd49e54f",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "84044b10-cc38-3dc6-b5a1-3d1ae107b0b9",
                        "456bfd92-36c5-3fd9-8803-aed14a8ec2d0",
                        "36a9a461-0ce7-3e5e-bc50-08059008fecd",
                        "589687f2-9044-364c-aaac-707b0f97823c",
                        "378fb80b-3f49-35db-a3a3-09d2cca355f1",
                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                        "41794e2f-1527-3aa8-a594-ce47f97a857d",
                        "1946a92e-91ee-3dd8-9b68-dd04d4cc8747",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "c3f30764-2155-3be3-ab4c-91383e8cadac",
                        "88e41fc4-5017-3b85-b56d-7a3a9feda349",
                        "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4",
                        "dc53afad-12a0-3784-858f-00d211e93e90",
                        "ce422a1e-705d-3108-8141-d9bb318b50ce",
                        "0749bf7d-d4e7-35fa-aea8-3da9bb5b3937",
                        "260d44be-d7fa-3dca-b1d8-96019c54f35a",
                        "fb9a8b64-0c46-3a4a-a7ca-8951cb3a41f7",
                        "1bc1abec-4cdd-3bfd-a2cc-56666581251f",
                        "37cc44e3-1053-3f01-ba4b-b7cee5d65c85",
                        "9c564811-95c0-3848-87bc-132b90327655",
                        "af13e9f3-b720-3a88-b110-d3da50d6671d",
                        "4f3be65d-3506-3cd4-a538-ea72f62baa95",
                        "9d502900-4b70-3bc5-8723-daa4db8308a8",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                        "5ec0a5dd-41b1-3cfa-8c37-0d803bcc79d4",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "6ad37a60-5968-37cf-a753-b39ddd19e379",
                        "6214d45c-e9b8-30c3-9887-4acaee29688e",
                        "47dbf2b2-9d17-378f-b54b-ad48adfcdd1f",
                        "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                        "9b645bb4-da5c-34a3-a6dc-2063b5c3faf4",
                        "676d3538-9c27-340d-af8c-dec20faf357b",
                        "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                        "08a9b134-8813-352b-90bb-807c80c4b69b",
                        "a5e34328-e12e-3425-add0-7428af49849e",
                        "51eed60c-eceb-3511-8506-03c78bfaebed",
                        "233bc275-5a82-338e-8bb6-ecca7feb7762",
                        "52a834ce-1be0-32e0-b9df-5ff1b3364874",
                        "9e5e4071-a9d1-39e3-b657-84083543f591",
                        "cc0fe12b-f306-32df-bd3d-635784fe57c9",
                        "3717bd7a-b503-325b-be3e-bf6dbc73ec6e",
                        "2b68a56e-970a-3df3-bf67-70665e758a03",
                        "4f563011-ab0d-30a7-9851-097347585757",
                        "d2a66d40-466d-3cd7-824e-b1c172bed1e1",
                        "b0e3a6ee-8141-3d32-8a4a-e3b08c47a7ac",
                        "bfaf87cb-327c-3863-922f-5b21037726d2",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "f49aebeb-90a4-3d3a-b26b-adae55e4200e",
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                        "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                        "893a3274-dc45-3f91-b256-983a83c8b153",
                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                        "fc22b497-2a0a-39ca-915b-8037852bb5a9",
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                        "d24c523a-8ef4-3b91-a35a-85a55ac2da72",
                        "7a67a861-e234-35ad-b7cd-f01e617b1dfa",
                        "f183e434-6f36-339b-9348-de3d83e5b7f8",
                        "cacb2a3c-4558-30e7-93cd-c700f6ed12ea",
                        "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                        "a8caa264-b568-3bad-9e7a-13fb2c1eb459",
                        "b07f5eb7-a050-352d-80c3-6de294a6ba3f",
                        "c80cda9e-fd4d-38c1-9240-3bd0ab9e6143",
                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                    ]
                },
                {
                    "accession": "KW-1185",
                    "id": "Reference proteome",
                    "category": [
                        "Technical term"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9e9ed0b1-71c7-32bc-b2b8-7b9fa84028bb",
                        "e8d0005d-a3a3-349a-9db4-d3d95377b718",
                        "0b0d8759-2956-37cb-8c34-5679bc86a9ec",
                        "a649cce1-bf69-3fe5-beec-c47f52985976",
                        "a4a016f7-af3c-37b2-bbb0-869da4729061",
                        "e0cfd0ed-8abf-376e-b766-987fded19ccb",
                        "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                        "4f77b69d-5001-3a9f-8759-0c12d8fa535f",
                        "958ba293-34f7-39f4-b0d0-7397b634d739",
                        "3ebfa9ba-a4ef-3004-8b2d-24c105fbd7db",
                        "24e689b0-03b1-3408-a49a-151692cf629c",
                        "3ae4d435-05c9-3016-9419-f906e48968a9",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752",
                        "b5d80ba7-a45d-3460-98ef-af0d15f43c1c",
                        "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                        "ff9ecfec-7cdf-3b16-9f58-df1393f56211",
                        "035d3181-8ee3-3152-bed8-9644e47d97dc",
                        "5492ee99-1adc-3f4a-8494-70a45e6b2141",
                        "73cf2129-d481-3182-8234-5c58426cbd8b",
                        "8d98038c-6dec-3415-91e1-26fe011fc089",
                        "8aa62ad5-4534-391c-9963-a1c34edc7e3d",
                        "5364d153-bc0b-39b7-aabd-d129e885ca0a",
                        "17a73fa6-5041-35e9-99ec-2bc37e59a4af",
                        "238b3de0-b3c2-3f07-911a-decfc93154ab",
                        "d29caf3d-891e-37fc-806b-e50965ca02db",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65",
                        "5cf06ce6-2b9e-3618-a775-454fd37dbb54",
                        "ce48f6ed-2f5e-3052-95b4-c46400f3e1d6",
                        "e7d1fcbc-7893-3f9f-b547-6c58816c821b",
                        "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                        "7a4ac01d-b269-30e8-9f8c-fd7017aa84f2",
                        "94de208d-a804-3a38-9f33-570c2fb3ac2d",
                        "36871dcf-e157-3efa-83dd-6a24b6f8caf0",
                        "3a22854b-60f2-3397-a949-92484f1a8b31",
                        "d06e7bef-fcad-354b-8d23-7e537c434a6c",
                        "76463a0c-c647-39ea-b9e9-12ca32648522",
                        "1c7ba006-0968-38fb-81e4-325187699247",
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5",
                        "09f9c8a9-8471-372e-ae8a-4f00a1503757",
                        "e2082585-5d94-3f70-bae9-f801d1264938",
                        "5bff90af-6815-31bc-b355-6196ce21148c",
                        "fb76d466-0b71-3eca-8541-66496335aa3d",
                        "05afc641-0745-31bb-bc3c-ff9689e350ff",
                        "00926371-a557-341d-8941-45ad2fafee4b",
                        "0d552860-d03d-3427-9dfb-58c6c723f3b7",
                        "3d0070cc-e1db-31f6-8f49-580883bc373d",
                        "b9f03a52-e78f-3a0c-9f3d-4d4301c34388",
                        "25039a14-8246-3b5f-ae47-56711abe51d9",
                        "3279d575-a3d1-3c30-8518-47f89440205b",
                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                        "00bc8ba7-a63c-3b8b-8b2f-031a6c496b49",
                        "10ad4b48-fcee-3460-a4d5-710b2cf281c5",
                        "9179b0c0-c547-3ccf-b39e-6304517a2ee7",
                        "806279ae-507c-35c6-bffa-477f138c7919",
                        "5ead7332-b2e5-3089-a871-a22450e06376",
                        "a5e84058-2b68-3cd8-8e1e-1d13ef92c2f3",
                        "e39c02ac-664f-3b5e-8d8a-b20af3741469",
                        "1c961799-c8e6-3571-b63c-81642cf2ab91",
                        "01c0a4d3-91cd-3222-b59c-2cb2c5b1bea8",
                        "bcbd4cdb-0268-3a8a-ac02-2422865ad231",
                        "dccf3c9e-dddf-39e4-9061-2e563185c9b9",
                        "90996f0f-25a2-3513-b36d-a389a5d2166d",
                        "328fc96a-37df-345d-8150-16e902fc921c",
                        "d9e18784-e818-3532-bdf3-0ea5ef71f2fa",
                        "91e1fc09-c4c0-371f-b84e-7272bf3d625a",
                        "9e2aad1c-75e8-3a3d-b508-aa845635d7b7",
                        "0791e3a6-5ee6-329f-a7d1-b8450305c4f7",
                        "fee40d4c-a185-33ea-b50a-dd88f5e77e13",
                        "afb5765c-5d4f-354c-92be-d11f02153167",
                        "9aa82d48-90c7-39ac-9158-573fb5dcfc10",
                        "675fa701-56cc-36aa-b6cc-bfa67ecfaf15",
                        "fa03fcb6-1a8e-3c1b-9b91-fad4d162279e",
                        "3f062636-bed0-3f06-ba22-e81645b5893b",
                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                        "71a490c7-33e5-39df-bd3b-5b5c0a876a35",
                        "448e34c1-4fd2-3d95-b078-9642f7347452",
                        "8738a805-3daf-35e9-9fe4-295483914470",
                        "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                        "03bc60d2-2bbe-34fb-87c4-0413718dd28a",
                        "741fa666-fa9f-3650-990b-c1458057cfdf",
                        "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                        "41885b05-5303-35e8-b45c-b8ae67bb2a19",
                        "8bc41f8b-7a09-3d58-94e5-f48e87ab5611",
                        "4939a1cd-d4b2-3c7d-9053-b02c16db7e41",
                        "3651de6c-1487-3148-bbd8-963d04a47b07",
                        "8b4efabd-0c5b-3f60-af7e-fd20fd31a750",
                        "ff9d2182-ad09-3f34-9be0-f72eb299b944",
                        "e7011909-03cc-3eb0-aef7-b8a96ce4f2ce",
                        "89ded291-4348-3f8d-be43-56e42b9311d0",
                        "dbf5255d-b46f-3ff6-af72-b63754671791",
                        "952a1258-8809-3789-8903-5bbbb8605308",
                        "1f420626-52e3-37a6-aa7f-ccdab455504e",
                        "4fb159bd-103d-3af0-a750-ffb1d7efc7c4",
                        "51aaf3dc-b27b-37b8-b84c-06c1895d0b3c",
                        "5add01d1-9319-37e9-85e1-a7da984ac384",
                        "b32cef3a-9da0-3b0d-b90e-b1895645e93a",
                        "6ed557e6-3f94-30a4-87a0-ba7bc8dff3e8",
                        "b640034e-80a9-37d0-aa8b-98b1180e4740",
                        "8eb384ee-fef1-33ab-8a42-8a2f5b50ba71",
                        "c43a0411-8126-3d5f-8344-bb6d01bcee7b",
                        "da971fe7-5f46-30e6-bd0f-7a765e069a77",
                        "c893794f-f410-32ba-9458-4f795169055c",
                        "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                        "b378c052-6677-38dc-a7b5-3174eed7d879",
                        "5c9e9fca-f450-396d-87a0-2972c30b517c",
                        "d3380e9e-3ca9-30d9-9fbe-d22064b59906",
                        "5e26b926-1841-3edf-83f8-86e196ad600b",
                        "9675ae45-136a-397a-984c-15dedd7ec1c6",
                        "231cde29-fd90-3fde-b0d3-6f6deea14d2e",
                        "db66e621-4ba9-3952-8ecb-c161094c8675",
                        "affccc91-69b0-350a-b326-170650a89211",
                        "a6e25223-f140-35f4-8e16-af6ec7717ba1",
                        "4a37b120-5d96-307b-9f0e-47e33b8e4e86",
                        "d1085cd5-55e3-3f30-9c78-69b9c267bdd5",
                        "278f5526-d165-387c-a9bb-f3981199b3ac",
                        "4bb7e457-7040-302b-a82c-076c07ca7304",
                        "d1cdacd5-d00d-333b-bdba-0df200229b25",
                        "16b312fe-c87b-3fba-b087-d72530affc71",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "76d373ba-0068-3d85-894e-2559dd07a94a",
                        "73f9cc84-4db1-3636-90c2-5f1a7990353e",
                        "4231711d-4aa5-3b4f-b679-3a29453a8390",
                        "59c6e653-4609-3099-9739-383b53092172",
                        "57e0a113-56ce-3ca0-9c08-6400937f1a7e",
                        "4d2a0cc7-b7a4-3829-a48e-74396df42212",
                        "f9202d3d-2dde-33d7-bae6-e3b73b4adb52",
                        "80831e58-97dc-3bfa-995d-6398bb4b3eea",
                        "cf7b4c0c-5e3e-35ed-80de-4e6a56a14bee",
                        "0daba42f-9cb1-320c-95ca-742c40e802af",
                        "8a2d4bdf-d18b-38b3-8cf5-426d05a5b344",
                        "7db61943-6cc0-36bd-a5e9-7e46e4f5de58",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "3cce71b9-bb9f-3f5f-a191-89455ca5884d",
                        "e8c576ae-9b77-3b43-991e-d7dca04bcfd9",
                        "96efecbb-175f-371e-97b4-badca3f602a9",
                        "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                        "bbea8af7-1654-3c8a-8057-b60449e7f1ff",
                        "e6c1af58-a061-30fc-94e7-02b4ca64be72",
                        "850143d4-9315-3889-9aad-60bd107000ef",
                        "d45584d2-50ec-3dca-9d34-f5bd15067a15",
                        "b314da77-03a0-3453-babc-0e706147208f",
                        "f3f9af9f-0f3b-3a9e-8331-9b1573a90dfc",
                        "f322a8ae-ec9a-3952-9737-150d950df3d6",
                        "2da1b3db-c2fd-3a86-bf9b-48e45087b1d6",
                        "bf533f2a-ef1f-3d05-b6d0-754109696a77",
                        "80ee5820-8165-3fc1-858f-330ea6212f00",
                        "9fad2f14-2923-3e89-ae56-7411fd6817d6",
                        "c168653e-3c8d-3bb3-9f52-032fea2ce506",
                        "7a387466-0a6c-3701-8cd7-5b3a253359e3",
                        "f6c700aa-e087-3675-889d-290443b8a219",
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "d76a3d39-6566-3b7f-a805-9d8501c00579",
                        "3a5efd80-dc8f-3aa7-a075-7ad4a1d6d1d5",
                        "18f7f6b8-26d0-397f-af46-71b3e2fa2ea8",
                        "16725ace-e627-3252-ae2a-45c4c783d2b6",
                        "4b892b3a-20ad-3325-ba60-7d03b9f71716",
                        "71b3ae7c-a1eb-3390-ae68-92cf4f9f8c00",
                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4",
                        "5509eda4-8bbf-3260-a080-c90d3a56a279",
                        "ff86e71c-2da2-3e32-83da-8fa5fc4342ad",
                        "9fb7f91c-80c2-3572-b7a2-26f2fc597147",
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "0a97130d-8b9d-3d33-8086-f6a9716d89d4",
                        "aff29f09-97aa-3745-82d5-95a81522ee3f",
                        "1776f92b-eb74-3f0c-8d9a-c47a65663644",
                        "b90310be-af79-39b4-b6c0-6d6a413d2b8f",
                        "d846b344-6c74-3696-b0da-84764ee91886",
                        "5c3e9984-9197-37f7-8c69-deb288ba224a",
                        "456bfd92-36c5-3fd9-8803-aed14a8ec2d0",
                        "36a9a461-0ce7-3e5e-bc50-08059008fecd",
                        "a38b9965-bdc0-325b-9a57-2b970b1188ea",
                        "72687302-febc-300a-8b6a-aa54e09d2bef",
                        "589687f2-9044-364c-aaac-707b0f97823c",
                        "3a1dc7b1-7eef-302a-8d65-c1b776272a30",
                        "fc16d218-6350-3e2e-9c9c-05fefd49e5a3",
                        "ce422a1e-705d-3108-8141-d9bb318b50ce",
                        "73d2ba4e-5d84-3d4b-86ef-9989dcc9beba",
                        "260d44be-d7fa-3dca-b1d8-96019c54f35a",
                        "fb9a8b64-0c46-3a4a-a7ca-8951cb3a41f7",
                        "1bc1abec-4cdd-3bfd-a2cc-56666581251f",
                        "9c564811-95c0-3848-87bc-132b90327655",
                        "4f3be65d-3506-3cd4-a538-ea72f62baa95",
                        "556eb168-c8d8-36f0-98d0-edf9b331c30b",
                        "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                        "6f239857-fde2-3286-b67a-868ac42113d0",
                        "65a926bf-d216-3943-9809-9013a707a4ac",
                        "5ec0a5dd-41b1-3cfa-8c37-0d803bcc79d4",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "47dbf2b2-9d17-378f-b54b-ad48adfcdd1f",
                        "2d49aa19-6f94-3cd0-8753-99f792333c9a",
                        "9b645bb4-da5c-34a3-a6dc-2063b5c3faf4",
                        "676d3538-9c27-340d-af8c-dec20faf357b",
                        "08a9b134-8813-352b-90bb-807c80c4b69b",
                        "a5e34328-e12e-3425-add0-7428af49849e",
                        "233bc275-5a82-338e-8bb6-ecca7feb7762",
                        "6d505dc1-74f4-3a3d-a659-b5f22e58656d",
                        "9e5e4071-a9d1-39e3-b657-84083543f591",
                        "cc0fe12b-f306-32df-bd3d-635784fe57c9",
                        "3717bd7a-b503-325b-be3e-bf6dbc73ec6e",
                        "4f563011-ab0d-30a7-9851-097347585757",
                        "42d49741-9a55-38f8-9fbb-f0897fb40734",
                        "b0e3a6ee-8141-3d32-8a4a-e3b08c47a7ac",
                        "bfaf87cb-327c-3863-922f-5b21037726d2",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "5de89c9e-69d5-346a-8e30-928078064499",
                        "0a0d0a63-de13-3b3a-abe1-bc2f9c027855",
                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                        "5be94738-4dd2-3fa2-92a2-508e53a21ef7",
                        "d24c523a-8ef4-3b91-a35a-85a55ac2da72",
                        "78b9d6d6-8e32-38f0-862f-745586f1af9b",
                        "f183e434-6f36-339b-9348-de3d83e5b7f8",
                        "71b366fe-e434-327c-90f6-5222f4cb5ea6",
                        "3cf4551f-131d-3653-b022-7023b6c1c1fb",
                        "362ca19e-c97b-3ac5-a5bd-34a13e2a82da",
                        "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                        "b07f5eb7-a050-352d-80c3-6de294a6ba3f",
                        "c540cf02-6cfa-3db3-96bb-ab069be25221",
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "764ab033-a4f5-3e54-b067-b601a3c64ab2",
                        "64bea006-ef1b-376c-8639-c5dd6c38a563",
                        "7a19fc76-3018-3ea7-a941-ca7feb7122bf",
                        "a87ba622-c92d-35b1-935e-52f538af07f4",
                        "91bd43f5-ae68-3b80-aa75-7a9c967b9398",
                        "a9eb05a1-16a7-34d3-9881-e542cf92b1be",
                        "78e337f5-5c30-38db-a09d-8c2c35083444",
                        "a3aebedd-25dd-3dfe-9ade-83d1e8475627",
                        "07e6492c-255a-3b65-8400-bb99932324d2",
                        "ed1aec27-3381-3466-912b-0a050407a275",
                        "c4cdd1b7-0c46-3cb6-90c9-f2b2a1983a58",
                        "2cf69a4d-d912-302f-930d-4313ca35a9d3",
                        "7494b152-dbc6-395d-abdd-1b549511a731",
                        "8ce280e3-65c9-3634-8594-3ffe355b2d58",
                        "d303538a-3c87-3104-ab84-288d4297de26",
                        "8ef52e4a-e7b1-3daf-ba97-b79e67a3d302",
                        "08db55e4-1db7-3bce-9bf1-770750acbb97",
                        "f008373a-488a-36c1-85e9-5fdfb99fa95a",
                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                        "0ff36b3a-ba1f-3928-a34c-e9e69083cf4f",
                        "450d855a-66b5-3220-a455-c3824dd9226b",
                        "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                        "2b4f5dc3-75d1-35b3-abcd-059ce2986a5b",
                        "9f714626-b769-35ac-aafd-600c07ba1299",
                        "89743da0-e8d3-3386-967f-46ec472079d8",
                        "b2a03d2e-7fe3-3375-a542-95440bd78a9a",
                        "56a38ea7-2f0b-3578-bfef-7f17b93bd275",
                        "d6e069e7-f59d-3a6e-9d5c-cea42b8ae93a",
                        "1d192ad9-a2cf-3749-8986-2f362feb18bc",
                        "e4776e4a-90b7-3421-af46-968294e1b9ec",
                        "83d458c1-eb11-3b0d-956b-a66b398fdfab",
                        "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                        "17d3560c-19d3-3141-b1e2-c20ba87599ce",
                        "cdf8527e-4138-3172-9447-a37f11f48ad1",
                        "f1778da0-779c-30a6-a761-8d0f864a75d5",
                        "88dafdd2-f1e6-3992-aee5-9fd6763d3056",
                        "f69c6421-9e2f-3f78-94c8-2b7c6d5a6bc7",
                        "aee8e5d8-a562-367c-aef5-9667b804c2ed",
                        "1f8e5f6c-cb93-3658-a7d0-407519b276a7",
                        "ad28a8bf-fcd4-3132-968d-795293745983",
                        "9a58754a-5c56-3c07-84b7-5360a962f090",
                        "8b9be9c8-0db3-3271-bd73-dd7b1e08fe27",
                        "3e6c6495-8fff-3169-846d-0e965cdd2dfe",
                        "f95204fe-2fc7-3512-9ae4-69e673ef6f03",
                        "4027df06-664f-319e-a3d3-bfb1459894cf",
                        "275512af-b219-34ae-bcc9-a9a502193acd",
                        "a5f2faa4-5802-3b3a-bc58-e907abc60363",
                        "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                        "8bfa1252-1f9b-3043-93d8-f5639d9cf074",
                        "d572aa6a-b6a6-3fad-9282-a20959414f10",
                        "e4686fc4-66af-3047-9254-e77709e9fcd6",
                        "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                        "65886732-974b-3f7a-a011-90682a3b3ef5",
                        "b3db6762-1dfb-3268-818b-228994a05da0",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "0cea6dce-3ee8-34fd-bbaa-3dfde75abf19",
                        "8e45ccea-21f6-38f7-b11e-aa8914e50a61",
                        "a32c23cc-0d6c-3bb5-a17f-484494775219",
                        "09824aca-3ccb-3f94-9b21-7d95f424c697",
                        "18dca3e9-3ec2-3bfd-ae59-bd227ada733e",
                        "fb5cad1b-df5c-3572-82b2-88c2e61ba77c",
                        "56ef52fc-84e7-3855-a588-dd43eb465591",
                        "41f8e0c9-6167-3fdf-b413-51b3b1031efe",
                        "acd6e236-7d87-3bff-b971-d0b93bd179e7",
                        "97eec5eb-fd52-3f21-b970-f2ab55a49582",
                        "a681313b-a131-3404-a255-6ff70a8ea5db",
                        "24875adb-4d25-30b6-9a24-948f620f7f72",
                        "62418546-9356-318c-93fe-d6830db6d92a",
                        "124c6ffa-0fb6-38b2-b175-247458f097f9",
                        "ce57e563-d2a4-34e5-80c0-9854d14c2e25",
                        "488e8ee7-b931-3d0a-a726-7f130f8bf390",
                        "8854d795-3ec4-373d-a0c1-10b3373df75d",
                        "2438b215-94d2-3e1e-8722-63acfe9ba76f",
                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "20366e64-78e7-38e8-9158-29d148f86506",
                        "2b8b93cd-9934-3ca7-bf02-031505a26bbe",
                        "108cc793-6765-346b-85b7-9ea261151b2d",
                        "3d521670-8170-3b6b-a8e3-4ff38f616c53",
                        "82b18d47-2bab-379a-8c6f-7c73093e156d",
                        "fd3ba7bc-eb46-3a01-85a3-d00a8dd4a8e9",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "adec9c8c-296a-3769-9203-3969d932bc85",
                        "ad64cec4-eb7f-39a5-b2f0-5cb4e3f501d8",
                        "fbfddc81-7c4c-3720-8aa0-df1f266bcd50",
                        "6f42f3aa-5abc-3149-9c90-8514279c3116",
                        "5532cc3e-59d5-358b-8546-2ce4b04f665f",
                        "7adccd89-b1dd-3e60-b54a-814b65b92b41",
                        "ba9ea4f2-a2be-3e4d-b65c-5a32e4fd7009",
                        "8dada997-5a23-3c7b-9833-68bc56ba560f",
                        "72e2520f-2738-38f2-bd55-9569705062d9",
                        "76c71bc8-c46d-3acc-ad17-d1ca3625783a",
                        "5a887edb-44ab-3e1a-b8c5-115a9fa91f79",
                        "2850d15f-f930-31e8-8764-356a1cb98b51",
                        "41ac41e5-9f5c-3581-89e0-4af87818445c",
                        "806d2373-2eec-33a0-bd4a-5478b9c46f34",
                        "831a5687-da56-3321-a6ae-8999dc605323",
                        "1a3d18fa-4a35-313d-b73e-9a69b4707a58",
                        "8a61fe9f-cea2-3471-8e8f-d8ed4543e47e",
                        "3fb2549c-0452-3210-8be4-9f3a0d2cffea",
                        "3d21682c-3229-3273-aa6f-4ff872d59326",
                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                        "81c0dd1a-7823-3d99-a23c-d0ae419b6e0f",
                        "6c1b608e-bee8-33fc-b621-d63906ff068a",
                        "7b2ac681-8f57-378c-9087-def2c187a208",
                        "16a4e396-bea4-3fb2-b83c-8e0d157eaa1d",
                        "59bd70e6-31fe-3e3e-9f6b-a0e4fbdaef5d",
                        "ab622b9a-421a-3a59-a5fe-ff745ba0d578",
                        "e83511de-fef5-3734-8e5a-9d02ae774102",
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1",
                        "3fe5ee9b-cebd-3413-ad18-e2b50b8b92c3",
                        "a1fec884-fbd2-33ee-94fe-6eaf5b7f10c7",
                        "cafca265-a3bf-3afb-a1ec-32aea9845b77",
                        "9ec8e26f-cf5c-360f-933b-f9a69dec0212",
                        "984c81a6-e816-32b6-b07b-69dadffc4109",
                        "fa0d3c94-9697-3f67-b435-b8aef0a96a5a",
                        "347fa5f1-329d-3667-823d-4c5873cd8b63",
                        "74428bf0-3d3c-35c9-84ea-0b2eb632701f",
                        "5f414d39-9df3-3411-b4a1-7c6e35b3487a",
                        "6ca00c54-ed8f-3b5d-8329-051e1839dbf9",
                        "d7906d1a-5ded-3887-95dd-1dd0f82ce01a",
                        "4363326f-335e-3c29-87d8-e609b33aa633",
                        "d90a1ebb-127f-354d-b3d4-a7c56ce5df07",
                        "1405208a-ed5c-36f9-b344-3b549f31dbc1",
                        "ccdb8a84-5e15-3840-87f4-9a2366f7bc56",
                        "e130092e-4136-3244-a836-2f098cdd0a85",
                        "ba36ec06-b78c-367a-8292-4910795c560c",
                        "3f6a3d6f-f223-3a91-bb28-445f5e4c327c",
                        "96a76a6b-7f10-333e-b525-f2784214ee48",
                        "a7f8251c-9e08-3d2a-acd8-cfd2e14c8b24",
                        "438a6158-29ba-3eaa-b7a6-b39401811d6e",
                        "4086e8f6-19f3-3e85-876e-0cf86edea088",
                        "6eaa3a76-c2e7-3366-8a74-34cd0d3f6a23",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3",
                        "c638711c-bdde-33be-a890-efdd9a8123be",
                        "c907cf8f-adfa-3c36-8478-87a02c27fd25",
                        "70d716e7-2e21-3fc7-9d5a-eaf73bac2f60",
                        "be9a89b7-05d1-37b2-9735-8a7f019fb0f8",
                        "61d1c28a-4135-3674-af97-75e61945e833",
                        "3a715b3d-0635-389e-a7f3-3e920bee8f54",
                        "d14ba956-67ec-339b-bc82-f5d87a3b9a28",
                        "c16a16e2-43d4-3291-a840-fefd5939be2e",
                        "33859abd-93ad-305a-839c-1c145e0e2e4a",
                        "62169f87-c02b-34c1-a2ea-1adb1fec4587",
                        "d0a863af-2c08-32bb-a635-085fb5147e18",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd",
                        "716e68a9-7fbc-316b-b5ec-7ff2a5d22076",
                        "e2a65a19-7fb3-3a1c-9891-38d23a86bc98",
                        "93fa29eb-4f4d-35c1-8ab2-ab5e3612a356",
                        "45b221a7-f65f-318a-afee-f1bec5704c51",
                        "878e895a-9d6c-367c-ab7a-ea963aedcd08",
                        "b5d2ac66-2405-3d81-a2bc-520d5bb79ca1",
                        "0754593e-315c-348c-abd3-f7d40cdf821f",
                        "ecad0a2b-6166-3c60-a6ff-039f2211b326",
                        "2adc85c0-4e20-3bb3-a2f6-838caacfc474",
                        "1bec76be-687a-3641-8a3a-114017a4ad6f",
                        "1d3a670d-1bfa-3117-9025-fba33bbc4a4c",
                        "ee7e3205-3af8-3742-84ba-5d1197f85c7e",
                        "1f2107a6-54c7-349a-9ecb-0304f17fb7f7",
                        "0b0bf4f1-305f-3216-953b-fac86a8062a8",
                        "d96aa128-8e0e-3e85-9a47-31e1dd49e54f",
                        "e9f58804-0b00-30a7-a5a0-9edb29d4dc3c",
                        "84044b10-cc38-3dc6-b5a1-3d1ae107b0b9",
                        "f35ceae8-be8a-3b1d-a98b-a3a79f389400",
                        "378fb80b-3f49-35db-a3a3-09d2cca355f1",
                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                        "41794e2f-1527-3aa8-a594-ce47f97a857d",
                        "5dfb8cec-10b6-33ad-b722-072f815ea2e4",
                        "1946a92e-91ee-3dd8-9b68-dd04d4cc8747",
                        "45589e0d-6140-3039-b9ea-5e4c18478b35",
                        "01b38606-5c3f-3d61-a546-3a7369bbc6c7",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "c3f30764-2155-3be3-ab4c-91383e8cadac",
                        "88e41fc4-5017-3b85-b56d-7a3a9feda349",
                        "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4",
                        "dc53afad-12a0-3784-858f-00d211e93e90",
                        "62942481-2b09-3054-8699-744b7968ce01",
                        "0749bf7d-d4e7-35fa-aea8-3da9bb5b3937",
                        "db79e4db-ecd2-3e0c-ba77-36850a23e810",
                        "49d56ab9-28be-353c-921c-5ab492835eaf",
                        "af13e9f3-b720-3a88-b110-d3da50d6671d",
                        "25aacea4-18fe-38b7-a817-a73403749874",
                        "18d90788-23e9-3bd4-a17e-dbb417129a72",
                        "9d502900-4b70-3bc5-8723-daa4db8308a8",
                        "262dfcac-448e-3bfa-818a-ad6589081358",
                        "6214d45c-e9b8-30c3-9887-4acaee29688e",
                        "7d791ccf-a335-3359-bacd-9673f209c23a",
                        "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                        "33ddad67-eb91-398c-965b-de74be7d1547",
                        "55adc234-ce10-3909-a3a2-04887fe6fa3b",
                        "2d207fb8-1116-3415-92ee-b7ee964cf312",
                        "5af2fdc9-ce6e-3da3-af79-f1557c99fa94",
                        "52a834ce-1be0-32e0-b9df-5ff1b3364874",
                        "1a5b4cfd-7c25-3375-86ea-0f23a4f8d07d",
                        "2b68a56e-970a-3df3-bf67-70665e758a03",
                        "d2a66d40-466d-3cd7-824e-b1c172bed1e1",
                        "01f51658-daa5-3b6f-8e36-cc556c007d30",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "00ed2ace-ce39-3bc2-94fb-a1e81e860271",
                        "1de40c11-6bd2-3f06-b231-27c4d2cb796e",
                        "f49aebeb-90a4-3d3a-b26b-adae55e4200e",
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0",
                        "893a3274-dc45-3f91-b256-983a83c8b153",
                        "fc22b497-2a0a-39ca-915b-8037852bb5a9",
                        "a195423a-3a12-3931-95da-fa2d60d0a3ab",
                        "e4f20fbf-39c5-391b-8f30-96141689d0d9",
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "b245d5fa-5d26-376e-808a-2c8c37d3471e",
                        "fe506aea-138e-36a4-ad7f-b74ea420b651",
                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                        "7a67a861-e234-35ad-b7cd-f01e617b1dfa",
                        "cacb2a3c-4558-30e7-93cd-c700f6ed12ea",
                        "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "dad5c340-4a7c-3b50-9851-087af9277516",
                        "a8caa264-b568-3bad-9e7a-13fb2c1eb459",
                        "fa1a4201-301f-3503-9d5d-51aa06fb8a10",
                        "c80cda9e-fd4d-38c1-9240-3bd0ab9e6143",
                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                    ]
                }
            ],
            "proteinIds": []
        },
        {
            "accession": "KW-9999",
            "id": "Biological process",
            "category": [],
            "children": [
                {
                    "accession": "KW-0660",
                    "id": "Purine salvage",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "e4f20fbf-39c5-391b-8f30-96141689d0d9"
                    ]
                },
                {
                    "accession": "KW-0545",
                    "id": "Nucleotide biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "19579ce4-0a7e-389d-beaf-ceb415f215e8",
                        "504bbf4f-f5da-3e94-966a-0a3a6e793cfe",
                        "188a9c3f-c403-3be2-a94c-df304e4a6cb2",
                        "b60ea458-1a31-3a7e-af6d-1bb8a1af8d12",
                        "e6f72b1a-051d-3cd0-8205-145dd9f16f65"
                    ]
                },
                {
                    "accession": "KW-0215",
                    "id": "Deoxyribonucleotide synthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "f9d2eccb-4ff0-3f48-9d1e-0eba3537db2b",
                        "e8c576ae-9b77-3b43-991e-d7dca04bcfd9",
                        "e2a65a19-7fb3-3a1c-9891-38d23a86bc98"
                    ]
                },
                {
                    "accession": "KW-0259",
                    "id": "Enterobactin biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d86cbc59-170e-3195-83ca-269d1d94c5c1"
                    ]
                },
                {
                    "accession": "KW-0819",
                    "id": "tRNA processing",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "09f9c8a9-8471-372e-ae8a-4f00a1503757",
                        "3cf4551f-131d-3653-b022-7023b6c1c1fb",
                        "9c4f1688-3ddb-3192-ad15-f77e1284e619",
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "5a422db3-84aa-3ff6-a9de-57f0759b81fb",
                        "f35ceae8-be8a-3b1d-a98b-a3a79f389400"
                    ]
                },
                {
                    "accession": "KW-0130",
                    "id": "Cell adhesion",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "893a3274-dc45-3f91-b256-983a83c8b153"
                    ]
                },
                {
                    "accession": "KW-0233",
                    "id": "DNA recombination",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "2e51831b-d5af-3419-8685-ea29ced2c512",
                        "59ec5d46-b9f3-3d5f-8929-7c10221bdeba",
                        "ff9f96d7-f456-39da-8a23-a25a01e0c64d",
                        "51aaf3dc-b27b-37b8-b84c-06c1895d0b3c",
                        "cac64301-0e27-3e86-918a-d3bb0a83df4c"
                    ]
                },
                {
                    "accession": "KW-0448",
                    "id": "Lipopolysaccharide biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "4958103b-c713-31f7-956b-ffe93334285c",
                        "d63d6106-8f94-3528-9d7c-8b5c6079f26a",
                        "1405208a-ed5c-36f9-b344-3b549f31dbc1",
                        "fc22b497-2a0a-39ca-915b-8037852bb5a9",
                        "82b18d47-2bab-379a-8c6f-7c73093e156d",
                        "f1778da0-779c-30a6-a761-8d0f864a75d5"
                    ]
                },
                {
                    "accession": "KW-0443",
                    "id": "Lipid metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0276",
                            "id": "Fatty acid metabolism",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "ecad122e-a2e9-35a8-b798-d20bffccbf20",
                                "982e8fe6-4b3c-3e5a-9ad4-4dbbd2c9d022",
                                "17517a24-1b56-396f-b53c-1748f4deac12",
                                "25774368-eecd-3e1b-85be-65bb8277cfaa",
                                "10c00c05-61de-3dcf-8030-f0d748031d11",
                                "d6474761-a2d8-3278-bd17-c08775c8d77c",
                                "c22a1936-08c9-38f2-b5ee-499e05b45f4b",
                                "41dd90ee-e557-32a8-928c-ce90ddaca9a8",
                                "36871dcf-e157-3efa-83dd-6a24b6f8caf0",
                                "f02c85ff-e778-3203-a229-1bb5cd740ffe",
                                "cdf8527e-4138-3172-9447-a37f11f48ad1",
                                "a5e34328-e12e-3425-add0-7428af49849e"
                            ]
                        },
                        {
                            "accession": "KW-0442",
                            "id": "Lipid degradation",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "83d458c1-eb11-3b0d-956b-a66b398fdfab"
                            ]
                        },
                        {
                            "accession": "KW-1208",
                            "id": "Phospholipid metabolism",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "1a8711b9-d137-3136-af7c-a5a23ee78b64"
                            ]
                        },
                        {
                            "accession": "KW-0444",
                            "id": "Lipid biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0275",
                                    "id": "Fatty acid biosynthesis",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-0276",
                                            "id": "Fatty acid metabolism",
                                            "category": [
                                                "Biological process"
                                            ],
                                            "children": [],
                                            "proteinIds": [
                                                "ecad122e-a2e9-35a8-b798-d20bffccbf20",
                                                "982e8fe6-4b3c-3e5a-9ad4-4dbbd2c9d022",
                                                "17517a24-1b56-396f-b53c-1748f4deac12",
                                                "25774368-eecd-3e1b-85be-65bb8277cfaa",
                                                "10c00c05-61de-3dcf-8030-f0d748031d11",
                                                "d6474761-a2d8-3278-bd17-c08775c8d77c",
                                                "c22a1936-08c9-38f2-b5ee-499e05b45f4b",
                                                "41dd90ee-e557-32a8-928c-ce90ddaca9a8",
                                                "36871dcf-e157-3efa-83dd-6a24b6f8caf0",
                                                "f02c85ff-e778-3203-a229-1bb5cd740ffe",
                                                "cdf8527e-4138-3172-9447-a37f11f48ad1",
                                                "a5e34328-e12e-3425-add0-7428af49849e"
                                            ]
                                        }
                                    ],
                                    "proteinIds": []
                                },
                                {
                                    "accession": "KW-0441",
                                    "id": "Lipid A biosynthesis",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "741fa666-fa9f-3650-990b-c1458057cfdf",
                                        "67d5473b-2d87-339e-8eb3-8dacbf198014"
                                    ]
                                },
                                {
                                    "accession": "KW-0594",
                                    "id": "Phospholipid biosynthesis",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-1208",
                                            "id": "Phospholipid metabolism",
                                            "category": [
                                                "Biological process"
                                            ],
                                            "children": [],
                                            "proteinIds": [
                                                "1a8711b9-d137-3136-af7c-a5a23ee78b64"
                                            ]
                                        }
                                    ],
                                    "proteinIds": []
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0753",
                            "id": "Steroid metabolism",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0088",
                                    "id": "Bile acid catabolism",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [
                                        {
                                            "accession": "KW-0442",
                                            "id": "Lipid degradation",
                                            "category": [
                                                "Biological process"
                                            ],
                                            "children": [],
                                            "proteinIds": [
                                                "83d458c1-eb11-3b0d-956b-a66b398fdfab"
                                            ]
                                        }
                                    ],
                                    "proteinIds": []
                                }
                            ],
                            "proteinIds": []
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0046",
                    "id": "Antibiotic resistance",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "b837a743-46c7-3c8a-9580-8938a4589cfa",
                        "741fa666-fa9f-3650-990b-c1458057cfdf",
                        "5e26b926-1841-3edf-83f8-86e196ad600b",
                        "162027c6-2ad1-36be-ba31-257d5026d55d",
                        "76463a0c-c647-39ea-b9e9-12ca32648522"
                    ]
                },
                {
                    "accession": "KW-0690",
                    "id": "Ribosome biogenesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1f2107a6-54c7-349a-9ecb-0304f17fb7f7",
                        "b5d2ac66-2405-3d81-a2bc-520d5bb79ca1",
                        "83b737ff-b679-380a-a9c0-d56ed58ff728",
                        "ccdb8a84-5e15-3840-87f4-9a2366f7bc56",
                        "2807b230-fcc1-32c9-acd0-e0d31cd3fcf3",
                        "d6d01f40-3ba2-3c38-9616-3106ecc766cc",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497"
                    ]
                },
                {
                    "accession": "KW-0184",
                    "id": "Conjugation",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "19ba0f71-8f9b-31d9-90d8-73666790014f"
                    ]
                },
                {
                    "accession": "KW-0546",
                    "id": "Nucleotide metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "54fef755-2ef7-3990-9656-2610b89c5009",
                        "10a0f74c-8ecc-30a4-8f42-d060baed0b77",
                        "a5f2faa4-5802-3b3a-bc58-e907abc60363",
                        "d44a2fd7-d351-36cf-9108-95eb20e4d3bd"
                    ]
                },
                {
                    "accession": "KW-0784",
                    "id": "Thiamine biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "dad5c340-4a7c-3b50-9851-087af9277516"
                    ]
                },
                {
                    "accession": "KW-0350",
                    "id": "Heme biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "28ea057f-2be4-32c5-91aa-36c12293c25a"
                    ]
                },
                {
                    "accession": "KW-0071",
                    "id": "Autoinducer synthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9e01ce48-38b0-3bee-8178-f00a8b68ada0"
                    ]
                },
                {
                    "accession": "KW-0319",
                    "id": "Glycerol metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "41885b05-5303-35e8-b45c-b8ae67bb2a19",
                        "5209eac8-8071-3d0d-b7e3-e72045ac99b9",
                        "4231711d-4aa5-3b4f-b679-3a29453a8390"
                    ]
                },
                {
                    "accession": "KW-0205",
                    "id": "Cytosine metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "a6e25223-f140-35f4-8e16-af6ec7717ba1"
                    ]
                },
                {
                    "accession": "KW-0554",
                    "id": "One-carbon metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "6e54240b-2467-35d5-a285-824d43665226",
                        "e2082585-5d94-3f70-bae9-f801d1264938",
                        "92cbdcce-3849-3418-9dd9-edf958868746",
                        "dcfc4b07-5be4-3d8d-8d4d-72d556b4bdb7",
                        "2c242910-cb3c-3f41-b750-2dac1ebc7215"
                    ]
                },
                {
                    "accession": "KW-0661",
                    "id": "Putrescine biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "3ba845c9-be72-3c9c-93e7-2f5f445d2750",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3"
                    ]
                },
                {
                    "accession": "KW-0235",
                    "id": "DNA replication",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                        "51aaf3dc-b27b-37b8-b84c-06c1895d0b3c",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "b8c84d6f-36a9-37c1-b5c6-8a7edb9861d4",
                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                        "b32cef3a-9da0-3b0d-b90e-b1895645e93a"
                    ]
                },
                {
                    "accession": "KW-0298",
                    "id": "Galactitol metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "ed097c9c-cd7b-38c0-bcde-f3e6f706a3e2",
                        "6f42f3aa-5abc-3149-9c90-8514279c3116",
                        "124c6ffa-0fb6-38b2-b175-247458f097f9",
                        "97eec5eb-fd52-3f21-b970-f2ab55a49582"
                    ]
                },
                {
                    "accession": "KW-0474",
                    "id": "Menaquinone biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "fee40d4c-a185-33ea-b50a-dd88f5e77e13"
                    ]
                },
                {
                    "accession": "KW-0810",
                    "id": "Translation regulation",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "2e51831b-d5af-3419-8685-ea29ced2c512",
                        "59ec5d46-b9f3-3d5f-8929-7c10221bdeba",
                        "56a38ea7-2f0b-3578-bfef-7f17b93bd275",
                        "aa221aae-ffc9-3950-ae60-2943921ef46a",
                        "d06e7bef-fcad-354b-8d23-7e537c434a6c"
                    ]
                },
                {
                    "accession": "KW-0902",
                    "id": "Two-component regulatory system",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "1917eff5-5d42-3295-934a-3e3ab9e8c178",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "893a3274-dc45-3f91-b256-983a83c8b153"
                    ]
                },
                {
                    "accession": "KW-0816",
                    "id": "Tricarboxylic acid cycle",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "5cf06ce6-2b9e-3618-a775-454fd37dbb54",
                        "e7d1fcbc-7893-3f9f-b547-6c58816c821b",
                        "de6f6043-0b31-3db5-b498-f9c0d4a4d943",
                        "d3e35944-1fbb-3d15-ad5d-4ade4c910750",
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd",
                        "e900d423-6746-3bfd-9643-6dab2615ca4b",
                        "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                        "952a1258-8809-3789-8903-5bbbb8605308",
                        "ba9ea4f2-a2be-3e4d-b65c-5a32e4fd7009",
                        "80831e58-97dc-3bfa-995d-6398bb4b3eea",
                        "20f413c2-29b6-3b10-8ba4-cc4d3ba2e3f4",
                        "b3db6762-1dfb-3268-818b-228994a05da0",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                        "8b9be9c8-0db3-3271-bd73-dd7b1e08fe27",
                        "7a387466-0a6c-3701-8cd7-5b3a253359e3"
                    ]
                },
                {
                    "accession": "KW-0961",
                    "id": "Cell wall biogenesis/degradation",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "72687302-febc-300a-8b6a-aa54e09d2bef",
                        "fc16d218-6350-3e2e-9c9c-05fefd49e5a3",
                        "8854d795-3ec4-373d-a0c1-10b3373df75d",
                        "da971fe7-5f46-30e6-bd0f-7a765e069a77",
                        "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                        "e83511de-fef5-3734-8e5a-9d02ae774102"
                    ]
                },
                {
                    "accession": "KW-0501",
                    "id": "Molybdenum cofactor biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "3d21682c-3229-3273-aa6f-4ff872d59326",
                        "984c81a6-e816-32b6-b07b-69dadffc4109",
                        "1d192ad9-a2cf-3749-8986-2f362feb18bc"
                    ]
                },
                {
                    "accession": "KW-0060",
                    "id": "Ascorbate biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "88e41fc4-5017-3b85-b56d-7a3a9feda349",
                        "42d49741-9a55-38f8-9fbb-f0897fb40734"
                    ]
                },
                {
                    "accession": "KW-0665",
                    "id": "Pyrimidine biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "65fa8534-717f-3498-8121-97a9bedf951b",
                        "4bb7e457-7040-302b-a82c-076c07ca7304",
                        "7747d65c-098a-364b-8683-352e8734b7c0",
                        "2a381e1e-09c7-33d6-9823-ce7f7d5643e3",
                        "9d0c600b-70c8-37b4-b4c4-c35dd66337e1",
                        "3b479813-9750-307d-944a-7cf29d8b335c",
                        "ed1aec27-3381-3466-912b-0a050407a275"
                    ]
                },
                {
                    "accession": "KW-0028",
                    "id": "Amino-acid biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0641",
                            "id": "Proline biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "ad28a8bf-fcd4-3132-968d-795293745983",
                                "e465935f-f61a-353e-8542-14ab25c51637"
                            ]
                        },
                        {
                            "accession": "KW-0486",
                            "id": "Methionine biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "6214d45c-e9b8-30c3-9887-4acaee29688e",
                                "0e6003b1-75c6-3911-bc68-fb215270015c",
                                "92cbdcce-3849-3418-9dd9-edf958868746",
                                "eaaada09-fde0-346f-8595-8c91e4029379"
                            ]
                        },
                        {
                            "accession": "KW-0791",
                            "id": "Threonine biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "3fe5ee9b-cebd-3413-ad18-e2b50b8b92c3",
                                "6214d45c-e9b8-30c3-9887-4acaee29688e"
                            ]
                        },
                        {
                            "accession": "KW-0057",
                            "id": "Aromatic amino acid biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0822",
                                    "id": "Tryptophan biosynthesis",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "1f1188d5-fb1c-300b-8f1c-9e3dd8f05eb8"
                                    ]
                                }
                            ],
                            "proteinIds": [
                                "1f1188d5-fb1c-300b-8f1c-9e3dd8f05eb8",
                                "8b4efabd-0c5b-3f60-af7e-fd20fd31a750",
                                "ef90ba8d-7bb1-386d-ac40-6e3764fb4b2d",
                                "4027df06-664f-319e-a3d3-bfb1459894cf"
                            ]
                        },
                        {
                            "accession": "KW-0368",
                            "id": "Histidine biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "dea33edd-752f-3ad5-8e6b-7172e0c08b5f",
                                "fb76d466-0b71-3eca-8541-66496335aa3d",
                                "3a1dc7b1-7eef-302a-8d65-c1b776272a30",
                                "92cbdcce-3849-3418-9dd9-edf958868746",
                                "ade07610-2156-3b77-af52-71aaa33a8a32",
                                "5efa2c07-4e05-30c7-8cfb-1f83c789005a"
                            ]
                        },
                        {
                            "accession": "KW-0100",
                            "id": "Branched-chain amino acid biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0412",
                                    "id": "Isoleucine biosynthesis",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "d14ba956-67ec-339b-bc82-f5d87a3b9a28"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0457",
                            "id": "Lysine biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                                "6214d45c-e9b8-30c3-9887-4acaee29688e",
                                "45663484-0ecc-3c24-be1f-fa2773ca7012",
                                "f639e2ef-b23c-3eac-82d4-2f822e5ad56f"
                            ]
                        },
                        {
                            "accession": "KW-0718",
                            "id": "Serine biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                                "3717bd7a-b503-325b-be3e-bf6dbc73ec6e"
                            ]
                        },
                        {
                            "accession": "KW-0198",
                            "id": "Cysteine biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "9675ae45-136a-397a-984c-15dedd7ec1c6",
                                "dbf5255d-b46f-3ff6-af72-b63754671791"
                            ]
                        },
                        {
                            "accession": "KW-0314",
                            "id": "Glutamate biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "8d3ca885-fcf8-369e-84ac-a6864ab563a1",
                                "5af2fdc9-ce6e-3da3-af79-f1557c99fa94"
                            ]
                        },
                        {
                            "accession": "KW-0055",
                            "id": "Arginine biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "4bb7e457-7040-302b-a82c-076c07ca7304",
                                "ed1aec27-3381-3466-912b-0a050407a275"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0119",
                    "id": "Carbohydrate metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0299",
                            "id": "Galactose metabolism",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "c3a5104c-6764-35d8-9332-10aa5391f2f8"
                            ]
                        },
                        {
                            "accession": "KW-0313",
                            "id": "Glucose metabolism",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "5adfc32c-6203-33e3-8308-e89f4358b1d3",
                                "060a51c9-1c15-3b67-a4d5-ea2d7f7ffcc4",
                                "5de89c9e-69d5-346a-8e30-928078064499",
                                "2adc85c0-4e20-3bb3-a2f6-838caacfc474",
                                "4f77b69d-5001-3a9f-8759-0c12d8fa535f"
                            ]
                        },
                        {
                            "accession": "KW-0321",
                            "id": "Glycogen metabolism",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "7db61943-6cc0-36bd-a5e9-7e46e4f5de58",
                                "e4686fc4-66af-3047-9254-e77709e9fcd6",
                                "f1dee98d-c68e-3f06-869a-54029dbd5aa2"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0659",
                    "id": "Purine metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "e4f20fbf-39c5-391b-8f30-96141689d0d9"
                    ]
                },
                {
                    "accession": "KW-0320",
                    "id": "Glycogen biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "e4686fc4-66af-3047-9254-e77709e9fcd6",
                        "f1dee98d-c68e-3f06-869a-54029dbd5aa2"
                    ]
                },
                {
                    "accession": "KW-0686",
                    "id": "Riboflavin biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "726e62e2-50bb-3136-baca-4fd18d794103",
                        "ba36ec06-b78c-367a-8292-4910795c560c",
                        "5364d153-bc0b-39b7-aabd-d129e885ca0a"
                    ]
                },
                {
                    "accession": "KW-0658",
                    "id": "Purine biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0332",
                            "id": "GMP biosynthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                                "694c6f10-0972-33dd-bc1f-d8859599cbca"
                            ]
                        }
                    ],
                    "proteinIds": [
                        "116bddd6-8b8f-36de-86d9-da6d574d1a85",
                        "b3550340-b861-318d-bbd2-e853824eb6ea",
                        "93ad988d-60fa-32a7-b4eb-6f08673a912f",
                        "e2082585-5d94-3f70-bae9-f801d1264938",
                        "694c6f10-0972-33dd-bc1f-d8859599cbca",
                        "fb9a8b64-0c46-3a4a-a7ca-8951cb3a41f7",
                        "2b8b93cd-9934-3ca7-bf02-031505a26bbe",
                        "92cbdcce-3849-3418-9dd9-edf958868746",
                        "9fb7f91c-80c2-3572-b7a2-26f2fc597147",
                        "8772cb04-0f2b-3d72-93f5-83d2f328de82",
                        "c75382ff-f9ec-30ed-a3bf-989eaa3734cf"
                    ]
                },
                {
                    "accession": "KW-0843",
                    "id": "Virulence",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "cf7b4c0c-5e3e-35ed-80de-4e6a56a14bee"
                    ]
                },
                {
                    "accession": "KW-0324",
                    "id": "Glycolysis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "aff29f09-97aa-3745-82d5-95a81522ee3f",
                        "b44f9392-66db-3350-9732-6798f5f7f138",
                        "37cc44e3-1053-3f01-ba4b-b7cee5d65c85",
                        "18d90788-23e9-3bd4-a17e-dbb417129a72",
                        "8bfa1252-1f9b-3043-93d8-f5639d9cf074",
                        "4fb159bd-103d-3af0-a750-ffb1d7efc7c4",
                        "33b4c0cf-74a0-325e-a1d3-086af7ba8f90",
                        "b364e47c-2699-3e4e-943e-4d21553d6140",
                        "a48ece5c-8d4b-324f-879a-baeaa387af9f",
                        "b27dd7db-316f-3eb6-a914-64fc033cd01c",
                        "2850d15f-f930-31e8-8764-356a1cb98b51",
                        "631f444e-b319-3aaf-a57c-b7836d934fa5",
                        "d196eab6-2d33-3619-a7f6-3166a310bad3",
                        "adec9c8c-296a-3769-9203-3969d932bc85",
                        "6661e42b-7659-3483-a127-a1fe3c7d0752",
                        "9f6606c2-e93c-3ac7-9fdc-60dff2f54955",
                        "71a490c7-33e5-39df-bd3b-5b5c0a876a35"
                    ]
                },
                {
                    "accession": "KW-0662",
                    "id": "Pyridine nucleotide biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "3f6a3d6f-f223-3a91-bb28-445f5e4c327c",
                        "7b2ac681-8f57-378c-9087-def2c187a208"
                    ]
                },
                {
                    "accession": "KW-0066",
                    "id": "ATP synthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0139",
                            "id": "CF(1)",
                            "category": [
                                "Cellular component",
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "d21bdcc2-f9fc-3b3e-87ce-5eaed473d7bb",
                                "9493d28b-2334-335f-bf3a-5971f94c0d6b",
                                "5eedc69d-cc8a-394a-ac34-31061697c90c",
                                "87a0407f-808f-3b21-b2ac-263219666221"
                            ]
                        }
                    ],
                    "proteinIds": [
                        "d21bdcc2-f9fc-3b3e-87ce-5eaed473d7bb",
                        "9493d28b-2334-335f-bf3a-5971f94c0d6b",
                        "5eedc69d-cc8a-394a-ac34-31061697c90c",
                        "87a0407f-808f-3b21-b2ac-263219666221"
                    ]
                },
                {
                    "accession": "KW-0648",
                    "id": "Protein biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0396",
                            "id": "Initiation factor",
                            "category": [
                                "Molecular function",
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "c9697069-9494-3aca-92c7-21498e70a573",
                                "bf16af9c-8140-3c4c-8f16-7b4d9487a2d0"
                            ]
                        },
                        {
                            "accession": "KW-0251",
                            "id": "Elongation factor",
                            "category": [
                                "Molecular function",
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "8a819b13-19a3-30a3-a369-306016a392b8",
                                "b837a743-46c7-3c8a-9580-8938a4589cfa",
                                "3ba5ec97-1eda-3dc5-b5c1-c093a5df4e11",
                                "5ed92216-217c-3fca-9f10-3a2d101359d4",
                                "5c95adf3-bad4-3a4f-b76b-b94204974137"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0329",
                    "id": "Glyoxylate bypass",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "f995a4b2-b1de-3a2b-aef3-164261dcedc9",
                        "1461bb5c-0af6-31a3-aeba-e69669861fdd",
                        "e42af84f-3d92-3cbb-8b6d-a7334acd580a",
                        "8b9be9c8-0db3-3271-bd73-dd7b1e08fe27"
                    ]
                },
                {
                    "accession": "KW-0534",
                    "id": "Nitrate assimilation",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "62942481-2b09-3054-8699-744b7968ce01",
                        "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                        "8f80a937-9e52-3232-a1a7-cf06252eb4cc",
                        "af13e9f3-b720-3a88-b110-d3da50d6671d"
                    ]
                },
                {
                    "accession": "KW-0823",
                    "id": "Tryptophan catabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "c1c007e5-cd0f-3bdd-9089-7ed8ba9a91b9"
                    ]
                },
                {
                    "accession": "KW-0566",
                    "id": "Pantothenate biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "4b6b69c8-934c-3886-8346-1a12c57bfc30",
                        "4e89fea4-1ab6-3157-a335-2adf9b1b33c2",
                        "0c4b65c4-4498-3227-8121-0287e17540e2"
                    ]
                },
                {
                    "accession": "KW-0155",
                    "id": "Chromate resistance",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "5d5ac211-ab72-323e-ad88-dc3e1e0850a5"
                    ]
                },
                {
                    "accession": "KW-0145",
                    "id": "Chemotaxis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "25039a14-8246-3b5f-ae47-56711abe51d9",
                        "c907cf8f-adfa-3c36-8478-87a02c27fd25",
                        "d1cdacd5-d00d-333b-bdba-0df200229b25"
                    ]
                },
                {
                    "accession": "KW-0627",
                    "id": "Porphyrin biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "c93a72a2-8bd0-3165-9bc2-221b16efd538",
                        "28ea057f-2be4-32c5-91aa-36c12293c25a",
                        "c11af8cb-9a5d-3df5-a3e0-2e64908817c1"
                    ]
                },
                {
                    "accession": "KW-0312",
                    "id": "Gluconeogenesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "b27dd7db-316f-3eb6-a914-64fc033cd01c",
                        "b44f9392-66db-3350-9732-6798f5f7f138",
                        "37cc44e3-1053-3f01-ba4b-b7cee5d65c85",
                        "67322d9e-fd74-3328-ae1f-0f9c34caf12f"
                    ]
                },
                {
                    "accession": "KW-0642",
                    "id": "Proline metabolism",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "cb099d7a-f9dc-38d3-884e-cfdafd86c2f0"
                    ]
                },
                {
                    "accession": "KW-0745",
                    "id": "Spermidine biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1c4eb8da-7d0e-3933-9ecc-32a9284914d4",
                        "3ba845c9-be72-3c9c-93e7-2f5f445d2750",
                        "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3"
                    ]
                },
                {
                    "accession": "KW-0120",
                    "id": "Carbon dioxide fixation",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "a75fa3e2-2c2a-364e-8d56-32867b4e2dd8"
                    ]
                },
                {
                    "accession": "KW-0598",
                    "id": "Phosphotransferase system",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "124c6ffa-0fb6-38b2-b175-247458f097f9",
                        "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                        "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                        "76d373ba-0068-3d85-894e-2559dd07a94a",
                        "91bd43f5-ae68-3b80-aa75-7a9c967b9398"
                    ]
                },
                {
                    "accession": "KW-0346",
                    "id": "Stress response",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "ce48f6ed-2f5e-3052-95b4-c46400f3e1d6",
                        "18dca3e9-3ec2-3bfd-ae59-bd227ada733e",
                        "56a38ea7-2f0b-3578-bfef-7f17b93bd275",
                        "fa0d3c94-9697-3f67-b435-b8aef0a96a5a",
                        "41f8e0c9-6167-3fdf-b413-51b3b1031efe",
                        "9aa82d48-90c7-39ac-9158-573fb5dcfc10",
                        "78e337f5-5c30-38db-a09d-8c2c35083444",
                        "0d39280b-f625-3e4e-bfb5-b40449c56b29",
                        "893a3274-dc45-3f91-b256-983a83c8b153",
                        "4305fb19-88a4-3d68-9990-01842b2345ea",
                        "a3aebedd-25dd-3dfe-9ade-83d1e8475627",
                        "fb8c2814-f376-3032-9735-34386ddd20a2",
                        "d6ef643f-0804-3b84-ab4a-77a7091bab22",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497",
                        "b6ec19d9-dd21-3506-899a-f675868a1b58",
                        "d8521a76-d508-3ea0-867c-0daab9e28465",
                        "3fb2549c-0452-3210-8be4-9f3a0d2cffea",
                        "7494b152-dbc6-395d-abdd-1b549511a731",
                        "c43a0411-8126-3d5f-8344-bb6d01bcee7b",
                        "bd628120-a237-3e4e-9d75-52d98e2ea191",
                        "fe3e07d9-70ca-330a-b4d9-1a3fa7e4f9e2",
                        "f5147ac0-d118-3474-b83e-df1f4d673e67",
                        "035d3181-8ee3-3152-bed8-9644e47d97dc",
                        "cf7b4c0c-5e3e-35ed-80de-4e6a56a14bee",
                        "fdac5ebd-57f1-3553-80d7-ac31bae488cd",
                        "5585a2b8-bcfe-3dae-8ea7-194d845b1744",
                        "e704c214-2dab-3b1b-afcb-173e95fc3a36",
                        "6c89d112-4e19-3cfd-bd2b-3ffa959812e4",
                        "fbe5b850-e94f-3077-af8a-dd514946b672",
                        "620d3785-9f0f-3f78-98f8-44b138ba847e",
                        "8aa62ad5-4534-391c-9963-a1c34edc7e3d",
                        "b07f5eb7-a050-352d-80c3-6de294a6ba3f",
                        "c80cda9e-fd4d-38c1-9240-3bd0ab9e6143",
                        "46be049f-6f54-305e-8e47-50ffc9649e9d"
                    ]
                },
                {
                    "accession": "KW-0227",
                    "id": "DNA damage",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0742",
                            "id": "SOS response",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "ff9f96d7-f456-39da-8a23-a25a01e0c64d",
                                "1c7ba006-0968-38fb-81e4-325187699247"
                            ]
                        },
                        {
                            "accession": "KW-0234",
                            "id": "DNA repair",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0267",
                                    "id": "Excision nuclease",
                                    "category": [
                                        "Molecular function",
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "1c7ba006-0968-38fb-81e4-325187699247"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0945",
                    "id": "Host-virus interaction",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                        "850143d4-9315-3889-9aad-60bd107000ef",
                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497"
                    ]
                },
                {
                    "accession": "KW-0664",
                    "id": "Pyridoxine biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "09824aca-3ccb-3f94-9b21-7d95f424c697",
                        "b7a12bfd-41d1-33d3-a194-d047eb9198dc",
                        "30ff119c-62bd-325c-986f-1e7889578200",
                        "703f9b97-7871-3f81-80d9-df88d0cc741a"
                    ]
                },
                {
                    "accession": "KW-0228",
                    "id": "DNA excision",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1c7ba006-0968-38fb-81e4-325187699247"
                    ]
                },
                {
                    "accession": "KW-0376",
                    "id": "Hydrogen peroxide",
                    "category": [
                        "Molecular function",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "589687f2-9044-364c-aaac-707b0f97823c",
                        "ebd5357d-3ce1-3857-8f70-9ea93cbd420c"
                    ]
                },
                {
                    "accession": "KW-0620",
                    "id": "Polyamine biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1c4eb8da-7d0e-3933-9ecc-32a9284914d4",
                        "3ba845c9-be72-3c9c-93e7-2f5f445d2750",
                        "48e7bddb-5dbe-36b3-840a-806bdcf3efd5",
                        "202dc8fc-4a86-3ba6-a131-35dd9fb94db3"
                    ]
                },
                {
                    "accession": "KW-0133",
                    "id": "Cell shape",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0573",
                            "id": "Peptidoglycan synthesis",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "72687302-febc-300a-8b6a-aa54e09d2bef",
                                "fc16d218-6350-3e2e-9c9c-05fefd49e5a3",
                                "8854d795-3ec4-373d-a0c1-10b3373df75d",
                                "da971fe7-5f46-30e6-bd0f-7a765e069a77",
                                "54e61131-1e0e-3a23-9edc-3e7c5749607a",
                                "e83511de-fef5-3734-8e5a-9d02ae774102"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0570",
                    "id": "Pentose shunt",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "fe506aea-138e-36a4-ad7f-b74ea420b651",
                        "1776f92b-eb74-3f0c-8d9a-c47a65663644",
                        "8bc41f8b-7a09-3d58-94e5-f48e87ab5611"
                    ]
                },
                {
                    "accession": "KW-0409",
                    "id": "Iron storage",
                    "category": [
                        "Ligand",
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                        "f280d9f9-48d7-3ad2-a551-a8eb6e58ff3f",
                        "41794e2f-1527-3aa8-a594-ce47f97a857d"
                    ]
                },
                {
                    "accession": "KW-0311",
                    "id": "Gluconate utilization",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "1776f92b-eb74-3f0c-8d9a-c47a65663644"
                    ]
                },
                {
                    "accession": "KW-0220",
                    "id": "Diaminopimelate biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "82bf9d8d-a90b-38f7-80df-586b4387bc0e",
                        "6214d45c-e9b8-30c3-9887-4acaee29688e",
                        "45663484-0ecc-3c24-be1f-fa2773ca7012",
                        "f639e2ef-b23c-3eac-82d4-2f822e5ad56f"
                    ]
                },
                {
                    "accession": "KW-0813",
                    "id": "Transport",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0592",
                            "id": "Phosphate transport",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "3fb2549c-0452-3210-8be4-9f3a0d2cffea"
                            ]
                        },
                        {
                            "accession": "KW-0406",
                            "id": "Ion transport",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0375",
                                    "id": "Hydrogen ion transport",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "d21bdcc2-f9fc-3b3e-87ce-5eaed473d7bb",
                                        "9493d28b-2334-335f-bf3a-5971f94c0d6b",
                                        "5eedc69d-cc8a-394a-ac34-31061697c90c",
                                        "87a0407f-808f-3b21-b2ac-263219666221"
                                    ]
                                },
                                {
                                    "accession": "KW-0626",
                                    "id": "Porin",
                                    "category": [
                                        "Domain",
                                        "Molecular function",
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                        "b6ec19d9-dd21-3506-899a-f675868a1b58"
                                    ]
                                }
                            ],
                            "proteinIds": [
                                "d21bdcc2-f9fc-3b3e-87ce-5eaed473d7bb",
                                "19ba0f71-8f9b-31d9-90d8-73666790014f",
                                "9493d28b-2334-335f-bf3a-5971f94c0d6b",
                                "5eedc69d-cc8a-394a-ac34-31061697c90c",
                                "b6ec19d9-dd21-3506-899a-f675868a1b58",
                                "87a0407f-808f-3b21-b2ac-263219666221"
                            ]
                        },
                        {
                            "accession": "KW-0050",
                            "id": "Antiport",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4"
                            ]
                        },
                        {
                            "accession": "KW-0249",
                            "id": "Electron transport",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "0791e3a6-5ee6-329f-a7d1-b8450305c4f7",
                                "e9d50cfb-faea-37e8-abb5-9f9add77f07a",
                                "0d2c0f1a-cdd0-3ab0-957f-2308531a58e4",
                                "af13e9f3-b720-3a88-b110-d3da50d6671d",
                                "850143d4-9315-3889-9aad-60bd107000ef",
                                "052ebfac-6762-3ca0-b2c4-9ed489e6a56a",
                                "65886732-974b-3f7a-a011-90682a3b3ef5",
                                "24e689b0-03b1-3408-a49a-151692cf629c",
                                "2393e1b7-614e-37f7-b893-bb8f9ea39198",
                                "ff86e71c-2da2-3e32-83da-8fa5fc4342ad",
                                "d58a08a8-4997-39ef-a72d-9df42f21ed0f",
                                "e8c576ae-9b77-3b43-991e-d7dca04bcfd9",
                                "b9f03a52-e78f-3a0c-9f3d-4d4301c34388"
                            ]
                        },
                        {
                            "accession": "KW-0653",
                            "id": "Protein transport",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0571",
                                    "id": "Peptide transport",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                                        "d1cdacd5-d00d-333b-bdba-0df200229b25"
                                    ]
                                },
                                {
                                    "accession": "KW-0811",
                                    "id": "Translocation",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                                        "cdf158db-6f74-3ade-84ab-6e18b09bab2a"
                                    ]
                                }
                            ],
                            "proteinIds": [
                                "abc501f5-c77c-3a30-b015-13b7fa11f8ce",
                                "1baad8fe-0770-30f7-897e-1f3d00a93ace",
                                "1182f19c-d9cf-3577-b30d-fd6f2ec5d4de",
                                "cdf158db-6f74-3ade-84ab-6e18b09bab2a",
                                "d1cdacd5-d00d-333b-bdba-0df200229b25"
                            ]
                        },
                        {
                            "accession": "KW-0029",
                            "id": "Amino-acid transport",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "91e1fc09-c4c0-371f-b84e-7272bf3d625a",
                                "c4cdd1b7-0c46-3cb6-90c9-f2b2a1983a58",
                                "96a76a6b-7f10-333e-b525-f2784214ee48",
                                "d572aa6a-b6a6-3fad-9282-a20959414f10",
                                "1eb6f5d3-c5a4-35f9-99a6-c3dca49a68d4",
                                "65a926bf-d216-3943-9809-9013a707a4ac",
                                "8dada997-5a23-3c7b-9833-68bc56ba560f"
                            ]
                        },
                        {
                            "accession": "KW-0762",
                            "id": "Sugar transport",
                            "category": [
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "25039a14-8246-3b5f-ae47-56711abe51d9",
                                "c907cf8f-adfa-3c36-8478-87a02c27fd25",
                                "9e2aad1c-75e8-3a3d-b508-aa845635d7b7",
                                "124c6ffa-0fb6-38b2-b175-247458f097f9",
                                "c25d6964-0a5d-3ceb-8df5-831ecaa02add",
                                "eee28c23-0b56-3de5-9322-e7a02f56dcc3",
                                "76d373ba-0068-3d85-894e-2559dd07a94a",
                                "91bd43f5-ae68-3b80-aa75-7a9c967b9398"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0673",
                    "id": "Quorum sensing",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "9e01ce48-38b0-3bee-8178-f00a8b68ada0",
                        "2adc85c0-4e20-3bb3-a2f6-838caacfc474"
                    ]
                },
                {
                    "accession": "KW-0317",
                    "id": "Glutathione biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "b378c052-6677-38dc-a7b5-3174eed7d879",
                        "89cccdf2-60e0-341f-85e5-960f73e90682"
                    ]
                },
                {
                    "accession": "KW-0237",
                    "id": "DNA synthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "5dfb8cec-10b6-33ad-b722-072f815ea2e4"
                    ]
                },
                {
                    "accession": "KW-0414",
                    "id": "Isoprene biosynthesis",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "8fdfc5b7-72bb-357d-9e57-c9710e060f7e",
                        "29dbb1a1-17a0-3251-a3ad-258fc2b25495"
                    ]
                },
                {
                    "accession": "KW-0698",
                    "id": "rRNA processing",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "a8cee9c2-7162-30ee-b9ef-3a76382cc58f",
                        "fa03fcb6-1a8e-3c1b-9b91-fad4d162279e",
                        "b81ec75a-547b-3fc9-b14c-0fc07e7e9f7f",
                        "83b737ff-b679-380a-a9c0-d56ed58ff728"
                    ]
                },
                {
                    "accession": "KW-0131",
                    "id": "Cell cycle",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0132",
                            "id": "Cell division",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0717",
                                    "id": "Septation",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "d0894567-8e24-31ed-9ad2-5fc20c27125f",
                                        "c16a16e2-43d4-3291-a840-fefd5939be2e",
                                        "9d502900-4b70-3bc5-8723-daa4db8308a8"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0804",
                    "id": "Transcription",
                    "category": [
                        "Biological process"
                    ],
                    "children": [
                        {
                            "accession": "KW-0805",
                            "id": "Transcription regulation",
                            "category": [
                                "Biological process"
                            ],
                            "children": [
                                {
                                    "accession": "KW-0889",
                                    "id": "Transcription antitermination",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "b5d2ac66-2405-3d81-a2bc-520d5bb79ca1",
                                        "ccdb8a84-5e15-3840-87f4-9a2366f7bc56",
                                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497"
                                    ]
                                },
                                {
                                    "accession": "KW-0731",
                                    "id": "Sigma factor",
                                    "category": [
                                        "Molecular function",
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "76c71bc8-c46d-3acc-ad17-d1ca3625783a"
                                    ]
                                },
                                {
                                    "accession": "KW-0806",
                                    "id": "Transcription termination",
                                    "category": [
                                        "Biological process"
                                    ],
                                    "children": [],
                                    "proteinIds": [
                                        "b5d2ac66-2405-3d81-a2bc-520d5bb79ca1",
                                        "e130092e-4136-3244-a836-2f098cdd0a85",
                                        "3e03ed8d-35dd-3858-b919-fa9b04e5a497"
                                    ]
                                }
                            ],
                            "proteinIds": []
                        },
                        {
                            "accession": "KW-0240",
                            "id": "DNA-directed RNA polymerase",
                            "category": [
                                "Cellular component",
                                "Biological process"
                            ],
                            "children": [],
                            "proteinIds": [
                                "4181cf83-cf26-3a2f-acc1-8adef347d76c",
                                "a30532fe-425b-31bf-a47c-fa9b90939faa",
                                "5a6db542-70d3-3f1e-8bfa-723559a235e2",
                                "456bfd92-36c5-3fd9-8803-aed14a8ec2d0"
                            ]
                        }
                    ],
                    "proteinIds": []
                },
                {
                    "accession": "KW-0216",
                    "id": "Detoxification",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "d14ba956-67ec-339b-bc82-f5d87a3b9a28"
                    ]
                },
                {
                    "accession": "KW-0226",
                    "id": "DNA condensation",
                    "category": [
                        "Biological process"
                    ],
                    "children": [],
                    "proteinIds": [
                        "36aa46c3-c11c-36dd-81f2-16524ccbb299",
                        "f008373a-488a-36c1-85e9-5fdfb99fa95a",
                        "4939a1cd-d4b2-3c7d-9053-b02c16db7e41"
                    ]
                }
            ],
            "proteinIds": []
        }
    ],
    "proteinIds": []
}