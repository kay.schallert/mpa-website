import { Component, OnInit } from '@angular/core';
import { ArrayDataSource } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';

import { trigger, state, style, transition, animate } from '@angular/animations';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { Keyword } from '../../model/experiment-data-json';

@Component({
  selector: 'app-keywords-tab',
  templateUrl: './keywords-tab.component.html',
  styleUrls: ['./keywords-tab.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ visibility: 'hidden', height: 0, opacity: 0 })),
      state('expanded', style({ height: '*', opacity: 1 })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ])
  ]
})
export class KeywordsTabComponent implements OnInit {

  treeControl: FlatTreeControl<Keyword> = new FlatTreeControl<Keyword>(
    (node) => node.level,
    (node) => node.expandable,
  )
  flatTree: Keyword[] = [];
  dataSource: ArrayDataSource<Keyword>;

  selectedKeyword: Keyword;
  filterString: string;
  filterThreshhold: number = 1;
  // keywordCategories = UniProtKeywordCategory;

  constructor(
    public mpaTableDataService: MpaTableDataService
  ) {}

  ngOnInit(): void {
    this.flatTree = this.mpaTableDataService.getKeywords();
    this.dataSource = new ArrayDataSource(this.flatTree);
    this.treeControl.dataNodes = this.flatTree;
    this.filterString = '';
    this.selectedKeyword = this.flatTree[0];
  }

  hasChild = (_: number, node: Keyword) => node.expandable;
  isCategoryNode = (_:number,node: Keyword) => node.kwCategory === undefined; // category-nodes shouldn't have a category assigned

  getParentNode(node: Keyword): Keyword {
    const nodeIndex = this.flatTree.indexOf(node);
    for (let i = nodeIndex - 1; i >= 0; i--) {
      if (this.flatTree[i].level === node.level - 1) {
        return this.flatTree[i];
      }
    }
    return null;
  }
  

  shouldRender(node: Keyword) {
    if (!node.isDisplayed) {
      return false;
    }
    let parent: Keyword = this.getParentNode(node);
    while(parent) {
      if (!parent.isExpanded) {
        return false;
      }
      parent = this.getParentNode(parent);
    }
    return true;
  }

  isSelected(node: Keyword) {
    return this.selectedKeyword.kwAccession == node.kwAccession;
  }

  clickNode(node: Keyword) {
    this. selectedKeyword = node;
    if (node.expandable) {
      node.isExpanded = !node.isExpanded;
    }
    console.log(node);
  }

  applyFilter(): void {
    let nodes = this.flatTree
    let regExp = new RegExp(this.filterString, 'i');
    if (this.filterString.length > this.filterThreshhold) {
      for (let i = 0; i < nodes.length; i++) {
        nodes[i].isDisplayed = false;
        nodes[i].isExpanded = false;

        if (regExp.test(nodes[i].kwAccession) || regExp.test(nodes[i].kwId)) {
          nodes[i].isDisplayed = true;
          nodes[i].isExpanded = false;

          // custom "getParent" method as this.getParentNode crashes the site
          let level = nodes[i].level - 1;
          let k = i -1;
          while (level >= 0 && k >= 0) {
            if(nodes[k].level === level){
              nodes[k].isDisplayed = true;
              nodes[k].isExpanded = true;
              level--;
            }
            k--;
          }
          this.treeControl.getDescendants(nodes[i]).map(desc => {
            desc.isDisplayed = true;
            i++; // skip descendants for regExp.test()
          });
        }
        }
    } else {
      this.flatTree[0].isExpanded = true;
      this.flatTree.map(node => {
        node.isDisplayed = true;
        node.isExpanded = false;
      });
    }
  }

  expandAll() {
    this.flatTree.map(node => node.expandable ? node.isExpanded = true : {});
  }

  collapseAll() {
    this.flatTree.map(node => node.expandable ? node.isExpanded = false : {});
  }
}
