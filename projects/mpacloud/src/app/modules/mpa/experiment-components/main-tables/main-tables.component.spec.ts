import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainTablesComponent } from './main-tables.component';

describe('ProteinGroupTableComponent', () => {
  let component: MainTablesComponent;
  let fixture: ComponentFixture<MainTablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MainTablesComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
