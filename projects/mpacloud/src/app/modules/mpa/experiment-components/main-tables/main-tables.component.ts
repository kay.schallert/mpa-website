import { Component } from '@angular/core';
import { MpaTableDataService } from '../../services/mpa-table-data.service';


export enum GroupSelection {
  MAINGROUPS = 'maingroups',
  SUBGROUPS = 'subgroups',
}

@Component({
  selector: 'app-protein-group-table',
  templateUrl: './main-tables.component.html',
  styleUrl: './main-tables.component.scss'
})
export class MainTablesComponent {

  constructor(public mpaTableDataService: MpaTableDataService) {}

  GroupSelection = GroupSelection;

  // has the groupselection buttons
  // has group/subgroup table
  // has the detail view
  // gets reported the selected element from the group/subgroup table, passes on to detail view



}
