import {
  Component,
  ViewChild,
  AfterViewInit,
  Input,
  OnInit,
  OnDestroy
} from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { PeptideScope } from '../proteingroup-detail-view/proteingroup-detail-view.component';
import { Peptide } from '../../model/experiment-data-json';

@Component({
  selector: 'app-peptide-table',
  templateUrl: './peptide-table.component.html',
  styleUrls: ['./peptide-table.component.css'],
})
export class PeptideTableComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns = ['id'];
  dataSource: MatTableDataSource<Peptide>;
  quantificationColumns = [];
  index2experimentIdMap: Map<number, string> = new Map();

  filterString: string;

  selectedAll: boolean = false;
  highlightedPeptide: Peptide = null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() scope: PeptideScope;

  constructor(private mpaTableDataService: MpaTableDataService) {
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.filterString = '';
    this.index2experimentIdMap = new Map();
    this.mpaTableDataService.getExperiment2IndexMap().forEach((value: number, key: string) => {
      this.index2experimentIdMap.set(value, key);
    });
    for (let i = 0; i < this.index2experimentIdMap.size; i++) {
      // quantificationColumns is used in template to generate the right amount of columns and extract the relevant data
      this.quantificationColumns.push({"expID":this.index2experimentIdMap.get(i),"index":i});
      this.displayedColumns.push(this.index2experimentIdMap.get(i));
    }
    if (this.scope === 0) {
      this.dataSource.data = this.mpaTableDataService.getProteinGroupPeptides();

    } else if (this.scope === 1) {
      this.dataSource.data = this.mpaTableDataService.getProteinPeptides();
    }
    this.displayedColumns.push('checkbox');
    this.applyFilter();
    this.mpaTableDataService.detailViewDataChange.subscribe(() => {
      if (this.scope === 0) {
        this.dataSource.data = this.mpaTableDataService.getProteinGroupPeptides();
      } else if (this.scope === 1) {
        this.dataSource.data = this.mpaTableDataService.getProteinPeptides();
      }
      this.applyFilter();
    });

  }

  ngOnDestroy(){
    this.highlightedPeptide.isHighlighted = false;
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = this.filterString.toLowerCase();
    // TODO: add actual filtering ...
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active,direction:this.sort.direction});
    } else {
      this.sortData({active:'proteinID',direction:'asc'});
    };
  }

  sortData(sort: Sort) {
    if (sort.direction == '') {
      sort.direction = 'asc';
      // update ui
      this.sort.direction = sort.direction;
      this.sort.sortChange.emit(this.sort);
    }
    let data = this.dataSource.data.slice();
    const isAsc = sort.direction === 'asc';
    data.sort((a, b) => {
      switch (sort.active) {
        case 'id':
          return (a.sequenceId.localeCompare(b.sequenceId)) * (isAsc ? 1 : -1); // has to be changed if this ever becomes an integer
        case this.index2experimentIdMap.get(this.quantificationColumns[0].index):
          return (a.quantifications[0] - b.quantifications[0]) * (isAsc ? 1 : -1);
      }
    });
    let displayedEntries = [];
    let hiddenEntries = [];
    data.map(entry => entry.isDisplayed ? displayedEntries.push(entry) : hiddenEntries.push(entry));
    this.dataSource.data = [...displayedEntries, ...hiddenEntries];
  }

  setPeptide(row: Peptide) {
    if (this.highlightedPeptide !== null && this.highlightedPeptide !== row) {
      this.highlightedPeptide.isHighlighted = false;
      this.highlightedPeptide = null;
    }
    if (row.isDisplayed) {
      row.isHighlighted = !row.isHighlighted;
      this.highlightedPeptide = row;
      this.mpaTableDataService.selectedPeptideId = row.sequenceId;
      this.mpaTableDataService.detailViewDataChange.next(true);
    };
  }

  onSelectAllGroups(): void {
    this.dataSource.data.map((protein: Peptide) => {
      protein.isSelected = this.selectedAll;
    });
  }

  disableSelectedGroups(): void {
    this.dataSource.data.forEach((peptide: Peptide) => {
      if (peptide.isSelected) {
        if (peptide.isHighlighted) {
          this.highlightedPeptide = null;
          peptide.isHighlighted = false;
        }
        // peptide.isDisplayed = false;
        peptide.isSelected = false;
        console.log(peptide);
        this.mpaTableDataService.changeAndPropagatePeptideVisibility(peptide.sequenceId, false);
      }
    });
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active.slice(),direction:this.sort.direction});
    } else {
      this.sortData({active:'accession',direction:'asc'});
    };
  }

  enableSelectedGroups(): void {
    this.dataSource.data.forEach((peptide: Peptide) => {
      if (peptide.isSelected) {
        if (peptide.isHighlighted) {
          this.highlightedPeptide = null;
          peptide.isHighlighted = false;
        }
        // peptide.isDisplayed = true;
        peptide.isSelected = false;
        this.mpaTableDataService.changeAndPropagatePeptideVisibility(peptide.sequenceId, true);
      }
    });
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active.slice(),direction:this.sort.direction});
    } else {
      this.sortData({active:'accession',direction:'asc'});
    };
  }

}
