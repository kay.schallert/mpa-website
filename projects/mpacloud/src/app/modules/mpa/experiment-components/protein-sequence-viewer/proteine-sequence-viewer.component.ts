import { Component, OnInit } from '@angular/core';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { Peptide, ProteinSequence } from '../../model/experiment-data-json';

@Component({
  selector: 'app-protein-sequence-viewer',
  templateUrl: './proteine-sequence-viewer.component.html',
  styleUrls: ['./proteine-sequence-viewer.component.css'],
})
export class ProteineSequenceViewerComponent implements OnInit {
  proteinSequenceData: ProteinSequence;

  constructor(public mpaTableDataService: MpaTableDataService) {}

  ngOnInit() {
    this.mpaTableDataService.proteinSequenceData.subscribe(
      (proteinSequence) => {
        this.proteinSequenceData = proteinSequence;
      },
    );
  }
  
  getHighlightedSequence(): string {
    if (!this.proteinSequenceData || !this.proteinSequenceData.sequence) {
      return '';
    }
    
    // Start with the original sequence.
    let sequence = this.proteinSequenceData.sequence;
  
    const peptides: Peptide[] = this.mpaTableDataService.getProteinPeptides();
  
    // Sort peptides by length descending so that longer peptides are processed first.
    peptides.sort((a, b) => b.sequenceId.length - a.sequenceId.length);
  
    peptides.forEach(peptide => {
      const peptideSeq = peptide.sequenceId;
   
      // Escape regex special characters.
      const escapedPeptide = this.escapeRegExp(peptideSeq);
  
      // Create a global regex.
      const regex = new RegExp(escapedPeptide, 'g');
  
      // Count matches before replacement.
      const matches = sequence.match(regex);
      const matchCount = matches ? matches.length : 0;
      if (matchCount > 0) {
        // Replace all occurrences with a highlighted span.
        sequence = sequence.replace(regex, `<span class="highlighted-peptide">${peptideSeq}</span>`);
      } else {
      }
    });
    return sequence;
  }

  escapeRegExp(text: string): string {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
  }
    
}
