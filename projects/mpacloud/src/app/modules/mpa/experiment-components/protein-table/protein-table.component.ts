import {
  Component,
  AfterViewInit,
  Input,
  ViewChild,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { Protein } from '../../model/experiment-data-json';
import { GroupSelection } from '../group-table/group-table.component';

@Component({
  selector: 'app-protein-table',
  templateUrl: './protein-table.component.html',
  styleUrls: ['./protein-table.component.css'],
})
export class ProteinTableComponent implements OnInit, AfterViewInit, OnChanges {

  displayedColumns = ['accession', 'description'];  // TODO: quantifications
  quantificationColumns = [];
  dataSource: MatTableDataSource<Protein>;
  index2experimentIdMap: Map<number, string> = new Map();

  filterString: string;

  selectedAll: boolean = false;
  highlightedProtein: Protein = null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private mpaTableDataService: MpaTableDataService) {
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource([]);
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngOnInit() {
    this.filterString = '';
    this.dataSource.data = this.mpaTableDataService.getProteinData();
    // const index2experimentIdMap: Map<number, string> = new Map();
    this.index2experimentIdMap = new Map();
    this.mpaTableDataService.getExperiment2IndexMap().forEach((value: number, key: string) => {
      this.index2experimentIdMap.set(value, key);
    });
    for (let i = 0; i < this.index2experimentIdMap.size; i++) {
      // quantificationColumns is used in template to generate the right amount of columns and extract the relevant data
      this.quantificationColumns.push({"expID":this.index2experimentIdMap.get(i),"index":i});
      this.displayedColumns.push(this.index2experimentIdMap.get(i));
    }
    this.displayedColumns.push('checkbox');
    this.dataSource.data = this.mpaTableDataService.getProteinData();
    this.applyFilter();
    // TODO: saving and removal of subscription in NGDestroy
    this.mpaTableDataService.detailViewDataChange.subscribe(() => {
      this.dataSource.data = this.mpaTableDataService.getProteinData();
      this.applyFilter();
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter() {
    this.dataSource.filter = this.filterString.toLowerCase();
    // TODO: add actual filtering ...
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active,direction:this.sort.direction});
    } else {
      this.sortData({active:'accession',direction:'asc'});
    };
  }

  sortData(sort: Sort) {
    if (sort.direction == '') {
      sort.active = 'accession';
      sort.direction = 'asc';
      // update ui
      this.sort.active = sort.active;
      this.sort.direction = sort.direction;
      this.sort.sortChange.emit(this.sort);
    }
    let data = this.dataSource.data.slice();
    const isAsc = sort.direction === 'asc';
    data.sort((a, b) => {
      switch (sort.active) {
        case 'accession':
          return (a.representativeAccession.accession.localeCompare(b.representativeAccession.accession)) * (isAsc ? 1 : -1); // has to be changed if this ever becomes an integer
        case 'description':
          return (a.representativeDescription.description.localeCompare(b.representativeDescription.description)) * (isAsc ? 1 : -1);
        case this.index2experimentIdMap.get(this.quantificationColumns[0].index):
          return (a.quantifications[0] - b.quantifications[0]) * (isAsc ? 1 : -1);
      }
    });
    let displayedEntries = [];
    let hiddenEntries = [];
    data.map(entry => entry.isDisplayed ? displayedEntries.push(entry) : hiddenEntries.push(entry));
    this.dataSource.data = [...displayedEntries, ...hiddenEntries];
  }

  onSelectAllGroups(): void {
    this.dataSource.data.map((protein: Protein) => {
      protein.isSelected = this.selectedAll;
    });
  }

  disableSelectedGroups(): void {
    this.dataSource.data.forEach((protein: Protein) => {
      if (protein.isSelected) {
        if (protein.isHighlighted) {
          this.highlightedProtein = null;
          protein.isHighlighted = false;
        }
        // protein.isDisplayed = false;
        protein.isSelected = false;
        console.log(protein);
        this.mpaTableDataService.changeAndPropagateProteinVisibility(protein.proteinId, false);
      }
    });
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active.slice(),direction:this.sort.direction});
    } else {
      this.sortData({active:'accession',direction:'asc'});
    };
  }

  enableSelectedGroups(): void {
    this.dataSource.data.forEach((protein: Protein) => {
      if (protein.isSelected) {
        if (protein.isHighlighted) {
          this.highlightedProtein = null;
          protein.isHighlighted = false;
        }
        protein.isSelected = false;
        this.mpaTableDataService.changeAndPropagateProteinVisibility(protein.proteinId, true);
      }
    });
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active.slice(),direction:this.sort.direction});
    } else {
      this.sortData({active:'accession',direction:'asc'});
    };
  }

  setProtein(row: Protein) {
    console.log(row);
    if (this.highlightedProtein !== null && this.highlightedProtein !== row) {
      this.highlightedProtein.isHighlighted = false;
      this.highlightedProtein = null;
    }
    if (row.isDisplayed) {
      row.isHighlighted = !row.isHighlighted;
      this.highlightedProtein = row;
      this.mpaTableDataService.selectedProteinId = row.proteinId;
      this.mpaTableDataService.detailViewDataChange.next(true);
      this.mpaTableDataService.requestSequence();
    };
    console.log(row);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.dataSource);
  }

  // highlightRow(row) {
  //   row.isHighlighted = !row.isHighlighted;
  //   return row.isHighlighted;
  // }

}
