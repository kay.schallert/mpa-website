import { Component, OnChanges, SimpleChanges } from '@angular/core';

// TODO: better enums
export enum PeptideScope {
  GROUP,
  PROTEIN,
}

export enum PsmScope {
  GROUP,
  PROTEIN,
  PEPTIDE,
}

@Component({
  selector: 'app-proteingroup-detail-view',
  templateUrl: './proteingroup-detail-view.component.html',
  styleUrls: ['./proteingroup-detail-view.component.css'],
})
export class ProteingroupDetailViewComponent implements OnChanges {
  PeptideScopes = PeptideScope;
  PsmScopes = PsmScope

  tabSelectionState = 0;

  // TODO remove this
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

}
