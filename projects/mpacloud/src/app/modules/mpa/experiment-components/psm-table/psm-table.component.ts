import {
  Component,
  AfterViewInit,
  Input,
  ViewChild,
  OnInit, OnDestroy,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import {
  PsmScope,
} from '../proteingroup-detail-view/proteingroup-detail-view.component';
import { PSM, Spectrum } from '../../model/experiment-data-json';


@Component({
  selector: 'app-psm-table',
  templateUrl: './psm-table.component.html',
  styleUrls: ['./psm-table.component.css'],
})
export class PsmTableComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns = ['psmID', 'peptideID', 'spectrumID', 'qvalue', 'quant', 'checkbox'];
  dataSource: MatTableDataSource<PSM>;

  filterString: string;

  selectedAll: boolean = false;
  highlightedSpectrum: Spectrum = null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  PsmScope = PsmScope;
  @Input() scope: PsmScope;

  constructor(private mpaTableDataService: MpaTableDataService) {
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.filterString = '';
    if (this.scope === 0) {
      this.dataSource.data = this.mpaTableDataService.getPSMDataForGroup();
    } else if (this.scope === 1) {
      this.dataSource.data = this.mpaTableDataService.getPSMDataForProtein();
    } else if (this.scope === 2) {
      this.dataSource.data = this.mpaTableDataService.getPSMDataForPeptide();
    }
    this.applyFilter();
    this.mpaTableDataService.detailViewDataChange.subscribe(() => {
      if (this.scope === 0) {
        this.dataSource.data = this.mpaTableDataService.getPSMDataForGroup();
      } else if (this.scope === 1) {
        this.dataSource.data = this.mpaTableDataService.getPSMDataForProtein();
      } else if (this.scope === 2) {
        this.dataSource.data = this.mpaTableDataService.getPSMDataForPeptide();
      }
      this.applyFilter();
    });
  }

  ngOnDestroy(){
    this.highlightedSpectrum.isHighlighted = false;
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = this.filterString.toLowerCase();
    // TODO: add actual filtering ...
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active,direction:this.sort.direction});
    } else {
      this.sortData({active:'psmID',direction:'asc'});
    };
  }

  sortData(sort: Sort) {
    if (sort.direction == '') {
      sort.active = 'psmID';
      sort.direction = 'asc';
      // update ui
      this.sort.active = sort.active;
      this.sort.direction = sort.direction;
      this.sort.sortChange.emit(this.sort);
    }

    let data = this.dataSource.data.slice();
    const isAsc = sort.direction === 'asc';
    data.sort((a, b) => {
      switch (sort.active) {
        case 'psmID':
          return (a.psmId.localeCompare(b.psmId)) * (isAsc ? 1 : -1);
        case 'peptideID':
          return (a.peptideId.localeCompare(b.peptideId)) * (isAsc ? 1 : -1);
        case 'spectrumID':
          return (a.spectrumId.localeCompare(b.spectrumId)) * (isAsc ? 1 : -1);
        case 'quant':
          return (a.qvalue - b.qvalue) * (isAsc ? 1 : -1);
        case 'quant':
          return (a.spectrumQuant - b.spectrumQuant) * (isAsc ? 1 : -1);
      }
    });
    this.dataSource.data = data;
  }

  setPsm(row: PSM) {
    const spectrum: Spectrum = this.mpaTableDataService.getSpectrumFromId(row.spectrumId);
    if (this.highlightedSpectrum !== null && this.highlightedSpectrum !== spectrum) {
      this.highlightedSpectrum.isHighlighted = false;
      this.highlightedSpectrum = null;
    }
    if (spectrum.isDisplayed) {
      spectrum.isHighlighted = !spectrum.isHighlighted;
      this.highlightedSpectrum = spectrum;
      this.mpaTableDataService.selectedSpectrumId = row.spectrumId;
      this.mpaTableDataService.selectedPsmId = row.psmId;
      this.mpaTableDataService.detailViewDataChange.next(true);
    };
  }

  onSelectAllGroups(): void {
    this.dataSource.data.map((psm: PSM) => {
      psm.isSelected = this.selectedAll;
    });
  }

  disableSelectedGroups(): void {
    this.dataSource.data.forEach((psm: PSM) => {
      const spectrum: Spectrum = this.mpaTableDataService.getSpectrumFromId(psm.spectrumId);
      if (psm.isSelected) {
        if (spectrum.isHighlighted) {
          this.highlightedSpectrum = null;
          spectrum.isHighlighted = false;
        }
        // spectrum.isDisplayed = false;
        psm.isSelected = false;
        this.mpaTableDataService.changeAndPropagateSpectrumnVisibility(spectrum.spectrumId, false);
      }
    });
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active.slice(),direction:this.sort.direction});
    } else {
      this.sortData({active:'psmID',direction:'asc'});
    };
  }

  enableSelectedGroups(): void {
    this.dataSource.data.forEach((psm: PSM) => {
      const spectrum: Spectrum = this.mpaTableDataService.getSpectrumFromId(psm.spectrumId);
      if (psm.isSelected) {
        if (spectrum.isHighlighted) {
          this.highlightedSpectrum = null;
          spectrum.isHighlighted = false;
        }
        // spectrum.isDisplayed = true;
        psm.isSelected = false;
        this.mpaTableDataService.changeAndPropagateSpectrumnVisibility(spectrum.spectrumId, true);
      }
    });
    if (this.sort != undefined) {
      this.sortData({active:this.sort.active.slice(),direction:this.sort.direction});
    } else {
      this.sortData({active:'psmID',direction:'asc'});
    };
  }

  isHighlighted(row: PSM): boolean {
    return this.mpaTableDataService.getSpectrumFromId(row.spectrumId).isHighlighted;
  }

  isDisplayed(row: PSM): boolean {
    return this.mpaTableDataService.getSpectrumFromId(row.spectrumId).isDisplayed;
  }


}
