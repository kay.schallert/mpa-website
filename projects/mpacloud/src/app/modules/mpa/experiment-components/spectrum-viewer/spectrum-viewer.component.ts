import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { SpectrumDataObject } from './spectrum-data-object';
import { Spectrum } from '../../model/experiment-data-json';

export type DataPoint = {
  x: number;
  y: number;
};

@Component({
  selector: 'app-spectrum-viewer',
  templateUrl: './spectrum-viewer.component.html',
  styleUrls: ['./spectrum-viewer.component.css'],
})
export class SpectrumViewerComponent implements OnInit {
  // showIon states
  showY1 = false;
  showY2 = false;
  showB1 = false;
  showB2 = false;

  // colors
  private defaultColor = '#999999';
  private y1Color = '#10FFCB';
  private y2Color = '#418700';
  private b1Color = '#e107ae';
  private b2Color = '#f5064a';

  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;

  // Remove fixed xAxisLength and yAxisLength
  public scatterChartType: ChartType = 'scatter';
  public scatterChartData: ChartData<'scatter'> = {
    datasets: [
      {
        data: [],
        label: 'noise',
        pointStyle: 'line',
        pointRadius: 3,
        backgroundColor: this.defaultColor,
        borderColor: this.defaultColor,
        borderWidth: 1,
        showLine: true,
      },
      {
        data: [],
        label: 'y1',
        pointStyle: 'line',
        pointRadius: 3,
        backgroundColor: this.defaultColor,
        borderColor: this.defaultColor,
        borderWidth: 1,
        showLine: true,
      },
      {
        data: [],
        label: 'y2',
        pointStyle: 'line',
        pointRadius: 3,
        backgroundColor: this.defaultColor,
        borderColor: this.defaultColor,
        borderWidth: 1,
        showLine: true,
      },
      {
        data: [],
        label: 'b1',
        pointStyle: 'line',
        pointRadius: 3,
        backgroundColor: this.defaultColor,
        borderColor: this.defaultColor,
        borderWidth: 1,
        showLine: true,
      },
      {
        data: [],
        label: 'b2',
        pointStyle: 'line',
        pointRadius: 3,
        backgroundColor: this.defaultColor,
        borderColor: this.defaultColor,
        borderWidth: 1,
        showLine: true,
      },
    ],
  };

  // Remove fixed min/max so that we can update them dynamically.
  public scatterChartOptions: ChartConfiguration['options'] = {
    animation: false,
    scales: {
      x: {
        title: {
          display: true,
          text: 'm/z',
        },
        grid: {
          color: 'rgba(0, 0, 0, 0)',
        },
      },
      y: {
        title: {
          display: true,
          text: 'Relative Abundance',
        },
      },
    },
    plugins: {
      legend: {
        display: false,
      },
    },
  };

  public requestingSpectrum = true;

  constructor(public mpaTableDataService: MpaTableDataService) {}

  ngOnInit() {
    this.mpaTableDataService.detailViewDataChange.subscribe((val) => {
      this.requestingSpectrum = true;
      this.mpaTableDataService.requestSpectrum();
    });
    this.mpaTableDataService.spectrumDataObject$.subscribe((spectrum: Spectrum) => {
      const specObj = new SpectrumDataObject(spectrum);
      this.handleSpectrumServiceData(specObj);
      this.requestingSpectrum = false;
    });
  }

  handleSpectrumServiceData(dataObject: SpectrumDataObject) {
    this.scatterChartData.datasets[0].data = this.prepareData(dataObject.rest);
    this.scatterChartData.datasets[1].data = this.prepareData(dataObject.ySingle);
    this.scatterChartData.datasets[2].data = this.prepareData(dataObject.yDouble);
    this.scatterChartData.datasets[3].data = this.prepareData(dataObject.bSingle);
    this.scatterChartData.datasets[4].data = this.prepareData(dataObject.bDouble);
    this.updateAxisRange();
    this.chart?.update();
  }

  prepareData(localRawData: DataPoint[]) {
    const preparedData: DataPoint[] = [];
    if (localRawData.length > 0) {
      // Optionally, if you want the line to start at 0, you can add:
      if (localRawData[0].x !== 0) {
        preparedData.push({ x: 0, y: 0 });
      }

      for (let i in localRawData) {
        preparedData.push({
          x: localRawData[i].x - 0.01,
          y: 0,
        });
        preparedData.push({
          x: localRawData[i].x,
          y: localRawData[i].y,
        });
        preparedData.push({
          x: localRawData[i].x + 0.01,
          y: 0,
        });
      }
      // Optionally, end at the last data point's x value
      preparedData.push({ x: localRawData[localRawData.length - 1].x, y: 0 });
    }
    return preparedData;
  }

  updateAxisRange(): void {
    let allData: DataPoint[] = [];
    this.scatterChartData.datasets.forEach(dataset => {
      if (dataset.data && dataset.data.length > 0) {
        allData = allData.concat(dataset.data as DataPoint[]);
      }
    });
    if (allData.length > 0) {
      const xValues = allData.map(dp => dp.x);
      const yValues = allData.map(dp => dp.y);
      const minX = Math.min(...xValues);
      const maxX = Math.max(...xValues);
      const minY = Math.min(...yValues);
      const maxY = Math.max(...yValues);
      // Add a 5% padding to each range.
      const xPadding = (maxX - minX) * 0.05;
      const yPadding = (maxY - minY) * 0.05;
      this.scatterChartOptions.scales.x.min = minX - xPadding;
      this.scatterChartOptions.scales.x.max = maxX + xPadding;
      this.scatterChartOptions.scales.y.min = minY - yPadding;
      this.scatterChartOptions.scales.y.max = maxY + yPadding;
      console.log(`updateAxisRange: x: ${minX - xPadding} to ${maxX + xPadding}, y: ${minY - yPadding} to ${maxY + yPadding}`);
    }
  }

  toggleY1() {
    this.showY1 = !this.showY1;
    this.scatterChartData.datasets[1].borderColor =
      this.showY1 ? this.y1Color : this.defaultColor;
    this.scatterChartData.datasets[1].backgroundColor =
      this.showY1 ? this.y1Color : this.defaultColor;
    this.chart?.update();
  }

  toggleY2() {
    this.showY2 = !this.showY2;
    this.scatterChartData.datasets[2].borderColor =
      this.showY2 ? this.y2Color : this.defaultColor;
    this.scatterChartData.datasets[2].backgroundColor =
      this.showY2 ? this.y2Color : this.defaultColor;
    this.chart?.update();
  }

  toggleB1() {
    this.showB1 = !this.showB1;
    this.scatterChartData.datasets[3].borderColor =
      this.showB1 ? this.b1Color : this.defaultColor;
    this.scatterChartData.datasets[3].backgroundColor =
      this.showB1 ? this.b1Color : this.defaultColor;
    this.chart?.update();
  }

  toggleB2() {
    this.showB2 = !this.showB2;
    this.scatterChartData.datasets[4].borderColor =
      this.showB2 ? this.b2Color : this.defaultColor;
    this.scatterChartData.datasets[4].backgroundColor =
      this.showB2 ? this.b2Color : this.defaultColor;
    this.chart?.update();
  }

  resetChart() {
    this.scatterChartData.datasets[1].borderColor = this.defaultColor;
    this.scatterChartData.datasets[1].backgroundColor = this.defaultColor;
    this.scatterChartData.datasets[2].borderColor = this.defaultColor;
    this.scatterChartData.datasets[2].backgroundColor = this.defaultColor;
    this.scatterChartData.datasets[3].borderColor = this.defaultColor;
    this.scatterChartData.datasets[3].backgroundColor = this.defaultColor;
    this.scatterChartData.datasets[4].borderColor = this.defaultColor;
    this.scatterChartData.datasets[4].backgroundColor = this.defaultColor;
    this.showY1 = false;
    this.showY2 = false;
    this.showB1 = false;
    this.showB2 = false;
    this.chart?.update();
  }
}
