import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterViewInit,
  Component,
  Input, OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { ProteinSubgroup } from '../../model/experiment-data-json';
import { GroupSelection } from '../group-table/group-table.component';

@Component({
  selector: 'app-subgroup-table',
  templateUrl: './subgroup-table.component.html',
  styleUrls: ['./subgroup-table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ visibility: 'hidden' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ),
    ]),
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)'),
      ),
    ]),
  ],
})
export class SubgroupTableComponent implements OnInit, AfterViewInit, OnDestroy {
  // columns and data source
  displayedColumns = [];
  quantificationColumns = [];
  subgroupDataSource: MatTableDataSource<ProteinSubgroup>;
  index2experimentIdMap: Map<number, string> = new Map();

  // sorting and filtering
  filterString: string;

  // highlighting, selection and detail view
  highlightedSubgroup: ProteinSubgroup = null;
  showDetails: boolean = false;
  GroupSelection = GroupSelection;
  selectedAll: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // can be removed?
  @Input() experimentUUID: string;

  constructor(public mpaTableDataService: MpaTableDataService) {
    this.subgroupDataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.filterString = '';
    const proteinSubGroupData: ProteinSubgroup[] =
      this.mpaTableDataService.getProteinSubGroupData();
    // Dynamically adjust number of quantification columns (relevance: comparison)
    if (this.quantificationColumns.length == 0) {
      this.displayedColumns = ['proteinGroupId'];
      // const index2experimentIdMap: Map<number, string> = new Map();
      this.index2experimentIdMap = new Map();
      this.mpaTableDataService
        .getExperiment2IndexMap()
        .forEach((value: number, key: string) => {
          this.index2experimentIdMap.set(value, key);
        });
      for (let i = 0; i < this.index2experimentIdMap.size; i++) {
        // quantificationColumns is used in template to generate the right amount of columns and extract the relevant data
        this.quantificationColumns.push({
          expID: this.index2experimentIdMap.get(i),
          index: i,
        });
        this.displayedColumns.push(this.index2experimentIdMap.get(i));
      }
      this.displayedColumns.push(
        'representativeAccession',
        'representativeDescription',
        'checkbox',
      );
    }
    this.subgroupDataSource.data = proteinSubGroupData;
    this.applyFilter(); // re-use filterString if groupSelection changes
    this.mpaTableDataService.detailViewDataChange.subscribe(() => {
      this.subgroupDataSource.data = proteinSubGroupData;
      this.applyFilter(); // re-use filterString if groupSelection changes
    });
  }

  ngOnDestroy(){
    this.highlightedSubgroup.isHighlighted = false;
  }

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngAfterViewInit() {
    this.subgroupDataSource.paginator = this.paginator;
  }

  sortTableData(sort: Sort) {
    // check if sorting should be reset to proteinGroupID
    if (sort.direction == '') {
      sort.active = 'proteinGroupId';
      sort.direction = 'asc';
      // update ui
      this.sort.active = sort.active;
      this.sort.direction = sort.direction;
      this.sort.sortChange.emit(this.sort);
    }
    // if not, sort whole table
    let data = this.subgroupDataSource.data.slice();
    const isAsc = sort.direction === 'asc';
    data.sort((a, b) => {
      switch (sort.active) {
        case 'representativeAccession':
          return (
            a.representativeAccession.accession.localeCompare(b.representativeAccession.accession) *
            (isAsc ? 1 : -1)
          );
        case 'representativeDescription':
          return (
            a.representativeDescription.description.localeCompare(
              b.representativeDescription.description,
            ) * (isAsc ? 1 : -1)
          );
        // TODO handle multiple quantification columns
        case this.index2experimentIdMap.get(
          this.quantificationColumns[0].index,
        ):
          return (
            (a.quantifications[0] - b.quantifications[0]) * (isAsc ? 1 : -1)
          );
        case 'proteinGroupId':
          let aFront = parseInt(a.proteinSubgroupId.split('_')[0]);
          let aBack = parseInt(a.proteinSubgroupId.split('_')[1]);
          let bFront = parseInt(b.proteinSubgroupId.split('_')[0]);
          let bBack = parseInt(b.proteinSubgroupId.split('_')[1]);
          if (aFront != bFront) {
            return (aFront - bFront) * (isAsc ? 1 : -1);
          } else {
            return (aBack - bBack) * (isAsc ? 1 : -1);
          }
      }
    });
    //after sorting, split into displayed and hidden arrays, append to each other
    let displayedEntries = [];
    let hiddenEntries = [];
    data.map((entry) =>
      entry.isDisplayed
        ? displayedEntries.push(entry)
        : hiddenEntries.push(entry),
    );
    this.subgroupDataSource.data = [...displayedEntries, ...hiddenEntries];
  }

  applyFilter(): void {
    if (this.filterString.length > 0) {
      let regExp = new RegExp(this.filterString.toLowerCase(), 'i');
      let newData = this.mpaTableDataService.getProteinSubGroupData();
      newData = newData.filter((obj) => {
        return (
          regExp.test(obj.proteinSubgroupId.toLowerCase()) ||
          regExp.test(obj.representativeAccession.accession.toLowerCase()) ||
          regExp.test(obj.representativeDescription.description.toLowerCase())
        );
      });
      this.subgroupDataSource.data = newData;
    } else {
      this.subgroupDataSource.data =
        this.mpaTableDataService.getProteinSubGroupData();
    }
    //TODO does expandedElement break this?
    if (this.sort != undefined) {
      this.sortTableData({
        active: this.sort.active.slice(),
        direction: this.sort.direction,
      });
    } else {
      this.sortTableData({ active: 'proteinGroupId', direction: 'asc' });
    }
  }

  onClick(row: ProteinSubgroup): void {
    console.log(row);
    if (this.highlightedSubgroup !== null && this.highlightedSubgroup !== row) {
      this.highlightedSubgroup.isHighlighted = false;
      this.highlightedSubgroup = null;
    }
    if (row.isDisplayed) {
      row.isHighlighted = !row.isHighlighted;
      this.highlightedSubgroup = row;
      this.mpaTableDataService.selectedGroupId = row.proteinSubgroupId;
      this.mpaTableDataService.detailViewDataChange.next(true);
    }
    console.log(row);
  }

  onSelectAllGroups(): void {
    this.subgroupDataSource.data.map((group: ProteinSubgroup) => {
      group.isSelected = this.selectedAll;
    });
  }

  disableSelectedGroups(): void {
    this.subgroupDataSource.data.forEach((subgroup: ProteinSubgroup) => {
      if (subgroup.isSelected) {
        if (subgroup.isHighlighted) {
          this.highlightedSubgroup = null;
          subgroup.isHighlighted = false;
        }
        // subgroup.isDisplayed = false;
        subgroup.isSelected = false;
        this.mpaTableDataService.changeAndPropagateSubgroupVisibility(
          subgroup.proteinSubgroupId,
          false,
        );
      }
    });
    if (this.sort != undefined) {
      this.sortTableData({
        active: this.sort.active.slice(),
        direction: this.sort.direction,
      });
    } else {
      this.sortTableData({ active: 'proteinGroupId', direction: 'asc' });
    }
  }

  enableSelectedGroups(): void {
    this.subgroupDataSource.data.forEach((subgroup: ProteinSubgroup) => {
      if (subgroup.isSelected) {
        if (subgroup.isHighlighted) {
          this.highlightedSubgroup = null;
          subgroup.isHighlighted = false;
        }
        // subgroup.isDisplayed = true;
        subgroup.isSelected = false;
        this.mpaTableDataService.changeAndPropagateSubgroupVisibility(
          subgroup.proteinSubgroupId,
          true,
        );
      }
    });
    if (this.sort != undefined) {
      this.sortTableData({
        active: this.sort.active.slice(),
        direction: this.sort.direction,
      });
    } else {
      this.sortTableData({ active: 'proteinGroupId', direction: 'asc' });
    }
  }

  downloadSubgroupProteinTable(): void {
    const allRows = this.subgroupDataSource.data;
    // Filter for visible rows
    const visibleRows = allRows.filter((row) => row.isDisplayed);
    console.log(`Export: Found ${visibleRows.length} visible rows.`);
    if (visibleRows.length === 0) {
      console.warn('Export: No visible rows to export.');
      return;
    }
    // Build the CSV header
    const headers: string[] = [
      'Protein Subgroup ID',
      'Quantification',
      'Accession',
      'Description',
    ];
    let csvContent = headers.join(',') + '\n';
    visibleRows.forEach((row) => {
      const rowData: string[] = [];
      rowData.push(`"${row.proteinSubgroupId}"`);
      if (this.quantificationColumns && this.quantificationColumns.length > 0) {
        const quantValues = this.quantificationColumns.map(
          (col) => row.quantifications[col.index],
        );
        rowData.push(`"${quantValues.join(';')}"`);
      } else {
        rowData.push('""');
      }

      rowData.push(`"${row.representativeAccession}"`);
      rowData.push(`"${row.representativeDescription}"`);

      csvContent += rowData.join(',') + '\n';
    });
    console.log('Export: CSV Content\n', csvContent);
    const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'protein_subgroup_table.csv';
    a.style.visibility = 'hidden';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  }

  downloadProteinGroupDetails(): void {
    //TODO wait for decision on batching and simplification of getProteinGroups()
  }

  downloadSelectedGroups(): void {
    //TODO wait for decision on batching and simplification of getProteinGroups()
  }
}
