import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyHierarchicalViewComponent } from './taxonomy-hierarchical-view.component';

describe('TaxonomyHierarchicalViewComponent', () => {
  let component: TaxonomyHierarchicalViewComponent;
  let fixture: ComponentFixture<TaxonomyHierarchicalViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TaxonomyHierarchicalViewComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaxonomyHierarchicalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
