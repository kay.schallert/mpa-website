import { Component, OnInit } from '@angular/core';
import { Taxonomy } from '../../model/experiment-data-json';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ArrayDataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-taxonomy-hierarchical-view',
  templateUrl: './taxonomy-hierarchical-view.component.html',
  styleUrls: ['./taxonomy-hierarchical-view.component.scss'],
})
export class TaxonomyHierarchicalViewComponent implements OnInit {

  treeControl: FlatTreeControl<Taxonomy> = new FlatTreeControl<Taxonomy>(
    (node) => node.level,
    (node) => node.expandable,
  )
  flatTree: Taxonomy[] = [];
  dataSource: ArrayDataSource<Taxonomy>;
  expIds: string[] = [];
  
  // ui
  highlightedNode: Taxonomy = null;
  filterString: string;
  filterThreshhold: number = 2;
  selectedAll: boolean = false;

  constructor(public mpaTableDataService: MpaTableDataService) { }

  ngOnInit(): void {
    this.mpaTableDataService.getExperiment2IndexMap().forEach((value, key) => {
      this.expIds.push(key.toString());
    });
    this.setHierarchicalData();
  }

  private setHierarchicalData(isReset: boolean = false): void {
    const taxRoot: Taxonomy = this.mpaTableDataService.getTaxonomyTree();
    const flatTree: Taxonomy[] = this.mpaTableDataService.flattenTaxonomy(taxRoot, 0, isReset);
    this.flatTree = flatTree;
    this.dataSource = new ArrayDataSource(this.flatTree);
    this.treeControl.dataNodes = this.flatTree;
  }

  hasChild = (_: number, node: Taxonomy) => node.expandable;

  getParentNode(node: Taxonomy): Taxonomy {
    const nodes = this.treeControl.dataNodes;
    const nodeIndex = nodes.indexOf(node);
    for (let i = nodeIndex - 1; i >= 0; i--) {
      if (nodes[i].level === node.level - 1) {
        return nodes[i];
      }
    }
    return null;
  }

  isHighlightedNode(node: Taxonomy): boolean {
    if (this.highlightedNode) {
      return this.highlightedNode.taxId == node.taxId;
    }
    return false;
  }

  clickNode(node: Taxonomy): void {
    this.highlightedNode = node;
    console.log(node);
  }

  // separate dblClick-method to prevent problems with checkbox-interactions
  dblClickNode(node: Taxonomy): void {
    this.highlightedNode = node;
    if (node.expandable) {
      node.isExpanded = !node.isExpanded;
    }
  }

  shouldRenderNode(node: Taxonomy): boolean {
    if (!node.isDisplayed) {
      return false;
    }
    // get parents recursively, check if they are expanded. If they aren't, this child/leafNode shouldn't get displayed, else check the next node one level up
    // TODO should we render the first node in a sub-branch which is set to not display? > greyed out, non-interactable, but visualizes "here's some data you hid" ? (Ludwig)
    let parent: Taxonomy = this.getParentNode(node);
    while (parent) {
      if (!parent.isExpanded || !parent.isDisplayed) {
        return false;
      }
      parent = this.getParentNode(parent);
    }
    return true;
  }

  expandAll(): void {
    this.flatTree.map(node => node.expandable ? node.isExpanded = true : {});
  }

  collapseAll(): void {
    this.flatTree.map(node => node.expandable ? node.isExpanded = false : {});
  }

  onSelectAllEntries(): void {
    this.treeControl.dataNodes.forEach((taxNode: Taxonomy) => {
      taxNode.isSelected = this.selectedAll;
    })
  }

  // TODO see shouldRender(), maybe this is not needed here...
  enableSelectedEntries(): void {
    // create and populate set of taxIds to prevent going over nodes multiple times
    const nodeIdSet: Set<string> = new Set<string>();
    // select single explicitly clicked nodes
    this.treeControl.dataNodes.forEach((taxNode: Taxonomy) => {
      if (taxNode.isSelected) {
        taxNode.isSelected = false;
        nodeIdSet.add(taxNode.taxId);
        if (this.isHighlightedNode(taxNode)) {
          this.highlightedNode == null;
        }
        // add children of explicitly selected nodes 
        this.treeControl.getDescendants(taxNode).forEach((descendant: Taxonomy) => {
          descendant.isSelected = false;
          if (this.isHighlightedNode(descendant)) {
            this.highlightedNode == null;
          }
          nodeIdSet.add(descendant.taxId);
        });
      }
    });
    nodeIdSet.forEach((taxId: string) => this.mpaTableDataService.changeAndPropagateTaxonomyVisibility(taxId, true));
    this.applyFilter();
  }

  disableSelectedEntries(): void {
    // create and populate set of taxIds to prevent going over nodes multiple times
    const nodeIdSet: Set<string> = new Set<string>();
    // select single explicitly clicked nodes
    this.treeControl.dataNodes.forEach((taxNode: Taxonomy) => {
      if (taxNode.isSelected) {
        taxNode.isSelected = false;
        nodeIdSet.add(taxNode.taxId);
        // add children of explicitly selected nodes 
        this.treeControl.getDescendants(taxNode).forEach((descendant: Taxonomy) => {
          descendant.isSelected = false;
          nodeIdSet.add(descendant.taxId);
        });
      }
    });
    nodeIdSet.forEach((taxId: string) => this.mpaTableDataService.changeAndPropagateTaxonomyVisibility(taxId, false));
    //TODO re-filter data to update the view (does this work as intended?)
    this.applyFilter();
  }

  applyFilter(): void {
    // Filter out any undefined nodes
    let nodes = (this.treeControl.dataNodes || []).filter(n => n != null);
    let regExp = new RegExp(this.filterString, 'i');

    if (this.filterString.length > this.filterThreshhold) {
      // HIERARCHICAL DATA
      for (let i = 0; i < nodes.length; i++) {
        let node = nodes[i];
        if (!node) continue; // safeguard
        if (!node.isDisplayed) continue; // ensure hidden data does not get evaluated

        // this change only affects this component, should not interfere with global isDisplayedValues
        node.isDisplayed = false;
        node.isExpanded = false;

        // Combine scientific name with other names
        let allNames = [node.scientificname, ...node.othernames];
        for (let j = 0; j < allNames.length; j++) {
          if (regExp.test(allNames[j])) {
            node.isDisplayed = true;
            node.isExpanded = false;

            // Custom "getParent" logic
            let ancestorLevel = node.level - 1;
            let ancestorIndex = i - 1;
            while (ancestorLevel >= 0 && ancestorIndex >= 0) {
              if (nodes[ancestorIndex].level === ancestorLevel) {
                nodes[ancestorIndex].isDisplayed = true;
                nodes[ancestorIndex].isExpanded = true;
                ancestorLevel--;
              }
              ancestorIndex--;
            }

            // Get descendants and mark them as displayed
            const descendants = this.treeControl.getDescendants(node);
            descendants.forEach(desc => {
              desc.isDisplayed = true;
            });
            // Skip over the descendant nodes in the loop
            i += descendants.length;
            break;
          }
        }
      }
    } else {
      // If the filter string is too short, reset the treeData but keep current expansion properties 
      this.setHierarchicalData(true);
    }
  }

getMaxExpandedLevel(): number {
  const visibleNodes = this.flatTree.filter(n => this.shouldRenderNode(n));
  if (visibleNodes.length === 0) { 
    return 1;
  }
  return Math.max(...visibleNodes.map(n => n.level));
}
calculateEffectiveIndent(node: Taxonomy): number {
  const baseIndent = 60;
  const reductionFactor = 0.8;  
  const maxExpandedLevel = this.getMaxExpandedLevel();
  const effectivePerLevel = baseIndent * Math.pow(reductionFactor, maxExpandedLevel - 1);
  return node.level * effectivePerLevel;
}

}
