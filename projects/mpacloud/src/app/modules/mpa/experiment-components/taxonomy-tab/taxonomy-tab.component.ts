import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component } from '@angular/core';

export enum TaxonomyDisplayMode {
  HIERARCHICAL = 'hierarchical',
  FLAT = 'flat'
}

@Component({
  selector: 'app-taxonomy-tab',
  templateUrl: './taxonomy-tab.component.html',
  styleUrls: ['./taxonomy-tab.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ visibility: 'hidden', height: 0, opacity: 0 })),
      state('expanded', style({ height: '*', opacity: 1 })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ])
  ]
})
export class TaxonomyTabComponent {

  public taxonomyDisplayMode: TaxonomyDisplayMode = TaxonomyDisplayMode.FLAT;
  DisplayMode = TaxonomyDisplayMode;

}
