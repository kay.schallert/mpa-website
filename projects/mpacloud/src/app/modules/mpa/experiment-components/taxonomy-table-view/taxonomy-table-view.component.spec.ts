import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxonomyTableViewComponent } from './taxonomy-table-view.component';

describe('TaxonomyTableViewComponent', () => {
  let component: TaxonomyTableViewComponent;
  let fixture: ComponentFixture<TaxonomyTableViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TaxonomyTableViewComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaxonomyTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
