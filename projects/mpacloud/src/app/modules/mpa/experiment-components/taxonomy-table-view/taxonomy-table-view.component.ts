import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { MatPaginator } from '@angular/material/paginator';
import { Taxonomy } from '../../model/experiment-data-json';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';

@Component({
  selector: 'app-taxonomy-table-view',
  templateUrl: './taxonomy-table-view.component.html',
  styleUrls: ['./taxonomy-table-view.component.scss'],
})
export class TaxonomyTableViewComponent implements OnInit, AfterViewInit {

  tableData: Taxonomy[] = [];
  tableDataSource: MatTableDataSource<Taxonomy>;
  highlightedRow: Taxonomy = null;
  selectedAll: boolean = false;
  allRanks: string[] = [
    'superkingdom',
    'phylum',
    'class',
    'order',
    'family',
    'genus',
    'species',
    'strain'
  ]
  tableColumns: string[] = this.allRanks.slice();
  expIds: string[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  maxRankSelection: string = this.allRanks[this.allRanks.length - 1];

  constructor(public mpaTableDataService: MpaTableDataService) {
    this.tableDataSource = new MatTableDataSource([]);
  }

  ngOnInit(): void {
    // add headers for each experiment > show quantification per experiment
    // TODO get correct names for experiments
    this.mpaTableDataService.getExperiment2IndexMap().forEach((value, key) => {
      this.tableColumns.push(key.toString());
      this.expIds.push(key.toString());
    });
    this.tableColumns.push('checkbox');

    this.setTableData();
  }

  ngAfterViewInit(): void {
    this.tableDataSource.paginator = this.paginator;
  }

  private setTableData(): void {
    const taxRoot: Taxonomy = this.mpaTableDataService.getTaxonomyTree();
    let taxonomyFlat: Taxonomy[] = [];
    taxonomyFlat = this.mpaTableDataService.flattenTaxonomyAndAddLineage(taxRoot, taxonomyFlat, [], 0);
    this.tableDataSource.data = taxonomyFlat;
    console.log(taxRoot);
  }

  isHighlightedRow(row: Taxonomy): boolean {
    if (!this.highlightedRow) {
      return false;
    } else if (this.highlightedRow.taxId == row.taxId) {
      return true;
    }
    return false;
  }

  clickRow(row: Taxonomy): void {
    this.highlightedRow = row;
  }

  sortTableData(sort: Sort): void {
    if (sort.direction == '') {
      sort.active = 'superkingdom';
      sort.direction = 'asc';
      // update ui
      this.sort.active = sort.active;
      this.sort.direction = sort.direction;
      this.sort.sortChange.emit(this.sort);
    }
    let data = this.tableDataSource.data.slice();
    data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'superkingdom':
          return a.superkingdom.localeCompare(b.superkingdom) * (isAsc ? 1 : -1);
        case 'phylum':
          return a.phylum.localeCompare(b.phylum) * (isAsc ? 1 : -1);
        case 'class':
          return a.class.localeCompare(b.class) * (isAsc ? 1 : -1);
        case 'order':
          return a.order.localeCompare(b.order) * (isAsc ? 1 : -1);
        case 'family':
          return a.family.localeCompare(b.family) * (isAsc ? 1 : -1);
        case 'genus':
          return a.genus.localeCompare(b.family) * (isAsc ? 1 : -1);
        case 'species':
          return a.species.localeCompare(b.species) * (isAsc ? 1 : -1);
        case 'strain':
          return a.strain.localeCompare(b.strain) * (isAsc ? 1 : -1);
        default:
          // NOTE: default should mean one of the quantification columns
          console.log(sort.active);
          let quantIndex: number = this.mpaTableDataService.getExperiment2IndexMap().get(sort.active);
          return (a.quantifications[quantIndex] - b.quantifications[quantIndex]) * (isAsc ? 1 : -1);
      }
    });
    // after sorting, split into displayed and hidden arrays, append
    let displayedEntries = [];
    let hiddenEntries = [];
    data.forEach((tax: Taxonomy) => tax.isDisplayed ? displayedEntries.push(tax) : hiddenEntries.push(tax));
    this.tableDataSource.data = [...displayedEntries, ...hiddenEntries];
  }

  /**
   * Sets displayedColumns to only show ranks up to this.maxRankSelection
   * table gets automatically updated.
   */
  lastRankSelectionChange() {
    const index = this.allRanks.indexOf(this.maxRankSelection);
    let slice = this.allRanks.slice(0, index + 1);
    // re-add quantifications and checkbox
    slice = slice.concat(this.expIds);
    slice.push("checkbox");
    this.tableColumns = slice;

    //TODO consolidate all entries under same rank into one row
  }

  selectAllTableEntries(): void {
    this.tableDataSource.data.map((tax: Taxonomy) => {
      tax.isSelected = this.selectedAll;
    });
  }

  enableSelectedTableEntries(): void {
    this.tableDataSource.data.forEach((tax: Taxonomy) => {
      if (tax.isSelected) {
        tax.isSelected = false;
        this.mpaTableDataService.changeAndPropagateTaxonomyVisibility(tax.taxId, true);
      }
    });
    if (this.sort.active) {
      this.sortTableData({ active: this.sort.active.slice(), direction: this.sort.direction });
    } else {
      this.sortTableData({ active: this.tableColumns[0], direction: '' });
    };
    this.selectedAll = false;
  }

  disableSelectedTableEntries(): void {
    this.tableDataSource.data.forEach((taxEntry: Taxonomy) => {
      if (taxEntry.isSelected) {
        // De-select row if it was currently clicked and highlighted
        if (this.isHighlightedRow(taxEntry)) {
          this.highlightedRow == null;
        }
        taxEntry.isSelected = false;
        this.mpaTableDataService.changeAndPropagateTaxonomyVisibility(taxEntry.taxId, false);
      }
    });
    if (this.sort.active) {
      this.sortTableData({ active: this.sort.active.slice(), direction: this.sort.direction });
    } else {
      this.sortTableData({ active: this.tableColumns[0], direction: '' });
    };
    this.selectedAll = false;
  }

}
