export class DiaSearchParameters {
  ionToleracnce: number = 0.2;
  intensityThreshold: number = 200;

  checkToleranceInput(
    toleranceInput: number,
  ) {
    var adjustedInput: number;
    if (toleranceInput < 0.001) {
      adjustedInput = 0.001;
    } else if (toleranceInput > 1) {
      adjustedInput = 1;
    } else if (toleranceInput == undefined) {
      adjustedInput = 0.001;
    } else {
      adjustedInput = parseFloat(toleranceInput.toPrecision(5));
    }
    this.ionToleracnce = adjustedInput;
  }
}
