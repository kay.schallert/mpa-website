// enums
export enum NCBIRanks {
    NO_RANK = 'no rank',
    SUPERKINGDOM = 'superkingdom',
    PHYLUM = 'phylum',
    CLASS = 'class',
    ORDER = 'order',
    FAMILY = 'family',
    GENUS = 'genus',
    SPECIES = 'species',
    STRAIN = 'strain',
  }

// "wrapper"-object structure received from back-end
export interface ExperimentData {
    proteinData: ProteinData;
    annotationData: AnnotationData;
    experimentIdList: string[];
}

export interface ProteinData {
    proteinGroups: ProteinMainGroup[];
    // frontProteinGroups?: ProteinGroup[]; // TEMPORARY FIX > see table data service requestProteinGroups()
    proteins: Protein[];
    psms: PSM[];
}

export interface ProteinMainGroup {
    // back-end data
    proteinGroupId: string;
    representativeAccession: ProteinAccession;
    representativeDescription: ProteinDescription;
    subgroups: ProteinSubgroup[];
    quantifications?: number[];
    isDisplayed?: boolean;
    // front-end data
    isSelected?: boolean;
    isHighlighted?: boolean;
}

export interface ProteinSubgroup {
    // back-end data
    proteinSubgroupId: string;
    representativeAccession: ProteinAccession;
    representativeDescription: ProteinDescription;
    proteinIds: string[];
    quantifications?: number[];
    // front-end data
    isSelected?: boolean;
    isDisplayed?: boolean;
    isHighlighted?: boolean;
}

// temporary fix until we think of a better solution... > merge all groups into this interface during retrieval from back-end
export interface Protein {
    proteinId: string;
    representativeAccession: ProteinAccession;
    representativeDescription: ProteinDescription;
    taxId: string;
    kwIds: string[];
    koIds: string[];
    ecIds: string[];
    accessions: ProteinAccession[];
    descriptions: ProteinDescription[];
    peptideIds: string[];
    quantifications?: number[];
    // front-end data
    isSelected?: boolean;
    isDisplayed?: boolean;
    isHighlighted?: boolean;
}

export interface ProteinAccession {
  accession: string;
  proteinAccessionType: string;
}

export interface ProteinDescription {
  description: string;
  proteinDescriptionType: string;
}

export interface ProteinSequence {
  proteinID: string;
  sequence: string;
}
export interface PSM {
    psmId: string;
    peptideId: string;
    spectrumId: string;
    qvalue: number;
    spectrumQuant: number;
    expId: string;
    // TODO: missing
    searchEngine?: string;
    // ui only, this should be removed later
    isSelected?: boolean;
}

export interface Peptide {
    sequenceId: string;
    psmIds: string[];
    quantifications?: number[];
    // front-end data
    isSelected?: boolean;
    isDisplayed?: boolean;
    isHighlighted?: boolean;
}


// Maybe send the half-empty spectrum-Object to the server and receive the filled one
export interface Spectrum {
  spectrumId: string;
  spectrumQuant: number;
  psmIds: string[];
  spectrumString?: string;
  peptideSequence?: string;
  peakArray?;
  massToChargeArray?: number[];
  intensityArray?: number[];
  // front-end data
  // isSelected?: boolean;
  isDisplayed?: boolean;
  isHighlighted?: boolean;

}

export interface AnnotationData {
    // back-end data
    taxRoot: Taxonomy;
    kwRoot: Keyword;
    kos: KO[];
    ecs: EC[];
    // front-end data
    taxFlat?: Taxonomy[];
    kwFlat?: Keyword[];
}

// generic name, as GTDBTax could potentially be applied using the same structure
export interface Taxonomy {
    // back-end data
    taxId: string;
    children: Taxonomy[];
    rank: string;
    scientificname: string;
    othernames: string[];
    proteinIds: string[];
    // front-end data
    level?: number;
    expandable?: boolean;
    isExpanded?: boolean;
    isDisplayed?: boolean;
    isSelected?: boolean;
    quantifications?: number[];

    superkingdom?: string;
    phylum?: string;
    class?: string;
    order?: string;
    family?: string;
    genus?: string;
    species?: string;
    strain?: string;
}

export class TaxonomyTableObject {
    // front-end data
    superkingdom: string = '';
    phylum: string = '';
    class: string = '';
    order: string = '';
    family: string = '';
    genus: string = '';
    species: string = '';
    strain: string = '';
    proteinIds: string[] = [];
    quantifications: number[];
    isDisplayed?: boolean;
    level?: number;
    expandable?: boolean;
    scientificname?: string;
    othernames?: string[];
    isExpanded?: boolean;
  }

export interface Keyword {
    // back-end data
    kwId: string;
    kwAccession: string;
    kwCategory: string;
    children: Keyword[];
    proteinIds: string[];
    quantifications?: number[];
    isDisplayed?: boolean;
    // front-end data
    level?: number;
    expandable?: boolean;
    isExpanded?: boolean;
}

export interface KO {
    // back-end data
    koId: string;
    koNames: string[];
    koSymbols: string[];
    proteinIds: string[];
    quantifications?: number[];

    isDisplayed?: boolean;
    isSelected?: boolean;
    isHighlighted?: boolean;
}

export interface EC {
    // back-end data
    ecId: string;
    ecNames: string[];
    ecSymbols: string[];
    proteinIds: string[];
    quantifications?: number[];

    isDisplayed?: boolean;
    isSelected?: boolean;
    isHighlighted?: boolean;
}

// export interface ProteinFunction {
//     // transformed back-end data
//     id: string;
//     names: string[];
//     symbols: string[];
//     proteinIds: string[];
//     type: string;
//     quantifications?: number[];
//     isDisplayed?: boolean;
// }



