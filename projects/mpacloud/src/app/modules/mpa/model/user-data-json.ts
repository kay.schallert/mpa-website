export enum NodeType {
  USER = "USER",
  PROTEINDB = "PROTEINDB",
  FOLDER = "FOLDER",
  EXPERIMENT = "EXPERIMENT",
  COMPARISON = "COMPARISON",
}

export interface UserDataTreeObject {

  // tree related
  id: number;
	parent: number;
  children: UserDataTreeObject[];

    // ui related
	depth: number;
	icon: string;
	displayName: string;
	expanded: boolean;

  // data
  type: NodeType;
  description: string;
  comments: string[];
  creationDate: string;
  // child object
  experiment?: ExperimentObject;
  comparison?: ComparisonObject;
  proteinDB?: ProteinDBObject;

  userId: string;

}

export interface ExperimentObject {

  experimentid: string;
	userid: string;
	status: ExperimentStatus;
	creationdate: string;
  uploadType: string; // TODO ANZEIGEN NUR BEI HAS DATA
	searches: Search[]; // TODO ANZEIGEN SPEZIELL
  experimentFiles: ExperimentFiles; // TODO ANZEIGEN SPEZIELL
  targetFDR: number;
  proteinGroupJsonFileId: string;

}

export enum MpaFileType {
    MGF = 'MGF',
    MZML = 'MZML',
    DAT = 'DAT',
    MASCOT_FASTA = 'MASCOT_FASTA',
    MZID = "MZID",
}

export enum SearchStatus {
  SEARCH_REQUESTED = 'SEARCH_REQUESTED',
  SEARCH_QUEUED = 'SEARCH_QUEUED',
  SEARCH = 'SEARCH',
  SEARCH_FAILED = 'SEARCH_FAILED',
  SEARCH_FINISHED = 'SEARCH_FINISHED',
}


export enum ProteinDbStatus {
  REQUEST_CREATION = 'REQUEST_CREATION',
  CREATED = 'CREATED',
  WAITING_FOR_UPLOAD = 'WAITING_FOR_UPLOAD',
  FILES_UPLOADED = 'FILES_UPLOADED',
  PROCESSING = 'PROCESSING',
  AVAILABLE_AND_LOCKED = 'AVAILABLE_AND_LOCKED',
  DELETE = 'DELETE',
  DELETED = 'DELETED',
  ERROR_STATE = 'ERROR_STATE',
}

export enum ComparisonStatus {
  REQUEST_CREATION = 'REQUEST_CREATION',
  CREATED = 'CREATED',
  CHOOSE_EXPERIMENTS = 'CHOOSE_EXPERIMENTS',
  RESULT_PROCESSING = 'RESULT_PROCESSING',
  RESULTS_AVAILABLE = 'RESULTS_AVAILABLE',
  RESULTS_AVAILABLE_AND_LOCKED = 'RESULTS_AVAILABLE_AND_LOCKED',
  DELETE = 'DELETE',
  DELETED = 'DELETED',
  ERROR_STATE = 'ERROR_STATE',
}

export enum ExperimentStatus {
  REQUEST_CREATION = 'REQUEST_CREATION',
  REQUEST_CREATION_BATCH = 'REQUEST_CREATION_BATCH',
  CREATED = 'CREATED',
  WAITING_FOR_UPLOAD = 'WAITING_FOR_UPLOAD',
  FILES_UPLOADED = 'FILES_UPLOADED',
  PEAKLIST_PROCESSING = 'PEAKLIST_PROCESSING',
  SEARCH_QUEUED = 'SEARCH_QUEUED',
  SEARCH = 'SEARCH',
  RESULT_PROCESSING = 'RESULT_PROCESSING',
  RESULTS_AVAILABLE = 'RESULTS_AVAILABLE',
  RESULTS_AVAILABLE_AND_LOCKED = 'RESULTS_AVAILABLE_AND_LOCKED',
  DELETE = 'DELETE',
  DELETED = 'DELETED',
  ERROR_STATE = 'ERROR_STATE',
}
export interface XtandemRescoreSearchParameters {
  fragemntIonTolerance: string;
  precursorIonTolerance: string;
  modificationCarbamidomethyl: boolean;
  modificationOxidation: boolean;
}

export interface ExperimentFiles {
  uploadedPeakLists: ExperimentFileMetadata[];  // TODO ANZEIGEN als Liste wenn nicht leer, originalFileName: uploadStatus
  mergedPeakListsMgf: string;
  mergedPeakListsMzMl: string;
  mascotFastaFilesForDat: ExperimentFileMetadata[]; // TODO ANZEIGEN als Liste wenn nicht leer, originalFileName: uploadStatus
  mergedMascotFasta: string;
  uploadedSearchResultFiles: ExperimentFileMetadata[]; // TODO ANZEIGEN als Liste wenn nicht leer, originalFileName: uploadStatus
  mergedSearchResultsMzId: string;
}

export interface ExperimentFileMetadata {
  originalFileName: string;
  uploadId: string;
  fileId: string;
  fileType: MpaFileType;
  uploadStatus: string;
}
export interface Search {
  searchid: string;
  experimentid: string;
  userid: string;
  searchQueuedTime: number;
  status: SearchStatus; // TODO ANZEIGEN
  proteinDBId: string; // TODO ANZEIGEN
  searchParameters: SearchParameters;
  peakListDownloadUrl: string;
  proteinDBDownloadUrl: string;
  spectralLibraryId: string;
  spectralLibraryDownloadUrl: string;
  rescoreFileId: string;
  searchAlgorithm: SearchAlgorithm;
}

export enum SearchAlgorithm {
  XTANDEM_RESCORE = 'XTANDEM_RESCORE',
  RUMINATION = 'RUMINATION',
}

export interface SearchParameters {
  ruminationSearchParameters: RuminationSearchParameters;
  xtandemRescoreSearchParameters: XtandemRescoreSearchParameters;
}

export interface RuminationSearchParameters {
  msAccuracy: number;
  intensityThreshold: number;
}

export interface XtandemRescoreSearchParameters {
  fragemntIonTolerance: string;
  fragemntIonToleranceUnit: string;
  precursorIonTolerance: string;
  precursorIonToleranceUnit: string;
  modificationCarbamidomethyl: boolean;
  modificationOxidation: boolean;
}

export interface ComparisonObject {

	comparisonid: string;
	userid: string;
	name: string;
	description: string;
	comments: string[];
	status: ComparisonStatus;
  targetFDR: number;
  dbExperimentId: string;
	creationdate: string ;
	experimentIds: string[];
  proteinGroupJsonFileId: string;

}

export interface ProteinDBObject {

  userID: string;
	protdbId: string;
	status: ProteinDbStatus;
	proteinDBFiles: ProteinDBFileObject[];
	creationdate: string;
	totalProteins: number;
  spectralLibraries: SpectralLibrary[];
}

export interface SpectralLibrary {
  id: string;
  name: string;
  status?: SpectralLibraryStatus;
  ms2pipParams: MS2PipParams;
}

export enum SpectralLibraryStatus {
  WAITING_FOR_DB_PROCESSING = 'WAITING_FOR_DB_PROCESSING',
  WAITING_FOR_SPECTRAL_LIBRARY_PROCESSING = 'WAITING_FOR_SPECTRAL_LIBRARY_PROCESSING',
  PROCESSING = 'PROCESSING',
  AVAILABLE_AND_LOCKED = 'AVAILABLE_AND_LOCKED',
  DELETE = 'DELETE',
  DELETED = 'DELETED',
  ERROR_STATE = 'ERROR_STATE',
}

export interface MS2PipParams {
  minLength: number;
  maxLength: number;
  cleavageRule: string;
  maxVariableModifications: number;
  missedCleavages: number;
  charges: number[];
  modifications: Modification[];
}

export interface Modification {
  label: string;
  aminoAcid: string;
  fixed: boolean;
}



export interface ProteinDBFileObject {
  originalFileName: string;
  uploadId: string;
  fileId: string;
  dbType: string;
  uploadStatus: string;
}
