import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MpaNavigationComponent } from './navigation/mpa-navigation/mpa.navigation.component';

const routes: Routes = [
  { path: '', component: MpaNavigationComponent }, // there is only one route here for the top level component
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MpaRoutingModule { }
