import { NgModule } from '@angular/core';
import { AsyncPipe, CommonModule } from '@angular/common';

import { MpaRoutingModule } from './mpa-routing.module';
import { MpaNavigationComponent } from './navigation/mpa-navigation/mpa.navigation.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { SideNavTreeComponent } from './navigation/side-nav-tree/side-nav-tree.component';
import { SideNavTreeNodeComponent } from './navigation/side-nav-tree-node/side-nav-tree-node.component';
import { MatIconModule } from '@angular/material/icon';
import { FolderComponent } from './navigation-components/folder/folder.component';
import { ExperimentComponent } from './navigation-components/experiment/experiment.component';
import { ProteinDbComponent } from './navigation-components/protein-db/protein-db.component';
import { UserComponent } from './navigation-components/user/user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { A11yModule } from '@angular/cdk/a11y';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { TextfieldDialogComponent } from './shared-components/textfield-dialog/textfield-dialog.component';
import { DescriptionDetailsComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/description-details/description-details.component';
import { CdkDetailRowDirective } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/group-table/cdk-detail-row.directive';
import { GroupTableComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/group-table/group-table.component';
import { PeptideTableComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/peptide-table/peptide-table.component';
import { ProteinRequestComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/protein-request/protein-request.component';
import { ProteineSequenceViewerComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/protein-sequence-viewer/proteine-sequence-viewer.component';
import { ProteinTableComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/protein-table/protein-table.component';
import { ProteingroupDetailViewComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/proteingroup-detail-view/proteingroup-detail-view.component';
import { PsmTableComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/psm-table/psm-table.component';
//import { ResultTableComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/result-table/result-table.component';
import { SpectrumViewerComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/spectrum-viewer/spectrum-viewer.component';
import { FunctionTabComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/function-tab/function-tab.component';
import { KeywordsTabComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/keywords-tab/keywords-tab.component';
import { TaxonomyTabComponent } from 'projects/mpacloud/src/app/modules/mpa/experiment-components/taxonomy-tab/taxonomy-tab.component';
import { FileUploadComponent } from './experiment-components/file-upload/file-upload.component';
import { FileDropzoneComponent } from './shared-components/file-dropzone/file-dropzone.component';
import { ExperimentSelectionComponent } from './experiment-components/experiment-selection/experiment-selection.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BaseChartDirective, provideCharts, withDefaultRegisterables } from 'ng2-charts';
import { ExperimentTableComponent } from './navigation-components/experiment/experiment-table/experiment-table.component';
import {
  ComparisonTableComponent
} from './navigation-components/experiment/comparison-table/comparison-table.component';
import { DeleteDialogComponent } from './navigation-components/experiment/delete-dialog/delete-dialog.component';
import { MainTablesComponent } from './experiment-components/main-tables/main-tables.component';
import { SubgroupTableComponent } from './experiment-components/subgroup-table/subgroup-table.component';
import { SpectralLibraryComponent } from './navigation-components/protein-db/spectral-library/spectral-library.component';
import { TaxonomyHierarchicalViewComponent } from './experiment-components/taxonomy-hierarchical-view/taxonomy-hierarchical-view.component';
import { TaxonomyTableViewComponent } from './experiment-components/taxonomy-table-view/taxonomy-table-view.component';
import { FunctionECComponent } from './experiment-components/function-ec/function-ec.component';
import { FunctionKOComponent } from './experiment-components/function-ko/function-ko.component';

@NgModule({
  declarations: [
    MpaNavigationComponent,
    TextfieldDialogComponent,
    SideNavTreeComponent,
    SideNavTreeNodeComponent,
    FolderComponent,
    ExperimentComponent,
    ProteinDbComponent,
    UserComponent,
    DeleteDialogComponent,

    // TODO: to be moved
    MainTablesComponent,
    GroupTableComponent,
    SubgroupTableComponent,
    PeptideTableComponent,
    CdkDetailRowDirective,
    ProteingroupDetailViewComponent,
    ProteinTableComponent,
    DescriptionDetailsComponent,

    // DatabaseSearchPageComponent,
    // PeaklistPageComponent,
    // SearchResultPageComponent,
    // TextfieldDialogComponent,
    // ProteinDatabaseDialogComponent,
    // ProteinDatabaseComponent,
    PsmTableComponent,
    SpectrumViewerComponent,
    ProteineSequenceViewerComponent,
    TaxonomyTabComponent,
    TaxonomyHierarchicalViewComponent,
    TaxonomyTableViewComponent,
    FunctionTabComponent,
    FunctionECComponent,
    FunctionKOComponent,
    // CompareExperimentsDialogComponentComponent,
    // ExperimentComparisonComponent,
    FileUploadComponent,
    KeywordsTabComponent,
    ProteinRequestComponent,
    FileDropzoneComponent,
    ExperimentSelectionComponent,
    ExperimentTableComponent,
    ComparisonTableComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    DragDropModule,
    ReactiveFormsModule,
    MpaRoutingModule,
    BaseChartDirective,
    // material module last
    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    CdkAccordionModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule,
    MatChipsModule,
    MatFormFieldModule,
    AsyncPipe,
    SpectralLibraryComponent,
    MatDialogModule,
  ],
  providers: [provideCharts(withDefaultRegisterables())],
  exports: [ExperimentTableComponent],
})
export class MpaModule {}
