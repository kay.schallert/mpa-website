import { UserDataTreeObject } from "../model/user-data-json";

export interface ContentComponent {
  dataItemOfThisComponent: UserDataTreeObject;
}
