import { Component, Input } from '@angular/core';
import { ComparisonStatus, UserDataTreeObject } from '../../../model/user-data-json';

interface Datstats {
  totalNoProteinGroups: number;
  totalNoProteins: number;
  totalNoPeptides: number;
  totalNoPsms: number;
  totalNoSpectra: number;
}

@Component({
  selector: 'app-comparison-table',
  templateUrl: './comparison-table.component.html',
  styleUrl: './comparison-table.component.scss',
})
export class ComparisonTableComponent {

  @Input({required: true}) dataItemOfThisComponent: UserDataTreeObject;
  @Input({required: true}) datStats!: Datstats;

  setDescription(e): void {
    console.log(e.target.value)
    this.dataItemOfThisComponent.description = e.target.value;
  }

  protected readonly ComparisonStatus = ComparisonStatus;


  searches = [
    {
      id: '1.0.1',
      date: 'mm/dd/yyyy',
      changelog: 'changes...'
    },
  ];
}
