
import { Component, Input } from '@angular/core';
import {
  ExperimentStatus,
  UserDataTreeObject,
  SearchAlgorithm,
} from '../../../model/user-data-json';
import { EnumToTextService } from '../../../../../enum-to-text.service';

interface Datstats {
  totalNoProteinGroups: number;
  totalNoProteins: number;
  totalNoPeptides: number;
  totalNoPsms: number;
  totalNoSpectra: number;
}

@Component({
  selector: 'app-experiment-table',
  templateUrl: './experiment-table.component.html',
  styleUrl: './experiment-table.component.scss',
})

export class ExperimentTableComponent {

  ViewSelection?: string = "General";

  protected readonly ExperimentStatus = ExperimentStatus;
  protected readonly EnumToTextService = EnumToTextService;

  SearchAlgorithm = SearchAlgorithm;

  constructor(public ETTS: EnumToTextService) {}


  @Input({required: true}) dataItemOfThisComponent: UserDataTreeObject;
  @Input({required: true}) datStats!: Datstats;

  setDescription(e): void {
    this.dataItemOfThisComponent.description = e.target.value;
  }

}
