import { Component, EventEmitter, OnInit } from '@angular/core';
import { ComparisonStatus, ExperimentStatus, NodeType, UserDataTreeObject } from '../../model/user-data-json';
import { ContentComponent } from '../content-component';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';
import { MatDialog } from '@angular/material/dialog';
import { TextfieldDialogComponent } from '../../shared-components/textfield-dialog/textfield-dialog.component';
import { MpaTableDataService } from '../../services/mpa-table-data.service';
import { HttpClientService } from 'shared-lib';
import { Endpoints, MpaWebserverAddressService } from 'projects/mpacloud/src/app/mpa-webserver-address.service';
import { HttpParams } from '@angular/common/http';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { ExperimentData } from '../../model/experiment-data-json';
import { Observable } from 'rxjs';

export class Datstats {
  totalNoProteinGroups: number;
  totalNoProteins: number;
  totalNoPeptides: number;
  totalNoPsms: number;
  totalNoSpectra: number;
}

@Component({
  selector: 'app-experiment',
  templateUrl: './experiment.component.html',
  styleUrls: ['./experiment.component.scss']
})
export class ExperimentComponent  implements OnInit, ContentComponent {


  // Declare enums to make them accessible in the template
  // PGRequestStatus = PGRequestStatus;
  ExperimentStatus = ExperimentStatus;
  ComparisonStatus = ComparisonStatus;
  NodeType = NodeType;

  dataItemOfThisComponent: UserDataTreeObject;
  datStats: Datstats;
  displayNameEditing: string;

  // TODO: check if can be removed:
  selectedPeaklistFile: string;
  selectedSearchFile: string;

  // ui-related
  selectedTab: number = 0;
  // protReqStatus: PGRequestStatus = PGRequestStatus.UNINITIATED;
  hasMpaData: boolean = false;


  constructor(
    private dataService: MPAUserDataNavigationService,
    private dialog: MatDialog,
    private mpaTableDataService: MpaTableDataService,
    private httpClient: HttpClientService,
    private mpaWebserverAddress: MpaWebserverAddressService,
  ) {}

  ngOnInit() {
    // if (this.hasStatusData()) {
    //   switch (this.dataItemOfThisComponent.type) {
    //     case NodeType.EXPERIMENT:
    //       this.mpaTableDataService.expID.next(this.dataItemOfThisComponent.experiment.experimentid);
    //       this.mpaTableDataService.expIDType.next("EXPERIMENT");
    //       break;
    //     case NodeType.COMPARISON:
    //       this.mpaTableDataService.expID.next(this.dataItemOfThisComponent.comparison.comparisonid);
    //       this.mpaTableDataService.expIDType.next("COMPARISON");
    //       break;
    //   }
      this.displayNameEditing = this.dataItemOfThisComponent.displayName;
      // this.mpaTableDataService.proteinGroupRequestStatus.next(PGRequestStatus.UNINITIATED)
      // this.mpaTableDataService.proteinGroupRequestStatus.subscribe((status) => {
      //   this.protReqStatus = status;
      //   if (this.protReqStatus === PGRequestStatus.INITIATED) {
      //     this.selectedTab = 1;
      //   } else if (this.protReqStatus === PGRequestStatus.FULFILLED) {
      //     this.datStats = this.calculateDataStats();
      //   }
      // })
    // }
  }

  public isComparison(): boolean {
    if (this.dataItemOfThisComponent.type === NodeType.EXPERIMENT) {
      return false;
    } else if (this.dataItemOfThisComponent.type === NodeType.COMPARISON) {
      return true;
    }
    return false;
  }

  public hasStatusData(): boolean {
    switch (this.dataItemOfThisComponent.type) {
      case NodeType.EXPERIMENT:
        switch(this.dataItemOfThisComponent.experiment.status) {
          case ExperimentStatus.REQUEST_CREATION:
          case ExperimentStatus.REQUEST_CREATION_BATCH:
          case ExperimentStatus.CREATED:
          case ExperimentStatus.WAITING_FOR_UPLOAD:
          case ExperimentStatus.FILES_UPLOADED:
          case ExperimentStatus.PEAKLIST_PROCESSING:
          case ExperimentStatus.SEARCH_QUEUED:
          case ExperimentStatus.SEARCH:
          case ExperimentStatus.RESULT_PROCESSING:
          case ExperimentStatus.DELETE:
          case ExperimentStatus.DELETED:
          case ExperimentStatus.ERROR_STATE:
            return false;
          case ExperimentStatus.RESULTS_AVAILABLE:
          case ExperimentStatus.RESULTS_AVAILABLE_AND_LOCKED:
            return true;
        }
      case NodeType.COMPARISON:
        switch(this.dataItemOfThisComponent.comparison.status) {
          case ComparisonStatus.REQUEST_CREATION:
          case ComparisonStatus.CREATED:
          case ComparisonStatus.CHOOSE_EXPERIMENTS:
          case ComparisonStatus.RESULT_PROCESSING:
          case ComparisonStatus.RESULT_PROCESSING:
          case ComparisonStatus.DELETE:
          case ComparisonStatus.DELETED:
            return false;
          case ComparisonStatus.RESULTS_AVAILABLE:
          case ComparisonStatus.RESULTS_AVAILABLE_AND_LOCKED:
            return true;
        }
        return false;
    }
  }

  public hasStatusFiles(): boolean {
    switch (this.dataItemOfThisComponent.type) {
      case NodeType.EXPERIMENT:
        switch(this.dataItemOfThisComponent.experiment.status) {
          case ExperimentStatus.CREATED:
            return false;
          case ExperimentStatus.REQUEST_CREATION:
          case ExperimentStatus.WAITING_FOR_UPLOAD:
          case ExperimentStatus.FILES_UPLOADED:
          case ExperimentStatus.PEAKLIST_PROCESSING:
          case ExperimentStatus.SEARCH_QUEUED:
          case ExperimentStatus.SEARCH:
          case ExperimentStatus.RESULT_PROCESSING:
          case ExperimentStatus.DELETE:
          case ExperimentStatus.DELETED:
          case ExperimentStatus.RESULTS_AVAILABLE:
          case ExperimentStatus.RESULTS_AVAILABLE_AND_LOCKED:
          case ExperimentStatus.REQUEST_CREATION_BATCH:
          case ExperimentStatus.ERROR_STATE:
            return true;
          }
          case NodeType.COMPARISON:
            return false;
        }
  }

  onSetName(): void {
    const dialogRef = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });

    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.dialogPrompt = 'Edit Experiment name';
    dialogInstance.value = this.dataItemOfThisComponent.displayName;
    dialogInstance.valueLabel = 'name';
    dialogInstance.hasValidators = true;

    dialogRef.beforeClosed().subscribe((expName) => {
      if (expName) {
        this.dataItemOfThisComponent.displayName = expName;
      }
    })
  }

  submitWithTargetFdr(targetFdr: string) {
    switch (this.dataItemOfThisComponent.type) {
      case NodeType.EXPERIMENT:
        this.dataItemOfThisComponent.experiment.targetFDR = parseFloat(targetFdr);
        // this.dataRequestSuccess = this.mpaTableDataService.requestProteinGroups(targetFdr, NodeType.EXPERIMENT, this.dataItemOfThisComponent.experiment.experimentid);
        this.mpaTableDataService.requestProteinGroups(targetFdr, NodeType.EXPERIMENT, this.dataItemOfThisComponent.experiment.experimentid).subscribe((success: boolean) => {
          this.hasMpaData = success;
        });
        break;
      case NodeType.COMPARISON:
        this.dataItemOfThisComponent.comparison.targetFDR = parseFloat(targetFdr);
        this.mpaTableDataService.requestProteinGroups(targetFdr, NodeType.COMPARISON, this.dataItemOfThisComponent.comparison.comparisonid).subscribe((success: boolean) => {
          this.hasMpaData = success;
        });
        break;
    }
  }

  onRemoveExperiment(): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: {
        title: 'Delete Experiment',
        message: 'Are you sure you want to delete this experiment? This action cannot be undone.'
      }
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        // If user clicked "Yes"
        this.dataService.removeNode(this.dataItemOfThisComponent);
      }
    });
  }

  // TODO move to mpaTableDataService? --> yes please
  calculateDataStats(): Datstats {
    // let mpaData: ExperimentData = this.mpaTableDataService.experimentDataMap.get(this.dataItemOfThisComponent.experiment.experimentid);
    // let proteinCount = 0;
    // let peptideCount = 0;
    // let psmCount = 0;
    // let spectrumCount = 0;

    // for (const group of mpaData) {
    //   proteinCount += group.proteinList.length;
    //   peptideCount += group.peptideList.length;
    //   psmCount += group.psmList.length;
    //   spectrumCount += group.spectrumIDs.length;
    // }

    // if (mpaData.length > 0) {
    //   this.hasMpaData = true;
    // } else {
    //   this.hasMpaData = false;
    // }

    // return {
    //   totalNoProteinGroups: mpaData.length,
    //   totalNoProteins: proteinCount,
    //   totalNoPeptides: peptideCount,
    //   totalNoPsms: psmCount,
    //   totalNoSpectra: spectrumCount,
    // };
    return {
      totalNoProteinGroups: 0,
      totalNoProteins: 0,
      totalNoPeptides: 0,
      totalNoPsms: 0,
      totalNoSpectra: 0,
    };

  }

  downloadFile(fileTask: string, filePrefix: string, fileSuffix: string) {
    let type: string = 'UNKNOWN';
    let expId: string = 'X';
    let userId: string = 'X';
    let fileName: string = filePrefix + this.dataItemOfThisComponent.displayName + fileSuffix;

    switch (this.dataItemOfThisComponent.type) {
      case NodeType.EXPERIMENT:
        type = 'EXPERIMENT';
        expId = this.dataItemOfThisComponent.experiment.experimentid
        userId = this.dataItemOfThisComponent.experiment.userid

        break;
      case NodeType.COMPARISON:
        type = 'COMPARISON';
        expId = this.dataItemOfThisComponent.comparison.comparisonid
        userId = this.dataItemOfThisComponent.comparison.userid
        break;
    }
    let url: string = this.mpaWebserverAddress.getEndpoint(Endpoints.DOWNLOAD_FILE);
    url += '?experimentid=';
    url +=  expId;
    url += '&taskid=';
    url +=  fileTask;
    url += '&type=';
    url +=  type;
    url += '&label=';
    url +=  userId;
    this.httpClient.downloadFile(url, fileName);
  }

  downloadMZML() {
    //"MZML"
    this.downloadFile('MZML', 'Spectra_', '.mzml');
  }
  downloadMGF() {
    //"MGF"
    this.downloadFile('MGF', 'Spectra_', '.mgf');
  }
  downloadReportProphane() {
    //"PROPHANE_REPORT"
    this.downloadFile('PROPHANE_REPORT', 'MPAMultiForProphane_', '.csv');
  }
  downloadFastaProphane() {
    //"PROPHANE_FASTA"
    this.downloadFile('PROPHANE_FASTA', 'FastaForProphane_', '.fasta');
  }
  downloadProteinGroups() {
    //"PROTEIN_GROUPS"
    this.downloadFile('PROTEIN_GROUPS', 'ProteinGroups', '.tsv');
  }

}
