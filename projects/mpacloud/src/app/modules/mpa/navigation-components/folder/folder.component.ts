import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ContentComponent } from '../content-component';
import { MatDialog } from '@angular/material/dialog';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';
import { NodeType, UserDataTreeObject } from '../../model/user-data-json';
import { TextfieldDialogComponent } from '../../shared-components/textfield-dialog/textfield-dialog.component';
import { DeleteDialogComponent } from '../experiment/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ visibility: 'hidden', height: 0, opacity: 0 })),
      state('expanded', style({ height: '*', opacity: 1 })),
      transition(
        'expanded <=> collapsed',
        animate('325ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ]
})
export class FolderComponent implements OnInit, ContentComponent {

  constructor(
    public dialog: MatDialog,
    private dataService: MPAUserDataNavigationService,
  ) {
    this.subfolders = [];
    this.experiments = [];
    this.protdbs = [];
    this.description = '';
  }

  dataItemOfThisComponent: UserDataTreeObject;
  existingNodeNames = [];
  subfolders: string[];
  experiments: string[];
  protdbs: string[];
  description: string;

  ngOnInit(): void {
    const nexperiments = [];
    const nprotdbs = [];
    const nsubfolders = [];
    this.dataItemOfThisComponent.children.forEach((child) => {
      if (child.type === NodeType.FOLDER) {
        nsubfolders.push(child.displayName);
      } else if (child.type === NodeType.EXPERIMENT) {
        nexperiments.push(child.displayName);
      } else if (child.type === NodeType.PROTEINDB) {
        nprotdbs.push(child.displayName);
      }
    });
    this.experiments = nexperiments;
    this.protdbs = nprotdbs;
    this.subfolders = nsubfolders;
  }

  onSetName(): void {
    const dialogRef = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });
    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.dialogPrompt = 'Edit Folder name';
    dialogInstance.value = this.dataItemOfThisComponent.displayName;
    dialogInstance.valueLabel = 'name';
    dialogInstance.hasValidators = true;
    dialogRef.beforeClosed().subscribe((folderName) => {
      if (folderName) {
        this.dataItemOfThisComponent.displayName = folderName;
      }
    })
  }

  onSetDescription() {
    const dialogRef = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });
    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.dialogPrompt = 'Edit folder description';
    dialogInstance.value = this.dataItemOfThisComponent.description;
    dialogInstance.valueLabel = 'Description'
    dialogRef.beforeClosed().subscribe((folderdescription) => {
      if (folderdescription) {
        this.dataItemOfThisComponent.description = folderdescription;
      }
    });
  }

  onRemoveFolder(): void {
  const dialogRef = this.dialog.open(DeleteDialogComponent, {
       width: '400px',
       data: {
         title: 'Delete Experiment',
         message: 'Are you sure you want to delete this experiment? This action cannot be undone.'
       }
     });
 
     dialogRef.afterClosed().subscribe((result: boolean) => {
       if (result === true) {
         // If user clicked "Yes"
         this.dataService.removeNode(this.dataItemOfThisComponent);
       }
     });
   }

  setDescription(e): void {
    this.dataItemOfThisComponent.description = e.target.value;
  }

  onAddElement(type: string): void {
    // this.intentToAddExperiment = !this.intentToAddExperiment;
    const dialogRef = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });
    // gets instance of the dialog component...
    const dialogInstance = dialogRef.componentInstance;
    var nodeType: NodeType;
    switch (type) {
      case 'experiment':
        nodeType = NodeType.EXPERIMENT;
        dialogInstance.dialogPrompt = 'Please set an Experiment name!';
        dialogInstance.valueLabel = 'Experiment Name';
        break;
      case 'folder':
        nodeType = NodeType.FOLDER;
        dialogInstance.dialogPrompt = 'Please set a Folder name!';
        dialogInstance.valueLabel = 'Folder Name';
        break;
      case 'proteindb':
        nodeType = NodeType.PROTEINDB;
        dialogInstance.dialogPrompt = 'Please set a ProteinDB name!';
        dialogInstance.valueLabel = 'ProteinDB Name';
        break;
      case 'comparison':
        nodeType = NodeType.COMPARISON;
        dialogInstance.dialogPrompt = 'Please set a Comparison name!';
        dialogInstance.valueLabel = 'Comparison Name';
        break;
    }

    dialogRef.afterClosed().subscribe((experimentName) => {
      if (experimentName) {
        console.log('add experiment');
        this.dataService.createNode(
          this.dataItemOfThisComponent,
          experimentName,
          nodeType,
        );
      }
    });
  }

}
