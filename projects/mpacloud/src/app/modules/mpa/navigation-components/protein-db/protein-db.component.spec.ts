import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProteinDbComponent } from './protein-db.component';

describe('ProteinDbComponent', () => {
  let component: ProteinDbComponent;
  let fixture: ComponentFixture<ProteinDbComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProteinDbComponent]
    });
    fixture = TestBed.createComponent(ProteinDbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
