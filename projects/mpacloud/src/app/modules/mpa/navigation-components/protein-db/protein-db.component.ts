import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ProteinDBFileObject,
  ProteinDbStatus,
  SpectralLibrary,
  SpectralLibraryStatus,
  UserDataTreeObject,
} from '../../model/user-data-json';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MultiFileUploadData, UploadFile } from 'shared-lib';
import { TextfieldDialogComponent } from '../../shared-components/textfield-dialog/textfield-dialog.component';
import { ContentComponent } from '../content-component';
import * as uuid from 'uuid';
import { SpectralLibraryComponent } from './spectral-library/spectral-library.component';
import { DeleteDialogComponent } from '../experiment/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-protein-db',
  templateUrl: './protein-db.component.html',
  styleUrls: ['./protein-db.component.scss'],
})
export class ProteinDbComponent implements ContentComponent, OnInit {

  @ViewChild(SpectralLibraryComponent) spectralLibraryComponent: SpectralLibraryComponent;

  dataItemOfThisComponent: UserDataTreeObject;
  acceptedFileTypes: string[] = ['.xml', '.fasta'];
  selectedFiles: File[] = [];
  runSpecLibPrediction: boolean = false;
  spectralLibraryName: string = '';


  constructor(
    private dataService: MPAUserDataNavigationService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.spectralLibraryName = this.dataItemOfThisComponent.displayName + '_SpectralLibrary';
    if (!this.dataItemOfThisComponent.proteinDB.spectralLibraries) {
      this.dataItemOfThisComponent.proteinDB.spectralLibraries = [];
    }
  }

  onSetName(): void {
    const dialogRef: MatDialogRef<TextfieldDialogComponent, any> = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });
    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.dialogPrompt = 'Edit ProteinDB name';
    dialogInstance.value = this.dataItemOfThisComponent.displayName;
    dialogInstance.valueLabel = 'name';
    dialogInstance.hasValidators = true;
    dialogRef.beforeClosed().subscribe((proteinDBName) => {
      if (proteinDBName) {
        this.dataItemOfThisComponent.displayName = proteinDBName;
      }
    });
  }

  setDescription(e): void {
    this.dataItemOfThisComponent.description = e.target.value;
  }

  removeProteinDB(): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: {
        title: 'Delete Experiment',
        message: 'Are you sure you want to delete this experiment? This action cannot be undone.'
      }
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result === true) {
        // If user clicked "Yes"
        this.dataService.removeNode(this.dataItemOfThisComponent);
      }
    });
  }

  getNamesOfUploadedFiles(): string {
    let originalfileNames: string = '';
    this.dataItemOfThisComponent.proteinDB.proteinDBFiles.forEach((protDbFile: ProteinDBFileObject) => {
      if (originalfileNames.length == 0) {
        originalfileNames = protDbFile.originalFileName;
      } else {
        originalfileNames += ',' + protDbFile.originalFileName;
      }
    });
    return originalfileNames;
  }

  handleFileInput(fileList: File[]): void {
    this.selectedFiles = [...fileList];
  }

  handleSubmit(): void {
    let fileList = [...this.selectedFiles]; //copy currently selected files
    if (fileList.length < 1) {
      return;
    }
    const multiFile: MultiFileUploadData = {
      files: [],
    };
    fileList.forEach((thisFile: File) => {
      // create and uploadId
      const uploadId = uuid.v4();
      // prepare the object for httpclient mutipart-form file upload
      const uploadFile: UploadFile = {
        uploadFile: thisFile,
        fileID: uploadId,
      };
      multiFile.files.push(uploadFile);
      // Add the file to the proteinDBObject
      const proteinDBFileObject: ProteinDBFileObject = {
        originalFileName: thisFile.name,
        uploadId: uploadId,
        fileId: null,
        dbType: (thisFile.name.endsWith('.fasta') ? 'FASTA' : 'UNIPROTXML'),
        uploadStatus: 'WAITING_FOR_UPLOAD',
      };
      this.dataItemOfThisComponent.proteinDB.proteinDBFiles.push(proteinDBFileObject);
    });
    if (this.runSpecLibPrediction) {
      const formValues = this.spectralLibraryComponent.form.getRawValue();

      this.dataItemOfThisComponent.proteinDB.spectralLibraries.push(this.mapForm(formValues));
    }
    this.dataItemOfThisComponent.proteinDB.status = ProteinDbStatus.WAITING_FOR_UPLOAD;
    this.dataService.triggerHttpUpdateEvent$.next(true);
    this.dataService.executeUpload(multiFile);
  }

  canUploadFiles(): boolean {
    return this.dataItemOfThisComponent.proteinDB.status === ProteinDbStatus.CREATED;
  }

  predictLibrary() {
    console.log(this.dataItemOfThisComponent.proteinDB.spectralLibraries);
    const formValues = this.spectralLibraryComponent.form.getRawValue();
    const specLib = this.mapForm(formValues);
    const exists = this.dataItemOfThisComponent.proteinDB.spectralLibraries?.some(lib =>
      JSON.stringify(lib.ms2pipParams) === JSON.stringify(specLib.ms2pipParams),
    );
    if (!exists) {
      this.dataItemOfThisComponent.proteinDB.spectralLibraries.push(specLib);
    }

    this.dataService.triggerHttpUpdateEvent$.next(true);

    console.log(this.dataItemOfThisComponent.proteinDB.spectralLibraries);
  }

  private mapForm(formValues): SpectralLibrary {
    return {
      id: uuid.v4(),
      name: formValues.name,
      status: this.dataItemOfThisComponent.proteinDB.status === ProteinDbStatus.AVAILABLE_AND_LOCKED ? SpectralLibraryStatus.WAITING_FOR_DB_PROCESSING : SpectralLibraryStatus.WAITING_FOR_DB_PROCESSING,
      ms2pipParams: {
        minLength: formValues.ms2pipParams.minLength,
        maxLength: formValues.ms2pipParams.maxLength,
        cleavageRule: formValues.ms2pipParams.cleavageRule,
        maxVariableModifications: formValues.ms2pipParams.maxVariableModifications,
        missedCleavages: formValues.ms2pipParams.missedCleavages,
        charges: this.spectralLibraryComponent.chargesArray,
        modifications: formValues.ms2pipParams.modifications.map((mod: any) => ({
          label: mod.label,
          aminoAcid: mod.aminoAcid,
          fixed: mod.fixed,
        })),
      },
    };
  }
}
