import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpectralLibraryComponent } from './spectral-library.component';

describe('SpectralLibraryComponent', () => {
  let component: SpectralLibraryComponent;
  let fixture: ComponentFixture<SpectralLibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SpectralLibraryComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpectralLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
