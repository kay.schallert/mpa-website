import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatIcon } from '@angular/material/icon';
import { MatButton, MatIconButton } from '@angular/material/button';
import { NgForOf, NgIf } from '@angular/common';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { FormArray, FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { SpectralLibrary } from '../../../model/user-data-json';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { MatChip, MatChipInput, MatChipsModule } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatOption, MatSelect } from '@angular/material/select';
import { MatTooltip } from '@angular/material/tooltip';
import { FlexModule } from '@angular/flex-layout';
import { MatDivider } from '@angular/material/divider';
import { MatAccordion, MatExpansionPanel, MatExpansionPanelHeader, MatExpansionPanelTitle } from '@angular/material/expansion';

@Component({
  selector: 'app-spectral-library',
  standalone: true,
  imports: [
    MatCheckbox,
    MatIcon,
    MatIconButton,
    NgIf,
    FormsModule,
    MatFormField,
    MatInput,
    ReactiveFormsModule,
    MatChipsModule,
    NgForOf,
    MatSelect,
    MatOption,
    MatButton,
    MatLabel,
    MatTooltip,
    FlexModule,
    MatDivider,
    MatAccordion,
    MatExpansionPanel,
    MatExpansionPanelHeader,
    MatExpansionPanelTitle
  ],
  templateUrl: './spectral-library.component.html',
  styleUrl: './spectral-library.component.scss',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ visibility: 'hidden', height: 0, opacity: 0 })),
      state('expanded', style({ height: '*', opacity: 1 })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ),
    ]),
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)'),
      ),
    ]),
  ],
})
export class SpectralLibraryComponent implements OnInit, OnChanges {

  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  @Input() name: string;
  @Input() runSpecLibPrediction: boolean = true;
  expandSpecLibPredictionParams: boolean = false;

  form: FormGroup;
  chargeInput = new FormControl('');
  chargesArray: number[] = [2, 3];

  constructor(private fb: FormBuilder) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['runSpecLibPrediction']) {
      console.log('runSpecLibPrediction changed:', this.runSpecLibPrediction);
      this.updateFormState();
    }
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: [this.name, Validators.required],
      ms2pipParams: this.fb.group({
        maxLength: [30, [Validators.required, Validators.min(1)]],
        minLength: [7, [Validators.required, Validators.min(1)]],
        cleavageRule: ['trypsin', Validators.required],
        maxVariableModifications: [3, [Validators.required, Validators.min(0)]],
        missedCleavages: [2, [Validators.required, Validators.min(0)]],
        charges: ['', Validators.required],
        modifications: this.fb.array([])
      })
    });

    this.addModification('UNIMOD:Oxidation', 'M', false);
    this.addModification('UNIMOD:Carbamidomethyl', 'C', true);
  }

  updateFormState() {
    if (this.runSpecLibPrediction) {
      this.form?.get('name')?.enable();
    } else {
      this.form?.get('name')?.disable();
    }
  }

  addCharge(event: any): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const numberValue = parseInt(value.trim());
      if (!isNaN(numberValue) && numberValue >= 0 && !this.chargesArray.includes(numberValue)) {
        this.chargesArray.push(numberValue);
        this.form.get('charges')?.setValue(this.chargesArray);
      }
    }

    // Clear the input field
    if (input) {
      input.value = '';
    }

    this.chargeInput.setValue('');
  }

  removeCharge(index: number): void {
    if (index >= 0) {
      this.chargesArray.splice(index, 1);
      this.form.get('charges')?.setValue(this.chargesArray);
    }
  }

  removeModification(index: number) {
    this.modifications.removeAt(index);
  }

  addModification(label = '', aminoAcid = '', fixed = false) {
    this.modifications.push(this.fb.group({
      label: [label, Validators.required],
      aminoAcid: [aminoAcid.toUpperCase(), [Validators.required, Validators.maxLength(1)]],
      fixed: [fixed]
    }));
  }

  get modifications() {
    return this.form.get('ms2pipParams.modifications') as FormArray;
  }
}
