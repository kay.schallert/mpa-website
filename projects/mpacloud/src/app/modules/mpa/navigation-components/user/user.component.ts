import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserDataTreeObject, NodeType } from '../../model/user-data-json';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';
import { TextfieldDialogComponent } from '../../shared-components/textfield-dialog/textfield-dialog.component';
import { ContentComponent } from '../content-component';
import { AuthService } from 'shared-lib';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements ContentComponent, OnInit {
  dataItemOfThisComponent: UserDataTreeObject;

  // contains folder names for overview
  public folders: string[];
  public experiments: string[];
  public comparisons: string[];
  public proteinDB: string[];

  username: string = 'Anonymous';

  constructor(
    private dataService: MPAUserDataNavigationService,
    public dialog: MatDialog,
    private auth: AuthService,
  ) {
    this.folders = [];
    this.experiments = [];
    this.comparisons = [];
    this.proteinDB = [];
  }

  ngOnInit(): void {
    const newFolders = [];
    const newExperiments = [];
    const newComparisons = [];
    const newDataBases = [];
    this.dataService
      .getAllNodesAsListByType([NodeType.FOLDER])
      .forEach((folder: UserDataTreeObject) => {
        newFolders.push(folder.displayName);
      });
    this.dataService
      .getAllNodesAsListByType([NodeType.EXPERIMENT])
      .forEach((experiment: UserDataTreeObject) => {
        newExperiments.push(experiment.displayName);
      });
    this.dataService
      .getAllNodesAsListByType([NodeType.COMPARISON])
      .forEach((comparison: UserDataTreeObject) => {
        newComparisons.push(comparison.displayName);
      });
    this.dataService
      .getAllNodesAsListByType([NodeType.PROTEINDB])
      .forEach((proteinDB: UserDataTreeObject) => {
        newDataBases.push(proteinDB.displayName);
      });
    this.folders = newFolders;
    this.experiments = newExperiments;
    this.comparisons = newComparisons;
    this.proteinDB = newDataBases;

  }

  onAddFolder(): void {
    const dialogRef = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });
    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.dialogPrompt = 'Please set a Folder name!';
    dialogInstance.valueLabel = 'Folder Name';
    dialogRef.afterClosed().subscribe((folderName) => {
      if (folderName) {
        this.dataService.createNode(
          this.dataItemOfThisComponent,
          folderName,
          NodeType.FOLDER,
        );
      }
    });
  }

  onSetName(): void {
    const dialogRef = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });
    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.dialogPrompt = 'Edit folder name';
    dialogInstance.value = this.dataItemOfThisComponent.displayName;
    dialogInstance.valueLabel = 'name';
    dialogInstance.hasValidators = true;
    dialogRef.beforeClosed().subscribe((folderName) => {
      if (folderName) {
        this.dataItemOfThisComponent.displayName = folderName;
      }
    });
  }
}
