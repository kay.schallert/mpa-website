import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MpaNavigationComponent } from './mpa.navigation.component';

describe('MpaComponent', () => {

  let component: MpaNavigationComponent;
  let fixture: ComponentFixture<MpaNavigationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MpaNavigationComponent]
    });
    fixture = TestBed.createComponent(MpaNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
