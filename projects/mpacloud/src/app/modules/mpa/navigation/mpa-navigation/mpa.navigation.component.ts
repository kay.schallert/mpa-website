import { Component, ComponentRef, Input, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';
import { NodeType, UserDataTreeObject } from '../../model/user-data-json';
import { UserComponent } from '../../navigation-components/user/user.component';
import { FolderComponent } from '../../navigation-components/folder/folder.component';
import { ExperimentComponent } from '../../navigation-components/experiment/experiment.component';
import { ProteinDbComponent } from '../../navigation-components/protein-db/protein-db.component';
import { Subscription, timeout, timer } from 'rxjs';
import { ContentComponent } from '../../navigation-components/content-component';
import { MatDrawerMode } from '@angular/material/sidenav';

import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'mpa-navigation',
  templateUrl: './mpa.navigation.component.html',
  styleUrls: ['./mpa.navigation.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
  ]
})
export class MpaNavigationComponent implements OnInit, OnDestroy {

  // treeoutlet - template reference variable
  // ViewChild can grab references to the DOM element with the variable #treeoutlet
  // with read: ViewContainerRef it grabs component views - views are display elements
  @ViewChild('mpaSideNavNavigatedOutlet', {read: ViewContainerRef}) mpaSideNavNavigatedOutlet: ViewContainerRef;

  constructor(public mpaUserDataNavigationService: MPAUserDataNavigationService) {}

  private navigateComponentEventSubscription: Subscription;

  // Sidenav expansion parameters
  public collapsed: boolean = true;
  public width = '15%';
  public mode: MatDrawerMode = 'side';

  ngOnInit() {
    this.navigateComponentEventSubscription = this.mpaUserDataNavigationService.navigateComponentEvent$.subscribe(
      (navigatedComponent: UserDataTreeObject) => {
        this.createComponent(navigatedComponent);
      });
  }

  ngOnDestroy() {
    this.navigateComponentEventSubscription.unsubscribe();
  }

  private createComponent(dataItem: UserDataTreeObject) {
    this.mpaSideNavNavigatedOutlet.clear();
    let componentRef: ComponentRef<any>;
    //TODO: had to introduce fix as some items had 'user' and some 'USER' as type
    // console.log(dataItem)
    switch (dataItem.type) {
      case NodeType.USER: {
        componentRef = this.mpaSideNavNavigatedOutlet.createComponent(UserComponent);
        break;
      }
      case NodeType.FOLDER: {
        componentRef = this.mpaSideNavNavigatedOutlet.createComponent(FolderComponent);
        break;
      }
      case NodeType.COMPARISON:
      case NodeType.EXPERIMENT: {
        componentRef = this.mpaSideNavNavigatedOutlet.createComponent(ExperimentComponent);
        break;
      }
      case NodeType.PROTEINDB: {
        console.log("CREATED PROTEINDB");
        console.log(dataItem)
        componentRef = this.mpaSideNavNavigatedOutlet.createComponent(ProteinDbComponent);
        break;
      }
      default: {
        // TODO: add default case?
      }
    }
    (<ContentComponent>componentRef.instance).dataItemOfThisComponent = dataItem;
  }

  public toggleCollapse() {
    this.collapsed = !this.collapsed;
    if (this.collapsed) {
      // prevent sidenav-content from using up less space than available due to the sidenav
      // switching instantly back to mode = side
      setTimeout(() => {
        this.mode = 'side';
      },151)
      // this.width = '10vw';
      this.width = '15%';
    } else if (!this.collapsed) {
      this.mode = 'push';
      this.width = '30%';
    }
  }

}
