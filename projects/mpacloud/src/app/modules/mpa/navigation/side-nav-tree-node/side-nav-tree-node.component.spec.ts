import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavTreeNodeComponent } from './side-nav-tree-node.component';

describe('SideNavTreeComponent', () => {

  let component: SideNavTreeNodeComponent;
  let fixture: ComponentFixture<SideNavTreeNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavTreeNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavTreeNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
