import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { NodeType, UserDataTreeObject } from '../../model/user-data-json';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';

@Component({
  selector: 'side-nav-tree-node',
  templateUrl: './side-nav-tree-node.component.html',
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
  ],
  styleUrls: ['./side-nav-tree-node.component.scss'],
})
export class SideNavTreeNodeComponent {
  @Input() node: UserDataTreeObject;
  @Input() isSelected: boolean;
  @Output() treeNodeSelectionEvent = new EventEmitter<number>();

  NodeType = NodeType;

  constructor(
    private mpaUserDataNavigationService: MPAUserDataNavigationService
  ) {}

  public onExpand() {
    if (this.node.children.length !== 0 && this.node.type !== NodeType.USER) {
      this.node.expanded = !this.node.expanded;
    }
  }

  public onNavigate() {
    this.treeNodeSelectionEvent.emit(this.node.id); // used to highlight the currently selected node
    this.mpaUserDataNavigationService.navigateOutlet(this.node);
  }
}
