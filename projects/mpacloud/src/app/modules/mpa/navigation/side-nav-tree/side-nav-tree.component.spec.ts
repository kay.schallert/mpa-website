import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNavTreeComponent } from './side-nav-tree.component';

describe('SideNavTreeComponent', () => {

  let component: SideNavTreeComponent;
  let fixture: ComponentFixture<SideNavTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNavTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
