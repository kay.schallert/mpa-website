import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MPAUserDataNavigationService } from '../../services/mpa-user-data-navigation.service';
import { NodeType, UserDataTreeObject } from '../../model/user-data-json';
import { CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'side-nav-tree',
  templateUrl: './side-nav-tree.component.html',
  styleUrls: ['./side-nav-tree.component.scss'],
})
export class SideNavTreeComponent implements OnInit, OnDestroy {
  @Input() public flatTreeNodesList: UserDataTreeObject[];
  @Output() expansionToggleEvent = new EventEmitter();

  public selectedTreeNodeId: number;

  constructor(
    public mpaUserDataNavigationService: MPAUserDataNavigationService,
    public router: Router
  ) {}

  ngOnInit() {
    // this.userDataTreeUpdateEventSubscription = this.mpaUserDataNavigationService.triggerHttpUpdateEvent$.subscribe(() => {
    //   this.flatTreeNodesList = this.mpaUserDataNavigationService.getAllNodesForSideNav();
    // });
  }

  ngOnDestroy() {
    // this.userDataTreeUpdateEventSubscription.unsubscribe();
  }

  public isVisible(treeNode: UserDataTreeObject): boolean {
    let isVisible = true;
    let child = treeNode;
    while (child.type !== NodeType.USER) {
      const parent = this.flatTreeNodesList.find(
        (search) => search.id === child.parent
      );
      if (!parent?.expanded) {
        isVisible = false;
        break;
      }
      child = parent;
    }
    return isVisible;
  }

  public drop(event: CdkDragDrop<any, any, any>) {
    // TODO: add implementation
  }

  /** Called when child-node gets selected and informs parent on currently selected node. This is then fed back down to all children to adjust styling. */
  public treeNodeSelection(node) {
    this.selectedTreeNodeId = node.id
  }

  public toggleExpandSidenav() {
    this.expansionToggleEvent.emit();
  }

}
