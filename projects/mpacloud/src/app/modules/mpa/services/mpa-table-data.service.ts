import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientService } from 'shared-lib';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { SpectrumDataObject } from '../experiment-components/spectrum-viewer/spectrum-data-object';
import { Endpoints, MpaWebserverAddressService } from '../../../mpa-webserver-address.service';
import { EC, ExperimentData, Keyword, KO, NCBIRanks, Peptide, Protein, ProteinMainGroup, ProteinSequence, ProteinSubgroup, PSM, Spectrum, Taxonomy, TaxonomyTableObject } from '../model/experiment-data-json';
import { NodeType } from '../model/user-data-json';
import { GroupSelection } from '../experiment-components/group-table/group-table.component';

class ChangedElements {

  spectrumIds: Set<string> = new Set();
  peptideIds: Set<string> = new Set();
  proteinIds: Set<string> = new Set();
  koIds: Set<string> = new Set();
  ecIds: Set<string> = new Set();
  taxonomyIds: Set<string> = new Set();
  keywordIds: Set<string> = new Set();
  subgroupIds: Set<string> = new Set();
  groupIds: Set<string> = new Set();

}

@Injectable({
  providedIn: 'root'
})
export class MpaTableDataService {

  private _TaxonomyRootTransformer = (node: Taxonomy, level: number) => {
    return {
      // back-end data
      taxId: node.taxId,
      children: node.children,
      rank: node.rank,
      scientificname: node.scientificname,
      othernames: node.othernames,
      proteinIds: node.proteinIds,
      // experimentIDs: node.experimentIDs,
      // front-end data
      level: level,
      expandable: !!node.children && node.children.length > 0,
      isExpanded: false,
      isDisplayed: true,
      // quantification: node.quantification
    };
  };

  private _KeywordRootTransformer = (node:Keyword, level: number) => {
    return {
      // back-end data
      kwId: node.kwId,
      kwAccession: node.kwAccession,
      kwCategory: node.kwCategory,
      children: node.children,
      proteinIds: node.proteinIds,
      // front-end data
      level: level,
      expandable: !!node.children && node.children.length > 0,
      isExpanded: false,
      isDisplayed: true,
      quantification: node.quantifications
    };
  };

  private _KeggFunctionTransformer = (ko?: KO, ec?: EC) => {
    if (ko != null) {
      return {
        // back-end data
        id: ko.koId,
        names: ko.koNames,
        symbols: ko.koSymbols,
        proteinIds: ko.proteinIds,
        // front-end data
        type: 'KO',
        isDisplayed: true,
        quantifications: []
      }
    } else if (ec != null) {
      return {
        // back-end data
        id: ec.ecId,
        names: ec.ecNames,
        symbols: ec.ecSymbols,
        proteinIds: ec.proteinIds,
        // front-end data
        type: 'EC',
        isDisplayed: true,
        quantifications: []
      }
    }
  }


  // NEW/OLD APPROVED FIELDS
  private experimentData: ExperimentData = null; // TODO: add dummy data

  // object maps for convenience
  private proteinGroupMap: Map<string, ProteinMainGroup> = new Map();
  private proteinSubGroupMap: Map<string, ProteinSubgroup> = new Map();
  private proteinMap: Map<string, Protein> = new Map();
  private peptideMap: Map<string, Peptide> = new Map();
  private psmpMap: Map<string, PSM> = new Map();
  private spectrumMap: Map<string, Spectrum> = new Map();
  private taxonomyMap: Map<string, Taxonomy> = new Map();
  private keywordMap: Map<string, Keyword> = new Map();
  private ecMap: Map<string, EC> = new Map();
  private koMap: Map<string, KO> = new Map();

  // helper maps
  private experimentId2Index: Map<string, number> = new Map();
  private protein2SubgroupMap: Map<string, string> = new Map();
  private pep2prot: Map<string, Set<string>> = new Map();
  private tax2prot: Map<string, Set<string>> = new Map();



  // this single Subject is for detecting changes in detail view data
  public detailViewDataChange: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  public groupSelection: GroupSelection = GroupSelection.SUBGROUPS;
  public selectedGroupId: string = '';
  public selectedProteinId: string = '';
  public selectedPeptideId: string  = '';
  public selectedPsmId: string  = '';
  public selectedSpectrumId: string  = '';
  public spectrumDataObject$: BehaviorSubject<Spectrum>;



  // OLD FIELDS, UNCLEAR
  // save experimentData after requesting > should optimally prevent from having to request again all the time when switching views
  // private saveDataMax: number = 5;
  // public experimentDataMap: Map<string, ExperimentData> = new Map<string, ExperimentData>();
  // private savedExpIds: string[] = [];

  // These variables are for current view
  // public expIDType = new BehaviorSubject<string>(undefined);
  // public expID = new BehaviorSubject<string>(undefined);
  // public proteinTableData = new BehaviorSubject<ProteinGroup[]>([]);
  public keywordFlatData = new BehaviorSubject<Keyword[]>([]);
  public taxonomyHierarchicalData = new BehaviorSubject<Taxonomy[]>([]);
  public taxonomyFlatTableData = new BehaviorSubject<Taxonomy[]>([]);
  //public functionData = new BehaviorSubject<ProteinFunction[]>([]);
  public proteinSequenceData = new BehaviorSubject<ProteinSequence>(undefined);

  public spectrumData = new BehaviorSubject<Spectrum>(undefined);
  public requestingSpectrum = new Subject<boolean>();



  // TODO: should be simple maps
  private proteinID2proteinGroupIDMap$ = new BehaviorSubject<Map<string,string[]>>(new Map<string,string[]>());
  public proteinGroupID2proteinGroupObjectMap$ = new BehaviorSubject<Map<string,ProteinMainGroup>>(new Map<string,ProteinMainGroup>());

  // These variables are for current selection
  public selectedPeptide = new BehaviorSubject<Peptide>(undefined);
  public selectedPsm = new BehaviorSubject<PSM>(undefined);
  // public selectedProteinGroup = new BehaviorSubject<ProteinGroup>(undefined);
  public selectedProtein = new BehaviorSubject<Protein>(undefined);
  public peptidesForSelectedProtein = new BehaviorSubject<Peptide[]>([]);
  public psmsForSelectedPeptide = new BehaviorSubject<PSM[]>([]);

  public requestingProteinSequence = false;

  // public proteinGroupRequestStatus = new BehaviorSubject<PGRequestStatus>(PGRequestStatus.UNINITIATED);

  // public groupSelection: GroupSelection = GroupSelection.MAINGROUPS;

  constructor(
    private httpClientService: HttpClientService,
    private addressService: MpaWebserverAddressService) {

    // TODO: reevaluate if this is necessary
    this.requestingSpectrum.next(true);
    this.selectedProtein.subscribe(protein => {
      if (typeof protein !== 'undefined') {
        this.setPeptidesForSelectedProtein();
        this.requestSequence();
      }
    });

    this.selectedPeptide.subscribe(peptide => {
      if (typeof peptide !== 'undefined') {
        this.setPsmsForSelectedPeptide();
      }
    });

    this.selectedPsm.subscribe(psm => {
      if (typeof psm !== 'undefined') {
        this.requestSpectrum();
      }
    });
    this.spectrumDataObject$ = new BehaviorSubject<Spectrum>({
      spectrumId: '',
      spectrumQuant: 0,
      psmIds: [],
    });
  }

  public getExperiment2IndexMap(): Map<string, number> {
    return this.experimentId2Index;
  }

  public getProteinGroupData(): ProteinMainGroup[] {
    return this.experimentData.proteinData.proteinGroups;
  }

  public getProteinSubGroupData(): ProteinSubgroup[] {
    const subgroups: ProteinSubgroup[] = [];
    this.experimentData.proteinData.proteinGroups.forEach((group: ProteinMainGroup) => {
      group.subgroups.forEach((subgroup: ProteinSubgroup) => {
        subgroups.push(subgroup);
      });
    });
    return subgroups;
  }

  public getTaxonomyTree(): Taxonomy {
    return this.experimentData.annotationData.taxRoot;
  }

  public getKeywords(): Keyword[] {
    return this.flattenKeyword(this.experimentData.annotationData.kwRoot, 0);
  }

  public getECs(): EC[] {
    return this.experimentData.annotationData.ecs;
  }

  public getKOs(): KO[] {
    return this.experimentData.annotationData.kos;
  }

  public getProteinAnnotationDetails(): any {
    if (this.selectedProteinId !== '') {
      const koList = [];
      const ecList = [];
      const kwList = [];
      this.proteinMap.get(this.selectedProteinId).koIds.forEach((koId: string) => {
        console.log(koId);
        console.log(this.koMap);
        if (this.koMap.get(koId) !== undefined) {
          koList.push(this.koMap.get(koId));
        }
      });
      this.proteinMap.get(this.selectedProteinId).ecIds.forEach((ecId: string) => {
        console.log(ecId);
        console.log(this.ecMap);
        if (this.ecMap.get(ecId) !== undefined) {
          ecList.push(this.ecMap.get(ecId));
        }

      });
      this.proteinMap.get(this.selectedProteinId).kwIds.forEach((kwId: string) => {
        kwList.push(this.keywordMap.get(kwId));
      });
      console.log(koList);
      console.log(ecList);
      console.log(kwList);
      return {
        accessions: this.proteinMap.get(this.selectedProteinId).accessions,
        descriptions: this.proteinMap.get(this.selectedProteinId).descriptions,
        kos: koList,
        ecs: ecList,
        kws: kwList,
        tax: this.taxonomyMap.get(this.proteinMap.get(this.selectedProteinId).taxId),
      }
    } else {
      return null;
    }

  }

  getPSMDataForGroup(): PSM[] {
    const psmSet: Set<PSM> = new Set();
    if (this.groupSelection === GroupSelection.MAINGROUPS) {
      this.proteinGroupMap.get(this.selectedGroupId).subgroups.forEach((subgroup: ProteinSubgroup) => {
        subgroup.proteinIds.forEach((proteinId: string) => {
          this.proteinMap.get(proteinId).peptideIds.forEach((pepId: string) => {
            this.peptideMap.get(pepId).psmIds.forEach((psmId: string) => {
              psmSet.add(this.psmpMap.get(psmId));
            });
          });
        });
      });
    } else if (this.groupSelection === GroupSelection.SUBGROUPS) {
      this.proteinSubGroupMap.get(this.selectedGroupId).proteinIds.forEach((proteinId: string) => {
        this.proteinMap.get(proteinId).peptideIds.forEach((pepId: string) => {
          this.peptideMap.get(pepId).psmIds.forEach((psmId: string) => {
            psmSet.add(this.psmpMap.get(psmId));
          });
        });
      });
    }
    const psms: PSM[] = [];
    psmSet.forEach((psm: PSM) => {
      psms.push(psm);
    });
    return psms;
  }

  getPSMDataForProtein(): PSM[] {
    const psmSet: Set<PSM> = new Set();
    if (this.selectedProteinId !== '') {
      this.proteinMap.get(this.selectedProteinId).peptideIds.forEach((pepId: string) => {
        this.peptideMap.get(pepId).psmIds.forEach((psmId: string) => {
          psmSet.add(this.psmpMap.get(psmId));
        });
      });
    }
    const psms: PSM[] = [];
    psmSet.forEach((psm: PSM) => {
      psms.push(psm);
    });
    return psms;
  }

  getPSMDataForPeptide(): PSM[] {
    const psmSet: Set<PSM> = new Set();
    if (this.selectedPeptideId !== '') {
      this.peptideMap.get(this.selectedPeptideId).psmIds.forEach((psmId: string) => {
        psmSet.add(this.psmpMap.get(psmId));
      });
    }
    const psms: PSM[] = [];
    psmSet.forEach((psm: PSM) => {
      psms.push(psm);
    });
    return psms;
  }

  getSpectrumFromId(spcetrumId: string): Spectrum {
    return this.spectrumMap.get(spcetrumId);
  }

  public getProteinData(): Protein[] {
    const proteins: Protein[] = [];
    if (this.groupSelection === GroupSelection.MAINGROUPS) {
      this.proteinGroupMap.get(this.selectedGroupId).subgroups.forEach((subgroup: ProteinSubgroup) => {
        subgroup.proteinIds.forEach((proteinId: string) => {
          proteins.push(this.proteinMap.get(proteinId));
        });
      });
    } else  if (this.groupSelection === GroupSelection.SUBGROUPS) {
      this.proteinSubGroupMap.get(this.selectedGroupId).proteinIds.forEach((proteinId: string) => {
        proteins.push(this.proteinMap.get(proteinId));
      });
    }
    return proteins;
  }

  getProteinGroupPeptides(): Peptide[] {
    const peptideSet: Set<Peptide> = new Set();
    if (this.groupSelection === GroupSelection.MAINGROUPS) {
      this.proteinGroupMap.get(this.selectedGroupId).subgroups.forEach((subgroup: ProteinSubgroup) => {
        subgroup.proteinIds.forEach((proteinId: string) => {
          this.proteinMap.get(proteinId).peptideIds.forEach((peptideId: string) => {
            peptideSet.add(this.peptideMap.get(peptideId))
          })
        });
      });
    } else  if (this.groupSelection === GroupSelection.SUBGROUPS) {
      this.proteinSubGroupMap.get(this.selectedGroupId).proteinIds.forEach((proteinId: string) => {
        this.proteinMap.get(proteinId).peptideIds.forEach((peptideId: string) => {
          peptideSet.add(this.peptideMap.get(peptideId))
        });
      });
    }
    const peptides: Peptide[] = [];
    peptideSet.forEach((peptide: Peptide) => {
      peptides.push(peptide);
    });
    return peptides;
  }

  getProteinPeptides(): Peptide[] {
    if (this.selectedProteinId !== '') {
      const peptideSet: Set<Peptide> = new Set();
      this.proteinMap.get(this.selectedProteinId).peptideIds.forEach((peptideId: string) => {
        peptideSet.add(this.peptideMap.get(peptideId))
      });
      const peptides: Peptide[] = [];
      peptideSet.forEach((peptide: Peptide) => {
        peptides.push(peptide);
      });
      return peptides;
    }
  }


  public requestProteinGroups(targetFdr: string, nodeType: NodeType, objectId: string): Observable<boolean> {
    let returnValue: Subject<boolean> = new Subject<boolean>();
    const params: HttpParams = new HttpParams({
      fromObject: {
        experimentid: objectId,
        type: nodeType,
        taskid: targetFdr,
      }
    });
    this.httpClientService.getObject<ExperimentData>(this.addressService.getEndpoint(Endpoints.GET_PROTEIN_GROUPS), params).subscribe({
      next: (experimentData: ExperimentData) => {
        // make maps for convenience
        // clear all old data first
        this.experimentData = experimentData;
        this.experimentId2Index.clear();
        for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
          this.experimentId2Index.set(this.experimentData.experimentIdList[i], i);
        }
        this.proteinGroupMap.clear();
        this.proteinMap.clear();
        this.peptideMap.clear();
        this.psmpMap.clear();
        this.spectrumMap.clear();
        this.taxonomyMap.clear();
        this.keywordMap.clear();
        this.ecMap.clear();
        this.koMap.clear();
        // TODO: backend support for quantification and isDisplayed
        // init quantification as 0,
        // init isDisplayed = true,
        experimentData.proteinData.proteinGroups.forEach((group: ProteinMainGroup) => {
          this.proteinGroupMap.set(group.proteinGroupId, group);
          group.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
          for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
            group.quantifications[i] = 0;
          }
          group.isDisplayed = true;
          group.isHighlighted = false;
          group.isSelected = false;
          group.subgroups.forEach((sg: ProteinSubgroup) => {
            sg.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
            for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
              sg.quantifications[i] = 0;
            }
            sg.isDisplayed = true;
            group.subgroups.forEach((subgroup: ProteinSubgroup) => {
              this.proteinSubGroupMap.set(subgroup.proteinSubgroupId, subgroup);
              subgroup.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
              for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
                subgroup.quantifications[i] = 0;
              }
              subgroup.isDisplayed = true;
              subgroup.isHighlighted = false;
              subgroup.isSelected = false;
              subgroup.proteinIds.forEach((proteinId: string) => {
                this.protein2SubgroupMap.set(proteinId, subgroup.proteinSubgroupId);
              });

          });
          });
        });
        experimentData.proteinData.proteins.forEach((protein: Protein) => {
          protein.peptideIds.forEach((pepId: string) => {
            if (!this.pep2prot.has(pepId)) {
              this.pep2prot.set(pepId, new Set<string>);
            }
            this.pep2prot.get(pepId).add(protein.proteinId);
          });
          protein.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
          for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
            protein.quantifications[i] = 0;
          }
          if (!this.tax2prot.has(protein.taxId)) {
            this.tax2prot.set(protein.taxId, new Set<string>);
          }
          this.tax2prot.get(protein.taxId).add(protein.proteinId);
          protein.isDisplayed = true;
          protein.isHighlighted = false;
          protein.isSelected = false;
          this.proteinMap.set(protein.proteinId, protein);
        });
        experimentData.proteinData.psms.forEach((psm: PSM) => {
          psm.isSelected = false;
           this.psmpMap.set(psm.psmId, psm);
           if (!this.peptideMap.has(psm.peptideId)) {
            const pep: Peptide = {
              sequenceId: psm.peptideId,
              psmIds: [psm.psmId],
              quantifications: new Array<number>(this.experimentData.experimentIdList.length),
              isDisplayed: true,
              isHighlighted: false,
              isSelected: false,
            };
            for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
              pep.quantifications[i] = 0;
            }
            this.peptideMap.set(pep.sequenceId, pep);
           } else {
            this.peptideMap.get(psm.peptideId).psmIds.push(psm.psmId);
           }
           if (!this.spectrumMap.has(psm.spectrumId)) {
            const spectrum: Spectrum = {
              spectrumId: psm.spectrumId,
              spectrumQuant: psm.spectrumQuant,
              isDisplayed: true,
              isHighlighted: false,
              psmIds: [],
            };
            spectrum.psmIds.push(psm.psmId);
            this.spectrumMap.set(spectrum.spectrumId, spectrum);
           } else {
            this.spectrumMap.get(psm.spectrumId).psmIds.push(psm.psmId);
           }
        });
        experimentData.annotationData.ecs.forEach((ec: EC) => {
          ec.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
          // solution to populate array without the need to iterate through entries > was this considered and dismissed? (Ludwig)
          // ec.quantifications.fill(0);
          for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
            ec.quantifications[i] = 0;
          }
          ec.isDisplayed = true;
          this.ecMap.set(ec.ecId, ec);
        });
        experimentData.annotationData.kos.forEach((ko: KO) => {
          ko.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
          for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
            ko.quantifications[i] = 0;
          }
          ko.isDisplayed = true;
          this.koMap.set(ko.koId, ko);
        });
        console.log(experimentData);
        this.flattenTaxonomy(experimentData.annotationData.taxRoot).forEach((taxonomy: Taxonomy) => {
          taxonomy.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
          for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
            taxonomy.quantifications[i] = 0;
          }
          taxonomy.isDisplayed = true;
          this.taxonomyMap.set(taxonomy.taxId, taxonomy);
        });
        this.flattenKeyword(experimentData.annotationData.kwRoot, 0).forEach((kw: Keyword) => {
          kw.quantifications = new Array<number>(this.experimentData.experimentIdList.length);
          for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
            kw.quantifications[i] = 0;
          }
          kw.proteinIds = [];
          kw.isDisplayed = true;
          kw.isExpanded = false;
          this.keywordMap.set(kw.kwId, kw);
        });
        this.proteinMap.forEach((value: Protein, key: string) => {
          value.kwIds.forEach((kwId: string) => {
            this.keywordMap.get(kwId).proteinIds.push(key);
          });
        });
        const changedElements: ChangedElements = new ChangedElements();
        this.spectrumMap.forEach((_keys: Spectrum, key: string) => {
          changedElements.spectrumIds.add(key);
        });
        this.recalculateQuantification(changedElements);
        returnValue.next(true);
      },
      error: () => {
        // TODO: More error handling?
        returnValue.next(false);
      }
    });
    return returnValue.asObservable();
  }

  public flattenTaxonomy(node: Taxonomy, level: number = 0, isReset?: boolean): Taxonomy[] {
    node.level = level;
    node.expandable = node.children && node.children.length > 0;
    if(!isReset) { // this is useful when resetting from filteredData to non-filtered data while keeping expansion status
      node.isExpanded = false; // default collapsed
    }
    let flatNodes: Taxonomy[] = [node];

    //if taxonomyMap has been seeded, check for visibility an update node accordingly
    if (this.taxonomyMap.has(node.taxId)) {
      node.isDisplayed = this.taxonomyMap.get(node.taxId).isDisplayed;
    }

    if (node.children && node.children.length > 0) {
      node.children.forEach(child => {
        // update children if parent is not displayed
        if (!node.isDisplayed) {
          child.isDisplayed = false;
        }
        flatNodes = flatNodes.concat(this.flattenTaxonomy(child, level + 1));
      });
    }

    return flatNodes;
  }

  /**
   * recursive function that adds levels to each taxNode in the nested tree-structure reveived from the back-end
   * also keeps track of lineage and adds it to leafNodes
   * @param node node from which the process is started
   * @param taxonomyFlat
   * @param lineage
   * @param level
   * @returns
   */
  public flattenTaxonomyAndAddLineage(node: Taxonomy, taxonomyFlat: Taxonomy[], lineage: Taxonomy[], level: number): Taxonomy[] {
    level = level + 1;
    node.level = level;
    node.isSelected = false;
    //if taxonomyMap has been seeded, check for visibility an update node accordingly
    if (this.taxonomyMap.has(node.taxId)) {
      node.isDisplayed = this.taxonomyMap.get(node.taxId).isDisplayed;
    }
    if (node.children.length === 0) {
      node.expandable = false;
      // finalize this lineage
      node.superkingdom = 'Unspecified';
      node.phylum = 'Unspecified';
      node.class = 'Unspecified';
      node.order = 'Unspecified';
      node.family = 'Unspecified';
      node.genus = 'Unspecified';
      node.species = 'Unspecified';
      node.strain = 'Unspecified';
      lineage.forEach((taxInLineage: Taxonomy) => {
        //"no rank""superkingdom""kingdom""phylum""class""order""family""genus""species"
        switch (taxInLineage.rank) {
          case 'superkingdom':
            node.superkingdom = taxInLineage.scientificname;
            break;
          case 'phylum':
            node.phylum = taxInLineage.scientificname;
            break;
          case 'class':
            node.class = taxInLineage.scientificname;
            break;
          case 'order':
            node.order = taxInLineage.scientificname;
            break;
          case 'family':
            node.family = taxInLineage.scientificname;
            break;
          case 'genus':
            node.genus = taxInLineage.scientificname;
            break;
          case 'species':
            node.species = taxInLineage.scientificname;
            break;
          case 'strain':
            node.strain = taxInLineage.scientificname;
            break;
          default:
            break;
        }
      });
      switch (node.rank) {
        case 'superkingdom':
          node.superkingdom = node.scientificname;
          break;
        case 'phylum':
          node.phylum = node.scientificname;
          break;
        case 'class':
          node.class = node.scientificname;
          break;
        case 'order':
          node.order = node.scientificname;
          break;
        case 'family':
          node.family = node.scientificname;
          break;
        case 'genus':
          node.genus = node.scientificname;
          break;
        case 'species':
          node.species = node.scientificname;
          break;
        case 'strain':
          node.strain = node.scientificname;
          break;
        default:
          break;
      }
      taxonomyFlat.push(node);
    } else {
      node.expandable = true;
      lineage.push(node);
      node.children.forEach((taxChild: Taxonomy) => {
        taxonomyFlat = this.flattenTaxonomyAndAddLineage(taxChild, taxonomyFlat, lineage, level);
      });
      lineage.pop();
    }
    level = level - 1;
    return taxonomyFlat;
  }


  private flattenKeyword(keyword: Keyword, level: number = 0): Keyword[] {
    keyword.level = level;
    keyword.expandable = (keyword.children && keyword.children.length > 0);
    keyword.isExpanded = false; // default to collapsed
    let keywordList: Keyword[] = [keyword];
    if (keyword.children && keyword.children.length > 0) {
      keyword.children.forEach(child => {
        keywordList = keywordList.concat(this.flattenKeyword(child, level + 1));
      });
    }
    return keywordList;
  }

  public changeAndPropagateECVisibility(ecId: string, isVisible: boolean): void {
    if (this.ecMap.get(ecId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.ecMap.get(ecId).isDisplayed = isVisible;
      changedElements = this.propagateECToProteinVisibility(ecId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagateKOVisibility(koId: string, isVisible: boolean): void {
    if (this.koMap.get(koId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.koMap.get(koId).isDisplayed = isVisible;
      changedElements = this.propagateKOToProteinVisibility(koId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagateKeywordVisibility(keywordId: string, isVisible: boolean): void {
    if (this.keywordMap.get(keywordId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.keywordMap.get(keywordId).isDisplayed = isVisible;
      changedElements = this.propagateKeywordToProteinVisibility(keywordId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagateTaxonomyVisibility(taxonomyId: string, isVisible: boolean): void {
    if (this.taxonomyMap.get(taxonomyId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.taxonomyMap.get(taxonomyId).isDisplayed = isVisible;
      changedElements = this.propagateTaxonomyToProteinVisibility(taxonomyId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagateGroupVisibility(groupId: string, isVisible: boolean): void {
    if (this.proteinGroupMap.get(groupId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.proteinGroupMap.get(groupId).isDisplayed = isVisible;
      changedElements = this.propagateGroupToSubgroupVisibility(groupId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagateSubgroupVisibility(subgroupId: string, isVisible: boolean): void {
    if (this.proteinSubGroupMap.get(subgroupId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.proteinSubGroupMap.get(subgroupId).isDisplayed = isVisible;
      changedElements = this.propagateSubgroupToGroupVisibility(subgroupId, isVisible, changedElements);
      changedElements = this.propagateSubgroupToProteinVisibility(subgroupId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagateProteinVisibility(proteinId: string, isVisible: boolean): void {
    if (this.proteinMap.get(proteinId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.proteinMap.get(proteinId).isDisplayed = isVisible;
      changedElements = this.propagateProteinToPeptideVisibility(proteinId, isVisible, changedElements);
      changedElements = this.propagateProteinToSubgroupVisibility(proteinId, isVisible, changedElements);
      // changedElements = this.propagateProteinToECVisibility(proteinId, isVisible, changedElements);
      // changedElements = this.propagateProteinToKOVisibility(proteinId, isVisible, changedElements);
      // changedElements = this.propagateProteinToKeywordVisibility(proteinId, isVisible, changedElements);
      // changedElements = this.propagateProteinToTaxonomyVisibility(proteinId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagatePeptideVisibility(peptideId: string, isVisible: boolean): void {
    if (this.peptideMap.get(peptideId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.peptideMap.get(peptideId).isDisplayed = isVisible;
      changedElements = this.propagatePeptideToProteinVisibility(peptideId, isVisible, changedElements);
      changedElements = this.propagatePeptideToSpectrumVisibility(peptideId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  public changeAndPropagateSpectrumnVisibility(spectrumId: string, isVisible: boolean): void {
    if (this.spectrumMap.get(spectrumId).isDisplayed !== isVisible) {
      let changedElements: ChangedElements = new ChangedElements();
      this.spectrumMap.get(spectrumId).isDisplayed = isVisible;
      changedElements = this.propagateSpectrumToPeptideVisibility(spectrumId, isVisible, changedElements);
      this.recalculateQuantification(changedElements);
      this.detailViewDataChange.next(true);
    }
  }

  private recalculateQuantification(changedElements: ChangedElements) {
    // SPECTRUM
    changedElements.spectrumIds.forEach((spectrumId: string) => {
      this.spectrumMap.get(spectrumId).psmIds.forEach((psmId: string) => {
        changedElements.peptideIds.add(this.psmpMap.get(psmId).peptideId);
      })
    });
    // PEPTIDE
    changedElements.peptideIds.forEach((peptideId: string) => {
      const peptide: Peptide = this.peptideMap.get(peptideId);
      const experiment2spectrumMap: Map<string, Spectrum[]> = new Map();
      peptide.psmIds.forEach((psmId: string) => {
        const psm: PSM = this.psmpMap.get(psmId);
        if (!experiment2spectrumMap.has(psm.expId)) {
          experiment2spectrumMap.set(psm.expId, []);
        }
        experiment2spectrumMap.get(psm.expId).push(this.spectrumMap.get(psm.spectrumId));
      });
      experiment2spectrumMap.forEach((value: Spectrum[], key: string) => {
        const experimentIndex = this.experimentId2Index.get(key);
        peptide.quantifications[experimentIndex] = 0;
        value.forEach((spectrum: Spectrum) => {
          const peptideIdSet: Set<string> = new Set();
          spectrum.psmIds.forEach((psmId: string) => {
            if (this.peptideMap.get(peptideId).isDisplayed) {
              peptideIdSet.add(this.psmpMap.get(psmId).peptideId);
            }
          });
          if (spectrum.isDisplayed) {
            peptide.quantifications[experimentIndex] += spectrum.spectrumQuant / peptideIdSet.size;
          }
        });
      });
      this.pep2prot.get(peptideId).forEach((proteinId: string) => {
        changedElements.proteinIds.add(proteinId);
      });
    });
    // PROTEIN
    changedElements.proteinIds.forEach((proteinId: string) => {
      const protein: Protein = this.proteinMap.get(proteinId);
      for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
        protein.quantifications[i] = 0;
      }
      protein.peptideIds.forEach((pepId: string) => {
        const pep2ProtSet: Set<string> = new Set();
        this.pep2prot.get(pepId).forEach((protId: string) => {
          if (this.proteinMap.get(protId).isDisplayed) {
            pep2ProtSet.add(protId);
          }
        });
        for (let i = 0; i < protein.quantifications.length; i++) {
          protein.quantifications[i] += this.peptideMap.get(pepId).quantifications[i] / pep2ProtSet.size;
        }
      });
      changedElements.subgroupIds.add(this.protein2SubgroupMap.get(proteinId));
      changedElements.taxonomyIds.add(this.proteinMap.get(proteinId).taxId);
      this.proteinMap.get(proteinId).ecIds.forEach((ecId: string) => {
        changedElements.ecIds.add(ecId);
      });
      this.proteinMap.get(proteinId).koIds.forEach((koId: string) => {
        changedElements.koIds.add(koId);
      });
      this.proteinMap.get(proteinId).kwIds.forEach((kwId: string) => {
        changedElements.keywordIds.add(kwId);
      });
    });
    // SUBGROUPS
    changedElements.subgroupIds.forEach((subgroupId: string) => {
      const subgroup: ProteinSubgroup = this.proteinSubGroupMap.get(subgroupId);
      for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
        subgroup.quantifications[i] = 0;
      }
      subgroup.proteinIds.forEach((proteinId: string) => {
        if (this.proteinMap.get(proteinId).isDisplayed) {
          for (let i = 0; i < subgroup.quantifications.length; i++) {
            subgroup.quantifications[i] += this.proteinMap.get(proteinId).quantifications[i];
          }
        }
      });
      changedElements.groupIds.add(this.getGroupIdFromSubgroupId(subgroup.proteinSubgroupId));
    });
    // GROUPS
    changedElements.groupIds.forEach((groupId: string) => {
      const group: ProteinMainGroup = this.proteinGroupMap.get(groupId);
      for (let i = 0; i < this.experimentData.experimentIdList.length; i++) {
        group.quantifications[i] = 0;
      }
      group.subgroups.forEach((subgroup: ProteinSubgroup) => {
        if (subgroup.isDisplayed) {
          for (let i = 0; i < subgroup.quantifications.length; i++) {
            group.quantifications[i] += subgroup.quantifications[i]
          }
        }
      });
    });
    // TAXONOMY
    // Update directly affected taxonomyEntries
    changedElements.taxonomyIds.forEach((taxId: string) => {
      const taxNode = this.taxonomyMap.get(taxId);
      if (!taxNode) {
        console.warn("Taxonomy node not found for taxId:", taxId);
        return;
      }
      // Special case: if this is the "root" node, sum over all proteins regardless of taxId.
      // let descendantTaxIds: string[] = [];
      // if (taxNode.taxId === "root") {
      //   this.proteinMap.forEach((protein: Protein) => {
      //     descendantTaxIds.push(protein.taxId);
      //   });
      // } else {
      //   descendantTaxIds = this.getDescendantTaxIds(taxNode);
      // }
      // console.log("Aggregating proteins with taxIds:", descendantTaxIds, "for taxonomy:", taxNode.scientificname);

      // Reset quantification values for all experiments
      taxNode.quantifications.fill(0);

      // update quantifications for affected taxonomy
      this.tax2prot.get(taxId).forEach((protId: string) => {
        let protein: Protein = this.proteinMap.get(protId);
        for (let i = 0; i < protein.quantifications.length; i++) {
          taxNode.quantifications[i] += protein.quantifications[i];
        }
      });
      // update entry in map with new quantification
      this.taxonomyMap.set(taxId,taxNode);
      console.log("Updated quantifications for taxonomy", taxNode.scientificname, ":", taxNode.quantifications);

      // TODO what was the intend here? (Ludwig)
      // Loop over all proteins and check if their taxId is among the descendant taxIds
      // this.proteinMap.forEach((protein: Protein) => {
      //   if (descendantTaxIds.indexOf(protein.taxId) !== -1) {
      //     // console.log("Found protein", protein.proteinId, "with taxId", protein.taxId, "for taxonomy:", taxNode.scientificname,
      //     // "isDisplayed:", protein.isDisplayed, "quantifications:", protein.quantifications);
      //     // Remove the isDisplayed check if you want to sum all proteins
      //     for (let i = 0; i < protein.quantifications.length; i++) {
      //       taxNode.quantifications[i] += protein.quantifications[i];
      //     }
      //   }
      // });
    });
    // (unfeasible) Update ancestors of directly affected taxonomyEntries > propagates quantification up towards root
    // calculate quantification for whole tree again, update this.taxonomyMap with new values
    let flatTaxonomy: Taxonomy[] = this.flattenTaxonomy(this.experimentData.annotationData.taxRoot).reverse(); // IMPORTANT: bottom-up
    let childLevel: number = flatTaxonomy[flatTaxonomy.length - 1].level; // root level, as array has been reversed
    let level2quant = new Map<number, number[]>();
    let subBranchQuant: number[] = new Array(this.experimentId2Index.size);
    subBranchQuant.fill(0);
    flatTaxonomy.forEach((taxNode: Taxonomy) => {
      if (taxNode.level > childLevel) {
        // current level greater than previous level > new sub-branch
        level2quant.set(childLevel, subBranchQuant);
        subBranchQuant.fill(0);
      }

      for (let i = 0; i < subBranchQuant.length - 1; i++) {
        subBranchQuant[i] += taxNode.quantifications[i];
      }
      if(taxNode.level != childLevel) {
        taxNode.quantifications = subBranchQuant;
      }
      childLevel = taxNode.level;

      // this is likely the cause of the problem...
      if (level2quant.has(taxNode.level)) {
        // current level already represented in map > siblings in same branch > add other branch ending on the same level to quantification
        for (let i = 0; i < subBranchQuant.length - 1; i++) {
          subBranchQuant[i] += level2quant.get(taxNode.level)[i];
          level2quant.delete(taxNode.level);
        } 
      }
    });

    // changedElements.ecIds
    changedElements.ecIds.forEach((ecId: string) => {
      const ec: EC = this.ecMap.get(ecId);
      ec.proteinIds.forEach((pId: string) => {
        const protein: Protein = this.proteinMap.get(pId);
        for (let i = 0; i < protein.quantifications.length; i++) {
          ec.quantifications[i] += protein.quantifications[i];
        }
      });
    });
    // changedElements.koIds
    changedElements.koIds.forEach((koId: string) => {
      const ko: KO = this.koMap.get(koId);
      ko.proteinIds.forEach((pId: string) => {
        const protein: Protein = this.proteinMap.get(pId);
        for (let i = 0; i < protein.quantifications.length; i++) {
          ko.quantifications[i] += protein.quantifications[i];
        }
      });
    });
    // UNIPROT KEYWORDS
    changedElements.keywordIds.forEach((kwId: string) => {
      const kw: Keyword = this.keywordMap.get(kwId);
      console.log(kw);
      kw.proteinIds.forEach((pId: string) => {
        const protein: Protein = this.proteinMap.get(pId);
        for (let i = 0; i < protein.quantifications.length; i++) {
          kw.quantifications[i] += protein.quantifications[i];
        }
      });
    });
  }

  private getAllProteinIds(tax: Taxonomy): string[] {
    let ids: string[] = [];
    // If this node already has proteinIds defined, use them
    if (tax.proteinIds && Array.isArray(tax.proteinIds)) {
      ids.push(...tax.proteinIds);
    }
    // Recursively add proteinIds from children
    if (tax.children && tax.children.length > 0) {
      tax.children.forEach(child => {
        ids.push(...this.getAllProteinIds(child));
      });
    }
    return ids;
  }

  private getDescendantTaxIds(tax: Taxonomy): string[] {
    let ids: string[] = [tax.taxId];
    if (tax.children && tax.children.length > 0) {
      tax.children.forEach(child => {
        ids.push(...this.getDescendantTaxIds(child));
      });
    }
    return ids;
  }


  private propagateProteinToECVisibility(proteinId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.proteinIds.add(proteinId);
    const changedEcList: EC[] = [];
    const ecIdSet: Set<string> = new Set<string>();
    this.proteinMap.get(proteinId).ecIds.forEach((ecId: string) => {
      if (!ecIdSet.has(ecId)) {
        ecIdSet.add(ecId);
        const ec: EC = this.ecMap.get(ecId);
        if (isVisible && !ec.isDisplayed) {
          ec.isDisplayed = true;
          changedEcList.push(ec);
        }
        if (!isVisible) {
          let ecRemainsVisible: boolean = false;
            this.proteinMap.get(proteinId).ecIds.forEach(ecId => {
              if (this.ecMap.get(ecId).isDisplayed) {
                ecRemainsVisible = true;
              }
            });
            ecRemainsVisible = true;
          if (!ecRemainsVisible) {
            ec.isDisplayed = false;
            changedEcList.push(ec);
          }
        }
      }
    });
    return changedElements;
  }

// propagateKOToProteinVisibility
  private propagateECToProteinVisibility(ecId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.ecIds.add(ecId);
    const changedProteinList: Protein[] = [];
    const protIdSet: Set<string> = new Set<string>();
    this.ecMap.get(ecId).proteinIds.forEach((proteinId: string) => {
      if (!protIdSet.has(proteinId)) {
        protIdSet.add(proteinId);
        const prot: Protein = this.proteinMap.get(proteinId);
        if (isVisible && !prot.isDisplayed) {
          prot.isDisplayed = true;
          changedProteinList.push(prot);
        }
        if (!isVisible) {
          let proteinRemainsVisible: boolean = false;
            this.ecMap.get(ecId).proteinIds.forEach(proteinId => {
              if (this.proteinMap.get(proteinId).isDisplayed) {
                proteinRemainsVisible = true;
              }
            });
            proteinRemainsVisible = true;
          if (!proteinRemainsVisible) {
            prot.isDisplayed = false;
            changedProteinList.push(prot);
          }
        }
      }
    });
    // call propagateProteinToPeptideVisibility, propagateProteinToSubgroupVisibility, propagateProteinToTaxonomyVisibility, propagateProteinToKeywordVisibility, propagateProteinToKOVisibility
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToPeptideVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToSubgroupVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToTaxonomyVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToKeywordVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToKOVisibility(prot.proteinId, isVisible, changedElements);
    });
    return changedElements;
  }

  private propagateProteinToKOVisibility(proteinId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.proteinIds.add(proteinId);
    const changedKoList: KO[] = [];
    const koIdSet: Set<string> = new Set<string>();
    this.proteinMap.get(proteinId).koIds.forEach((koId: string) => {
      if (!koIdSet.has(koId)) {
        koIdSet.add(koId);
        const ko: KO = this.koMap.get(koId);
        if (isVisible && !ko.isDisplayed) {
          ko.isDisplayed = true;
          changedKoList.push(ko);
        }
        if (!isVisible) {
          let koRemainsVisible: boolean = false;
            this.proteinMap.get(proteinId).koIds.forEach(koId => {
              if (this.koMap.get(koId).isDisplayed) {
                koRemainsVisible = true;
              }
            });
            koRemainsVisible = true;
          if (!koRemainsVisible) {
            ko.isDisplayed = false;
            changedKoList.push(ko);
          }
        }
      }
    });
    return changedElements;
     // no call to other propagateFunction
  }

  private propagateKOToProteinVisibility(koId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.koIds.add(koId);
    const changedProteinList: Protein[] = [];
    const protIdSet: Set<string> = new Set<string>();
    this.koMap.get(koId).proteinIds.forEach((proteinId: string) => {
      if (!protIdSet.has(proteinId)) {
        protIdSet.add(proteinId);
        const prot: Protein = this.proteinMap.get(proteinId);
        if (isVisible && !prot.isDisplayed) {
          prot.isDisplayed = true;
          changedProteinList.push(prot);
        }
        if (!isVisible) {
          let proteinRemainsVisible: boolean = false;
            this.koMap.get(koId).proteinIds.forEach(proteinId => {
              if (this.proteinMap.get(proteinId).isDisplayed) {
                proteinRemainsVisible = true;
              }
            });
            proteinRemainsVisible = true;
          if (!proteinRemainsVisible) {
            prot.isDisplayed = false;
            changedProteinList.push(prot);
          }
        }
      }
    });
      // call propagateProteinToPeptideVisibility, propagateProteinToSubgroupVisibility, propagateProteinToTaxonomyVisibility, propagateProteinToKeywordVisibility, propagateProteinToECVisibility
      changedProteinList.forEach((prot: Protein) => {
        changedElements = this.propagateProteinToPeptideVisibility(prot.proteinId, isVisible, changedElements);
      });
      changedProteinList.forEach((prot: Protein) => {
        changedElements = this.propagateProteinToSubgroupVisibility(prot.proteinId, isVisible, changedElements);
      });
      changedProteinList.forEach((prot: Protein) => {
        changedElements = this.propagateProteinToTaxonomyVisibility(prot.proteinId, isVisible, changedElements);
      });
      changedProteinList.forEach((prot: Protein) => {
        changedElements = this.propagateProteinToKeywordVisibility(prot.proteinId, isVisible, changedElements);
      });
      changedProteinList.forEach((prot: Protein) => {
        changedElements = this.propagateProteinToECVisibility(prot.proteinId, isVisible, changedElements);
      });
      return changedElements;
  }


  private propagateProteinToKeywordVisibility(proteinId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.proteinIds.add(proteinId);
    const changedKeywordList: Keyword[] = [];
    const keywordIdSet: Set<string> = new Set<string>();
    this.proteinMap.get(proteinId).kwIds.forEach((keywordId: string) => {
      if (!keywordIdSet.has(keywordId)) {
        keywordIdSet.add(keywordId);
        const key: Keyword = this.keywordMap.get(keywordId);
        if (isVisible && !key.isDisplayed) {
          key.isDisplayed = true;
          changedKeywordList.push(key);
        }
        if (!isVisible) {
          let keywordsRemainsVisible: boolean = false;
            this.proteinMap.get(proteinId).kwIds.forEach(keywordId => {
              if (this.keywordMap.get(keywordId).isDisplayed) {
                keywordsRemainsVisible = true;
              }
            });
            keywordsRemainsVisible = true;
          if (!keywordsRemainsVisible) {
            key.isDisplayed = false;
            changedKeywordList.push(key);
          }
        }
      }
    });
    return changedElements;
     // no call to other propagateFunction
  }

  private propagateKeywordToProteinVisibility(keywordId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.keywordIds.add(keywordId);
    const changedProteinList: Protein[] = [];
    const protIdSet: Set<string> = new Set<string>();
    this.keywordMap.get(keywordId).proteinIds.forEach((proteinId: string) => {
      if (!protIdSet.has(proteinId)) {
        protIdSet.add(proteinId);
        const prot: Protein = this.proteinMap.get(proteinId);
        if (isVisible && !prot.isDisplayed) {
          prot.isDisplayed = true;
          changedProteinList.push(prot);
        }
        if (!isVisible) {
          let proteinRemainsVisible: boolean = false;
            this.keywordMap.get(keywordId).proteinIds.forEach(proteinId => {
              if (this.proteinMap.get(proteinId).isDisplayed) {
                proteinRemainsVisible = true;
              }
            });
            proteinRemainsVisible = true;
          if (!proteinRemainsVisible) {
            prot.isDisplayed = false;
            changedProteinList.push(prot);
          }
        }
      }
    });
     // call propagateProteinToPeptideVisibility, propagateProteinToSubgroupVisibility, propagateProteinToTaxonomyVisibility, propagateProteinToKOVisibility, propagateProteinToECVisibility
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToPeptideVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToSubgroupVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToTaxonomyVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToKOVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToECVisibility(prot.proteinId, isVisible, changedElements);
    });
    return changedElements;
  }

  private propagateProteinToTaxonomyVisibility(proteinId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.proteinIds.add(proteinId);
    const changedTaxonomyList: Taxonomy[] = [];
    const taxIdSet: Set<string> = new Set<string>();
    const taxId = this.proteinMap.get(proteinId).taxId;
      if (!taxIdSet.has(taxId)) {
        taxIdSet.add(taxId);
        const tax: Taxonomy = this.taxonomyMap.get(taxId);
        if (isVisible && !tax.isDisplayed) {
          tax.isDisplayed = true;
          changedTaxonomyList.push(tax);
        }
        if (!isVisible) {
          let taxRemainsVisible: boolean = false;
          if (this.taxonomyMap.get(taxId).isDisplayed) {
            taxRemainsVisible = true;
          }
          taxRemainsVisible = true;
          if (!taxRemainsVisible) {
            tax.isDisplayed = false;
            changedTaxonomyList.push(tax);
          }
      }
    return changedElements;
     // no call to other propagateFunction
  }
}


  private propagateTaxonomyToProteinVisibility(taxonomyId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.taxonomyIds.add(taxonomyId);
    const changedProteinList: Protein[] = [];
    const protIdSet: Set<string> = new Set<string>();
    if (this.tax2prot.has(taxonomyId)) {
      this.tax2prot.get(taxonomyId).forEach((proteinId: string) => {
        if (!protIdSet.has(proteinId)) {
          protIdSet.add(proteinId);
          const prot: Protein = this.proteinMap.get(proteinId);
          if (isVisible && !prot.isDisplayed) {
            prot.isDisplayed = true;
            changedProteinList.push(prot);
          }
          if (!isVisible) {
            let proteinRemainsVisible: boolean = false;
            // what is this supposed to do? seems like it always defaults to proteinRemainsVisible... (Ludwig)
            this.tax2prot.get(taxonomyId).forEach((proteinId: string) => {
              if (this.proteinMap.get(proteinId).isDisplayed) {
                proteinRemainsVisible = true;
              }
            });
            proteinRemainsVisible = true;
            if (!proteinRemainsVisible) {
              prot.isDisplayed = false;
              changedProteinList.push(prot);
            }
          }
        }
      });
    }
    // call propagateProteinToPeptideVisibility, propagateProteinToSubgroupVisibility, propagateProteinToKeywordVisibility, propagateProteinToKOVisibility, propagateProteinToECVisibility
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToPeptideVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToSubgroupVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToKeywordVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToKOVisibility(prot.proteinId, isVisible, changedElements);
    });
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToECVisibility(prot.proteinId, isVisible, changedElements);
    });
    return changedElements;
  }

  private propagateGroupToSubgroupVisibility(groupId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.groupIds.add(groupId);
    const changedSubgroupList: ProteinSubgroup[] = [];
    const subgroupIdSet: Set<string> = new Set<string>();
    this.proteinGroupMap.get(groupId).subgroups.forEach((subgroup: ProteinSubgroup) => {
      if (!subgroupIdSet.has(subgroup.proteinSubgroupId)) {
        subgroupIdSet.add(subgroup.proteinSubgroupId);
        const sub: ProteinSubgroup = this.proteinSubGroupMap.get(subgroup.proteinSubgroupId);
        if (isVisible && !sub.isDisplayed) {
          sub.isDisplayed = true;
          changedSubgroupList.push(sub);
        }
        if (!isVisible) {
          let subgroupRemainsVisible: boolean = false;
            this.proteinSubGroupMap.get(groupId).proteinIds.forEach(proteinSubgroupId => {
              if (this.proteinSubGroupMap.get(proteinSubgroupId).isDisplayed) {
                subgroupRemainsVisible = true;
              }
            });
            subgroupRemainsVisible = true;

          if (!subgroupRemainsVisible) {
            sub.isDisplayed = false;
            changedSubgroupList.push(sub);
          }
        }
      }
    });
    //call propagateSubgroupToProteinVisibility
    changedSubgroupList.forEach((sub: ProteinSubgroup) => {
      changedElements = this.propagateSubgroupToProteinVisibility(sub.proteinSubgroupId, isVisible, changedElements);
    });

    return changedElements;
  }

  // DO WE NEED THIS? Or should we integrate this in propagateSubgroupToGroupVisibility
  private getGroupIdFromSubgroupId(subgroupId: string): string {
    const underscoreIndex = subgroupId.indexOf("_");
    return subgroupId.substring(0, underscoreIndex);
  }

  private propagateSubgroupToGroupVisibility(subgroupId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.subgroupIds.add(subgroupId);
    const proteinGroupId = this.getGroupIdFromSubgroupId(subgroupId);
    const group: ProteinMainGroup = this.proteinGroupMap.get(proteinGroupId);
    let groupRemainsVisible: boolean = false;
    group.subgroups.forEach((subgroup: ProteinSubgroup) => {
      changedElements.subgroupIds.add(subgroup.proteinSubgroupId);
      if (subgroup.isDisplayed) {
        groupRemainsVisible = true;
      }
    });
    if (!groupRemainsVisible) {
      group.isDisplayed = false;
      changedElements.groupIds.add(proteinGroupId);
    }
    // no call to other propagateFunction
    return changedElements;
  }


  private propagateSubgroupToProteinVisibility(subgroupId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.subgroupIds.add(subgroupId);
    const changedProteinList: Protein[] = [];
    const changedProteinIdSet: Set<string> = new Set();
    this.proteinSubGroupMap.get(subgroupId).proteinIds.forEach((proteinId: string) => {
      const protein: Protein = this.proteinMap.get(proteinId);
      protein.isDisplayed = isVisible;
      changedProteinIdSet.add(proteinId);
      changedProteinList.push(protein);
    });
    // call propagateProteinToPeptideVisibility, propagateProteinToTaxonomyVisibility, propagateProteinToKeywordVisibility, propagateProteinToKOVisibility, propagateProteinToECVisibility
    changedProteinList.forEach((prot: Protein) => {
      changedElements = this.propagateProteinToPeptideVisibility(prot.proteinId, isVisible, changedElements);
    });
    // changedProteinList.forEach((prot: Protein) => {
    //   changedElements = this.propagateProteinToTaxonomyVisibility(prot.proteinId, isVisible, changedElements);
    // });
    // changedProteinList.forEach((prot: Protein) => {
    //   changedElements = this.propagateProteinToKeywordVisibility(prot.proteinId, isVisible, changedElements);
    // });
    // changedProteinList.forEach((prot: Protein) => {
    //   changedElements = this.propagateProteinToKOVisibility(prot.proteinId, isVisible, changedElements);
    // });
    // changedProteinList.forEach((prot: Protein) => {
    //   changedElements = this.propagateProteinToECVisibility(prot.proteinId, isVisible, changedElements);
    // });
    return changedElements;
  }

  private propagateProteinToSubgroupVisibility(proteinId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.proteinIds.add(proteinId);
    const subgroup: ProteinSubgroup = this.proteinSubGroupMap.get(this.protein2SubgroupMap.get(proteinId));
    if (isVisible && !subgroup.isDisplayed) {
      subgroup.isDisplayed = true;
    }
    if (!isVisible) {
      let subgroupRemainsVisible: boolean = false;
      subgroup.proteinIds.forEach((proteinId: string) => {
        if (this.proteinMap.get(proteinId).isDisplayed) {
          subgroupRemainsVisible = true;
        }
      });
      if (!subgroupRemainsVisible) {
        subgroup.isDisplayed = false;
      }
    }
    // call propagateSubgroupToGroupVisibility
    changedElements = this.propagateSubgroupToGroupVisibility(subgroup.proteinSubgroupId, isVisible, changedElements);
    return changedElements
  }


  private propagateProteinToPeptideVisibility(proteinId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.proteinIds.add(proteinId);
    const changedPeptideList: Peptide[] = [];
    const pepIdSet: Set<string> = new Set<string>();
    this.proteinMap.get(proteinId).peptideIds.forEach((peptideId: string) => {
      if (!pepIdSet.has(peptideId)) {
        pepIdSet.add(peptideId);
        const pep: Peptide = this.peptideMap.get(peptideId);
        if (isVisible && !pep.isDisplayed) {
          pep.isDisplayed = true;
        }
        if (!isVisible) {
          let peptideRemainsVisible: boolean = false;
          this.pep2prot.get(pep.sequenceId).forEach(proteinId => {
            if (this.proteinMap.get(proteinId).isDisplayed) {
              peptideRemainsVisible = true;
            }
          });
          if (!peptideRemainsVisible) {
            pep.isDisplayed = false;
          }
        }
        changedPeptideList.push(pep);
      }
    });
    // call propagatePeptideToSpectrumVisibility
    changedPeptideList.forEach((pep: Peptide) => {
      changedElements = this.propagatePeptideToSpectrumVisibility(pep.sequenceId, isVisible, changedElements);
    });
    return changedElements;
  }

  private propagatePeptideToProteinVisibility(peptideId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
  changedElements.peptideIds.add(peptideId);
  const changedProteinList: Protein[] = [];
  const protIdSet: Set<string> = new Set<string>();
  this.pep2prot.get(peptideId).forEach((proteinId: string) => {
    if (!protIdSet.has(proteinId)) {
      protIdSet.add(proteinId);
      const prot: Protein = this.proteinMap.get(proteinId);
      if (isVisible && !prot.isDisplayed) {
        prot.isDisplayed = true;
        changedProteinList.push(prot);
      }
      if (!isVisible) {
        let proteinRemainsVisible: boolean = false;
        prot.peptideIds.forEach((peptideId: string) => {
          if (this.peptideMap.get(peptideId).isDisplayed) {
            proteinRemainsVisible = true;
          }
        });
        if (!proteinRemainsVisible) {
          prot.isDisplayed = false;
          changedProteinList.push(prot);
        }
      }
    }
  });
  // call propagateProteinToSubgroupVisibility, propagateProteinToTaxonomyVisibility, propagateProteinToKeywordVisibility, propagateProteinToKOVisibility, propagateProteinToECVisibility
  changedProteinList.forEach((prot: Protein) => {
    changedElements = this.propagateProteinToSubgroupVisibility(prot.proteinId, isVisible, changedElements);
  });
  // changedProteinList.forEach((prot: Protein) => {
  //   changedElements = this.propagateProteinToTaxonomyVisibility(prot.proteinId, isVisible, changedElements);
  // });
  // changedProteinList.forEach((prot: Protein) => {
  //   changedElements = this.propagateProteinToKeywordVisibility(prot.proteinId, isVisible, changedElements);
  // });
  // changedProteinList.forEach((prot: Protein) => {
  //   changedElements = this.propagateProteinToKOVisibility(prot.proteinId, isVisible, changedElements);
  // });
  // changedProteinList.forEach((prot: Protein) => {
  //   changedElements = this.propagateProteinToECVisibility(prot.proteinId, isVisible, changedElements);
  // });
  return changedElements;
}

  private propagatePeptideToSpectrumVisibility(peptideID: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.peptideIds.add(peptideID);
    const changedSpectrumList: Spectrum[] = [];
    const specIdSet: Set<string> = new Set<string>();
    this.peptideMap.get(peptideID).psmIds.forEach((psmId: string) => {
      if (!specIdSet.has(this.psmpMap.get(psmId).peptideId)) {
        specIdSet.add(this.psmpMap.get(psmId).spectrumId);
        const spec: Spectrum = this.spectrumMap.get(this.psmpMap.get(psmId).spectrumId);
        // if we change to VISIBLE, just change spectrum visibility
        if (isVisible && !spec.isDisplayed) {
          spec.isDisplayed = true;
          changedSpectrumList.push(spec);
        }
        if (!isVisible) {
          // get all other spectra for this peptide and check if any of them is visible
          let spectraRemainsVisible: boolean = false;
          spec.psmIds.forEach((psmId: string) => {
            if (this.peptideMap.get(this.psmpMap.get(psmId).peptideId).isDisplayed) {
              spectraRemainsVisible = true;
            }
          });
          // if none are visible, change peptide to invisible and add to changed list
          if (!spectraRemainsVisible) {
            spec.isDisplayed = false;
            changedSpectrumList.push(spec);
          }
        }
      }
    });
    return changedElements;
        // no call to other propagateFunction
  }


  private propagateSpectrumToPeptideVisibility(spectrumId: string, isVisible: boolean, changedElements: ChangedElements): ChangedElements {
    changedElements.spectrumIds.add(spectrumId);
    const changedPeptideList: Peptide[] = [];
    const pepIdSet: Set<string> = new Set<string>();
    this.spectrumMap.get(spectrumId).psmIds.forEach((psmId: string) => {
      if (!pepIdSet.has(this.psmpMap.get(psmId).peptideId)) {
        pepIdSet.add(this.psmpMap.get(psmId).peptideId);
        const pep: Peptide = this.peptideMap.get(this.psmpMap.get(psmId).peptideId);
        // if we change to VISIBLE, just change peptide visibility
        if (isVisible && !pep.isDisplayed) {
          pep.isDisplayed = true;
          changedPeptideList.push(pep);
        }
        if (!isVisible) {
          // get all other spectra for this peptide and check if any of them is visible
          let peptideRemainsVisible: boolean = false;
          pep.psmIds.forEach((psmId: string) => {
            if (this.spectrumMap.get(this.psmpMap.get(psmId).spectrumId).isDisplayed) {
              peptideRemainsVisible = true;
            }
          });
          // if none are visible, change peptide to invisible and add to changed list
          if (!peptideRemainsVisible) {
            pep.isDisplayed = false;
            changedPeptideList.push(pep);
          }
        }
      }
    });
    changedPeptideList.forEach((pep: Peptide) => {
      changedElements = this.propagatePeptideToProteinVisibility(pep.sequenceId, isVisible, changedElements);
    });
    return changedElements;
  }

  public requestSequence(): void {
    if (this.selectedProteinId !== '') {
      const params: HttpParams = new HttpParams(
        {
          fromObject: {
            proteinid: this.selectedProteinId,
          }
        });
      this.requestingProteinSequence = true;
      this.httpClientService.getObject<ProteinSequence>(this.addressService.getEndpoint(Endpoints.GET_PROTEIN_SEQUENCE), params).subscribe({
        next: (proteinSequence) => {
          this.proteinSequenceData.next(proteinSequence);
          this.requestingProteinSequence = false;
        },
        error: () => {
          this.proteinSequenceData.next({ proteinID: Math.random().toString(36).substring(7), sequence: Math.random().toString(36).substring(7) });
          console.log('ERRRORR...');
          this.requestingProteinSequence = false;
        }
      });
    }
  }

  public requestSpectrum() {
    // this.requestingSpectrum.next(true);
    if (this.selectedPsmId !== '') {
      const params: HttpParams = new HttpParams(
        {
          fromObject: {
            experimentid: this.experimentData.experimentIdList[0],
            spectrumid: this.selectedSpectrumId,
            pepseq: this.psmpMap.get(this.selectedPsmId).peptideId,
          }
        });
      this.httpClientService.getObject<Spectrum>(this.addressService.getEndpoint(Endpoints.GET_SPECTRUMDATA), params).subscribe({
        next: (spectrumObj: Spectrum) => {
          this.spectrumDataObject$.next(spectrumObj);
          // this.requestingSpectrum.next(false);
        },
        error: () => {
          console.log("requestSpectrum for experiment + this.expID.value + and spectrum " + this.selectedPsm.value.spectrumId + "failed!");
        }
      })
    }
  }

  // TODO ???
  private setPeptidesForSelectedProtein(): void {
    const peptides: Peptide[] = [];
    // if (this.selectedProteinGroup.value.proteinIds !== null) {
    //   for (let index: number = 0; index < this.selectedProteinGroup.value.proteinIds.length; index++) {
    //     const protein: Protein = null; // this.selectedProteinGroup.value.proteinIds[index];
    //     protein.peptideIds.forEach((pepid: string) => {

    //     });
    //   }
    // } else {

    // }
    this.peptidesForSelectedProtein.next(peptides);
  //   this.peptidesForSelectedProtein.next(this.selectedProteinGroup.value.peptideList.filter(
  //     peptide => this.selectedProtein.value.peptideNodes.includes(peptide.sequenceID)));
  }

  private setPsmsForSelectedPeptide(): void {
    // this.psmsForSelectedPeptide.next(this.selectedProteinGroup.value..filter(
    //   psm => psm.peptideID === this.selectedPeptide.value.sequenceId));
  }

  private saveFile(fileName: string, fileContent, fileType): void {
    const file = new Blob([fileContent], { type: fileType });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(file);
    link.download = fileName;
    link.click();
    link.remove();
  }

  public getAnnotationQuantification(proteinIDs: string[], experimentID: string): number {
    let quant: number = 0;
    proteinIDs.map(proteinID => {
      // there was a taxonomyEntry with a proteinID which didn't seem to belong to any proteinGroup, so this stays until it no longer occurs
      if (!this.proteinID2proteinGroupIDMap$.value.get(proteinID)) {
        console.log(proteinID);
        console.log(this.proteinID2proteinGroupIDMap$.value.get(proteinID));
      } else {
        let protQuant: number = 0;
        let groups: ProteinMainGroup[] = null; //this.proteinID2proteinGroupIDMap$.value.get(proteinID).map((groupId: string) => this.proteinGroupID2proteinGroupObjectMap$.value.get(groupId));
        // only count protein into quantification if corresponding groups are not hidden
        // TODO

        // get quantification for annotation
        // TODO utilize HashMaps at top to get quantifications

        // OLD
        // let protQuant: number = 0;
        // const groupID: string = this.proteinID2proteinGroupIDMap$.value.get(proteinID)[0]; // why only use the first group here? was this another 'quick'-fix? (Ludwig)
        // const group: ProteinGroup = this.proteinGroupID2proteinGroupObjectMap$.value.get(groupID);
        // // only count proteins if their proteinGroups are not hidden
        // if (group.displayed) {
        //   const pepIDs: string[] = group.proteinList.filter(protein => protein.proteinID == proteinID)[0].peptideNodes; // there is only exactly one protein with the fitting ID
        //   // iterate through peptideIDs
        //   pepIDs.map(pepID => {
        //     // get parentGroup, filter for psms listing current pepID, create a set of spectrumIDs so there are no duplicates
        //     let set: Set<string> = new Set(this.proteinGroupID2proteinGroupObjectMap$.value
        //       .get(group.parentProteinGroupID)
        //       .psmList.filter(psm => psm.peptideID == pepID)
        //       .map(psm => psm.spectrumID));
        //     protQuant += set.size;
        //   })
        //   quant += protQuant;
        // }
      }
    })
    return parseFloat(quant.toFixed(2));
  }

  public emptySpectrumData(): void {
    this.spectrumDataObject$.next({
      spectrumId: '',
      spectrumQuant: 0,
      psmIds: [],
    })
  }

  public highlightIfSelected(row: ProteinMainGroup | Protein | Peptide | PSM): boolean {

    if ('proteinGroupID' in row) {
      return null; //this.selectedProteinGroup.value && row.proteinGroupID === this.selectedProteinGroup.value.proteinGroupId;
    }

    if ('proteinID' in row) {
      return this.selectedProtein.value && row.proteinID === this.selectedProtein.value.proteinId;
    }

    if ('sequenceID' in row) {
      return this.selectedPeptide.value && row.sequenceID === this.selectedPeptide.value.sequenceId;
    }

    if ('psmID' in row) {
      return this.selectedPsm.value && row.psmID === this.selectedPsm.value.psmId;
    }

    return false;
  }

  // public resetCompleteSelection(): void {
  //   this.
  // }

  /**
   * Uses currently selected experimentID to set BehaviorSubjects with corresponding data
   * should be called after experiment-page changes
   */
  public setMPAData(): void {
    // // PREPARATION: HashMaps, subjects, ...
    // const proteinGroupIDMap = new Map<string, ProteinGroup>();
    // const proteinID2Protein = new Map<string, Protein>();
    // const peptideID2PSMs = new Map<string,PSM[]>();
    // const proteinGroupID2PSMs = new Map<string, PSM[]>();
    // const proteinID2proteinGroupIDMap = new Map<string,string[]>();
    // let experimentIDs: string[] = [];
    // this.experimentDataMap.get(this.expID.value).proteinData.proteins.forEach((protein: Protein) => {
    //   proteinID2Protein.set(protein.proteinId, protein);
    // });
    // this.experimentDataMap.get(this.expID.value).proteinData.psms.forEach((psm: PSM) => {
    //   if (peptideID2PSMs.has(psm.peptideId)) {
    //     let psms: PSM[] = peptideID2PSMs.get(psm.peptideId);
    //     psms.push(psm);
    //     peptideID2PSMs.set(psm.peptideId, psms);
    //   } else {
    //     peptideID2PSMs.set(psm.peptideId, [psm]);
    //   }
    //   experimentIDs.push(psm.experimentId);
    //   experimentIDs = [...new Set(experimentIDs)]; // this should ensure a unique set of expIDs after every iteration
    // });
    // this.experimentDataMap.get(this.expID.value).proteinData.frontProteinGroups.forEach((parentProteinGroup) => {
    //   proteinGroupIDMap.set(parentProteinGroup.proteinGroupId, parentProteinGroup);
    //   parentProteinGroup.subgroups.map(subGroup => {
    //     proteinGroupIDMap.set(subGroup.proteinGroupId, subGroup)
    //     let pepIds: string[] = [];
    //     subGroup.proteinIds.map((proteinId: string) => {
    //       let groupIDs: string[] = [];
    //       if (proteinID2proteinGroupIDMap.has(proteinId)) {
    //         groupIDs = proteinID2proteinGroupIDMap.get(proteinId);
    //       }
    //       groupIDs.push(subGroup.proteinGroupId);
    //       proteinID2proteinGroupIDMap.set(proteinId, groupIDs);
    //       // get peptideIds for psm retrieval later on
    //       let protein: Protein = proteinID2Protein.get(proteinId);
    //       pepIds = [...protein.peptideIds];
    //     });

    //     // use unique peptideIds to get unique PSMs for current subgroup and put them into the map
    //     // use peptides to get psms for current subgroup and put them into the map
    //     pepIds = [...new Set(pepIds)];
    //     let psms: PSM[] = [];
    //     pepIds.forEach((pepId: string) => {
    //       psms = psms.concat(peptideID2PSMs.get(pepId));
    //     });
    //     psms = [...new Set(psms)];
    //     proteinGroupID2PSMs.set(subGroup.proteinGroupId, psms);
    //   });
    // });

    // this.proteinID2Protein$.next(proteinID2Protein);
    // this.proteinGroupID2PSMs$.next(proteinGroupID2PSMs);
    // this.peptideID2PSMs$.next(peptideID2PSMs);
    // this.proteinGroupID2proteinGroupObjectMap$.next(proteinGroupIDMap);
    // this.proteinID2proteinGroupIDMap$.next(proteinID2proteinGroupIDMap);

    // // QUANTIFICATION OF PROTEINGROUPS
    // this.experimentData.proteinData.frontProteinGroups.forEach((parentProteinGroup: ProteinGroup) => {
    //   // INITIALIZE QUANTIFICATIONS TO 0
    //   parentProteinGroup.quantifications = new Array(experimentIDs.length);
    //   for (let i = 0; i < parentProteinGroup.quantifications.length; i++) {
    //     parentProteinGroup.quantifications[i] = 0;
    //   }
    //   for (let subg of parentProteinGroup.subgroups) {
    //     subg.quantifications = new Array(experimentIDs.length);
    //     for (let i = 0; i < subg.quantifications.length; i++) {
    //       subg.quantifications[i] = 0;
    //     }
    //   }
    //   // QUANTIFICATION FOR SUBGROUPS PER EXPERIMENT
    //   parentProteinGroup.subgroups.forEach((subGroup: ProteinGroup) => {
    //     if (subGroup.isDisplayed) {
    //       // get list of PSMs for current subgroup
    //       let psms: PSM[] = this.proteinGroupID2PSMs$.value.get(subGroup.proteinGroupId);
    //       // get set of unique spectrumIds per experiment for current subgroup
    //       let setOfSpectrumIdsPerExperiment: Set<string>[] = new Array(experimentIDs.length);
    //       experimentIDs.forEach((expId: string) => {
    //         let uniqueSpectrumIds: Set<string> = new Set(psms.filter((psm: PSM) => psm.experimentId == expId).map((psm: PSM) => psm.spectrumId));
    //         setOfSpectrumIdsPerExperiment[experimentIDs.indexOf(expId)] = uniqueSpectrumIds;
    //       });
    //       // go through other subgroups of parentgroup and count where peptides from current subgroup also appear
    //       let counts: number[] = new Array(subGroup.quantifications.length).fill(0);
    //       parentProteinGroup.subgroups.forEach((otherSubgroup: ProteinGroup) => {
    //         if (otherSubgroup.isDisplayed) {
    //           experimentIDs.forEach((expId: string) => {
    //             // get peptideIds per experiment (for current subgroup and currently looked at OTHERsubgroup)
    //             let groupPepIds: Set<string> = new Set(psms.filter((psm: PSM) => psm.experimentId == expId).map((psm: PSM) => psm.peptideId));
    //             let otherGroupPepIds: Set<string> = new Set(this.proteinGroupID2PSMs$.value.get(otherSubgroup.proteinGroupId).filter((psm: PSM) => psm.experimentId == expId).map((psm: PSM) => psm.peptideId));
    //             // get count of shared peptides between both subgroups (for same experimentid)
    //             let sharedPepIds: string[] = [];
    //             otherGroupPepIds.forEach((otherPepId: string) => {
    //               groupPepIds.has(otherPepId) ? sharedPepIds.push(otherPepId) : {};
    //             });
    //             counts[experimentIDs.indexOf(expId)] += sharedPepIds.length;
    //           });
    //         }
    //       });
    //       // calculate actual quantifications for each experiment
    //       counts.forEach((count: number) => {
    //         subGroup.quantifications[counts.indexOf(count)] += setOfSpectrumIdsPerExperiment[counts.indexOf(count)].size / count;
    //       })
    //     };
    //     // ADD QUANTIFICATIONS OF CURRENT SUBGROUP TO PARENTGROUP
    //     for (let i in subGroup.quantifications) {
    //       subGroup.quantifications[i] = parseFloat(subGroup.quantifications[i].toFixed(2));
    //       parentProteinGroup.quantifications[i] += subGroup.quantifications[i];
    //     }
    //   });
    //   // seems odd to round parentGroup quant to different decimal from subgroup (Ludwig)
    //   for (let quant of parentProteinGroup.quantifications) {
    //     quant = parseFloat(quant.toFixed(1));
    //   }
    // })
    // // Taxonomy-Tab
    // // let flatTaxTree = this.experimentData.taxFlattened;
    // let flatTaxTree = null;
    // flatTaxTree.map((taxEntry: Taxonomy) => {
    //   if (taxEntry.proteinIds.length > 0) {
    //     // // get quantification for each experiment (relevance: comparison)
    //     // for (let i in taxEntry.experimentIDs) {
    //     //   taxEntry.quantifications[i] = this.getAnnotationQuantification(taxEntry.proteinIds, taxEntry.experimentIDs[i]);
    //     // }

    //     taxEntry.quantifications = [1];
    //   } else {
    //     // for (let i in taxEntry.experimentIDs){
    //     //   taxEntry.quantifications[i] = 0;
    //     // }
    //   }
    // });

    // // use received quantifications to add up from children to parents, end to start of array
    // // for (let i = flatTaxTree.length - 1; 0 < i; i--) {
    // //   console.log()
    // // }

    // // can this be done directly after receiving data from back-end? consider how to calculate quantification!
    // const flatTaxTableData = this.prepareTaxonomyTableData(flatTaxTree);
    // this.taxonomyFlatTableData.next(flatTaxTableData);

    // // // filter out branches where leafNode.quant == 0 AFTER giving the flatTaxTree to the taxTable, because we want to actually display the hidden taxNodes there (although grayed out)
    // // flatTaxTree = this.filterHierarchicalDataByQuantification(flatTaxTree);
    // this.taxonomyHierarchicalData.next(flatTaxTree);
    // console.log(flatTaxTree);

    // // // Function-Tab
    // // let functionData: KeggFunctionObject[] = this.mpaDataMap.get(this.expID.value).functionObjects;
    // // functionData.map(functionEntry => {
    // //   if (functionEntry.proteinIds && functionEntry.proteinIds.length > 0) {
    // //     functionEntry.quantification = this.getProteinQuantification(functionEntry.proteinIds);
    // //   } else {
    // //     functionEntry.quantification = 0;
    // //   }
    // // })
    // // this.functionData.next(functionData);

    // // // Keyword-Tab
    // let flatKeywordTree = this.experimentData.annotationData.kwFlat;
    // flatKeywordTree.map(keyword => {
    //   if (keyword.proteinIds && keyword.proteinIds.length > 0) {
    //     // TODO: index 0 just for testiong
    //     // keyword.quantifications = this.getAnnotationQuantification(keyword.proteinIds,);
    //   } else {
    //     // keyword.quantifications = 0;
    //   }
    // })
    // flatKeywordTree = this.filterHierarchicalDataByQuantification(flatKeywordTree);
    // this.keywordFlatData.next(flatKeywordTree);
  }

  /**
   * filter out nodes where proteinIDs.length > 0 && quantification == 0 due to hidden proteinGroups, then go bottom to top, rebuilding the array with branches where leafNode.quant > 0
   * @param flatData used for TaxonomyObject[] and KeywordObject[]
   * @returns filteredData where only the branches containing a leafNode with quant > 0 are contained
   */
  private filterHierarchicalDataByQuantification(flatData: any[]): any[] {
    // filterOut then bottomUp
    // could also be done by utilizing array.prototype.reverse() to go through data fully without the need to check up to root afterwards (possibly)
    let filteredData = [];
    let branchData = [];
    // filter out leafNodes with quantification == 0 due to hidden proteinGroups
    let data = flatData.filter(node => {
      if (node.proteinIds.length > 0 && node.quantification == 0) {
        return false;
      } else {
        return true;
      }
    });
    // go through data backwards
    for (let i = data.length - 1; i > 0; i--) {
      let node = data[i];
      branchData.unshift(node);
      // check if level of this node is exactly next node.level + 1. If this isn't the case, then there is a gap in the array due to
      // a node with proteinIDs and quant == 0 that has been filtered out
      // e.g taxNode.level = 8, (removedNode.level = 7), nextNode.level = 6 => remove current branchData, start new branchData with nextNode in next iteration
      if (!(node.level == data[i-1].level + 1)) {
        if (branchData[branchData.length - 1].quantification > 0) {
          filteredData.unshift(...branchData);
        }
        branchData = [];
      }
    }
    // handle last branch up to root => check leafNode (which should be the only node in this with possible proteinIDs and quant > 0)
    branchData.unshift(data[0]);
    if (branchData[branchData.length - 1].quantification > 0) {
      filteredData.unshift(...branchData);
    } else {
      // leafNode.quant == 0, so check filteredData[0] to get the level up to which branchData has to get extracted to complete the path to root
      let neededLevel = filteredData[0].level;
      let i = branchData.length - 1;
      let branchCopy = branchData.slice();
      while (i >= 0) {
        if (branchData[i].level >= neededLevel) {
          branchCopy.pop();
          i--;
        } else {
          filteredData.unshift(...branchCopy);
          break;
        }
      }
    }
    return filteredData;
  }

  // private sortTaxonomyRootNode(node: Taxonomy): void {
  //   node.children.sort((a, b) => a.scientificname.localeCompare(b.scientificname));
  //   node.children.map(child => this.sortTaxonomyRootNode(child));
  // }

  // private prepareProteinFunctionData(kos: KO[], ecs: EC[], list: ProteinFunction[]): void {
  //   kos.forEach((ko: KO) => {
  //     let trfKO = this._KeggFunctionTransformer(ko, null);
  //     list.push(trfKO);
  //   });
  //   ecs.forEach((ec: EC) => {
  //     let trfEC = this._KeggFunctionTransformer(null, ec);
  //     list.push(trfEC);
  //   });
  // }

  // private sortProteinTableData(tableData: ProteinGroup[]): ProteinGroup[] {
  //   let disabledGroups: ProteinGroup[] = [];
  //   let enabledGroups: ProteinGroup[] = [];

  //   tableData.sort((a, b) => {
  //     return parseInt(a.proteinGroupId) - parseInt(b.proteinGroupId);
  //   })
  //   tableData.map(group => {
  //     group.subgroups.sort((a, b) => {
  //       let aString = a.proteinGroupId.split("_");
  //       let bString = b.proteinGroupId.split("_");
  //       let returnValue = parseInt(aString[0]) - parseInt(bString[0]);
  //       if (returnValue == 0) {
  //         returnValue = parseInt(aString[1]) - parseInt(bString[1]);
  //       }
  //       return returnValue;
  //     })
  //     let disabledSubGroups: ProteinGroup[] = [];
  //     let enabledSubGroups: ProteinGroup[] = [];
  //     group.subgroups.map(subgroup => {
  //       subgroup.isDisplayed ? enabledSubGroups.push(subgroup) : disabledSubGroups.push(subgroup);
  //     })
  //     group.subgroups = [...enabledSubGroups, ...disabledSubGroups];
  //     group.isDisplayed ? enabledGroups.push(group) : disabledGroups.push(group);
  //   })
  //   let groups = [...enabledGroups, ...disabledGroups];
  //   return groups;
  // }

  // public prepareTaxonomyTableData(flatTree: Taxonomy[]): TaxonomyTableObject[] {
  //   let sk: string = '';
  //   let ph: string = '';
  //   let cl: string = '';
  //   let ord: string = '';
  //   let fam: string = '';
  //   let genus: string = '';
  //   let species: string = '';
  //   let strain: string = '';
  //   let currentEntry: TaxonomyTableObject = new TaxonomyTableObject();
  //   const map = new Map<string,string[]>(); // put ncbirank, rankNames[]
  //   let tableData: TaxonomyTableObject[] = [];

  //   for (let i in flatTree) {
  //     switch (flatTree[i].rank) {
  //       case NCBIRanks.NO_RANK:
  //         // TODO
  //         break;
  //       case NCBIRanks.SUPERKINGDOM:
  //         sk = flatTree[i].scientificname;
  //         break;
  //       case NCBIRanks.PHYLUM:
  //         ph = flatTree[i].scientificname;
  //         break;
  //       case NCBIRanks.CLASS:
  //         cl = flatTree[i].scientificname;
  //         break;
  //       case NCBIRanks.ORDER:
  //         ord = flatTree[i].scientificname;
  //         break;
  //       case NCBIRanks.FAMILY:
  //         fam = flatTree[i].scientificname;
  //         break;
  //       case NCBIRanks.GENUS:
  //         genus = flatTree[i].scientificname;
  //         break;
  //       case NCBIRanks.SPECIES:
  //         species = flatTree[i].scientificname;
  //         break;
  //       case NCBIRanks.STRAIN:
  //         strain = flatTree[i].scientificname;
  //         break;
  //       default:
  //         // TODO ?
  //         break;
  //     };

  //     if (flatTree[i].rank.toLowerCase() == this.taxonomyMaxRankDisplayed.value.toLowerCase()) {
  //       console.log(flatTree[i].rank.toLowerCase() + ' ' + this.taxonomyMaxRankDisplayed.value.toLowerCase());
  //       switch (this.taxonomyMaxRankDisplayed.value.toLowerCase()) {
  //         case NCBIRanks.STRAIN:
  //           currentEntry.strain = strain;
  //         case NCBIRanks.SPECIES:
  //           currentEntry.species = species;
  //         case NCBIRanks.GENUS:
  //           currentEntry.genus = genus;
  //         case NCBIRanks.FAMILY:
  //           currentEntry.family = fam;
  //         case NCBIRanks.ORDER:
  //           currentEntry.order = ord;
  //         case NCBIRanks.CLASS:
  //           currentEntry.class = cl;
  //         case NCBIRanks.PHYLUM:
  //           currentEntry.phylum = ph;
  //         case NCBIRanks.SUPERKINGDOM:
  //           currentEntry.superkingdom = sk;
  //       };
  //       currentEntry.quantifications = flatTree[i].quantifications;
  //       currentEntry.proteinIds = flatTree[i].proteinIds;
  //       tableData.push(currentEntry);

  //       // clean-up
  //       sk = '';
  //       ph = '';
  //       cl = '';
  //       ord = '';
  //       fam = '';
  //       genus = '';
  //       species = '';
  //       strain = '';
  //       currentEntry = new TaxonomyTableObject();
  //     }
  //   }
  //   return tableData;
  // }

  public onGroupSelection(): void {
    // let proteinTableData: ProteinGroup[] = this.experimentDataMap.get(this.expID.value).proteinGroups;
    // let newProteinTableData: ProteinGroup[] = [];
    // if (this.groupSelection == GroupSelection.SUBGROUPS) {
    //   proteinTableData.map(group => group.proteinSubGroupList.map(subgroup => newProteinTableData.push(subgroup)));
    // }
    // else {
    //   newProteinTableData = proteinTableData.filter(group => group.proteinSubGroupList)
    // }
    // newProteinTableData = this.sortProteinTableData(newProteinTableData);
    // this.proteinTableData.next(newProteinTableData);
    // if (!this.selectedProteinGroup.value) {
    //   this.selectedProteinGroup.next(newProteinTableData[0]);
    // }
  }

  //updates the 'hidden' property of the selected groups in this.mpaData and sends an update to the back-end and this.mpaTableData
  public onToggleDisableGroup(isDisableAction: boolean): void {
    // this.changeAndPropagateGroupVisibility()
    // let groupsToUpdate: ProteinGroupObject[] = [];
    // let mpaData: ProteinGroupDataObject = this.experimentDataMap.get(this.expID.value);
    // mpaData.proteinGroups.map(group => {
    //   if (group.isSelected) {
    //     group.hidden = (isDisableAction) ? true : false;
    //     let strippedGroup = new ProteinGroupObject;
    //     strippedGroup.experimentIDs = group.experimentIDs;
    //     strippedGroup.proteinGroupID = group.proteinGroupID;
    //     strippedGroup.hidden = group.hidden;
    //     strippedGroup.groupType = group.groupType;
    //     groupsToUpdate.push(strippedGroup);
    //   }
    //   group.isSelected = false;
    //   group.proteinSubGroupList.map(subgroup => {
    //     if (subgroup.isSelected) {
    //       subgroup.hidden = (isDisableAction) ? true : false;
    //       let strippedSubGroup = new ProteinGroupObject;
    //       strippedSubGroup.experimentIDs = subgroup.experimentIDs;
    //       strippedSubGroup.proteinGroupID = subgroup.proteinGroupID;
    //       strippedSubGroup.groupType = subgroup.groupType;
    //       strippedSubGroup.hidden = subgroup.hidden;
    //       groupsToUpdate.push(strippedSubGroup);
    //     }
    //     subgroup.isSelected = false;
    //   })
    // })
    // this.experimentDataMap.set(this.expID.value, mpaData);
    // this.setMPAData();
    // this.onGroupSelection();
    // // TODO reintroduce update
    // //this.httpClientService.postObject<ExperimentData, any>(groupsToUpdate, this.addressService.getEndpoint(Endpoints.POST_UPDATE_PROTEIN_GROUPS));
  }

  public downloadProteinTableData(): void {
    // this.httpClientService.postObject<{ experimentID: string, groupSelection: GroupSelection, targetFdr: string }, { message: string; }>({
    //   experimentID: "",
    //   // groupSelection: this.groupSelection,
    //   targetFdr: "0.01",
    //   //targetFdr: this.experimentDataMap.get(this.expID.value).targetFdr.toString(),
    // }, this.addressService.getEndpoint(Endpoints.GET_DOWNLOADPROTEINGROUPS)).subscribe({
    //   next: (json) => {
    //     console.log(json)
    //     this.saveFile("proteinGroupsReport", json.message, "text/csv;charset=utf-8")
    //   },
    //   error: () => {
    //     console.log('ERROR: mpa-table-data.service.downloadProteinTableData')
    //   }
    // });
  }

  public getTargetFdrValue(expID: string): string {
    if (this.experimentId2Index.has(expID)) {
      return "0.01";
      // return this.experimentDataMap.get(expID).targetFdr.toString();
    } else {
      return "not set";
    }
  }
}
