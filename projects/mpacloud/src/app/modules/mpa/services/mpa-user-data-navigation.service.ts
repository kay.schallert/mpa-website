import { Injectable } from "@angular/core";
import { ComparisonStatus, ExperimentObject, ExperimentStatus, NodeType, ProteinDbStatus, UserDataTreeObject } from "../model/user-data-json";
import { Observable, Subject } from "rxjs";
import { AuthService, HttpClientService, MultiFileUploadData, UploadDialogComponent, UploadFile, UploadProgressService } from "shared-lib";
import { Endpoints, MpaWebserverAddressService } from "../../../mpa-webserver-address.service";
import { HttpEventType, HttpParams } from "@angular/common/http";
import { GetDateService } from "./get-date.service";
import { MatDialog } from "@angular/material/dialog";

@Injectable({
  providedIn: 'root',
})
export class MPAUserDataNavigationService {

    public navigateComponentEvent$: Subject<UserDataTreeObject> = new Subject<UserDataTreeObject>();

    public triggerHttpUpdateEvent$: Subject<boolean> = new Subject<boolean>();

    private readonly userdataTreeObjects: UserDataTreeObject[] = [];

    uploadDialogId: string = 'mpaUpload';

    constructor(
      private authService: AuthService,
      private httpClientService: HttpClientService,
      private uploadProgressService: UploadProgressService,
      private mpaWebserverAddressService: MpaWebserverAddressService,
      private currentDateService: GetDateService,
      public dialog: MatDialog,
    ) {
      // the initial call to the server to retrieve the user data
      this.callGetUserData();
      this.authService.loginChange.subscribe(() => {
        this.callGetUserData();
      });
      this.triggerHttpUpdateEvent$.subscribe(() => {
        // if we detect an update event we first post the object
        this.httpClientService.postObject<UserDataTreeObject[], UserDataTreeObject[]>(
          this.userdataTreeObjects,
          this.mpaWebserverAddressService.getEndpoint(Endpoints.UPDATE_USER_DATA),
          new HttpParams(),
        ).subscribe((updatedUserData: UserDataTreeObject[]) => {
          // we need to merge the data we retrieve from the server to preserve the objects everywhere
          const oldNodes = this.getAllNodesForSideNav();
          const newNodes = [];
          updatedUserData.forEach((user: UserDataTreeObject) => {
            this.resursiveListing(user, newNodes);
          });
          oldNodes.forEach((oldNode: UserDataTreeObject) => {
            newNodes.forEach((newNode: UserDataTreeObject) => {
              if (oldNode.id === newNode.id) {
                // the only things that are changed by the backend are these objects
                oldNode.experiment = newNode.experiment;
                oldNode.comparison = newNode.comparison;
                oldNode.proteinDB = newNode.proteinDB;
                oldNode.userId = newNode.userId;
                return;
              }
            });
          });
        });
      });
      // timed updates
      setInterval(() => {
        this.triggerHttpUpdateEvent$.next(true);
      }, 30000);
    }

    private callGetUserData() {
      this.httpClientService
      .getObject<UserDataTreeObject[]>(this.mpaWebserverAddressService.getEndpoint(Endpoints.GET_USER_DATA)
      )
      .subscribe((newUserDataTreeObjects: UserDataTreeObject[]) => {
        this.userdataTreeObjects.splice(0);
        newUserDataTreeObjects.forEach((user: UserDataTreeObject) => {
          if (user.displayName === "ff4b41e6-9e75-4ec7-bc34-a3f0fe55ba5a") {
            if (this.authService.loggedIn()) {
              user.displayName = this.authService.getName();
            } else if (this.authService.getUserAuthorization() === "ANONYMOUS:noEmail") {
              user.displayName = "Anonymous";
            } else {
              user.displayName = this.authService.getEmail();
            }
          }
          this.userdataTreeObjects.push(user);
        });
        // once the data is available through the data map we will navigate to the user page
        // index=0 : publicUser, index=1 : logged in user
        const user: UserDataTreeObject = this.userdataTreeObjects[1];
        this.navigateOutlet(user);
    });
    }

    public navigateOutlet(newActiveComponentNode: UserDataTreeObject): void {
      this.navigateComponentEvent$.next(newActiveComponentNode);
    }

    public createNode(
      parentNode: UserDataTreeObject,
      createNodeName: string,
      createNodeType: NodeType,
      experimentNodeForBatch?: ExperimentObject,
    ): UserDataTreeObject {

      const newNodeObj: UserDataTreeObject = {
        id: -1,
        parent: parentNode.id,
        children: [],
        depth: parentNode.depth + 1,
        type: createNodeType,
        icon: undefined,
        displayName: createNodeName,
        expanded: true,
        creationDate: this.currentDateService.getDate(),
        userId: parentNode.userId,
        description: '',
        comments: [],
      };
      // handle node-type specific data
      switch (createNodeType) {
        case NodeType.EXPERIMENT:
          newNodeObj.icon = 'computer';
          if (experimentNodeForBatch !== undefined) {
            newNodeObj.experiment = experimentNodeForBatch;
          } else {
            newNodeObj.experiment = {
              experimentid: '',
              userid: parentNode.userId,
              status: ExperimentStatus.REQUEST_CREATION,
              creationdate: this.currentDateService.getDate(),
              uploadType: 'PEAKLIST_AND_SEARCH',
              searches: [],
              experimentFiles: null,
              targetFDR: 0.01,
              proteinGroupJsonFileId: '',
            }
          }
          break;
        case NodeType.COMPARISON:
          newNodeObj.icon = 'poll';  // alternatives: layers , filter_none ?
          newNodeObj.comparison = {
            comparisonid: '',
            userid: parentNode.userId,
            name: newNodeObj.displayName,
            description: '',
            comments: [],
            targetFDR: 0.01,
            dbExperimentId: '',
            status: ComparisonStatus.REQUEST_CREATION,
            creationdate: this.currentDateService.getDate(),
            experimentIds: [],
            proteinGroupJsonFileId: '',
          }
          break;
        case NodeType.PROTEINDB:
          newNodeObj.icon = 'storage';
          //newNodeObj.icon = 'description';  // alternatives: storage, layers , filter_none, description, poll ?
          newNodeObj.proteinDB = {
            userID: parentNode.userId,
            protdbId: '',
            status: ProteinDbStatus.REQUEST_CREATION,
            proteinDBFiles: [],
            creationdate: this.currentDateService.getDate(),
            totalProteins: 0,
            spectralLibraries: [],
          }
          break;
        case NodeType.FOLDER:
          newNodeObj.icon = 'folder';
          break;
      }
      const flatListOfNode: UserDataTreeObject[] = this.getAllNodesForSideNav();
      let highestNodeId = -1;
      flatListOfNode.forEach((node: UserDataTreeObject) => {
        if (node.id > highestNodeId) {
          highestNodeId = node.id;
        }
      });
      newNodeObj.id = highestNodeId + 1;
      parentNode.children.push(newNodeObj);
      this.triggerHttpUpdateEvent$.next(true);
      return newNodeObj;
    }

    public updateNode(updateNode: UserDataTreeObject): void {
      // nodes now update automatically, only trigger update http
      this.triggerHttpUpdateEvent$.next(true);
    }

    public removeNode(removeNode: UserDataTreeObject): void {
      const flatListOfNode: UserDataTreeObject[] = this.getAllNodesForSideNav();
      flatListOfNode.forEach((node: UserDataTreeObject) => {
        // if condition: this is the parentnode, remove and navigate there
        if (node.id === removeNode.parent) {
          const newChildren = [];
          node.children.forEach((child: UserDataTreeObject) => {
            if (child.id !== removeNode.id) {
              newChildren.push(child);
            }
            node.children = newChildren;
          });
          this.triggerHttpUpdateEvent$.next(true);
          this.navigateOutlet(node);
          return;
        }
      });
    }

    public getAllNodesAsListByType(types: NodeType[]): UserDataTreeObject[] {
      const listOfNodesByTypes: UserDataTreeObject[] = [];
      this.getAllNodesForSideNav().forEach((node: UserDataTreeObject) => {
        types.forEach((type: NodeType) => {
          if (node.type === type) {
            listOfNodesByTypes.push(node);
          }
        });
      });
      return listOfNodesByTypes;
    }

    public getAllNodesForSideNav(): UserDataTreeObject[] {
      // TODO: this method is maybe called too often (in a row without a reason)
      let flatListOfNodes: UserDataTreeObject[] = [];
      this.userdataTreeObjects.forEach((user: UserDataTreeObject) => {
        flatListOfNodes = this.resursiveListing(user, flatListOfNodes);
      });
      return flatListOfNodes;
    }

    private resursiveListing(user: UserDataTreeObject, flatListOfNodes: UserDataTreeObject[]): UserDataTreeObject[] {
      flatListOfNodes.push(user);
      user.children.forEach((child: UserDataTreeObject) => {
        flatListOfNodes = this.resursiveListing(child, flatListOfNodes);
      });
      return flatListOfNodes;
    }

    executeUpload(files: MultiFileUploadData): void {

      this.uploadProgressService.reset();
      files.files.forEach((file: UploadFile) => {
        this.uploadProgressService.addToTotal(file.uploadFile.size);
      });

      const onDialogClosingObservable = this.invokeUploadDialog();
      // TODO: modify user data tree with file information
      // --- generate UploadID
      // --- update ProteinDBObject with new "ProteiNDBFileObject --> UploadID + original File Name"
      this.httpClientService
        .postMultiPartFilesEvents(files, this.mpaWebserverAddressService.getEndpoint(Endpoints.GENERAL_FILE_UPLOAD))
        .subscribe({
          next: (event) => {
            if (event.type === HttpEventType.UploadProgress) {
              console.log('Progress event');
              console.log(event);
              this.uploadProgressService.changeReportLoaded(event.loaded);
            } else if (event.type === HttpEventType.Response) {
              console.log('Response event');
              console.log(event);
            }
          },
          error: (error) => {
            console.log(error);
            if (error.status >= 400) {
              // handle failed upload
              if (this.dialog.getDialogById(this.uploadDialogId)) {
                this.dialog
                  .getDialogById(this.uploadDialogId)
                  .componentInstance.setUploadFailed();
                this.dialog.getDialogById(
                  this.uploadDialogId
                ).componentInstance.uploadFailedMessage = error.statusText;
              }
            } else {
              throw error;
            }
          },
        });
        onDialogClosingObservable.subscribe((uploadFailed) => {
          console.log('dialog closing');
          if (!uploadFailed) {
            if (this.authService.loggedIn()) {
              // this.r.navigate(['./prophanejobcontrol']);
            } else {
              // this.router.navigate([
              //   './results/' + this.currentProphaneJob.prophaneJobUUID,
              // ]);
            }
          }
        });
    }

    invokeUploadDialog(): Observable<boolean> {
      this.uploadProgressService.setUUID(
        'this.currentProphaneJob.prophaneJobUUID'
      );
      const dialogRef = this.dialog.open(UploadDialogComponent, {
        disableClose: true,
        data: { successMessage: 'Job successfully submitted.' },
        id: this.uploadDialogId,
      });

      return dialogRef.afterClosed();
    }

}
