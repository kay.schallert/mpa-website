import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Component({
  selector: 'app-file-dropzone',
  templateUrl: './file-dropzone.component.html',
  styleUrls: ['./file-dropzone.component.scss']
})
export class FileDropzoneComponent {

  @Input() allowMultiple: boolean = true;
  @Input({required: true}) acceptedFileTypes: string[] = [];
  @Input({required: true}) selectedFiles: File[] = [];
  @Output() fileInputEvent = new EventEmitter<File[]>(); // implement handler for this event in parent component to receive files

  // HostListener binds whole component to events, used here to prevent file-dropping from opening new tabs and to enable visual highlights
  @HostListener('dragover', ['$event']) onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();
    document.getElementById('file-input-dropzone').classList.add('dropzone-highlight');
  }

  @HostListener('dragleave', ['$event']) onDragLeave(event) {
    event.preventDefault();
    event.stopPropagation();
    document.getElementById('file-input-dropzone').classList.remove('dropzone-highlight');
  }

  @HostListener('drop', ['$event']) onDrop(event) {
    event.preventDefault();
    event.stopPropagation();
    document.getElementById('file-input-dropzone').classList.remove('dropzone-highlight');
    this.handleFileInput(event.dataTransfer.files);
  }

  handleFileInput(fileList: FileList): void {
    var acceptedFiles = [];
    for (let i = 0; i < fileList.length; i++) {
      // check file-extensions, because drop does not seem to consider the input file-extensions by default
      let nameSplit = fileList[i].name.split(".");
      let extension = nameSplit[nameSplit.length - 1];
      for (let j in this.acceptedFileTypes) {
        if (this.acceptedFileTypes[j] == ("." + extension)) {
          // check and filter for duplicates
          let filesWithName = this.selectedFiles.filter(file => { return file.name == fileList[i].name });
          if (filesWithName.length > 0) {
            // skip current for iteration for duplicates
            continue
          }
          // push non-duplicate file with correct file-extension
          acceptedFiles.push(fileList[i]);
        }
      }
      // break after first accepted file if only single file is allowed (as precaution for dropping off multiple files)
      if (!this.allowMultiple && acceptedFiles.length > 0) {
        this.selectedFiles = [];
        break;
      }
    }
    // update selected files, push to parent
    if (acceptedFiles.length > 0){
      this.selectedFiles.push(...acceptedFiles);
      this.selectedFilesChanged();
    }
  }

  removeFile(fileToRemove: File): void {
    // go through files, remove file with corresponding name
    this.selectedFiles = this.selectedFiles.filter(file => {
      return file.name != fileToRemove.name
    })
    this.selectedFilesChanged();
  }

  selectedFilesChanged(): void {
    this.fileInputEvent.emit(this.selectedFiles);
  }
}
