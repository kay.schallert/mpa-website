import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';


/**
 * Reusable textfield component
 * intended for name-editing, description editing etc with variable Validators to check for certain patterns etc.
 */
@Component({
  selector: 'app-textfield-dialog',
  templateUrl: './textfield-dialog.component.html',
  styleUrls: ['./textfield-dialog.component.scss'],
})
export class TextfieldDialogComponent implements OnInit {
  @Input() value: string;

  valueLabel: string = 'Edit';
  dialogPrompt: string = 'default dialog prompt';
  hasValidators: boolean = true;
  validatorPattern: string = '[äÄöÖüÜa-zA-Z0-9_-]|\s*'; // TODO: also allows tabs
  valueForm: UntypedFormGroup;

  constructor(
    public dialogRef: MatDialogRef<TextfieldDialogComponent>,
    private fb: UntypedFormBuilder
  ) {}

  ngOnInit(): void {
    let valueField: [any] = [this.value];
    this.hasValidators ? valueField.push([Validators.required,Validators.pattern(this.validatorPattern),Validators.minLength(1)]) : {};
    this.valueForm = this.fb.group({
      valueField
    });
  }

  onSubmit(): void {
    // can be triggered by pressing enter inside the textarea -> check if submission would be viable
    if (this.valueForm.status != "VALID") {
      return;
    }
    this.dialogRef.close(this.valueForm.get('valueField').value);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  // getErrorMessage(): string {
  //   if (this.hasValidators && this.valueForm.get('valueField').hasError('required')) {
  //     return 'Please input a ' + this.valueLabel;
  //   } else if (this.hasValidators && this.valueForm.get('valueField').hasError('pattern')) {
  //     return 'No white spaces or special chars'
  //   } else if (this.hasValidators && this.valueForm.get('valueField').value.length > this.maxLength) {
  //     return this.valueLabel + ' is too long!';
  //   } else {
  //     console.log(this.valueForm.get('valueField').errors)
  //     return '';
  //   }
  // }

}

export function NodeNameValidator(existingNames: string[]): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    let forbidden = false;
    for (const name of existingNames) {
      if (control.value === name) {
        forbidden = true;
      }
    }
    return forbidden ? { forbiddenName: { value: control.value } } : null;
  };
}
