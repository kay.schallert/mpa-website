import { Component } from '@angular/core';

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss']
})
export class PublicationComponent {

  publications = [
    {
      title: 'A complete and flexible workflow for metaproteomics data analysis based on MetaProteomeAnalyzer and Prophane',
      author: 'Henning Schiebenhoefer, Kay Schallert,  Bernhard Y Renard, Kathrin Trappe, Emanuel Schmid, Dirk Benndorf, Katharina Riedel, Thilo Muth, Stephan Fuchs',
      image: './assets/img/publications/naprot.svg',
      date: '2020 Oct 15',
      pubmedId: '32859984',
      url: 'https://pubmed.ncbi.nlm.nih.gov/32859984/'
    },
    {
      title: 'A Robust and Universal Metaproteomics Workflow for Research Studies and Routine Diagnostics Within 24 h Using Phenol Extraction, FASP Digest, and the MetaProteomeAnalyzer',
      author: 'Robert Heyer, Kay Schallert, Anja Büdel, Roman Zoun, Sebastian Dorl, Alexander Behne, Fabian Kohrs, Sebastian Püttker, Corina Siewert, Thilo Muth, Gunter Saake, Udo Reichl, Dirk Benndorf',
      image: './assets/img/publications/frontMic.jpg',
      date: '2019 Aug 16',
      pubmedId: '31474963',
      url: 'https://pubmed.ncbi.nlm.nih.gov/31474963/'
    },
    {
      title: 'MPA Portable: A Stand-Alone Software Package for Analyzing Metaproteome Samples on the Go',
      author: 'Thilo Muth, Fabian Kohrs, Robert Heyer, Dirk Benndorf, Erdmann Rapp, Udo Reichl, Lennart Martens, Bernhard Y Renard',
      image: './assets/img/publications/AnalyticalChemistry.png',
      date: '2017 Dec 7',
      pubmedId: '29215871',
      url: 'https://pmc.ncbi.nlm.nih.gov/articles/PMC5757220/'
    },
    {
      title: 'The MetaProteomeAnalyzer: A Powerful Open-Source Software Suite for Metaproteomics Data Analysis and Interpretation',
      author: 'Thilo Muth, Alexander Behne, Robert Heyer, Fabian Kohrs, Dirk Benndorf, Marcus Hoffmann, Miro Lehtevä, Udo Reichl, Lennart Martens, and Erdmann Rapp',
      image: './assets/img/publications/jofproteome.jpg',
      date: '2015 Mar 6',
      pubmedId: '25660940',
      url: 'https://pubs.acs.org/doi/10.1021/pr501246w'
    },
  ];
}
