import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamRoutingModule } from './team-routing.module';
import { TeamComponent } from './team.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MemberCardModule } from "./members-module/member-card.module";


@NgModule({
  declarations: [
    TeamComponent
  ],
  imports: [
    CommonModule,
    TeamRoutingModule,
    MatExpansionModule,
    MemberCardModule
]
})
export class TeamModule { }
