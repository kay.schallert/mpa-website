import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VersionHistoryComponent } from './version-history.component';


const routes: Routes = [
  { path: '', component: VersionHistoryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VersionHistoryRoutingModule { }
