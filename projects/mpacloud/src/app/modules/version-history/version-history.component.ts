import { Component } from '@angular/core';


@Component({
  selector: 'version-history',
  templateUrl: './version-history.component.html',
  styleUrls: ['./version-history.component.scss']
})
export class VersionHistoryComponent {

  versions = [
    {
      id: '1.0.1',
      date: 'mm/dd/yyyy',
      changelog: 'changes...'
    },
  ];
}
