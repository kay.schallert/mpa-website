
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { VersionHistoryRoutingModule } from './version-history-routing.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { VersionHistoryComponent } from './version-history.component';





@NgModule({
  declarations: [VersionHistoryComponent],
  imports: [
    CommonModule,
    VersionHistoryRoutingModule,
    MatCardModule,
    MatExpansionModule,
  ]
})
export class VersionHistoryModule { }
