import {Injectable} from '@angular/core';

export enum WebserverUrls {
  TEST = 'https://mdoa-tools.bi.denbi.de/test_server_mpacloud/',
  DENBIPUBLIC = 'https://mdoa-tools.bi.denbi.de/server_mpacloud/',
}

export enum Endpoints {

  // TODO: split into MPA_SEARCH and PROPHANE
  FILES_UPLOAD='mpasearch/searchfilesupload',

  UNIMPLEMENTED = '',

  // GET_EXPERIMENT = 'mpacloud/v1/getExperiment',
  // ADD_DB_EXPERIMENT = 'mpacloud/v1/addexperiment',
  // ADD_STREAMING_SESSION = 'mpacloud/v1/addstreamingsession',
  // UPDATE_EXPERIMENT = 'mpacloud/v1/updateExperiment',
  // LIST_STREAMINGSESSIONS = 'mpacloud/v1/liststreamingsessions',

  // results

  GET_DOWNLOADPROTEINGROUPS = 'mparesults/getproteingroupsdownload',
  POST_UPDATE_PROTEIN_GROUPS = 'mparesults/updateproteingroups',


  // LIST_DB_EXPERIMENTS = 'mpacloud/v1/listexperiments',


  // website
  GET_PROTEIN_GROUPS = 'mpauser/getproteingroups',
  UPDATE_USER_DATA = 'mpauser/updateuserdata',
  GET_USER_DATA = 'mpauser/getuserdata',
  GENERAL_FILE_UPLOAD='mpauser/uploadfiles',
  GET_PROTEIN_SEQUENCE = 'mpauser/getproteinsequence',
  GET_SPECTRUMDATA = 'mpauser/getspectrumdata',
  DOWNLOAD_FILE = 'mpauser/getmpafile',

  // experiment
  CREATE_EXPERIMENT = 'mpasearch/createexperiment',
  GET_EXPERIMENT_DATA = 'mpasearch/getexperimentdata',
  CREATE_COMPARISON = 'mpasearch/createcomparison',
  UPDATE_EXPERIMENT_DATA = 'mpasearch/updateexperimentdata',
  SEARCH_METADATA = 'mpasearch/searchmetadata',
  SEARCH_UPLOAD = 'mpasearch/searchfileupload',

  // proteinloader
  PROTEINLOADER_METADATA = 'proteinloader/fastametadata',
  PROTEINLOADER_FILEUPLOAD = 'proteinloader/uploadprotdb',
  PROTEINLOADER_GET_PROTDBDATA = 'proteinloader/getprotdbdata',
  PROTEINLOADER_UPDATE_PROTDBDATA= 'proteinloader/updateprotdbdata',
  // PROTEINLOADER_STATUS = 'proteinloader/jobstatus',
  DOWNLOAD_PROPHANE_TEST_DATA = ""


}

@Injectable({
  providedIn: 'root',
})
export class MpaWebserverAddressService {

  public getEndpoint(endpoint: string): string {
    //return WebserverUrls.PROPHANE + endpoint;
    return WebserverUrls.TEST + endpoint;
  }

}
