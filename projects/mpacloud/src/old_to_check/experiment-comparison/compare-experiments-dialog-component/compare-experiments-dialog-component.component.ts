import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { DataItem } from '../../model/data-item';
import { ExperimentJSONObject, ExperimentStatus } from '../../model/experimentjson';

import { MPAUserDataNavigationService } from 'projects/mpacloud/src/app/modules/mpa/services/mpa-user-data-navigation.service';
import { ExperimentObject, UserDataTreeObject } from 'projects/mpacloud/src/app/modules/mpa/model/user-data-json';

export enum DialogStatus {
  UNSUBMITTED = "unsubmitted",
  LOADING = "loading",
  SUBMITTED = "submitted", //currently not used, may be useless in this scenario
  FAILED = "failed",
  SUCCESS = "success"
}
@Component({
  selector: 'app-compare-experiments-dialog-component',
  templateUrl: './compare-experiments-dialog-component.component.html',
  styleUrls: ['./compare-experiments-dialog-component.component.scss']
})
export class CompareExperimentsDialogComponentComponent implements OnInit {
  compExpNameForm: UntypedFormGroup;
  comparisonExperimentName: string;


  availableExperimentsNames: string[];
  availableExperiments: Map<string, ExperimentObject>;
  experimentsForm: FormControl;

  //maybe only status needed, converted to boolean where needed? AND enum?
  status: DialogStatus;
  submitDisabled: boolean = false;
  errorMessage: string;

  constructor(
    public dialogRef: MatDialogRef<CompareExperimentsDialogComponentComponent>,
    private dataService: MPAUserDataNavigationService,
    private fb: UntypedFormBuilder,
    @Inject(MAT_DIALOG_DATA)
    public data: { parentFolderDataObject: UserDataTreeObject, expName?: string, expID?: string },
  ) {}

  ngOnInit(): void {
    this.availableExperimentsNames = [];
    // this.availableExperiments = this.dataService.getExperimentsMap();
    this.availableExperiments.forEach((expid, key) => {
      this.availableExperimentsNames.push(key)
    })
    this.experimentsForm = new FormControl([]);
    this.experimentsForm.valueChanges.subscribe(() => {
      // this.validateSubmits();
    })
    this.compExpNameForm = this.fb.group({
      comparisonExperimentName: [
        '',
        [
          Validators.required,
          Validators.pattern('[äÄöÖüÜa-zA-Z0-9_-]*'),
        ],
      ],
    });
    this.compExpNameForm.statusChanges.subscribe(() => {
      // this.validateSubmits();
    })
    // this.initializeDefaults();
  }

  // // used in the "goBack"-button after submission failed and in ngOnInit
  // initializeDefaults() {
  //   this.status = DialogStatus.UNSUBMITTED;
  //   this.errorMessage = '';
  //   if (this.data.expName) {
  //     this.experimentsForm.setValue([this.data.expName]);
  //   }
  // }

  // submitCompareExperiments() {
  //   this.status = DialogStatus.LOADING;
  //   let selectedExperimentsNames: string[] = this.experimentsForm.value
  //   let selectedExperimentsIDs: string[] = [];
  //   let responses: ExperimentObject[] = [];
  //   for (let i = 0; i < selectedExperimentsNames.length; i++) {
  //     selectedExperimentsIDs.push(this.availableExperiments.get(selectedExperimentsNames[i]).experimentid)
  //     this.dataService.dataMap
  //     .subscribe(dataMap => {
  //       const res: ExperimentObject = dataMap.experimentMap.get(selectedExperimentsIDs[i])
  //       responses.push(res);
  //       if(responses.length == selectedExperimentsIDs.length){
  //         for(let i in responses) {
  //           if (!(responses[i].status === ExperimentStatus.FINISHED)) {
  //             this.status = DialogStatus.FAILED;
  //             this.errorMessage.length > 1 ? this.errorMessage += ", " + responses[i].name : this.errorMessage += "Error in " + responses[i].name;
  //           }
  //         }
  //         this.status == DialogStatus.FAILED ? {} : this.createNewComparisonNode(selectedExperimentsIDs);
  //       }
  //     })
  //   }
  // }
  // createNewComparisonNode(selectedExperimentsIDs: string[]) {
  //   const nodeObject: UserDataTreeObject = this.dataService.createNewDataItem(this.data.parentFolderDataObject, this.compExpNameForm.value.comparisonExperimentName, NodeType.Comparison, selectedExperimentsIDs);
  //   this.dataService.dataMap
  //   .subscribe(dataMap => {
  //     const res: ExperimentObject = dataMap.experimentMap.get(nodeObject.objectId)
  //     console.log("Status update: " + res.status)
  //     res.status != ExperimentStatus.FAILED ? this.status = DialogStatus.SUCCESS : this.status = DialogStatus.FAILED;
  //   })
  // }

  // //errorMessage for the name form
  // getNameErrorMessage(): string {
  //   if (this.compExpNameForm.get('comparisonExperimentName').hasError('required')) {
  //     return 'Please enter a name';
  //   } else if (this.compExpNameForm.get('comparisonExperimentName').hasError('pattern')) {
  //     return 'No white spaces or special chars';
  //   } else {
  //     return '';
  //   }
  // }

  // //enables/disables submit button and sends back an errorMessage
  // validateSubmits(): void {
  //   if (this.compExpNameForm.status != "VALID"|| this.experimentsForm.value.length < 2) {
  //     this.submitDisabled = true;
  //   } else if (this.experimentsForm.value.length > 1 && this.compExpNameForm.status == "VALID") {
  //     this.submitDisabled = false;
  //     this.errorMessage = "";
  //   }
  // }

  // closeDialog(): void {
  //   this.dialogRef.close(this.status == DialogStatus.SUCCESS ? true : false);
  // }
}
