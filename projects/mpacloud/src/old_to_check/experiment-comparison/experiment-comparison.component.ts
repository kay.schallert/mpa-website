import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { UserDataTreeMap } from '../model/data-item-map';
import { ContentComponent } from '../../app/modules/mpa/navigation-components/content-component';
import { MpaTableDataService } from '../../app/modules/mpa/services/mpa-table-data.service';
import { TextfieldDialogComponent } from '../../app/modules/mpa/shared-components/textfield-dialog/textfield-dialog.component';
import { ExperimentObject, UserDataTreeObject } from '../../app/modules/mpa/model/user-data-json';
import { MPAUserDataNavigationService } from '../../app/modules/mpa/services/mpa-user-data-navigation.service';


@Component({
  selector: 'app-experiment-comparison',
  templateUrl: './experiment-comparison.component.html',
  styleUrls: ['./experiment-comparison.component.scss']
})
export class ExperimentComparisonComponent implements OnInit,ContentComponent {

  // dataItemOfThisComponent: UserDataTreeObject;

  // placeholder data
  hasMpaData: boolean = false;
  hasTaxonomyData: boolean = false;
  hasFunctionData: boolean = false;
  displayNameEditing: string;

  experimentData: ExperimentObject;

  constructor(
    private _snackBar: MatSnackBar,
    private dataService: MPAUserDataNavigationService,
    private mpaTableDataService: MpaTableDataService,
    private dialog: MatDialog,) {}
  dataItemOfThisComponent: UserDataTreeObject;

  ngOnInit() {
    this.mpaTableDataService.expID.next(this.dataItemOfThisComponent.experiment.experimentid);
    this.displayNameEditing = this.dataItemOfThisComponent.displayName;

    // this.dataService.dataMap
    // .subscribe((dataMap: UserDataTreeMap) => {
    //   const experimentData: ExperimentObject = dataMap.experimentMap.get(this.dataItemOfThisComponent.objectId);
    //   this.experimentData = experimentData;
    //   // if (experimentData.isSearched) {
    //   //   this.hasMpaData = true;
    //   //   this.mpaTableDataService.requestProteinGroups();
    //   // } else {
    //   //   this.hasMpaData = false;
    //   // }
    // });
  }

  onSetDescription(): void {
    /**
     * handles description change
     */
    const dialogRef = this.dialog.open(TextfieldDialogComponent, {
      disableClose: true,
    });

    const dialogInstance = dialogRef.componentInstance;
    dialogInstance.dialogPrompt = 'Edit experiment description';
    dialogInstance.value = this.dataItemOfThisComponent.description;

    dialogRef.afterClosed().subscribe((expDescription) => {
      this.dataItemOfThisComponent.description = expDescription;
      this.updateExperiment();
    });
  }

  onAccept(): void {
    if (this.displayNameEditing.length > 24) {
      this._snackBar.open('Names longer than 24 characters are not allowed!');
      this.displayNameEditing = '';
    } else if (this.displayNameEditing.length <= 0) {
      this._snackBar.open('Empty names are not allowed!');
    } else {
      this.updateExperiment();
    }
  }

  updateExperiment(): void {
    this.dataItemOfThisComponent.displayName = this.displayNameEditing;
    // if (this.dataItemOfThisComponent.objectId) {
    //   // this.dataService.updateExperiment(this.dataItemOfThisComponent, this.experimentData);
    // }
  }

  onRemoveExperiment(): void {
    this.dataService.removeNode(this.dataItemOfThisComponent);
  }
}
