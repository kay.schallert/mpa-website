import { NodeType } from '../../../../mpa/src/app/modules/mpa/services/user-data-tree.service';
import { ComparisonObject, ExperimentObject, ProteinDBObject, UserDataJson, UserDataTreeObject } from './user-data-json';

export class UserDataTreeMap {

  userDataJson: UserDataJson;
  experimentMap: Map<string, ExperimentObject> = new Map<string, ExperimentObject>();
  comparisonMap: Map<string, ComparisonObject> = new Map<string, ComparisonObject>();
  proteinDBMap: Map<string, ProteinDBObject> = new Map<string, ProteinDBObject>();
  isInitialized: boolean = false;
  highestNodeId: number;

  public initializeAndReturnUser(incomingUserDataJson: UserDataJson): UserDataTreeObject {
    this.userDataJson = incomingUserDataJson;
    this.userDataJson.rootUser = incomingUserDataJson.rootUser;
    if (this.userDataJson.rootUser.children === undefined) {
      this.userDataJson.rootUser.children = [];
    }
    this.userDataJson.experimentList.forEach((experiment: ExperimentObject) => {
      this.experimentMap.set(experiment.experimentid, experiment);
    });
    this.userDataJson.comparisonList.forEach((comparison: ComparisonObject) => {
      this.comparisonMap.set(comparison.comparisonid, comparison);
    });
    this.userDataJson.proteinDBList.forEach((proteinDB: ProteinDBObject) => {
      this.proteinDBMap.set(proteinDB.protdbId, proteinDB);
    });
    this.isInitialized = true;
    return this.userDataJson.rootUser;
  }

  public getDataItemList(): UserDataTreeObject[] {
    console.log(this.userDataJson);
    return this.addNodeAndChildren(this.userDataJson.rootUser);
  }

  private addNodeAndChildren(parentNode: UserDataTreeObject): UserDataTreeObject[] {
    const dataItemList = []
    dataItemList.push(parentNode);
    parentNode.children.forEach(child => {
      const childList = this.addNodeAndChildren(child);
      childList.forEach(dataItem => {
        dataItemList.push(dataItem);
      });
    });
    return dataItemList;
  }

  // TODO: what exactly is this for?
  public getAllDisplayNames(): string[] {
    const displayNames: string[] = [];
    this.getDataItemList().forEach(item => {
      displayNames.push(item.displayName);
    });
    return displayNames;
  }

  public addNewItemFromParent(newNode: UserDataTreeObject, parentNode: UserDataTreeObject): void {
    this.highestNodeId = -1;
    this.getDataItemList().forEach(node => {
      if (node.id > this.highestNodeId) {
        this.highestNodeId = node.id;
      }
    });
    newNode.id = this.highestNodeId + 1;
    this.findSpecificNode(this.userDataJson.rootUser, parentNode.id).children.push(newNode);
  }

  public findSpecificNode(startingNode: UserDataTreeObject, searchNodeId: number): UserDataTreeObject {
    let foundNode = null;
    if (startingNode.id === searchNodeId) {
      return startingNode;
    }
    for (const child of startingNode.children) {
      if (child.id === searchNodeId) {
        foundNode = child;
        break;
      }
      foundNode = this.findSpecificNode(child, searchNodeId);
      if (foundNode !== null) {
        break;
      }
    }
    return foundNode;
  }

  public removeItemReturnParent(removeNode: UserDataTreeObject): UserDataTreeObject {
    const parentNode = this.findSpecificNode(this.userDataJson.rootUser, removeNode.parent);
    const newChildren = [];
    parentNode.children.forEach(child => {
      if (child.id !== removeNode.id) {
        newChildren.push(child);
      }
    });
    parentNode.children = newChildren;
    return parentNode;
  }

  public moveItemAfterNode(): void {

  }

  public getAllChildren(dataItem: UserDataTreeObject): UserDataTreeObject[] {
    const parent = this.findSpecificNode(this.userDataJson.rootUser, dataItem.id);
    const allChildren = [];
    parent.children.forEach(child => {
      allChildren.concat(this.addNodeAndChildren(child));
    });
    return allChildren;
  }

}
