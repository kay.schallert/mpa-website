export enum ExperimentType {
  EXPERIMENT = 'EXPERIMENT',
  COMPARISON = 'COMPARISON'
}

export enum ExperimentStatus {
  READY = 'READY',
  QUEUED = 'QUEUED',
  RUNNING = 'RUNNING',
  FINISHED = 'FINISHED',
  FAILED = 'FAILED'
}

export interface ExperimentJSON {
  expid: string;
  type: ExperimentType;
  status: ExperimentStatus;
  description: string;
  experiments?: string[];
  //experiment_data: string;
  name: string;
  creationdate: string;
  targetFdr: string;

  peakListFiles: string[];
  searchResultFiles: string[];
  protDbId: string;
}

export class ExperimentJSONObject implements ExperimentJSON {
  expid: string;
  type: ExperimentType;
  description: string;
  experiments?: string[];
  //experiment_data: string;
  name: string;
  creationdate: string;
  targetFdr: string;

  peakListFiles: string[];
  searchResultFiles: string[];
  protDbId: string;
  status: ExperimentStatus;
}
