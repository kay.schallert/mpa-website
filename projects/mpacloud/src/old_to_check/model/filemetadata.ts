export interface Filemetadata {
  filename: string;
  fileType: string;
  fileUUID: string;
}

export class FileMetaDataObject implements Filemetadata {
  filename: string;
  fileType: string;
  fileUUID: string;
}


