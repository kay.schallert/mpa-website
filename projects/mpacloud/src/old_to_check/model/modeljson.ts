export interface ModelJSON {
  validator_model_id: string;
  file_name: string;
  validator_model_parameters: string;
}
