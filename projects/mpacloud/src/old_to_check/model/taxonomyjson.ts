export interface TaxonomyJSON {
  id: string;
  experimentIDs: string[];
  rank: string;
  scientificname: string;
  othernames?: string[];
  children: TaxonomyObject[];
  proteinIds?: string[];
  level?: number;
  expandable?: boolean;
  isExpanded?: boolean;
  isDisplayed?: boolean;
}

export class TaxonomyObject implements TaxonomyJSON {
  id: string;
  experimentIDs: string[];
  rank: string;
  scientificname: string;
  othernames?: string[];
  children: TaxonomyObject[];
  proteinIds?: string[];
  level?: number;
  expandable?: boolean;
  isExpanded?: boolean;
  isDisplayed?: boolean;
  quantifications?: number[];
}

export class TaxonomyTableObject {
  superkingdom: string = '';
  phylum: string = '';
  class: string = '';
  order: string = '';
  family: string = '';
  genus: string = '';
  species: string = '';
  strain: string = '';
  proteinIds: string[] = [];
  quantifications: number[];
  isDisplayed?: boolean;
}

export enum NCBIRanks {
  NO_RANK = 'no rank',
  SUPERKINGDOM = 'superkingdom',
  PHYLUM = 'phylum',
  CLASS = 'class',
  ORDER = 'order',
  FAMILY = 'family',
  GENUS = 'genus',
  SPECIES = 'species',
  STRAIN = 'strain',
}
