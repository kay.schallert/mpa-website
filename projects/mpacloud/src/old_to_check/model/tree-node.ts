export interface TreeNode {
    depth: number;
    displayName: string;
    expanded: boolean;
    hasChildren: boolean;
    icon: string;
    id: string;
    type: string;
}
