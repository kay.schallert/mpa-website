export class UploadConfig {

  fileUploadConfigs: FileUploadConfig[] = [];
  //...

}

export class FileUploadConfig {

  fileId: string;
  originalFileName: string;
  type: string;
  objectId: string;
  userDataTreeObjectId: number;
  //...

}
