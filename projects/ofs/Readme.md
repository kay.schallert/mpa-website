# Introduction

![OMEx logo](src\assets\omex_maths_01.svg)

Selecting molecule panels that are applicable to classify the health
state of patients is a common task in omics data analysis. The Omics
Molecule Extractor (OMEx) is an open-source web-application providing a
user-friendly workflow for selecting molecules and molecule panels for
sample classification from large datasets. OMEx’ user interface provides
interactive visualization for exploring input data and analysis results.
The feature selection strategy underlying the algorithm is based on
machine learning and has so far not been available in any software with
user interface. Extensive testing using synthetic datasets with known
ground truth showed that the algorithm discovers group-separating
molecules with high precision. Additionally, OMEx was tested on five
real-world omics datasets demonstrating high reproducibility and overlap
with reported molecules from other feature selection methods, while also
reporting alternative molecules of interest.

OMEx's preprint is available here: https://doi.org/10.21203/rs.3.rs-5914047/v1

## Index

- [About](#about)
- [Usage](#usage)
  - [Installation](#installation)
  - [Commands](#commands)
- [Development](#development)
  - [Pre-Requisites](#pre-requisites)
  - [Developmen Environment](#development-environment)
  - [File Structure](#file-structure)
  - [Build](#build)
  - [Deployment](#deployment)
- [Community](#community)
  - [Contribution](#contribution)
  - [Branches](#branches)
  - [Guideline](guideline)
- [FAQ](#faq)
- [Resources](#resources)
- [Gallery](#gallery)
- [Credit/Acknowledgment](#creditacknowledgment)
- [License](#license)

## About

OMEx is based on Angular 18 and Java.

## Usage

Go to https://mdoa-tools.bi.denbi.de/omex/home to use OMEx or run a local instance of it (see below).

### Installation

- Steps on how to install this project, to use it.
- Be very detailed here, For example, if you have tools which run on different operating systems, write installation steps for all of them.

```
$ add installations steps if you have to.
```

### Worflow

#### Data specifications

OFF utilizes tabular input data that should have the following specifications:

- data should contain abundance information about individual molecules, i.e. features, that occur in comparable groups, e.g. 'control' and 'disease' group
- data should be stored as tab-separated file in '.csv' format
- rows: - The first row should contain column names
  Each following row should represent one omics feature
- Columns:
  - First column (mandatory): Should be labeled ‘Description’ and contain unique feature identifiers
  - Meta-data columns (optional): Any descriptive label, may contain cross-references to databases (e.g., KEGG) or meta-data for Features
  - Abundance columns (mandatory): Labeld with group prefix, underscore, unique sample id (<group prefix>\_<sample id>), should contain feature abundances (absolute or relative)
  - Group prefixes must be strings
  - Abundance columns must contain numerical values (no missing entries, NaN, etc.)
  - At least three abundance columns per group are mandatory (3 replicates), the more the better

### Commands

- Commands to start the project.

## Development

If you want other people to contribute to this project, this is the section, make sure you always add this.

### Pre-Requisites

List all the pre-requisites the system needs to develop this project.

- A tool
- B tool

### Development Environment

Write about setting up the working environment for your project.

- How to download the project...
- How to install dependencies...

### File Structure

Add a file structure here with the basic details about files, below is an example.

```
ofs
└───src
    ├───app
    │   ├───components
    │   │   └───ofs-landing-page
    │   ├───modules
    │   │   └───workflow
    │   │       ├───components
    │   │       │   ├───classification-results
    │   │       │   ├───data-download
    │   │       │   ├───feature-table
    │   │       │   ├───overview-input
    │   │       │   ├───overview-results
    │   │       │   ├───preprocessing-input
    │   │       │   ├───preprocessing-results
    │   │       │   ├───wrapper-input
    │   │       │   └───wrapper-results
    │   │       ├───models
    │   │       └───services
    │   └───services
    ├───assets
    │   └───standard-logos
    └───environments
```

| No  | File Name | Details     |
| --- | --------- | ----------- |
| 1   | index     | Entry point |

### Build

Write the build Instruction here.

### Deployment

Write the deployment instruction here.

## Community

If it's open-source, talk about the community here, ask social media links and other links.

### Contribution

Your contributions are always welcome and appreciated. Following are the things you can do to contribute to this project.

1.  **Report a bug** <br>
    If you think you have encountered a bug, and I should know about it, feel free to report it [here]() and I will take care of it.

2.  **Request a feature** <br>
    You can also request for a feature [here](), and if it will viable, it will be picked for development.

3.  **Create a pull request** <br>
    It can't get better then this, your pull request will be appreciated by the community. You can get started by picking up any open issues from [here]() and make a pull request.

> If you are new to open-source, make sure to check read more about it [here](https://www.digitalocean.com/community/tutorial_series/an-introduction-to-open-source) and learn more about creating a pull request [here](https://www.digitalocean.com/community/tutorials/how-to-create-a-pull-request-on-github).

### Branches

I use an agile continuous integration methodology, so the version is frequently updated and development is really fast.

1. **`stage`** is the development branch.

2. **`master`** is the production branch.

3. No other permanent branches should be created in the main repository, you can create feature branches but they should get merged with the master.

**Steps to work with feature branch**

1. To start working on a new feature, create a new branch prefixed with `feat` and followed by feature name. (ie. `feat-FEATURE-NAME`)
2. Once you are done with your changes, you can raise PR.

**Steps to create a pull request**

1. Make a PR to `stage` branch.
2. Comply with the best practices and guidelines e.g. where the PR concerns visual elements it should have an image showing the effect.
3. It must pass all continuous integration checks and get positive reviews.

After this, changes will be merged.

### Guideline

coding guidelines or other things you want people to follow should follow.

## FAQ

You can optionally add a FAQ section about the project.

## Resources

Add important resources here

## Gallery

Pictures of your project.

## Credit/Acknowledgment

This Readme is adapted from [Raman Tehlan](https://gist.github.com/ramantehlan/602ad8525699486e097092e4158c5bf1).

## License

Add a license here, or a link to it.
