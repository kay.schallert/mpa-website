# Changelog

## version 0.1.0 - OMEx initial version

- classifier: d-LDA
- input: tab-separated files in .txt., tsv., .csv format, column headers of samples should contain group prefixes
- selectable and downloadable example data
- data preprocessing parameters: sample-wise normalization, sparsity filtering, splitting of training and test set with specified test fraction
- automatic mode: runs workflow with standard parameters
- data overview: class balance, sparsity overview, volcano plot
- filter: repeats, cross validation folds, classifier accuracy over number of molecules
- wrapper: max 150 molecules, optimal value chosen by algorithm, repeats, folds, panel ranking, single molecule ranking, panels selectable, list of molecules with number of selections to choose from
- classifier: classification using selected panel, PCA plot, pairwise scatter plot, hierarchical clustering, volcano plot with selected molecules, classification metrics
- download: zip containing all intermediate files and figures
