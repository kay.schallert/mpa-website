import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OfsLandingPageComponent } from './components/ofs-landing-page/ofs-landing-page.component';
import { AuthGuard, BetterLoginPageComponent } from 'shared-lib';
import { ChangelogComponent } from './components/changelog/changelog.component';
import { ImprintComponent } from './components/imprint/imprint.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';

// TODO: route guard? (the route guard for workflow routes still exists)

const routes: Routes = [
  { path: 'login', component: BetterLoginPageComponent },
  { path: 'home', component: OfsLandingPageComponent },
  {
    path: 'changelog',
    component: ChangelogComponent,
  },
  {
    path: 'imprint',
    component: ImprintComponent,
  },
  {
    path: 'privacy',
    component: PrivacyPolicyComponent,
  },
  {
    path: 'workflow',
    loadChildren: () =>
      import('./modules/workflow/workflow.module').then(
        (m) => m.WorkflowModule,
      ),
  },
  {
    path: 'joboverview',
    loadChildren: () =>
      import('./modules/job-overview/job-overview.module').then(
        (m) => m.JobOverviewModule,
      ),
    canActivate: [AuthGuard],
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
