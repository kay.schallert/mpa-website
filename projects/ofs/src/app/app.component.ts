import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs';
import {
  AuthService,
  SimpleNavigationRoute,
  NestedNavigationRoute,
  Logo,
} from 'shared-lib';
import { RouteStateService } from './services/route-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Omics Molecule Extractor';
  version = '0.1.0';

  logo: Logo = {
    assetPath: 'assets/omex_maths_white_01.svg',
    label: 'Omics Molecule Extractor',
  };

  homelink: SimpleNavigationRoute = {
    route: '/home',
    label: 'Home',
  };

  routes: NestedNavigationRoute[] = [
    {
      route: '/workflow',
      label: 'Start Workflow',
      requireAuth: false,
    },
    {
      route: '/changelog',
      label: 'Changelog',
      requireAuth: false,
    },
    // { route: '/joboverview', label: 'My Jobs', requireAuth: true },
    // { route: '/about', label: 'About', requireAuth: false },
  ];

  footerContent: NestedNavigationRoute[] = [
    {
      label: 'About',
      children: [
        { label: 'About OMEx', route: '/home' },
        // { label: 'Terms of Service', route: '/termsofservice' },
        { label: 'Privacy Policy', route: '/privacy' },
        { label: 'Imprint', route: '/imprint' },
      ],
    },
    {
      label: 'Funding & Support',
      children: [
        { label: 'ISAS e.V.', href: 'https://www.isas.de/' },
        {
          label: 'Bielefeld University',
          href: 'https://www.uni-bielefeld.de/',
        },
        { label: 'BMBF', href: 'https://www.bmbf.de' },
        { label: 'DFG', href: 'http://www.dfg.de' },
        { label: 'BMBF', href: 'https://www.bmbf.de' },
        { label: 'NRW', href: 'https://www.land.nrw' },
        { label: 'de.NBI', href: 'http://www.denbi.de' },
        { label: 'de.NBI Cloud', href: 'https://www.denbi.de/cloud' },
        { label: 'ELIXIR Germany', href: 'https://www.denbi.de/elixir-de' },
      ],
    },
  ];

  footerLogos: Logo[] = [
    {
      label: 'leibniz',
      assetPath: 'assets/standard-logos/leibniz_white.png',
      href: 'https://www.leibniz-gemeinschaft.de',
    },
    {
      label: 'ISAS',
      assetPath: 'assets/standard-logos/isaslogooffizielleformrgbweiss.png',
      href: 'https://www.isas.de',
    },
    {
      label: 'Bielefeld University',
      assetPath: 'assets/standard-logos/uni-bielefeld-logo.jpg',
      href: 'https://www.uni-bielefeld.de',
    },
    {
      label: 'bmbf',
      assetPath: 'assets/standard-logos/bmbf.svg',
      href: 'https://www.bmbf.de',
      backgroundColor: 'white',
    },
    {
      label: 'nrw',
      assetPath: 'assets/standard-logos/nrw_black.svg',
      href: 'https://www.land.nrw',
      backgroundColor: 'white',
    },
    {
      label: 'de.NBI',
      assetPath: 'assets/standard-logos/Denbi.svg',
      href: 'http://www.denbi.de',
    },
    {
      label: 'de.NBI Cloud',
      assetPath: 'assets/standard-logos/denbi-cloud.png',
      href: 'https://www.denbi.de/cloud',
    },
    {
      label: 'ELIXIR Germany',
      assetPath: 'assets/standard-logos/ELIXIR_germany_logo.jpg',
      href: 'https://www.denbi.de/elixir-de',
    },
  ];

  constructor(
    private router: Router,
    private routeState: RouteStateService,
    private authService: AuthService,
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.routeState.setCurrentRoute(event.url);
      });
    this.authService.initializeOAuth();
  }
}
