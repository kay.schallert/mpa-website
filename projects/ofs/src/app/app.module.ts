import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { OfsLandingPageComponent } from './components/ofs-landing-page/ofs-landing-page.component';
import { StandardPageLayoutModule, WorkflowPanelModule } from 'shared-lib';
import { provideMatomo, withRouter } from 'ngx-matomo-client';
import { ChangelogComponent } from './components/changelog/changelog.component';
import { ImprintComponent } from './components/imprint/imprint.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';

@NgModule({
  declarations: [
    AppComponent,
    OfsLandingPageComponent,
    ChangelogComponent,
    ImprintComponent,
    PrivacyPolicyComponent,
  ],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    StandardPageLayoutModule,
    WorkflowPanelModule,
  ],
  providers: [
    provideHttpClient(withInterceptorsFromDi()),
    provideMatomo(
      {
        trackerUrl: 'https://piwik.cebitec.uni-bielefeld.de/',
        siteId: '29',
      },
      withRouter(), // Enables route tracking
    ),
  ],
})
export class AppModule {}
