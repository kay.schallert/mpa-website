import { Component } from '@angular/core';

@Component({
  selector: 'ofs-changelog',
  templateUrl: './changelog.component.html',
  styleUrls: ['./changelog.component.scss'],
})
export class ChangelogComponent {}
