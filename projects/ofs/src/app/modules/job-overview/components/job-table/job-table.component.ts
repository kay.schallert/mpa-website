import { Component, OnInit, ViewChild } from '@angular/core';
import { JobOverviewService } from '../../services/job-overview.service';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { OfsJob } from '../../models/ofs-job.model';

@Component({
  selector: 'ofs-job-table',
  templateUrl: './job-table.component.html',
  styleUrls: ['./job-table.component.scss'],
})
export class JobTableComponent implements OnInit {
  Subscriptions: Subscription[] = [];

  dataSource = new MatTableDataSource<OfsJob>();
  columnsToDisplay: string[] = [
    'jobId',
    'jobName',
    'details',
    'creationDate',
    'actions',
  ];

  constructor(private jobService: JobOverviewService) {}

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.jobService.getJobs();

    this.Subscriptions.push(
      this.jobService.jobs$.subscribe((jobData) => {
        this.dataSource.data = jobData;
      })
    );
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.Subscriptions.forEach((sub) => sub.unsubscribe());
  }

  onContinue(jobId: string) {
    this.jobService.goToJob(jobId);
  }

  onDownload(jobId: string) {
    this.jobService.downloadJob(jobId);
  }

  onDelete(jobId: string) {
    this.jobService.deleteJob(jobId);
  }
}
