import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';


import { JobOverviewRoutingModule } from './job-overview-routing.module';
import { JobOverviewComponent } from './job-overview.component';
import { JobTableComponent } from './components/job-table/job-table.component';


@NgModule({
  declarations: [
    JobOverviewComponent,
    JobTableComponent
  ],
  imports: [
    CommonModule,
    JobOverviewRoutingModule,
    MatTableModule,
    MatButtonModule,
    MatSortModule,
    MatIconModule
  ]
})
export class JobOverviewModule { }
