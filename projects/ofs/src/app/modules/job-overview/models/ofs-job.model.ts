export enum OfsJobProcess {
  // NOJOB = 'nojob',
  // WAITING = 'waiting',
  // CREATED = 'created',
  // CREATIONFAIL = 'creationfail',
  // OVERVIEW_INPUT = 'overviewinput',
  // OVERVIEW_RESULTS = 'overviewresults',
  // OVERVIEW_FAIL = 'overviewfail',
  // PREPROCESSING_INPUT = 'preprocessinginput',
  // PREPROCESSING_RESULTS = 'preprocessingresults',
  // PREPROCESSING_FAIL = 'preprocessingfail',
  // WRAPPER_INPUT = 'wrapperinput',
  // WRAPPER_RESULTS = 'wrapperresults',
  // WRAPPER_FAIL = 'wrapperfail',
  // RESULTS = 'results',
  // RESULTS_FAIL = 'resultsfail',

  NOJOB = 'NOJOB',
  CREATED = 'CREATED',
  INPUT = 'INPUT',
  OVERVIEW = 'OVERVIEW',
  FILTER = 'FILTER',
  WRAPPER = 'WRAPPER',
  CLASSIFICATION = 'CLASSIFICATION',
  RESULTS = 'RESULTS',
  INVALIDCSV = 'INVALIDCSV',
}

export interface OfsJobState {
  process: OfsJobProcess;
  progress: number;
  startTime?: number;
}

export class OfsJobState implements OfsJobState {
  constructor(process: OfsJobProcess, progress: number, startTime?: number) {
    this.process = process;
    this.progress = progress;
    this.startTime = startTime;
  }

  public getProcess(): OfsJobProcess {
    return this.process;
  }
  public getProgress(): number {
    return this.progress;
  }

  public setProcess(process: OfsJobProcess): void {
    this.process = process;
  }
  public setProgress(progress: number): void {
    this.progress = progress;
  }

  public setStartTime(startTime: number): void {
    this.startTime = startTime;
  }
  public getStartTime(): number {
    return this.startTime;
  }

  public static getObjectFromJson(
    stateJson: {
      process: string;
      progress: number;
      startTime?: number;
    },
    startTime?,
  ): OfsJobState {
    return new OfsJobState(
      OfsJobProcess[stateJson.process],
      stateJson.progress,
      startTime,
    );
  }
}

export interface OfsJob {
  jobId: string;
  state: OfsJobState;
  message?: string;
  automaticJobId?: string;
  jobName?: string;
  details?: string;
  creationDate?: Date;
}
