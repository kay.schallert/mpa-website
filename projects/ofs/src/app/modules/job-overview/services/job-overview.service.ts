import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClientService } from 'shared-lib';
import {
  Endpoints,
  WebserveraddressService,
} from '../../../services/offwebserveraddress.service';
import { OFSData } from '../../workflow/models/ofs-data.model';
import { HttpParams } from '@angular/common/http';
import { OfsJob, OfsJobProcess, OfsJobState } from '../models/ofs-job.model';

const jobs: OfsJob[] = [
  {
    jobId: '1',
    jobName: 'Job 1',
    details: 'Another daay another job',
    creationDate: new Date(),
    state: { process: OfsJobProcess.NOJOB, progress: 0 } as OfsJobState,
  },
  {
    jobId: '1',
    jobName: 'My Metadata',
    details: 'Another daay another iob',
    creationDate: new Date(),
    state: { process: OfsJobProcess.NOJOB, progress: 0 } as OfsJobState,
  },
  {
    jobId: '1',
    jobName: 'Metadata deluxe',
    details: 'Another daay another jiob',
    creationDate: new Date(),
    state: { process: OfsJobProcess.NOJOB, progress: 0 } as OfsJobState,
  },
  {
    jobId: '1',
    jobName: 'Job3_4',
    details: 'Another daay another jib',
    creationDate: new Date(),
    state: { process: OfsJobProcess.NOJOB, progress: 0 } as OfsJobState,
  },
  {
    jobId: '1',
    jobName: 'Mefadafa',
    details: 'Another daay another jip',
    creationDate: new Date(),
    state: { process: OfsJobProcess.NOJOB, progress: 0 } as OfsJobState,
  },
];

@Injectable({
  providedIn: 'any',
})
export class JobOverviewService {
  /**
   * Fetches jobs for user from server, holds data for job overview table, manages job actions
   */

  jobs$ = new BehaviorSubject<OfsJob[]>(undefined);

  constructor(
    private http: HttpClientService,
    private address: WebserveraddressService
  ) {}

  getJobs() {
    this.jobs$.next(jobs);
    // TODO: fetch jobs from server
  }

  createJob() {
    /**
     * Request job from server
     * when created, go to workflow, request job id of created job and retrieve job from server
     */
  }

  goToJob(jobId: string) {
    /**
     * go to workflow, request job id and retrieve job from server
     */
  }

  deleteJob(jobId: string) {
    // delete job from server
  }

  downloadJob(jobId: string) {
    // download job from server
  }
}
