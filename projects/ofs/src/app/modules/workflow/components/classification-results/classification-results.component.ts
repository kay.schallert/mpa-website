import { Component } from '@angular/core';
import { Observable, Subscription, filter, map } from 'rxjs';
import { WorkflowService } from '../../services/workflow.service';
import { OFSData } from '../../models/ofs-data.model';
import { ClassifierResponse } from '../../models/classifier-response.model';

@Component({
  selector: 'ofs-classification-results',
  templateUrl: './classification-results.component.html',
  styleUrls: ['./classification-results.component.scss'],
})
export class ClassificationResultsComponent {
  classifierImages$: Observable<string[]>;
  displayedColumns: string[] = ['metric', 'value'];
  tabLables = [];

  metrics$: Observable<{ metric: string; value: number }[]> =
    this.workflow.ofsDataSubject$.pipe(
      map((ofsData: OFSData) => ofsData?.responseData?.classifierResponse),
      filter((response: ClassifierResponse) => response.precision != undefined),
      map((response: ClassifierResponse) =>
        this.getClassificationMetrics(response),
      ),
    );

  subscriptions: Subscription[] = [];

  constructor(private workflow: WorkflowService) {
    // retrieves image urls as observable, used with async pipe in template
    this.classifierImages$ = this.workflow.ofsDataSubject$.pipe(
      map((data: OFSData) => data?.responseData?.classifierResponse),
      filter((resp: ClassifierResponse) => resp.downloadLink != undefined),
      map((data: ClassifierResponse): string[] => this.getImageLinks(data)),
      // output: observable of image urls
    );
  }

  getImageLinks(data: ClassifierResponse): string[] {
    // PCA image can be missing if one feature was selected --> add tab headers dynamically
    const imageLinks = [];

    if (data?.pcaImage !== undefined) {
      imageLinks.push(data?.pcaImage);
      this.tabLables.push('PCA with Molecule Selection');
    }
    if (data?.scatterPlotImage !== undefined) {
      imageLinks.push(data?.scatterPlotImage);
      this.tabLables.push('Pairwise Scatter Plot');
    }
    if (data?.volcanoPlotWithSelection !== undefined) {
      imageLinks.push(data?.volcanoPlotWithSelection);
      this.tabLables.push('Volcano Plot');
    }
    if (data?.hacImage !== undefined) {
      imageLinks.push(data?.hacImage);
      this.tabLables.push('Hierarchical Clustering');
    }

    return this.workflow.getResourceUrls(imageLinks);
  }

  getClassificationMetrics(classifierResponse: ClassifierResponse): {
    metric: string;
    value: number;
  }[] {
    const output: {
      metric: string;
      value: number;
    }[] = [];

    const groups = Object.keys(classifierResponse.precision);

    output.push({
      metric: 'Accuracy',
      value: classifierResponse.accuracy,
    });

    if (groups.length == 2) {
      const controlGroup =
        this.workflow.ofsDataSubject$.value.configData.preprocessingConfig
          .controlGroup;

      output.push(
        ...this.generateMetricsArray(classifierResponse, controlGroup),
      );
      return output;
    }

    for (let group of groups) {
      output.push(...this.generateMetricsArray(classifierResponse, group));
    }
    return output;
  }

  generateMetricsArray(metrics, group): { metric: string; value: number }[] {
    const metricsArray = [];

    metricsArray.push(
      {
        metric: `Precision (${group})`,
        value: metrics.precision[group],
      },
      {
        metric: `Recall (${group})`,
        value: metrics.recall[group],
      },
      {
        metric: `F1-Score (${group})`,
        value: metrics.fScore[group],
      },
    );

    return metricsArray;
  }

  isLoading() {
    return this.workflow.loading;
  }
}
