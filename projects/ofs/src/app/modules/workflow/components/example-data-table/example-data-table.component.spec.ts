import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleDataTableComponent } from './example-data-table.component';

describe('ExampleDataTableComponent', () => {
  let component: ExampleDataTableComponent;
  let fixture: ComponentFixture<ExampleDataTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ExampleDataTableComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExampleDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
