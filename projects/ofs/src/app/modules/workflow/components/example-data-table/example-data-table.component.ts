import {
  AfterViewInit,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { WorkflowService } from '../../services/workflow.service';

export enum ExampleDataIds {
  NONE = <any>'NONE',
  SYNTHETIC = <any>'SYNTHETIC',
  STOOL_METAPROTEOMICS = <any>'STOOL_METAPROTEOMICS',
  BLOOD_TRANSCRIPTOMICS = <any>'BLOOD_TRANSCRIPTOMICS',
  BLOOD_PROTEOMICS = <any>'BLOOD_PROTEOMICS',
  URINE_METABOLOMICS = <any>'URINE_METABOLOMICS',
  GLIAL_TUMOR_METABOLOMICS = <any>'GLIAL_TUMOR_METABOLOMICS',
}

export interface ExampleData {
  name: ExampleDataIds;
  description: string;
  molecules: string;
  classes: string;
  reference: string[];
  fileName: string;
}

export interface DialogReturn {
  data: ExampleData;
  download: boolean;
}

const exampleData: ExampleData[] = [
  {
    name: ExampleDataIds.SYNTHETIC,
    description:
      'Synthetic data generated using the script for testing filter and wrapper.',
    molecules: '50',
    classes: 'TESTONE (32), \n TESTTWO (28)',
    reference: [],
    fileName: 'sampling-1',
  },
  {
    name: ExampleDataIds.STOOL_METAPROTEOMICS,
    description:
      'Stool metaproteomics data from controls, NASH and HCC patients.',
    molecules: '42,572',
    classes: 'C (19), \n NASH (32), \n HCC (29)',
    reference: ['https://doi.org/10.3390/ijms23168841'],
    fileName: 'sydor_stool',
  },
  {
    name: ExampleDataIds.BLOOD_TRANSCRIPTOMICS,
    description:
      'Transcriptomics data from rheumatoid arthritis patients and controls.',
    molecules: '10,527',
    classes: 'HC (35), \n RA (45)',
    reference: [
      'https://doi.org/10.1038/s41467-018-05044-4',
      'https://doi.org/10.1007/s00441-023-03816-z',
    ],
    fileName: 'ng_tasaki_transcriptomics_formatted',
  },
  {
    name: ExampleDataIds.BLOOD_PROTEOMICS,
    description:
      'Proteomics data from rheumatoid arthritis patients and controls.',
    molecules: '1,070',
    classes: 'HC (35), \n RA (44)',
    reference: ['https://doi.org/10.1038/s41467-018-05044-4'],
    fileName: 'tasaki_proteomics_formatted',
  },
  {
    name: ExampleDataIds.URINE_METABOLOMICS,
    description: 'Urine metabolomics from lung cancer patients and controls.',
    molecules: '1,070',
    classes: 'control (469), \n cancer (536)',
    reference: [
      'https://doi.org/10.1158/0008-5472.CAN-14-0109',
      'https://doi.org/10.1016/j.csbj.2024.03.016',
    ],
    fileName: 'mathe_lung_formatted',
  },
  {
    name: ExampleDataIds.GLIAL_TUMOR_METABOLOMICS,
    description:
      'Glial tumor metabolomics from IDH wildtype tumors and IDH mutant tumors.',
    molecules: '7,017',
    classes: 'wt (50), \nmutant (38)',
    reference: [
      'https://doi.org/10.1186/s12859-022-04900-x',
      'https://doi.org/10.1016/j.csbj.2024.03.016',
    ],
    fileName: 'chardin_brain_formatted',
  },
];

@Component({
  selector: 'ofs-example-data-table',
  templateUrl: './example-data-table.component.html',
  styleUrl: './example-data-table.component.scss',
})
export class ExampleDataTableComponent implements OnInit, AfterViewInit {
  dataSource = new MatTableDataSource<ExampleData>();
  columnsToDisplay: string[] = [
    'name',
    'description',
    'molecules',
    'classes',
    'reference',
    'actions',
  ];

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { selected: ExampleData },
  ) {}

  ngOnInit() {
    this.dataSource.data = exampleData;
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }
}
