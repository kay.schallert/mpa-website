import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import {
  CustomValidators,
  InputFormComponent,
  ALLOWEDSIMPLECHARS,
} from 'shared-lib';
import { OFSData } from '../../models/ofs-data.model';
import {
  DataGroupForm,
  GroupSelectionOptions,
  OverviewConfig,
  OverviewInputForm,
} from '../../models/overview.model';
import { WorkflowService } from '../../services/workflow.service';
import { map, Observable, tap } from 'rxjs';
import { OfsJobProcess } from '../../../job-overview/models/ofs-job.model';
import { MatDialog } from '@angular/material/dialog';
import {
  DialogReturn,
  ExampleData,
  ExampleDataIds,
  ExampleDataTableComponent,
} from '../example-data-table/example-data-table.component';

@Component({
  selector: 'ofs-overview-input',
  templateUrl: './overview-input.component.html',
  styleUrls: ['./overview-input.component.scss'],
})
export class OverviewInputComponent
  extends InputFormComponent
  implements OnInit, OnDestroy
{
  ofsJobMessage$: Observable<string>;

  groupSelectionOptions = Object.values(GroupSelectionOptions);
  formModel: OverviewInputForm;

  constructor(
    public builder: FormBuilder,
    private workflow: WorkflowService,
    private dialog: MatDialog,
  ) {
    super(builder);

    this.subscriptions = [];

    this.ofsJobMessage$ = this.workflow.ofsDataSubject$.pipe(
      map((data: OFSData) => data.job.message),
    );
  }

  // Getters and Setters
  get groups() {
    return this.formModel?.controls.groups;
  }

  //  Lifecycle Hooks
  ngOnInit(): void {
    this.formModel = this.buildForm();
    this.doSubscriptions();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

  //  Subscriptions
  doSubscriptions() {
    // updates Validators if form value changed, necessary to check for duplicate names (see arrayDuplicateValidator)
    this.subscriptions.push(
      this.groups.valueChanges.subscribe(() => {
        CustomValidators.updateValidators(this.groups);
      }),
    );

    // toggle testsetFraction depending on useTestset value
    this.subscriptions.push(
      this.formModel.controls.useTestset.valueChanges.subscribe((value) => {
        this.toggleTestFraction(value);
      }),
    );

    // form depends on values in ofsdata - react to changes
    this.subscriptions.push(
      this.workflow.ofsDataSubject$.subscribe((ofsData) => {
        this.setExistingFormInput(ofsData);
      }),
    );
  }

  // check if ofsdata contains values relevant to this form
  setExistingFormInput(ofsData: OFSData) {
    // default state: form disabled
    this.disableForm();

    // patch changed values
    this.formModel.patchValue(ofsData.configData.overviewConfig);

    // status job created or invalid csv are only states where form should be editable
    if (
      ofsData.job.state.getProcess() === OfsJobProcess.CREATED ||
      ofsData.job.state.getProcess() === OfsJobProcess.INVALIDCSV
    ) {
      this.enableForm();
      this.toggleTestFraction(ofsData.configData.overviewConfig.useTestset);
    }
  }

  //  Methods
  buildForm(): OverviewInputForm {
    let dataFile: File | null;

    const dataFileControl = new FormControl(dataFile, {
      // updateOn: 'change',
      // validators: Validators.required,
    });

    const formModel = this.builder.group(
      {
        data: dataFileControl,
        inputCSVID: ['', [Validators.required]],
        exampleDataset:
          this.workflow.ofsDataSubject$.value.configData.overviewConfig
            .exampleDataset,
        groupSelectionOptions: [this.groupSelectionOptions[0]],
        groups: this.builder.array(
          [this.buildGroup('control'), this.buildGroup('test')],
          {
            validators: [
              CustomValidators.arrayDuplicateValidator(['groupName']),
            ],
          },
        ),
        sparsityFraction: [
          0.66,
          [Validators.required, Validators.min(0.0), Validators.max(1.0)],
        ],
        normalize: [true],
        useTestset: [true],
        testsetFraction: [
          0.3,
          [Validators.required, Validators.min(0.05), Validators.max(0.4)],
        ],
      },
      {
        updateOn: 'change',
      },
    ) as OverviewInputForm;

    return formModel;
  }

  handleFileInputChange(files: FileList): void {
    if (files.length == 0) {
      return;
    }
    this.formModel.controls.data.setValue(files[0]);
    this.formModel.controls.inputCSVID.patchValue(files[0].name);
  }

  buildGroup(defaultName: string): DataGroupForm {
    return this.builder.group({
      groupName: [
        defaultName,
        [Validators.required, Validators.pattern(ALLOWEDSIMPLECHARS)],
      ],
      groupPrefix: [
        '',
        [Validators.required, Validators.pattern(ALLOWEDSIMPLECHARS)],
      ],
      groupStart: [undefined],
      groupEnd: [undefined],
    }) as DataGroupForm;
  }

  addGroup(): void {
    this.groups.push(this.buildGroup(''));
  }

  removeGroup(index: number) {
    if (this.groups.length > 2) {
      this.groups.removeAt(index);
    }
  }

  toggleTestFraction(hasTestset: boolean) {
    if (hasTestset) {
      this.formModel.controls.testsetFraction.enable();
    } else {
      this.formModel.controls.testsetFraction.disable();
    }
  }

  resetForm() {
    this.formModel = this.buildForm();
  }

  submitOverviewConfig() {
    const overviewConfig = this.formModel.getRawValue();
    this.disableForm();

    if (this.workflow.simpleMode) {
      this.workflow.runAutomaticMode(overviewConfig);
      return;
    }

    this.workflow.submitOverViewConfig(overviewConfig);
  }

  openDialog() {
    const dialogRef = this.dialog.open(ExampleDataTableComponent, {
      data: { selected: undefined },
    });

    dialogRef.afterClosed().subscribe((data: DialogReturn) => {
      if (data?.data == undefined) {
        return;
      }
      if (data.download) {
        this.workflow.downloadDataset(data.data.fileName + '.csv');
        return;
      }

      this.workflow.loadExampleJob(data.data);
    });
  }
}
