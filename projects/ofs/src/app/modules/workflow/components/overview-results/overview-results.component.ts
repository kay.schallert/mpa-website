import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { map, Observable, Subscription } from 'rxjs';
import { OverviewResponse } from '../../models/overview-response.model';
import { OFSData } from '../../models/ofs-data.model';
import { WorkflowService } from '../../services/workflow.service';
import { MatTableDataSource } from '@angular/material/table';
import { PValue } from '../../models/pValue.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { OfsJob } from '../../../job-overview/models/ofs-job.model';

@Component({
  selector: 'ofs-overview-results',
  templateUrl: './overview-results.component.html',
  styleUrls: ['./overview-results.component.scss'],
})
export class OverviewResultsComponent implements AfterViewInit {
  // displayedColumns: string[] = ['moleculeId', 'pValue'];
  // dataSource = new MatTableDataSource<PValue>();

  overViewImages$: Observable<string[]>;
  Subscriptions: Subscription[] = [];

  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;

  constructor(private workflow: WorkflowService) {
    // retrieves image urls as observable, used with async pipe in template
    this.overViewImages$ = this.workflow.ofsDataSubject$.pipe(
      map((data: OFSData): string[] =>
        this.getImageLinks(data.responseData.overviewResponse),
      ),
      // output: observable of image urls
    );
  }

  ngOnInit(): void {
    // this.Subscriptions.push(
    //   this.workflow.ofsData$.subscribe((ofsData) => {
    //     this.dataSource.data = ofsData.responseData.overviewResponse?.pValues;
    //   })
    // );
  }

  ngOnDestroy(): void {
    this.Subscriptions.forEach((sub) => sub.unsubscribe());
  }

  ngAfterViewInit(): void {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  // applyFilter(event: Event) {
  //   const filterValue = (event.target as HTMLInputElement).value;
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

  getImageLinks(data: OverviewResponse): string[] {
    if (!this.checkForOvervieImages(data)) {
      return [];
    }
    return this.workflow.getResourceUrls([
      data.classDistribution,
      data.volcanoPlot,
      data.dataSparsity,
    ]);
  }

  checkForOvervieImages(ofsData: OverviewResponse): boolean {
    return (
      ofsData?.classDistribution !== undefined &&
      ofsData?.dataSparsity !== undefined
    );
  }

  isLoading() {
    return this.workflow.loading;
  }
}
