import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { InputFormComponent } from 'shared-lib';
import { WrapperForm } from '../../models/wrapper.model';
import { WorkflowService } from '../../services/workflow.service';
import { OFSData } from '../../models/ofs-data.model';
import { MoleculeCutoff } from '../../models/preprocessing-response.model';

@Component({
  selector: 'ofs-wrapper-input',
  templateUrl: './wrapper-input.component.html',
  styleUrls: ['./wrapper-input.component.scss'],
})
export class WrapperInputComponent
  extends InputFormComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  formModel: WrapperForm;

  constructor(
    public builder: FormBuilder,
    private workflow: WorkflowService,
  ) {
    super(builder);
    this.subscriptions = [];
  }

  // Life Cycle Hooks
  ngOnInit(): void {
    this.formModel = this.buildForm();
    this.doSubscriptions();
  }

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

  //  Subscriptions
  doSubscriptions() {
    // form depends on values in ofsdata - react to changes
    this.subscriptions.push(
      this.workflow.ofsDataSubject$.subscribe((ofsData) => {
        this.setExistingFormInput(ofsData);
      }),
    );
  }

  // Methods
  // check if ofsdata contains values relevant to this form
  setExistingFormInput(ofsData: OFSData) {
    const wrapperConfig = ofsData.configData.wrapperConfig;
    if (
      wrapperConfig?.folds !== undefined &&
      wrapperConfig?.repeats !== undefined &&
      wrapperConfig.folds !== 0
    ) {
      //  yes: update form values and disable changes
      this.formModel.patchValue(wrapperConfig);
      this.disableForm();
      return;
    }
    // no: allow changes of form
    this.enableForm();
  }

  buildForm() {
    const formModel = this.builder.group(
      {
        moleculeCutoff: [
          this.workflow.ofsDataSubject$.value.responseData.preprocessingResponse
            ?.optimalMoleculeCutoff,
          [Validators.required, Validators.min(0), Validators.max(150)],
        ],
        repeats: [1000, [Validators.required, Validators.min(0)]],
        folds: [5, [Validators.required, Validators.min(0)]],
      },
      {
        updateOn: 'blur',
      },
    ) as WrapperForm;

    return formModel;
  }

  compareFunction(o1: MoleculeCutoff, o2: MoleculeCutoff) {
    return (
      o1.moleculeCutoff == o2.moleculeCutoff &&
      o1.pValueCutoff == o2.pValueCutoff
    );
  }

  submitWrapperConfig() {
    const wrapperConfig = this.formModel.getRawValue();
    this.workflow.submitWrapperConfig(wrapperConfig);
  }
}
