export interface ClassifierResponse {
  pcaImage: string;
  hacImage: string;
  scatterPlotImage: string;
  volcanoPlotWithSelection: string;
  downloadLink: string;

  accuracy: number;
  precision: Map<string, number>;
  recall: Map<string, number>;
  fScore: Map<string, number>;
}
