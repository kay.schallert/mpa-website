export interface Feature {
  featureID: string;
  picks: number;
}

export interface FeatureProfile {
  profileID: string;
  features: string[];
  picks: number;
}

export interface ClassifierConfig {
  classifierJobId: string;
  selectedFeatureIds: string[];
}

export class ClassifierConfig implements ClassifierConfig {
  constructor() {}
}
