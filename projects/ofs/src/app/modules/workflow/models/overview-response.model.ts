import { PValue } from './pValue.model';

export interface OverviewResponse {
  classDistribution: string;
  volcanoPlot: string;
  dataSparsity: string;
  testGroups: string[];
  controlGroup: string;
  testSamples: string[];
}

export class OverviewResponse implements OverviewResponse {
  constructor() {}
}
