import { FormArray, FormControl, FormGroup } from '@angular/forms';
import {
  ExampleData,
  ExampleDataIds,
} from '../components/example-data-table/example-data-table.component';

export enum GroupSelectionOptions {
  PREFIX = 'select by prefix',
  COLUMN = 'select by column numbers',
}

export interface DataGroup {
  groupName: string;
  groupPrefix: string;
  // groupStart: number;
  // groupEnd: number;
}

export class DataGroup implements DataGroup {
  constructor(groupName: string) {
    this.groupName = groupName;
  }
}

export interface DataGroupForm extends FormGroup {
  value: DataGroup;

  controls: {
    groupName: FormControl;
    groupPrefix: FormControl;
    // groupStart: FormControl;
    // groupEnd: FormControl;
  };
}

export interface OverviewConfig {
  overviewJobId: string;
  data: File;
  inputCSVID: string;
  exampleDataset: ExampleDataIds;
  groups: DataGroup[];
  groupSelectionOptions: GroupSelectionOptions;
  sparsityFraction: number;
  normalize: boolean;
  useTestset: boolean;
  testsetFraction: number;
}

export class OverviewConfig implements OverviewConfig {
  constructor() {
    this.groups = [new DataGroup('control'), new DataGroup('test')];
    this.exampleDataset = ExampleDataIds.NONE;
    this.sparsityFraction = 0.66;
    this.normalize = true;
    this.useTestset = true;
    this.testsetFraction = 0.3;
  }
}

export interface OverviewInputForm extends FormGroup {
  value: OverviewConfig;

  controls: {
    data: FormControl;
    inputCSVID: FormControl;
    exampleDataset: FormControl;
    groupSelectionOptions: FormControl;
    groups: FormArray;
    sparsityFraction: FormControl;
    normalize: FormControl;
    useTestset: FormControl;
    testsetFraction: FormControl;
  };
}
