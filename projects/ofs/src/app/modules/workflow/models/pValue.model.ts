export interface PValue {
  moleculeId: string;
  pValue: number;
}
