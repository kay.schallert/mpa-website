export interface MoleculeCutoff {
  moleculeCutoff: number;
  pValueCutoff: number;
}

export interface PreprocessingResponse {
  pvaluesMolecules?: string;
  predictivePerformance: string;
  optimalMoleculeCutoff: number;
}
