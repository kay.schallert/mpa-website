import { FormControl, FormGroup } from '@angular/forms';

export interface PreprocessingConfig {
  preprocessingJobId: string;
  controlGroup: string;
  testGroup: string;
  repeats: number;
  folds: number;
  // sparsityFraction: number;
  // normalize: boolean;
}

export class PreprocessingConfig implements PreprocessingConfig {
  constructor() {}
}

export interface PreprocessingConfigForm extends FormGroup {
  value: PreprocessingConfig;

  controls: {
    controlGroup: FormControl;
    testGroup: FormControl;
    repeats: FormControl;
    folds: FormControl;
    // sparsityFraction: FormControl;
    // normalize: FormControl;
  };
}
