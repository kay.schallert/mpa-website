import { FormControl, FormGroup } from '@angular/forms';
import { MoleculeCutoff } from './preprocessing-response.model';

export interface WrapperConfig {
  wrapperJobId: string;
  repeats: number;
  folds: number;
  moleculeCutoff: number;
}

export class WrapperConfig implements WrapperConfig {
  constructor() {}
}

export interface WrapperForm extends FormGroup {
  value: WrapperConfig;

  controls: {
    repeats: FormControl;
    folds: FormControl;
    moleculeCutoff: FormControl;
  };
}
