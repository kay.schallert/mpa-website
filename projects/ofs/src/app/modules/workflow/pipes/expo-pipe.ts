import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'expoPipe' })
export class ExpoPipe implements PipeTransform {
  transform(value: string, decimals: number): string {
    return Number.parseFloat(value).toExponential(decimals);
  }
}
