import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  Observable,
  catchError,
  concatMap,
  delay,
  filter,
  map,
  of,
  repeat,
  take,
  takeWhile,
  tap,
} from 'rxjs';
import { MultiFileUploadData } from 'shared-lib';
import { ClassifierConfig } from '../models/classifier.model';
import { ConfigData, OFSData } from '../models/ofs-data.model';
import { OverviewConfig } from '../models/overview.model';
import { PreprocessingConfig } from '../models/preprocessing.model';
import { WrapperConfig } from '../models/wrapper.model';
import { dummyConfig } from 'projects/ofs/src/assets/dummy-config';
import { HttpClientService } from 'shared-lib';
import {
  WebserveraddressService,
  Endpoints,
} from '../../../services/offwebserveraddress.service';
import { JobOverviewService } from '../../job-overview/services/job-overview.service';
import {
  OfsJobProcess,
  OfsJobState,
} from '../../job-overview/models/ofs-job.model';
import {
  ExampleData,
  ExampleDataIds,
} from '../components/example-data-table/example-data-table.component';

export interface SimpleMessage {
  message: string;
}

/**
 * Service for handling workflow related data
 * - store formdata from input form
 * - send form data to server
 * - handle accessible routes
 */
@Injectable({ providedIn: 'any' })
export class WorkflowService {
  loading: Boolean;

  public ofsDataSubject$ = new BehaviorSubject<OFSData>(undefined);
  public simpleMode = true;

  constructor(
    private http: HttpClientService,
    private address: WebserveraddressService,
    private jobService: JobOverviewService,
  ) {
    this.ofsDataSubject$.next(new OFSData());
    console.log('workflow service initialization');
  }

  // used for testing, reads an existing config from assets
  setDummyConfig() {
    const existingConfig = dummyConfig as unknown as OFSData;
    this.ofsDataSubject$.next(this.getOfsDataObjectFromJson(existingConfig));
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      // console.error(error);
      return of(result as T); // creates observable with type T
    };
  }

  createOfsJob() {
    /**
     * Requests new job from server
     */

    // TODO: get job id from job service, retrieve job from server for that id
    this.loading = true;

    this.http
      .postObject<
        OFSData,
        OFSData
      >(this.ofsDataSubject$.value, this.address.getEndpoint(Endpoints.CREATE_JOB), new HttpParams())
      .subscribe({
        next: (response: OFSData) => {
          this.ofsDataSubject$.next(this.getOfsDataObjectFromJson(response));
        },
        error: (error) => {
          this.loading = false;
          console.log(error);
        },
        complete: () => {
          this.loading = false;
        },
      });
  }

  /**
   * Retrieves config for one of the example datasets.
   * TODO: implement endpoint on server, create job folders containing example data, create method to copy example data and config into current job
   */
  loadExampleJob(dataset: ExampleData) {
    this.loading = true;

    const currentOfsData = this.ofsDataSubject$.value;

    currentOfsData.configData.overviewConfig.exampleDataset = dataset.name;
    currentOfsData.configData.overviewConfig.inputCSVID = dataset.fileName;

    this.ofsDataSubject$.next(currentOfsData);

    this.http
      .postObject<
        OFSData,
        OFSData
      >(this.ofsDataSubject$.value, this.address.getEndpoint(Endpoints.EXAMPLE_DATA), new HttpParams())
      .subscribe((response: OFSData) => {
        response.configData.overviewConfig.data = null; // add data property

        this.ofsDataSubject$.next(this.getOfsDataObjectFromJson(response));

        this.loading = false;
      });
  }

  submitOverViewConfig(config: OverviewConfig) {
    this.loading = true;

    const startTime = Date.now();

    const currentOfsData = this.ofsDataSubject$.value;

    currentOfsData.configData.overviewConfig = config;
    currentOfsData.job.state.progress = 0.0;
    currentOfsData.job.state.setProcess(OfsJobProcess.OVERVIEW);
    currentOfsData.job.message = '';

    this.ofsDataSubject$.next(currentOfsData);

    const configFile = new File(
      [JSON.stringify(this.ofsDataSubject$.value)],
      'config',
    );

    const filesToUpload: MultiFileUploadData = {
      files: [
        {
          uploadFile: config.data as File,
          fileID: 'inputCSV',
        },
        {
          uploadFile: configFile,
          fileID: 'inputJSON',
        },
      ],
      httpParameters: new HttpParams(),
    };

    // subscription closes automatically
    this.createOverviewSubmissionObservable(filesToUpload).subscribe(
      (response) => {
        this.ofsDataSubject$.next(
          this.getOfsDataObjectFromJson(response, startTime),
        );
        if (response.job.state.getProgress() == 1.0) {
          this.loading = false;
        }
      },
    );
  }

  /**
   * Chains multiple requests and triggers side effects that update ofs data object.
   * @param filesToUpload
   * @returns Observable that returns the results of the overview submission when closing; To trigger request chain, subscribe to this observable.
   */
  createOverviewSubmissionObservable(
    filesToUpload: MultiFileUploadData,
  ): Observable<OFSData> {
    // send files to server
    return (
      this.http
        .postMultiPartFiles(
          filesToUpload,
          this.address.getEndpoint(Endpoints.OVERVIEW_INPUT),
        )
        // chains side effects and requests
        .pipe(
          // check whether server is done and data are avaiable; concatMap maps the response to a new observable
          concatMap((response: OFSData, index: number): Observable<OFSData> => {
            return this.http.repeatedPostObjectWithEmission<OFSData, OFSData>(
              response,
              {
                checkProperty: [
                  'responseData',
                  'overviewResponse',
                  'dataSparsity',
                ],
                boolCondition: (obj) => {
                  return (
                    OfsJobProcess[obj.job.state.process] !=
                    OfsJobProcess.INVALIDCSV
                  );
                },
              },
              this.address.getEndpoint(Endpoints.OVERVIEW_RESOURCE_AVAIL),
              new HttpParams(),
            );
          }),
          catchError(
            this.handleError('submitOverviewInput', this.ofsDataSubject$.value),
          ),
        )
    );
  }

  submitPreprocessingConfig(config: PreprocessingConfig) {
    this.loading = true;

    const startTime = Date.now();

    const currentOfsData = this.ofsDataSubject$.value;
    currentOfsData.configData.preprocessingConfig = config;
    currentOfsData.job.state.progress = 0.0;

    this.ofsDataSubject$.next(currentOfsData);

    this.createPreprocessingSubmissionObservable(
      this.ofsDataSubject$.value,
    ).subscribe((response) => {
      this.ofsDataSubject$.next(
        this.getOfsDataObjectFromJson(response, startTime),
      );
      if (response.job.state.getProgress() == 1.0) {
        this.loading = false;
      }
    });
  }

  createPreprocessingSubmissionObservable(
    ofsData: OFSData,
  ): Observable<OFSData> {
    return this.http
      .postObject<
        OFSData,
        OFSData
      >(ofsData, this.address.getEndpoint(Endpoints.PREPROCESSING_INPUT))
      .pipe(
        // check whether server is done and data are avaiable; concatMap maps the response to a new observable
        concatMap((response: OFSData, index: number): Observable<OFSData> => {
          return this.http.repeatedPostObjectWithEmission<OFSData, OFSData>(
            response,
            {
              checkProperty: [
                'responseData',
                'preprocessingResponse',
                'predictivePerformance',
              ],
            },
            this.address.getEndpoint(Endpoints.PREPROCESSING_RESOURCE_AVAIL),
            new HttpParams(),
          );
        }),
        catchError(
          this.handleError('submitOverviewInput', this.ofsDataSubject$.value),
        ),
      );
  }

  submitWrapperConfig(config: WrapperConfig) {
    this.loading = true;
    const startTime = Date.now();

    const currentOfsData = this.ofsDataSubject$.value;
    currentOfsData.configData.wrapperConfig = config;
    currentOfsData.job.state.progress = 0.0;

    this.ofsDataSubject$.next(currentOfsData);

    this.createWrapperSubmissionObservable(
      this.ofsDataSubject$.value,
    ).subscribe((response) => {
      this.ofsDataSubject$.next(
        this.getOfsDataObjectFromJson(response, startTime),
      );
      if (response.job.state.getProgress() == 1.0) {
        this.loading = false;
      }
    });
  }

  createWrapperSubmissionObservable(ofsData: OFSData): Observable<OFSData> {
    return this.http
      .postObject<
        OFSData,
        OFSData
      >(ofsData, this.address.getEndpoint(Endpoints.WRAPPER_INPUT))
      .pipe(
        concatMap((response: OFSData, index: number): Observable<OFSData> => {
          return this.http.repeatedPostObjectWithEmission<OFSData, OFSData>(
            response,
            {
              checkProperty: [
                'responseData',
                'wrapperResponse',
                'featureSelection',
              ],
            },
            this.address.getEndpoint(Endpoints.WRAPPER_RESOURCE_AVAIL),
            new HttpParams(),
          );
        }),
        delay(1000),
        catchError(
          this.handleError('submitWrapperInput', this.ofsDataSubject$.value),
        ),
      );
  }

  submitClassifierConfig(config: ClassifierConfig) {
    this.loading = true;
    const startTime = Date.now();

    const currentOfsData = this.ofsDataSubject$.value;
    currentOfsData.configData.classifierConfig = config;
    currentOfsData.job.state.progress = 0.0;

    this.ofsDataSubject$.next(currentOfsData);

    return this.createClassifierSubmissionObservable(
      this.ofsDataSubject$.value,
    ).subscribe((response) => {
      this.ofsDataSubject$.next(
        this.getOfsDataObjectFromJson(response, startTime),
      );
      if (response.job.state.getProgress() == 1.0) {
        this.loading = false;
      }
    });
  }

  createClassifierSubmissionObservable(ofsData: OFSData): Observable<OFSData> {
    return this.http
      .postObject<
        OFSData,
        OFSData
      >(ofsData, this.address.getEndpoint(Endpoints.CLASSIFIER_INPUT), new HttpParams())
      .pipe(
        concatMap((response: OFSData, index: number): Observable<OFSData> => {
          return this.http.repeatedPostObjectWithEmission<OFSData, OFSData>(
            response,
            {
              checkProperty: [
                'responseData',
                'classifierResponse',
                'downloadLink',
              ],
            },
            this.address.getEndpoint(Endpoints.CLASSIFIER_RESOURCES),
            new HttpParams(),
          );
        }),
        catchError(
          this.handleError('submitClassifierInput', this.ofsDataSubject$.value),
        ),
      );
  }

  runAutomaticMode(config: OverviewConfig) {
    this.loading = true;
    const startTime = Date.now();

    const currentOfsData = this.ofsDataSubject$.value;
    currentOfsData.configData.overviewConfig = config;
    currentOfsData.job.state.setProcess(OfsJobProcess.OVERVIEW);
    currentOfsData.job.message = '';
    currentOfsData.job.state.progress = 0.0;

    this.ofsDataSubject$.next(currentOfsData);

    const configFile = new File(
      [JSON.stringify(this.ofsDataSubject$.value)],
      'config',
    );

    const filesToUpload: MultiFileUploadData = {
      files: [
        {
          uploadFile: config.data as File,
          fileID: 'inputCSV',
        },
        {
          uploadFile: configFile,
          fileID: 'inputJSON',
        },
      ],
      httpParameters: new HttpParams(),
    };

    this.http
      .postMultiPartFiles(
        filesToUpload,
        this.address.getEndpoint(Endpoints.RUN_AUTOMATIC_MODE),
      )
      .pipe(
        concatMap((response: OFSData, index: number): Observable<OFSData> => {
          return this.http.repeatedPostObjectWithEmission<OFSData, OFSData>(
            response,
            {
              checkProperty: [
                'responseData',
                'classifierResponse',
                'downloadLink',
              ],
              boolCondition: (obj: OFSData) => {
                return (
                  OfsJobProcess[obj.job.state.process] !=
                  OfsJobProcess.INVALIDCSV
                );
              },
            },
            this.address.getEndpoint(Endpoints.RESOURCES_AUTO),
            new HttpParams(),
          );
        }),
      )
      .subscribe((response) => {
        this.ofsDataSubject$.next(
          this.getOfsDataObjectFromJson(response, startTime),
        );
        if (response.job.state.getProgress() == 1.0) {
          this.loading = false;
        }
      });
  }

  private getOfsDataObjectFromJson(json: OFSData, startTime?: number): OFSData {
    // if (json.job.state) {
    json.job.state = OfsJobState.getObjectFromJson(json.job.state, startTime);
    // }

    return json;
  }

  getResourceUrls(resources: string[]) {
    console.log(this.ofsDataSubject$.value);
    const urls = [];
    for (let resource of resources) {
      urls.push(
        this.address.getEndpointWithQueryParams(
          Endpoints.GET_RESOURCE,
          new Map([
            ['jobid', this.ofsDataSubject$.value.job.jobId],
            ['name', resource],
          ]),
        ),
      );
    }
    return urls;
  }

  downloadDataset(dataset: string): void {
    const url = this.getResourceUrls([dataset])[0];
    this.http.downloadFile(url, dataset);
  }
}
