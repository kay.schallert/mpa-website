import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, Subscription } from 'rxjs';
import { WorkflowService } from './services/workflow.service';
import { CustomStepperService, Step } from 'shared-lib';
import { OFSData } from './models/ofs-data.model';

export const workflowSteps: Step[] = [
  {
    index: 0,
    label: 'Data input',
  },
  {
    index: 1,
    label: 'Filter',
    advanced: true,
  },
  {
    index: 2,
    label: 'Wrapper',
    advanced: true,
  },
  {
    index: 3,
    label: 'Biomarker panel selection',
    advanced: true,
  },
  {
    index: 4,
    label: 'Classification',
  },
];

/**
 * Parent component for the workflow stepper. Uses workflow service to track, send,
 *  and request data. Uses the stepper service to manage the stepper.
 */
@Component({
  selector: 'ofs-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss'],
})
export class WorkflowComponent implements OnInit, OnDestroy {
  private Subscriptions: Subscription[];

  public workflowSteps = workflowSteps;

  public completedSteps$ = this.stepper.completedSteps$;
  public currentStepIndex$ = this.stepper?.currentIndex$;

  public $jobState = this.workflow.ofsDataSubject$.pipe(
    map((data: OFSData) => {
      return data.job.state;
    }),
  );

  constructor(
    private workflow: WorkflowService,
    private stepper: CustomStepperService,
  ) {}

  ngOnInit(): void {
    this.doSubscriptions();
    this.stepper.initialize(this.workflowSteps);
    this.workflow.createOfsJob();
    // this.workflow.setDummyConfig();
  }

  ngOnDestroy() {
    this.Subscriptions.forEach((sub) => sub.unsubscribe());
  }

  doSubscriptions() {
    this.Subscriptions = [
      this.workflow.ofsDataSubject$.subscribe((data) => {
        console.log(data);
        this.updateStepper(data);
      }),
      this.stepper.toggleAdvanced$.subscribe((toggle) => {
        console.log('Advanced mode:', toggle);
        this.workflow.simpleMode = !toggle;
      }),
    ];
  }

  updateStepper(data: OFSData) {
    const completed = [
      data.responseData.overviewResponse?.classDistribution != undefined,
      data.responseData.preprocessingResponse?.predictivePerformance !=
        undefined,
      data.responseData.wrapperResponse?.featureSelection != undefined,
      data.configData.classifierConfig?.selectedFeatureIds != undefined,
      data.responseData.classifierResponse?.pcaImage != undefined,
    ];

    this.stepper.completedSteps$.next(completed);

    // Set the stepper to the first incomplete step
    for (let index = 0; index < completed.length; index++) {
      if (!completed[index]) {
        this.setStep(index);
        break;
      }
    }
  }

  setStep(selectedIndex: number) {
    this.stepper.setStep(selectedIndex);
  }
}
