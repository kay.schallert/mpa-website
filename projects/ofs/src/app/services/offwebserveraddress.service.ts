import { Injectable } from '@angular/core';

export enum WebserverUrls {
  LOCALHOST = 'http://localhost:9104/server_omex/omex/',
  DENBIPUBLIC = 'https://mdoa-tools.bi.denbi.de/server_omex/omex/',
}

export enum Endpoints {
  CREATE_JOB = 'createjob',
  GET_JOBS = 'getjobs',
  OVERVIEW_INPUT = 'overviewinput',
  OVERVIEW_RESOURCE_AVAIL = 'overviewresources',
  PREPROCESSING_INPUT = 'preprocessinginput',
  PREPROCESSING_RESOURCE_AVAIL = 'preprocessingresources',
  WRAPPER_INPUT = 'wrapperinput',
  WRAPPER_RESOURCE_AVAIL = 'wrapperresources',
  CLASSIFIER_INPUT = 'classifierinput',
  CLASSIFIER_RESOURCES = 'classifierresources',
  GET_RESOURCE = 'resource',
  RUN_AUTOMATIC_MODE = 'autorun',
  RESOURCES_AUTO = 'autoresources',
  EXAMPLE_DATA = 'exampledata',
}

@Injectable({
  providedIn: 'root',
})
export class WebserveraddressService {
  system: WebserverUrls = WebserverUrls.DENBIPUBLIC;

  constructor() {}

  public getEndpoint(endpoint: string): string {
    //return WebserverUrls.PROPHANE + endpoint;
    return this.system + endpoint;
  }

  public getEndpointWithQueryParams(
    endpoint: string,
    queryParams: Map<string, string>,
  ): string {
    let address: string = this.system + endpoint + '?';
    queryParams.forEach((value, key) => {
      address += key + '=' + value + '&';
    });
    return address;
  }
}
