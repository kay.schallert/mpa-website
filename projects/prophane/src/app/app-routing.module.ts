import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard, BetterLoginPageComponent } from 'shared-lib';
import { ProphaneResultViewComponent } from './modules/job-control/prophane-result-view/prophane-result-view.component';
import { TermsOfServicePageComponent } from './components/terms-of-service-page/terms-of-service-page.component';
import { ImpressumPageComponent } from './components/impressum-page/impressum-page.component';
import { PrivacyPolicyPageComponent } from './components/privacy-policy-page/privacy-policy-page.component';
import { ProphaneTutorialComponent } from './modules/about-prophane/prophane-tutorial/prophane-tutorial.component';

const routes: Routes = [
  { path: 'login', component: BetterLoginPageComponent },
  {
    path: 'jobsubmission',
    loadChildren: () =>
      import('./modules/job-submission/job-submission.module').then(
        (m) => m.JobSubmissionModule
      ),
    // canActivate: [AuthGuard],
  },
  {
    path: 'jobcontrol',
    loadChildren: () =>
      import('./modules/job-control/job-control.module').then(
        (m) => m.JobControlModule
      ),
    canActivate: [AuthGuard],
  },
  // TODO: entirely move to jobcontrol module
  { path: 'results/:job_uuid', component: ProphaneResultViewComponent,
    // canActivate: [AuthGuard]
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./modules/about-prophane/about-prophane.module').then(
        (m) => m.AboutProphaneModule
      ),
  },
  { path: 'termsofservice', component: TermsOfServicePageComponent },
  { path: 'impressum', component: ImpressumPageComponent },
  { path: 'privacypolicy', component: PrivacyPolicyPageComponent },

  { path: '', redirectTo: 'jobcontrol', pathMatch: 'full' },
  { path: '**', redirectTo: 'jobcontrol' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      scrollOffset: [0, 64], // [x, y]
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
