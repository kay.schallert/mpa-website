import { NgModule } from '@angular/core';
import { SafePipe } from './modules/job-control/safe.pipe';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { StandardPageLayoutModule } from 'dist/shared-lib';

import { MatExpansionModule } from '@angular/material/expansion';

import { ImpressumPageComponent } from './components/impressum-page/impressum-page.component';
import { PrivacyPolicyPageComponent } from './components/privacy-policy-page/privacy-policy-page.component';
import { TermsOfServicePageComponent } from './components/terms-of-service-page/terms-of-service-page.component';


@NgModule({ declarations: [
        AppComponent,
        TermsOfServicePageComponent, // should be somwhere else
        PrivacyPolicyPageComponent, // should be somwhere else
        ImpressumPageComponent, // should be somwhere else
    ],
    bootstrap: [AppComponent], imports: [CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        StandardPageLayoutModule,
        MatExpansionModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class AppModule {}



