export interface ProphaneReportStyle {
  id: number;
  name: string;
  valueString: string;
}

export interface ProphaneReportStyleLabel {
  id: number;
  prependLabel: string;
  htmlInputLabel: string;
}

