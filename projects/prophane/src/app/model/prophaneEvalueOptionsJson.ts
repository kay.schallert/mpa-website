
export interface ProphaneEvalueOptionsJson {
  id: number;
  numerical: string;
  text: string;
}

export class ProphaneEvalueOptions implements ProphaneEvalueOptionsJson {
  id: number;
  numerical: string;
  text: string;
}
