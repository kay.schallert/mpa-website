
export interface ProphaneQuantDataJSON {
  id: number;
  name: string;
  valueString: string;
}

export class ProphaneQuantDataObject implements ProphaneQuantDataJSON {
  id: number;
  name: string;
  valueString: string;
}
