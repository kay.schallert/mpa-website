export interface ProphaneSampleJSON {
  id: number;
  name: string;
  biocat: string;
  bioname: string;
  biocat_full:string;
  bioname_full:string;
}

export class ProphaneSampleObject implements ProphaneSampleJSON {
  id: number;
  name: string;
  biocat: string;
  bioname: string;
  biocat_full:string;
  bioname_full:string;
}
