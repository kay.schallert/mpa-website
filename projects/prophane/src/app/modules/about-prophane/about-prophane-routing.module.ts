import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProphaneAboutComponent } from './prophane-about/prophane-about.component';
import { ProphaneTutorialComponent } from './prophane-tutorial/prophane-tutorial.component';

const routes: Routes = [
  {path: '', component: ProphaneAboutComponent},
  {path: 'tutorial', component: ProphaneTutorialComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutProphaneRoutingModule { }
