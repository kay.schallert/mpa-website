import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutProphaneRoutingModule } from './about-prophane-routing.module';
import { ProphaneAboutComponent } from './prophane-about/prophane-about.component';
import { ProphaneTutorialComponent } from './prophane-tutorial/prophane-tutorial.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
  declarations: [
    ProphaneAboutComponent,
    ProphaneTutorialComponent,
  ],
  imports: [
    CommonModule,
    AboutProphaneRoutingModule,
    MatExpansionModule,
    MatButtonModule,
  ]
})
export class AboutProphaneModule { }
