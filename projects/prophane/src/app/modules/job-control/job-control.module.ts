import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import { MatButtonModule } from '@angular/material/button';

import { JobControlRoutingModule } from './job-control-routing.module';
import { ProphaneJobControlComponent } from './prophane-job-control/prophane-job-control.component';
import { SideBarResultsComponent } from './side-bar-results/side-bar-results.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { SafePipe } from './safe.pipe';
import { ProphaneResultViewComponent } from './prophane-result-view/prophane-result-view.component';


@NgModule({
  declarations: [
    ProphaneJobControlComponent,
    ProphaneResultViewComponent,
    SafePipe,
    SideBarResultsComponent,

  ],
  imports: [
    CommonModule,
    JobControlRoutingModule,
    MatButtonModule,
    MatSidenavModule,
    MatExpansionModule,
  ]
})
export class JobControlModule { }
