import { Component, Input, OnInit } from '@angular/core';
import { JobService } from '../../../services/job.service';
import { ActivatedRoute } from '@angular/router';
import { ProphaneJobObject } from '../../../model/prophanejobjson';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

export interface Task {
  taskid: number;
  name: string;
  database: string;
  algorithm: string;
  scope: string;
}

@Component({
  selector: 'app-side-bar-results',
  templateUrl: './side-bar-results.component.html',
  styleUrls: ['./side-bar-results.component.scss']
})

export class SideBarResultsComponent implements OnInit {
  @Input() job: ProphaneJobObject;

  //job: ProphaneJobObject;
  url: string;
  tasks: Task[] = [];
  params: any[] = [];
  lastClickedIndex: number = 0;
  uuid: string;

  kronaHtmlMap: Map<number, string> = new Map();

  constructor(
    private jobService: JobService,
  ) {}

  ngOnInit(): void {
    this.url = window.location.href;
    this.getJobTasks();
  }

/*   getJobTasks(): void {
    let i = 0;
    const kronaObservables: Map<number, Observable<string>> = new Map<number, Observable<string>>();
    for (const task of this.job.parameters.annotationTasks) {
      const task_name = [task.scope, 'Annotation: \n', task.algorithm, task.database].join(' ');
      const new_task: Task = {taskid: i, name: task_name, algorithm: task.algorithm, database: task.database, scope: task.scope};
      this.tasks.push(new_task);
      this.kronaHtmlMap[i] = '<b>Waiting for results.</b>';
      kronaObservables.set(i, this.jobService.getKrona(this.job.prophaneJobUUID, this.tasks[i].taskid.toString()));
      i = i + 1;
      this.getParams(task.optionstring);
    }
    // TODO: this solution is awkward, the problem is the use of a map and the lack of the TaskID in the http response
    for (let taskId of kronaObservables.keys()) {
      kronaObservables.get(taskId)
      .subscribe({
        next: (res: string) => {
          this.kronaHtmlMap.set(taskId, res);
          console.log("setting krona");
          console.log(i);
          console.log(res);
        },
        error: (e) => {
          console.log("Krona plot error.")
      }});
    }
  } */

  getJobTasks(): void {
    let i = 0;
    const kronaObservables: Map<number, string> = new Map<number, string>();
    for (const task of this.job.parameters.annotationTasks) {
      const task_name = [task.scope, 'Annotation: \n', task.algorithm, task.database].join(' ');
      const new_task: Task = {taskid: i, name: task_name, algorithm: task.algorithm, database: task.database, scope: task.scope};
      this.tasks.push(new_task);
      this.kronaHtmlMap[i] = this.jobService.getKronaUrl(this.job.prophaneJobUUID, this.tasks[i].taskid.toString());
      i = i + 1;
      this.getParams(task.optionstring);
    }
  }

  getParams(param_array_per_task): void {
    const params_per_task = [];
    for (const param_obj of param_array_per_task) {
      params_per_task.push([param_obj.param, ': ', param_obj.defaultValue].join(''));
    }
    this.params.push(params_per_task);
  }

  changeActive(i) {
    this.lastClickedIndex = i;
  }

  getKronaPlot(index: number): string  {
    return this.kronaHtmlMap[index];
  }

}
