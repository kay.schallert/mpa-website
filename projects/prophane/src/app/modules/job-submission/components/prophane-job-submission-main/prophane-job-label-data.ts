export const jobLabelData = [
  {
    label: 'Input',
    expertsOnly: false
  },
  {
    label: 'Sample Groups',
    expertsOnly: true
  },
  {
    label: 'Quantification',
    expertsOnly: true
  },
  {
    label: 'Taxonomy',
    expertsOnly: true
  },
  {
    label: 'Function',
    expertsOnly: true
  },
  {
    label: 'Submit',
    expertsOnly: false
  },
];
