import { Component, Input } from '@angular/core';
import { NestedNavigationRoute } from '../nav-toolbar/navigation-route.model';
import { Logo } from './footer-logo.model';

export class FooterContentMain {
  categoryName: string;
  elements: FooterContentSubElement[];
}

export class FooterContentSubElement {
  name: string = '';
  href?: string;
  routerLink?: string;
}

@Component({
  selector: 'shared-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  @Input() version: string = '';
  @Input() logos: Logo[];
  @Input() footerContent: NestedNavigationRoute[];
  @Input() copyRight: string = '2025. ISAS e.V.';

  panelOpenState = false;

  getVersion() {
    if (this.version === '') {
      return '0.1.0';
    } else {
      return this.version;
    }
  }
}
