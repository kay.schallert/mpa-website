import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpEvent,
  HttpEventType,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Observable, filter, repeat, take, takeWhile, tap } from 'rxjs';
import { UploadProgressService } from './upload-progress.service';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from './login/auth.service';

export interface Endpoints {}

export interface FileUploadData {
  uploadFile: File;
  httpParameters: HttpParams;
}

export interface MultiFileUploadData {
  files: UploadFile[];
  httpParameters?: HttpParams;
}

export interface UploadFile {
  uploadFile: File;
  // TODO: this is pointless?
  fileID: string;
}

/**
 * @member checkProperty - string array representing nested object properties that should be checked to be present
 * @member boolCondition - function that checks some condition on the response
 */
export interface requestConditions {
  checkProperty: string[];
  boolCondition?: (object: any) => boolean;
}

@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  //uploadFileArray: FileUploadData[];

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private uploadProgressService: UploadProgressService,
    private dialog: MatDialog,
  ) {}

  /**
   * Checks if a (nested) property exists in an object
   *
   * @param {any} obj - object that is checked
   * @param {string} keys - an array of properties that are checked on each level of the nested object
   * @returns {any | undefined} - undefined if the property does not exist, else the property
   */
  getProperty(obj: any, keys: string[]) {
    let property = obj;
    for (let key in keys) {
      property = property[keys[key]];
    }
    return property;
  }

  postObject<T1, T2>(
    obj: T1,
    url: string,
    params?: HttpParams,
  ): Observable<T2> {
    return this.http.post<T2>(url, obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.authService.getUserAuthorization().toString(),
      }),
      params: params,
    });
  }

  getFile(url: string, params?: HttpParams): void {
    console.log('calling ' + url);
    this.http
      .get(url, {
        headers: new HttpHeaders({
          'Content-Type': 'application/octet-stream"',
          Authorization: this.authService.getUserAuthorization().toString(),
        }),
        observe: 'response',
        responseType: 'blob',
        params: params,
      })
      .subscribe({
        next: (response: HttpResponse<Blob>) => {
          const contentDisposition = response.headers.get(
            'Content-Disposition',
          );
          const filename = contentDisposition
            ? contentDisposition.split(';')[1].split('filename=')[1].trim()
            : 'results.zip';
          const blob = new Blob([response.body as BlobPart], {
            type:
              response.headers.get('Content-Type') ||
              'application/force-download',
          });
          // Erstellen Sie einen Download-Link und fügen Sie ihn dem DOM hinzu
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(blob);
          downloadLink.download = filename;
          // Fügen Sie den Link zum DOM hinzu und klicken Sie ihn an, um den Download zu starten
          document.body.appendChild(downloadLink);
          downloadLink.click();
          // Entfernen Sie den Link aus dem DOM
          document.body.removeChild(downloadLink);
        },
        error: (error) => {
          console.error('Error during download', error);
        },
      });
  }

  getHtmlText(url, params?: HttpParams): Observable<string> {
    return this.http.get(url, {
      headers: new HttpHeaders({
        'Content-Type': 'text/html',
        Authorization: this.authService.getUserAuthorization().toString(),
      }),
      responseType: 'text',
      params: params,
    });
  }

  getObject<T>(url: string, params?: HttpParams): Observable<T> {
    return this.http.get<T>(url, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: this.authService.getUserAuthorization(),
      }),
      params: params,
    });
  }

  repeatedGetObject<T>(
    checkProperty: string[],
    api: string,
    params?: HttpParams,
    delay = 5_000,
  ): Observable<T> {
    return this.getObject<T>(api, params).pipe(
      repeat({ delay: delay }),
      filter((res: T) => this.getProperty(res, checkProperty) !== undefined),
      take(1),
    );
  }

  postFile(file: File, url: string, params?: HttpParams) {
    const fd = new FormData();
    fd.set('Content-Type', 'multipart/form-data');
    fd.append('uploaded_file', file);
    return this.http.post(url, fd, {
      headers: new HttpHeaders({
        Authorization: this.authService.getUserAuthorization().toString(),
      }),
      observe: 'events',
      params: params,
      reportProgress: true,
    });
  }

  postMultiPartFiles<T>(
    fileList: MultiFileUploadData,
    url: string,
  ): Observable<T> {
    const fd = new FormData();
    let multipartids = '';
    fileList.files.forEach((file) => {
      multipartids += file.fileID + ';';
    });

    // fd.set('Content-Type', 'multipart/form-data');
    fd.append('fileIDList', multipartids);
    fileList.files.map((file) => {
      fd.append(file.fileID, file.uploadFile);
    });
    return this.http.post<T>(url, fd, {
      headers: new HttpHeaders({
        Authorization: this.authService.getUserAuthorization().toString(),
      }),
      params: fileList.httpParameters,
      reportProgress: true, // currently no way to track? (dialogid)
    });
  }

  /**
   * repeated post requests to server
   * @param obj object thats posted
   * @param checkProperty if this property is available in the response, the request will resolve, else requests will be continued
   * @param api endpoint
   * @param params http params
   * @param delay interval between requests
   * @returns observable of the response object
   */
  repeatedPostObject<T1, T2>(
    obj: T1,
    checkProperty: string[],
    api: string,
    params?: HttpParams,
    delay = 5_000,
  ): Observable<T2> {
    return this.postObject<T1, T2>(obj, api, params).pipe(
      repeat({ delay: delay }),
      filter((res: T2) => this.getProperty(res, checkProperty) !== undefined),
      take(1),
    );
  }

  /**
   * Does the same as repeatedPostObject, but emits values of the reposne object
   * @param obj object thats posted
   * @param conditions conditions to fullfill before stopping requests
   * @param api endpoint
   * @param params http params
   * @param delay interval between requests
   * @returns observable of the response object
   */
  repeatedPostObjectWithEmission<T1, T2>(
    obj: T1,
    conditions: requestConditions,
    api: string,
    params?: HttpParams,
    delay = 5_000,
  ): Observable<T2> {
    if (conditions.boolCondition == undefined) {
      conditions.boolCondition = (res) => true;
    }

    return this.postObject<T1, T2>(obj, api, params).pipe(
      repeat({ delay: delay }),
      takeWhile(
        (res: T2) =>
          this.getProperty(res, conditions.checkProperty) === undefined &&
          conditions.boolCondition(res),
        true,
      ),
    );
  }

  postMultiPartFilesEvents(
    fileList: MultiFileUploadData,
    url: string,
  ): Observable<HttpEvent<Object>> {
    const fd = new FormData();
    let multipartids = '';
    fileList.files.forEach((file) => {
      multipartids += file.fileID + ';';
    });

    // fd.set('Content-Type', 'multipart/form-data');
    fd.append('fileIDList', multipartids);
    fileList.files.map((file) => {
      fd.append(file.fileID, file.uploadFile);
    });
    return this.http.post(url, fd, {
      headers: new HttpHeaders({
        Authorization: this.authService.getUserAuthorization().toString(),
      }),
      observe: 'events',
      params: fileList.httpParameters,
      reportProgress: true, // currently no way to track? (dialogid)
    });
  }

  performUpload(dialogId: string, fileList: MultiFileUploadData, url: string) {
    //TODO: handle big file-sizes -> split upload into multiple uploads
    fileList.files.map((fileUploadData) => {
      this.uploadProgressService.addToTotal(fileUploadData.uploadFile.size);
      // TODO: this is how we would do chunking: (roughly)
      // let i = 0
      // let part: Blob;
      // do {
      //     part = fileUploadData.uploadFile.slice(i, (i+1)*1024*1024*100-1)
      //     i++;
      // } while (part.size != 0);
    });

    const fd = new FormData();
    let multipartids = '';
    fileList.files.forEach((file) => {
      multipartids += file.fileID + ';';
    });

    // fd.set('Content-Type', 'multipart/form-data');
    fd.append('fileIDList', multipartids);
    fileList.files.map((file) => {
      fd.append(file.fileID, file.uploadFile);
    });

    this.http
      .post(url, fd, {
        headers: new HttpHeaders({
          Authorization: this.authService.getUserAuthorization().toString(),
        }),
        observe: 'events',
        params: fileList.httpParameters,
        reportProgress: true,
      })
      .subscribe({
        next: (event) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.uploadProgressService.changeReportLoaded(event.loaded);
            console.log(event);
          } else if (event.type === HttpEventType.Response) {
            console.log('File uploaded');
          }
        },
        error: (error) => {
          if (error.status >= 400) {
            // handle failed upload
            if (this.dialog.getDialogById(dialogId)) {
              this.dialog
                .getDialogById(dialogId)
                .componentInstance.setUploadFailed();
              this.dialog.getDialogById(
                dialogId,
              ).componentInstance.uploadFailedMessage = error.statusText;
            }
          } else {
            throw error;
          }
        },
      });
  }

  downloadFile(route: string, filename: string = null): void {
    // const baseUrl = 'http://myserver/index.php/api';
    this.http
      .get(route, { responseType: 'blob' })
      .subscribe((response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(
          new Blob(binaryData, { type: dataType }),
        );
        if (filename) {
          downloadLink.setAttribute('download', filename);
        }
        document.body.appendChild(downloadLink);
        downloadLink.click();
      });
  }
}
