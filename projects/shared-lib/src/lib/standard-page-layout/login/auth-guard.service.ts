import { Injectable } from '@angular/core';
import { Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard  {

  constructor(private _router: Router, private auth: AuthService) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
    if (this.auth.loggedIn()) {
      return true;
    } else {
      return this._router.parseUrl("/login");
    }
  }

}
