import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
// beginning with version 9 moved to own library, install via npm i angular-oauth2-oidc-jwks --save
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { BehaviorSubject, filter, Subject } from 'rxjs';
import { authConfigElixir } from './authConfigElixir';
import { authConfigGoogle } from './authConfigGoogle';
import { UserToken } from './user-token';
import { AST } from 'eslint';
import Token = AST.Token;

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  //public _user: BehaviorSubject<UserToken> = new BehaviorSubject(undefined);
  public loginChange: Subject<boolean> = new Subject<boolean>();

  private userToken: UserToken = undefined;
  private guestEmail: string = "noEmail";
  // public idProvider: string;

  constructor(private router: Router, private oauthService: OAuthService) {}

  initializeOAuth(): void {
    // Loads correct config for login provider
    // if (sessionStorage.getItem('login_provider') === 'elixir') {
    //   // this.setIdProvider('elixir');
    //   this.oauthService.configure(authConfigElixir);
    // } else if (sessionStorage.getItem('login_provider') === 'google'){
    //   console.log("provider google")
    //   // this.setIdProvider('google');
    //   this.oauthService.configure(authConfigGoogle);
    // }
    this.oauthService.configure(authConfigGoogle);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
    // Optional
    this.oauthService.setupAutomaticSilentRefresh();
    console.log("try to recover existing token")
    // Load existing login Google
    const savedItem = sessionStorage.getItem('user');
    if (savedItem !== null && savedItem !== '' && savedItem !== undefined) {
      const savedToken: UserToken = JSON.parse(savedItem) as UserToken;
      if (savedToken.exp * 1000 >= Date.now()) {
        console.log("recover existing token");
        this.userToken = savedToken;
      } else {
        console.log('token expired');
      }
    }

    // Subscription to Automatically load user profile
    this.oauthService.events.pipe(filter((e) => e.type === 'token_received')).subscribe(() => {
      this.oauthService.loadUserProfile().then((profile: UserToken) => {
        this.userToken = profile;
        sessionStorage.setItem('user', JSON.stringify((profile as any).info));
        // Emit loginChange only after userToken is fully updated
        this.loginChange.next(true);
      });
    });
  }

  // getUserId(): string {
  //   //if ((this.userToken as any)?.info?.id) {
  //   if (this.loggedIn()) {
  //     return (this.userToken as any).info.id; // Cast to access id
  //   } else if (this.guestEmail !== "noEmail") {
  //     return 'ANONYMOUS:' + this.guestEmail;
  //   } else {
  //     return 'ANONYMOUS:noEmail';
  //   }
  // }

  getEmail(): string {
    if ((this.userToken as any)?.info?.email) {
      return (this.userToken as any).info.email; // Cast to access email
    }
    return this.guestEmail;
  }

  getName(): string {
    if ((this.userToken as any)?.info?.name) {
      return (this.userToken as any).info.name; // Cast to access name
    }
    return 'Anonymous';
  }


  getUserAuthorization(): string {
    if (this.userToken !== null && this.userToken !== undefined) {
      return sessionStorage.getItem('id_token').toString();
    } else if (this.guestEmail !== "noEmail") {
      return 'ANONYMOUS:' + this.guestEmail;
    } else {
      return 'ANONYMOUS:noEmail';
    }
  }

  // setIdProvider(idP: string): void {
  //   this.idProvider = idP;
  // }

  // getIdProvider(): string {
  //   return this.idProvider;
  // }

  public logout(): void {
    this.userToken = null;
    this.guestEmail = "noEmail";
    sessionStorage.setItem('user', '');
    // sessionStorage.setItem('login_provider', '');
    sessionStorage.setItem('id_token', '');
    this.oauthService.logOut();
    this.loginChange.next(true);
    this.router.navigate(['login']);
  }

  loggedIn(): boolean {
    return (this.userToken !== null && this.userToken !== undefined);
  }

  setGuestEmail(email: string): void {
    this.logout();
    this.guestEmail = email;
    this.loginChange.next(true);
    //this.router.navigate();
  }

  loginGoogle(): void {
    this.logout();
    this.oauthService.configure(authConfigGoogle);
    this.oauthService.loadDiscoveryDocument();
    // sessionStorage.setItem('login_provider', 'google');
    this.oauthService.initLoginFlow();
  }

  // loginElixier(): void {
  //   this.logout();
  //   // this.oauthService.configure(authConfigElixir);
  //   // await this.oauthService.loadDiscoveryDocument();
  //   sessionStorage.setItem('login_provider', 'elixir');
  //   // this.oauthService.initLoginFlow();
  // }

  // //TODO: get rid of
  // allowExpert(): boolean {
  //   return this.loggedIn();
  // }

}
