import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetterLoginPageComponent } from './better-login-page.component';

describe('BetterLoginPageComponent', () => {
  let component: BetterLoginPageComponent;
  let fixture: ComponentFixture<BetterLoginPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BetterLoginPageComponent]
    });
    fixture = TestBed.createComponent(BetterLoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
