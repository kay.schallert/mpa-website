import { Component, Input } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'shared-better-login-page',
  templateUrl: './better-login-page.component.html',
  styleUrls: ['./better-login-page.component.scss'],
})
export class BetterLoginPageComponent {
  @Input() routeToHomeText: string = 'Proceed to new Analysis'; //this needs to be a variable
  @Input() showGuestLogin = true; //this needs to be a variable
  @Input() showGoogleLogin = true; //this needs to be a variable
  @Input() showLiveScienceLogin = true;

  email: FormControl<string> = new FormControl('', [Validators.email]);

  // loginType: string = "None";

  emailString: string = "noEmail";
  nameString: string = "Anonoymous";
  isConfirmed: boolean = false;

  constructor(private router: Router, public auth: AuthService) {} //private boolean showGuestLogin, private boolean: showGoogleLogin

  ngOnInit(): void {
    // Handle cases where the user is already logged in
    this.updateUserInfo();
      // Subscribe to loginChange to update user info on login state changes
      this.auth.loginChange.subscribe(() => {
      this.updateUserInfo();
    });
  }
  
  updateUserInfo(): void {
    this.nameString = this.auth.getName();
    this.emailString = this.auth.getEmail();
   }

  changeGuestEmail() {
    if (this.email.getRawValue() !== null && this.email.getRawValue().length > 0) {
      this.auth.setGuestEmail(this.email.getRawValue());
      this.isConfirmed = true; // Switch to "Delete" button
    } else {
      this.auth.setGuestEmail('noEmail');
    }
  }
  
  deleteGuestEmail() {
    this.auth.setGuestEmail('noEmail'); // Reset email
    this.email.reset(); // Clear the input field
    this.isConfirmed = false; // Switch back to "Confirm" button
  }

  logout() {
    this.email.reset();
    this.email.getRawValue();
    this.auth.logout();
  }

  
  // isLoggedIn() {
  //   return this.auth.loggedIn();
  // }

  //TODO;
  // isLoggedInWithGoogleLogin() {
  //   return (this.loginType === "google");
  // }

  // //TODO;
  // isLoggedInWithLifeScience() {
  //   return (this.loginType === "elixir");
  // }

  // loginLifeScience() {
  //   this.auth.loginElixier();
  // }

  // routeToJobsubmission(){
  //   this.router.navigateByUrl('jobsubmission')
  // }
  // routeToJobcontrol() {
  //   this.router.navigateByUrl('jobcontrol')
  // }
}
