import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  SimpleNavigationRoute,
  NestedNavigationRoute,
} from './navigation-route.model';
import { AuthService } from '../login/auth.service';
import { UserToken } from '../login/user-token';
import { Subscription } from 'rxjs';
import { Logo } from '../footer/footer-logo.model';
import { Router } from '@angular/router';

@Component({
  selector: 'shared-nav-toolbar',
  templateUrl: './nav-toolbar.component.html',
  styleUrls: ['./nav-toolbar.component.scss'],
})
export class NavToolbarComponent implements OnInit, OnDestroy {
  @Input() applicationName: string = '';
  @Input() routerLinks: NestedNavigationRoute[] = [];
  @Input() homeLink: SimpleNavigationRoute;
  @Input() hasLogin: Boolean = false;
  @Input() toolLogo: Logo;
  @Input() showLogo: boolean = false;

  visibleLinks: NestedNavigationRoute[] = []; // links that will be visible on the toolbar

  subscriptions: Subscription[] = [];

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.updateVisibleLinks();
    this.subscriptions.push(this.authService.loginChange.subscribe(() => {
      this.updateVisibleLinks();
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  // changes visible buttons
  updateVisibleLinks(): void {
    // if login is not required, all links are visible
    if (!this.hasLogin) {
      this.visibleLinks = this.routerLinks;
    } else {
      // filter for links that should be only visible upon login
      this.visibleLinks = this.routerLinks.filter((link) =>
        this.checkAuthOnLink(link)
      );
    }
  }

  checkAuthOnLink(link: NestedNavigationRoute): boolean {
    if (link.requireAuth) {
      return this.authService.loggedIn();
    }
    return true;
  }

  isLoggedIn() {
    return this.authService.loggedIn();
  }

  logIn() {
    this.router.navigate(['login']);
  }

  logOut() {
    this.authService.logout();
    this.router.navigate(['login']);
  }

  isActiveRoute(route: string): boolean {
    return this.router.url.includes(route);
  }
}
