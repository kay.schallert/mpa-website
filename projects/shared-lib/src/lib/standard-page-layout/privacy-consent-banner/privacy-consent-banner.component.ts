import { Component, AfterViewInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-privacy-consent-banner',
  templateUrl: './privacy-consent-banner.component.html',
  styleUrls: ['./privacy-consent-banner.component.scss'],
  animations: [
    trigger('policyConsent', [
      state(
        'initial',
        style({
          backgroundColor: '#001933',
          height: '0px',
          overflow: 'hidden',
        })
      ),
      state(
        'final',
        style({
          backgroundColor: '#001933',
          width: '100%',
          height: '100px',
        })
      ),
      transition('initial=>final', animate('300ms 500ms ease-in')),
      transition('final=>initial', animate('0ms 500ms ease-out')),
    ]),
  ],
})
export class PrivacyConsentBannerComponent implements AfterViewInit {
  consentDialogStatus = 'initial';
  expiryDays = 30;

  constructor() {
    console.log('Privacy Consent Banner is here!');
  }

  ngAfterViewInit() {
    if (!this.hasConsented() && this.consentDialogStatus === 'initial') {
      this.showBanner();
    }
  }

  showBanner() {
    this.consentDialogStatus = 'final';
  }

  hideBanner() {
    this.consentDialogStatus = 'initial';
  }

  getCurrentTimestamp() {
    return new Date().getTime();
  }

  isExpired(timestamp) {
    if (this.getCurrentTimestamp() - timestamp >= this.expiryDays * 86400000) {
      return true;
    } else {
      return false;
    }
  }

  hasConsented() {
    // return false;
    const value = localStorage.getItem('prophane_mpa_policy_consent');
    if (value === null || this.isExpired(parseInt(value))) {
      return false;
    } else {
      return true;
    }
  }

  consent() {
    this.setConsent();
    this.hideBanner();
  }

  setConsent() {
    localStorage.setItem(
      'prophane_mpa_policy_consent',
      this.getCurrentTimestamp().toString()
    );
  }
}
