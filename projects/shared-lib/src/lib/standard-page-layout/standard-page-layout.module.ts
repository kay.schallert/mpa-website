import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterModule } from '@angular/router';
import { OAuthModule } from 'angular-oauth2-oidc';

import { NavToolbarComponent } from './nav-toolbar/nav-toolbar.component';
import { PrivacyConsentBannerComponent } from './privacy-consent-banner/privacy-consent-banner.component';

import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { FileInputComponent } from './file-input/file-input.component';
import { UploadDialogComponent } from './dialog/upload-dialog.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FooterComponent } from './footer/footer.component';
import { DeleteWarningDialogComponent } from './dialog/delete-warning-dialog.component';
import { BetterLoginPageComponent } from './login/better-login-page/better-login-page.component';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [
    BetterLoginPageComponent,
    NavToolbarComponent,
    PrivacyConsentBannerComponent,
    FileInputComponent,
    UploadDialogComponent,
    FooterComponent,
    DeleteWarningDialogComponent,
    BetterLoginPageComponent,
  ],
  imports: [
    CommonModule,
    //BrowserModule,
    RouterModule,
    //BrowserAnimationsModule,
    FormsModule,
    OAuthModule.forRoot(),
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatRippleModule,
    MatDialogModule,
    MatMenuModule,
    MatExpansionModule,
    MatGridListModule,
  ],
  exports: [
    NavToolbarComponent,
    PrivacyConsentBannerComponent,
    FileInputComponent,
    UploadDialogComponent,
    FooterComponent,
    DeleteWarningDialogComponent,
    BetterLoginPageComponent,
  ],
})
export class StandardPageLayoutModule {}
