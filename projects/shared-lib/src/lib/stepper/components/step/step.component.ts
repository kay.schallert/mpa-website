import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomStepperService } from '../../services/custom-stepper.service';

@Component({
  selector: 'shared-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.scss'],
})
export class StepComponent implements OnInit {
  @Input() displayIndex: number;

  stepIndex: number;

  label: string;

  completedSteps$ = this.stepper.completedSteps$;

  constructor(private stepper: CustomStepperService) {}

  ngOnInit() {
    this.label = this.stepper.shownSteps$.value[this.displayIndex].label;
    this.stepIndex = this.stepper.shownSteps$.value[this.displayIndex].index;
  }

  get isLastStep() {
    return this.displayIndex === this.stepper.shownSteps$.value.length - 1;
  }

  get hasAdvanced() {
    return this.stepper.hasAdvancedSteps;
  }
}
