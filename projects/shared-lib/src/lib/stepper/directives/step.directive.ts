import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[shared-step]',
})
export class StepDirective {
  constructor(public tpl: TemplateRef<any>) {}
}
