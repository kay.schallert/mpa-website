import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Step {
  index: number;
  label: string;
  advanced?: boolean;
}

@Injectable({
  providedIn: 'any',
})
export class CustomStepperService {
  workflowSteps: Step[];
  stepNumber: number;

  currentIndex$: BehaviorSubject<number>;
  completedSteps$: BehaviorSubject<boolean[]>;

  hasAdvancedSteps: boolean;
  toggleAdvanced$: BehaviorSubject<boolean>;
  shownSteps$: BehaviorSubject<Step[]>;

  constructor() {
    this.currentIndex$ = new BehaviorSubject(0);
    this.completedSteps$ = new BehaviorSubject([]);
    this.toggleAdvanced$ = new BehaviorSubject(false);
    this.shownSteps$ = new BehaviorSubject([]);
  }

  initialize(steps: Step[]) {
    this.workflowSteps = steps;
    this.stepNumber = steps.length;

    const completedSteps = [];
    const shownSteps = [];

    for (let step of this.workflowSteps) {
      completedSteps.push(false);
      if (step.advanced) {
        this.hasAdvancedSteps = true;
        continue;
      }
      // only show steps that are not advanced at beginning
      shownSteps.push(step);
    }

    this.shownSteps$.next(shownSteps);
    this.currentIndex$.next(0);
  }

  allowPrev(): boolean {
    return (
      this.currentIndex$.value > 0 &&
      this.completedSteps$.value[this.currentIndex$.value - 1]
    );
  }

  allowNext(): boolean {
    return (
      this.currentIndex$.value < this.stepNumber - 1 &&
      this.completedSteps$.value[this.currentIndex$.value]
    );
  }

  setStep(index: number) {
    if (index < this.stepNumber - 1 && index >= 0) {
      this.currentIndex$.next(index);
    }
  }

  toggleAdvanced(toggle: boolean) {
    // reset to first step
    this.currentIndex$.next(0);

    this.toggleAdvanced$.next(toggle);
    this.shownSteps$.next(
      // this.workflowSteps.filter((step) => (step.advanced ? toggle : true))
      this.workflowSteps.filter(
        (step) => !step.advanced || (step.advanced && toggle)
      )
    );
  }
}
