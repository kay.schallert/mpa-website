import { Component, Input } from '@angular/core';

@Component({
  selector: 'shared-notification-box',
  templateUrl: './notification-box.component.html',
  styleUrls: ['./notification-box.component.scss'],
})
export class NotificationBoxComponent {
  @Input() message: string = '';
  @Input() isWarning: boolean = false;
}
