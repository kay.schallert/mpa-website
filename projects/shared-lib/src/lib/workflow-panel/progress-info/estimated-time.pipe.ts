import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estimatedTime',
})
export class EstimatedTimePipe implements PipeTransform {
  transform(startTime: number, progress: number): string {
    if (startTime === undefined || progress === 1.0 || progress === 0) {
      return;
    }

    const tillEndMinutes =
      ((1 - progress) * (Date.now() - startTime)) / progress / 60000;

    if (tillEndMinutes < 1) {
      return `less than a minute`;
    }

    if (tillEndMinutes < 60 && tillEndMinutes <= 1) {
      return `${Math.round(tillEndMinutes)} minute`;
    }

    if (tillEndMinutes < 60) {
      return `${Math.round(tillEndMinutes)} minutes`;
    }

    if (tillEndMinutes > 60 && tillEndMinutes < 60 * 24) {
      return `${Math.round(tillEndMinutes / 60)} hours ${Math.round(
        tillEndMinutes % 60
      )} minutes`;
    }

    if (tillEndMinutes > 60 * 24) {
      return `more than one day...`;
    }

    return 'unknown';
  }
}
