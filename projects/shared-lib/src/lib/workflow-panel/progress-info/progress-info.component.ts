import { Component, Input } from '@angular/core';

@Component({
  selector: 'shared-progress-info',
  templateUrl: './progress-info.component.html',
  styleUrls: ['./progress-info.component.scss'],
})
export class ProgressInfoComponent {
  @Input() process: string = '';
  @Input() progress: number = 0.5;
  @Input() startTime: number = undefined;

  constructor() {}
}
