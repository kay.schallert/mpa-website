import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowPanelComponent } from './workflow-panel/workflow-panel.component';
import { WorkflowPanelDirective } from './workflow-panel/workflow-panel-directive';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { IconDividerComponent } from './icon-divider/icon-divider.component';
import { NotificationBoxComponent } from './notification-box/notification-box.component';
import { ProgressInfoComponent } from './progress-info/progress-info.component';
import { EstimatedTimePipe } from './progress-info/estimated-time.pipe';

@NgModule({
  declarations: [
    WorkflowPanelComponent,
    WorkflowPanelDirective,
    IconDividerComponent,
    NotificationBoxComponent,
    EstimatedTimePipe,
    ProgressInfoComponent,
  ],
  imports: [CommonModule, MatIconModule, MatProgressBarModule],
  exports: [
    WorkflowPanelComponent,
    NotificationBoxComponent,
    WorkflowPanelDirective,
    ProgressInfoComponent,
  ],
})
export class WorkflowPanelModule {}
