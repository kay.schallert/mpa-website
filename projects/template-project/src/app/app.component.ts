import { Component } from '@angular/core';
import { Logo, NestedNavigationRoute, SimpleNavigationRoute } from 'shared-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'testexample';

  showLogo = true;

  toolLogo: Logo = {
    label: 'testexample',
    assetPath: 'assets/test.png',
  };

  homelink: SimpleNavigationRoute = {
    route: '/home',
    label: 'Home',
  };

  routes: NestedNavigationRoute[] = [
    { route: '/content', label: 'content', requireAuth: false },
    { route: '/home', label: 'about', requireAuth: false },
    {
      label: 'nested route',
      requireAuth: false,
      children: [
        { route: '/home', label: 'nested route 1' },
        { route: '/home', label: 'nested route 2' },
      ],
    },
  ];

  footerContent: NestedNavigationRoute[] = [
    {
      label: 'About',
      children: [
        { label: 'About Prophane', route: '/about' },
        { label: 'Terms of Service', route: '/termsofservice' },
        { label: 'Privacy Policy', route: '/privacypolicy' },
        { label: 'Impressum', route: '/impressum' },
      ],
    },
    {
      label: 'Funding & Support',
      children: [
        { label: 'DFG', href: 'http://www.dfg.de' },
        { label: 'de.NBI', href: 'http://www.denbi.de' },
        { label: 'de.NBI Cloud', href: 'https://www.denbi.de/cloud' },
      ],
    },
  ];

  footerLogos: Logo[] = [
    {
      label: 'leibniz',
      assetPath: 'assets/standard-logos/leibniz_white.png',
      href: 'https://www.leibniz-gemeinschaft.de',
    },
    {
      label: 'ISAS',
      assetPath: 'assets/standard-logos/isaslogooffizielleformrgbweiss.png',
      href: 'https://www.isas.de',
    },
    {
      label: 'nrw',
      assetPath: 'assets/standard-logos/nrw_black.svg',
      href: 'https://www.land.nrw',
      backgroundColor: 'white',
    },
    {
      label: 'bmbf',
      assetPath: 'assets/standard-logos/bmbf.svg',
      href: 'https://www.bmbf.de',
      backgroundColor: 'white',
    },
  ];
}
